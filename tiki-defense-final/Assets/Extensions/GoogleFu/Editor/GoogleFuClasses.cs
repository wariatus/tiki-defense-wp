//----------------------------------------------
//    GoogleFu: Google Doc Unity integration
//         Copyright © 2013 Litteratus
//----------------------------------------------

using UnityEngine;
using UnityEditor;


namespace GoogleFu
{
	using System;
	using System.Collections;
	using System.Collections.Generic;
	using System.IO;
	using System.Net;
	using System.Security.Cryptography.X509Certificates;
	using Google.GData.Client;
	using Google.GData.Client.ResumableUpload;
	using Google.GData.Documents;
	using Google.GData.Extensions;
	using Google.GData.Spreadsheets;
	using System.Net.Security;
	using System.Xml;
	using System.Text;
	using System.Linq;
	
	public partial class GoogleFuEditor : EditorWindow
	{
		private enum GF_PAGE
		{
			Settings,
			Workbooks,
			Toolbox,
			Help,
			Help_Main,
			Help_Docs
		}
		
		enum selectionContent
		{
			SimpleDatabase,
			NGUILocalization,
			XMLExport,
			JSONExport,
			AdvancedDatabase
		}
		
		public class WorkBookInfo
		{
			public SpreadsheetEntry spreadsheetEntry = null;
			public AtomEntryCollection WorksheetEntries = null;
			public List<WorksheetEntry> ManualEntries = new List<WorksheetEntry>();
			public string Url = "";
			public string Title = "";
			
			public WorkBookInfo()
			{
			}
			
			public WorkBookInfo( SpreadsheetEntry entry )
			{
				spreadsheetEntry = entry;
				WorksheetEntries = spreadsheetEntry.Worksheets.Entries;
				Title = entry.Title.Text;
				foreach ( var link in entry.Links )
				{
					if( link.Rel.ToLower() == "alternate" )
					{
						Url = link.HRef.ToString();
						break;
					}
				}
			}
	
			public void AddWorksheetEntry( WorksheetEntry entry, string url )
			{
				if ( spreadsheetEntry == null )
					spreadsheetEntry = new SpreadsheetEntry();
				SpreadsheetEntry.ImportFromFeed(entry);
				Title = entry.Feed.Title.Text;
				Url = url;
				ManualEntries.Add(entry);
			}
			
			public SpreadsheetFeed GetSpreadsheetFeed()
			{
				return spreadsheetEntry.Feed as SpreadsheetFeed;
			}
			
			public override string ToString ()
			{
				return string.Format ( Url + "." + Title );
			}
		}
		
		[System.Serializable]
		public class AdvancedDatabaseInfo
		{
			public GameObject DatabaseAttachObject;
			public string ComponentName;
			public WorksheetEntry entry;
			public List < string > entryStrings = new List < string >();
			public int entryStride = 0;
			public bool GeneratePlaymaker;
			public AdvancedDatabaseInfo(string ComponentName, WorksheetEntry _entry, SpreadsheetsService _service, GameObject _DatabaseAttachObject, bool bGeneratePlaymaker)
			{
				ParseWorksheetEntry ( _entry, _service );
				this.ComponentName = ComponentName;
				if ( _DatabaseAttachObject != null )
					DatabaseAttachObject = _DatabaseAttachObject;
				else
				{
					DatabaseAttachObject = GameObject.Find("databaseObj");
					if(DatabaseAttachObject == null)
						DatabaseAttachObject = new GameObject("databaseObj");
				}

				GeneratePlaymaker = bGeneratePlaymaker;
			}
			
			private void ParseWorksheetEntry ( WorksheetEntry entry, SpreadsheetsService _service )
			{
				if ( entry == null )
				{
					Debug.LogError("Could not read WorksheetEntry - retry count:  ");
					return;
				}

				// Define the URL to request the list feed of the worksheet.
				AtomLink listFeedLink = entry.Links.FindService(GDataSpreadsheetsNameTable.ListRel, null);
				
				// Fetch the list feed of the worksheet.
				ListQuery listQuery = new ListQuery(listFeedLink.HRef.ToString());
				ListFeed listFeed = _service.Query(listQuery);
				
				//int rowCt = listFeed.Entries.Count;
				//int colCt = ((ListEntry)listFeed.Entries[0]).Elements.Count;
				
				if ( listFeed.Entries.Count > 0 )
				{
					
					int curRow = 0;
					// Iterate through each row, printing its cell values.
					foreach (ListEntry row in listFeed.Entries)
					{
						
						// skip the first row. This is the title row, and we can get the values later
						if ( curRow > 0 )
						{
							int curCol = 0;
							// Iterate over the remaining columns, and print each cell value
							foreach (ListEntry.Custom element in row.Elements)
							{
								// this will be the list of all the values in the row excluding the first 'name' column
								if(curCol > 0)
									entryStrings.Add ( element.Value );
								curCol++;
							}
							entryStride = curCol-1;
							
						}
						curRow++;
					}
				}
			}
		}
	}
}