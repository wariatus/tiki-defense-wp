using UnityEngine;
using System;

[ConsoleCategory("Globals")]
public static class VConsole_GlobalCommands
{	
	/* TIME */
	[ConsoleCommand("time_timeScale", "Overrides the time scale of the game, speeding up or slowing down gameplay", "The scale to use")]
	public static float TimeScale {
		get{
			return Time.timeScale;
		}
		set{
			Time.timeScale = value;
		}
	}
	
	[ConsoleCommand("time_fixedDeltaTime", "The interval in seconds at which physics and other fixed frame rate updates are performed.")]
	public static float FixedDeltaTime
	{
		get{
			return Time.fixedDeltaTime;
		}
		set{
			Time.fixedDeltaTime = value;
		}
	}
	
	[ConsoleCommand("sysinfo", "Logs information about the current system")]
	public static void OS () 
	{ 
		VConsole.LogSecondary("Operating System: " + SystemInfo.operatingSystem); 	
		VConsole.LogSecondary("Processor: " + SystemInfo.processorType); 
		VConsole.LogSecondary("Processor Count: " + SystemInfo.processorCount.ToString()); 
		VConsole.LogSecondary("System Memory: " + SystemInfo.systemMemorySize.ToString()); 
		VConsole.LogSecondary("Graphics Memory: " + SystemInfo.graphicsMemorySize.ToString()); 
		VConsole.LogSecondary("Graphics Device Name: " + SystemInfo.graphicsDeviceName.ToString()); 
		VConsole.LogSecondary("Graphics Device Vendor: " + SystemInfo.graphicsDeviceVendor.ToString()); 
		VConsole.LogSecondary("Graphics Device ID: " + SystemInfo.graphicsDeviceID.ToString()); 
		VConsole.LogSecondary("Graphics Device Vendor ID: " + SystemInfo.graphicsDeviceVendorID.ToString());  
		VConsole.LogSecondary("Graphics Device Version: " + SystemInfo.graphicsDeviceVersion.ToString()); 
		VConsole.LogSecondary("Graphics Shader Level: " + SystemInfo.graphicsShaderLevel.ToString()); 
		VConsole.LogSecondary("Graphics Pixel Fill Rate: " + SystemInfo.graphicsPixelFillrate.ToString()); 
		VConsole.LogSecondary("Supports Shadows: " + SystemInfo.supportsShadows.ToString()); 
		VConsole.LogSecondary("Supports Render Texture: " + SystemInfo.supportsRenderTextures.ToString()); 
		VConsole.LogSecondary("Supports Image Effects: " + SystemInfo.supportsImageEffects.ToString()); 
		VConsole.LogSecondary("Max Render Targets: " + SystemInfo.supportedRenderTargetCount.ToString()); 
		VConsole.LogSecondary("Unique ID: " + SystemInfo.deviceUniqueIdentifier.ToString()); 
		VConsole.LogSecondary("System Name: " + SystemInfo.deviceName.ToString()); 
		VConsole.LogSecondary("System Model: " + SystemInfo.deviceModel.ToString()); 
		VConsole.LogSecondary("Supports Accelerometer: " + SystemInfo.supportsAccelerometer.ToString()); 
		VConsole.LogSecondary("Supports Gyroscope: " + SystemInfo.supportsGyroscope.ToString()); 
		VConsole.LogSecondary("Supports Location Services: " + SystemInfo.supportsLocationService.ToString()); 
		VConsole.LogSecondary("Supports Vibration: " + SystemInfo.supportsVibration.ToString()); 
		VConsole.LogSecondary("Device Type: " + Enum.GetName(typeof(DeviceType), SystemInfo.deviceType)); 
	}
}