using UnityEngine;
using System;
using System.Collections;

[ConsoleCategory("Network")]
public static class VConsole_Network
{
	[ConsoleCommand("net_password", "The password for the network")]
	public static string Password
	{
		get{ return Network.incomingPassword; }
		set{ Network.incomingPassword = value; }
	}
	
	[ConsoleCommand("net_log", "The log level of the network")]
	public static NetworkLogLevel LogLevel
	{
		get{ return Network.logLevel; }
		set{ Network.logLevel = value; }
	}
	
	[ConsoleCommand("net_connections", "Logs all current connections")]
	public static void LogConnections ()
	{
		VConsole.Log("Connections");
		for(int i = 0; i < Network.connections.Length; i++)
		{
			var player = Network.connections[i];
			VConsole.LogSecondary(i + "/tIP: " + player.externalIP + ":" + player.externalPort);
		}
	}
	
	[ConsoleCommand("net_isClient", "Logs whether this player is a network client.")]
	public static void IsClient ()
	{
		VConsole.LogSecondary(Network.isClient.ToString());
	}
	[ConsoleCommand("net_isServer", "Logs whether this player is a network server.")]
	public static void IsServer ()
	{
		VConsole.LogSecondary(Network.isServer.ToString());
	}
	
	[ConsoleCommand("net_peerType", "Logs the current peer type of the player.")]
	public static void PeerType ()
	{
		VConsole.LogSecondary(Enum.GetName(typeof(NetworkPeerType), Network.peerType));
	}
	
	[ConsoleCommand("net_sendRate", "The default send rate of network updates")]
	public static float SendRate
	{
		get{ return Network.sendRate; }
		set{ Network.sendRate = value; }
	}
	
	[ConsoleCommand("net_enabled", "Enables or disables the network")]
	public static bool NetEnabled
	{
		get{
			return Network.isMessageQueueRunning;
		}
		set{
			Network.isMessageQueueRunning = value;
		}
	}
	
	[ConsoleCommand("net_natIP", "The IP address of the NAT punchtrough facilitator")]
	public static string NatIP
	{
		get{ return Network.natFacilitatorIP; }
		set{ Network.natFacilitatorIP = value; }
	}
	
	[ConsoleCommand("net_natPort", "The Port of the NAT facilitator")]
	public static int NatPort
	{
		get{ return Network.natFacilitatorPort; }
		set{ Network.natFacilitatorPort = value; }
	}
	
	[ConsoleCommand("net_testIP", "The IP address used for network tests")]
	public static string TestIP
	{
		get{ return Network.connectionTesterIP; }
		set{ Network.connectionTesterIP = value; }
	}
	
	[ConsoleCommand("net_testPort", "The port used for network tests")]
	public static int TestPort
	{
		get{ return Network.connectionTesterPort; }
		set{ Network.connectionTesterPort = value; }
	}
	
	[ConsoleCommand("net_maxConnections", "The maximum connections allowed on this network")]
	public static int MaxConnections
	{
		get{ return Network.maxConnections; }
		set{ Network.maxConnections = value; }
	}
	
	[ConsoleCommand("net_proxyIP", "The ip address for the proxy server")]
	public static string ProxyIP
	{
		get{ return Network.proxyIP; }
		set{ Network.proxyIP = value; }
	}
	
	[ConsoleCommand("net_proxyPort", "The port for the proxy server")]
	public static int ProxyPort
	{
		get{ return Network.proxyPort; }
		set{ Network.proxyPort = value; }
	}
	
	[ConsoleCommand("net_useProxy", "Whether to use a proxy.")]
	public static bool UseProxy
	{
		get{ return Network.useProxy; }
		set{ Network.useProxy = value; }
	}
	
	[ConsoleCommand("net_proxyPassword", "The proxy server password")]
	public static string ProxyPassword
	{
		get{ return Network.proxyPassword; }
		set{ Network.proxyPassword = value; }
	}
	
	[ConsoleCommand("net_initserver", "Initialize a server", "Max connections", "Port", "Whether to use nat")]
	public static void InitServer (int connections, int port, bool useNat)
	{
		var error = Network.InitializeServer(connections, port, useNat);
		if(error != NetworkConnectionError.NoError)
		{
			VConsole.LogError(Enum.GetName(typeof(NetworkConnectionError), error));
		}
	}
	
	[ConsoleCommand("net_initsecurity", "Initialize security")]
	public static void InitSecurity ()
	{
		Network.InitializeSecurity();
	}
	
	[ConsoleCommand("net_connect", "Connect to a server without a password", "The ip address to connect to", "The port to connect to")]
	public static void Connect (string ip, int port)
	{
		Connect(ip, port, "");
	}
	[ConsoleCommand("net_connect", "Connect to a server", "The ip address to connect to", "The port to connect to", "The password of the server")]
	public static void Connect (string ip, int port, string password)
	{
		var error = Network.Connect(ip, port, password);
		if(error != NetworkConnectionError.NoError)
		{
			VConsole.LogError(Enum.GetName(typeof(NetworkConnectionError), error));
		}
	}
	[ConsoleCommand("net_connect", "Connect to a server with a guid", "The guid to connect to")]
	public static void Connect (string guid)
	{
		Connect(guid, "");
	}
	[ConsoleCommand("net_connect", "Connect to a server with a guid", "The guid to connect to", "The password of the server")]
	public static void Connect (string guid, string password)
	{
		var error = Network.Connect(guid, password);
		if(error != NetworkConnectionError.NoError)
		{
			VConsole.LogError(Enum.GetName(typeof(NetworkConnectionError), error));
		}
	}
	
	[ConsoleCommand("disconnect", "Disconnects from all connections")]
	[ConsoleCommand("net_disconnect", "Disconnects from all connections")]
	public static void Disconnect ()
	{
		Network.Disconnect();
	}
	
	[ConsoleCommand("net_destroy", "Destroys all objects belonging to the player")]
	public static void DestroyPlayerObjects ()
	{
		Network.DestroyPlayerObjects(Network.player);
	}
	[ConsoleCommand("net_destroy", "Destroys all objects belonging to a given player id")]
	public static void DestroyPlayerObjects (int i)
	{
		if(i < 0 || i <= Network.connections.Length)
		{
			VConsole.LogError("Invalid player id");
			return;
		}
		Network.DestroyPlayerObjects(Network.connections[i]);
	}
	
	[ConsoleCommand("ping", "Logs the current avg ping")]
	public static void LogPing ()
	{
		VConsole.LogSecondary("Ping: " + Network.GetAveragePing(Network.player));
	}
	[ConsoleCommand("ping_last", "Logs the latest ping")]
	public static void LogPingLast ()
	{
		VConsole.LogSecondary("Ping: " + Network.GetLastPing(Network.player));
	}
	
	[ConsoleCommand("net_test", "Tests the connection")]
	public static IEnumerator NetworkTest ()
	{
		VConsole.LogSecondary("Testing connection...");
		while(Network.TestConnection() == ConnectionTesterStatus.Undetermined)
		{
			yield return null;
		}
		VConsole.LogSecondary(Enum.GetName(typeof(ConnectionTesterStatus), Network.TestConnection()));
	}
	
	[ConsoleCommand("net_masterServerIP", "The address to the master server")]
	public static string MasterServerIP
	{
		get{ return MasterServer.ipAddress; }
		set{ MasterServer.ipAddress = value; }
	}
	
	[ConsoleCommand("net_masterServerPort", "The port of the master server")]
	public static int MasterServerPort
	{
		get{ return MasterServer.port; }
		set{ MasterServer.port = value; }
	}
	
	[ConsoleCommand("net_isDedicated", "Whether this machine should be considered dedicated")]
	public static bool IsDedicated
	{
		get{ return MasterServer.dedicatedServer; }
		set{ MasterServer.dedicatedServer = value; }
	}
	
	[ConsoleCommand("net_masterServerUpdateRate", "The minimum update rate for master server host information updates")]
	public static int MasterServerUpdateRate
	{
		get{ return MasterServer.updateRate; }
		set{ MasterServer.updateRate = value; }
	}
}
