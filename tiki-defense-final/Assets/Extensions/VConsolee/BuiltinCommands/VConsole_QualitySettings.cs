using UnityEngine;
using System.Linq;

[ConsoleCategory("Quality")]
public static class VConsole_QualitySettings
{
	
	[ConsoleCommand("quality_list", "Lists all available quality settings")]
	public static void ListQuality ()
	{
		VConsole.Log("Quality Settings:");
		foreach(var name in QualitySettings.names)
		{
			VConsole.LogSecondary("/t"+name);
		}
	}
	
	[ConsoleCommand("quality_set", "Sets the current quality setting. Must be a name which exists in the quality settings.")]
	public static void SetQuality (string name)
	{
		int index = -1;
		for(int i = 0; i < QualitySettings.names.Length; i++)
		{
			if(QualitySettings.names[i] == name)
			{
				index = i;
			}
		}
		if(index == -1)
		{
			VConsole.LogError(name + " does not exist in the Quality Settings. Use 'quality_list' to list all quality settings available.");
			return;
		}
		QualitySettings.SetQualityLevel(index, true);
	}
	
	[ConsoleCommand("quality_current", "Logs the name of the current quality level.")]
	public static void GetQuality ()
	{
		VConsole.LogSecondary(QualitySettings.names[QualitySettings.GetQualityLevel()]);
	}
	
	[ConsoleCommand("quality_pixelLightCount", "The maximum number of pixel lights that should affect any object")]
	public static int PixelLightCount
	{
		get{ return QualitySettings.pixelLightCount; }
		set{ QualitySettings.pixelLightCount = value; }
	}
	
	[ConsoleCommand("quality_shadowProjection", "Directional light shadow projection")]
	public static ShadowProjection ShadowProjectionQuality
	{
		get{ return QualitySettings.shadowProjection; }
		set{ QualitySettings.shadowProjection = value; }
	}
	
	[ConsoleCommand("quality_shadowCascades", "Number of cascades to use for directional light shadows")]
	public static int ShadowCascades
	{
		get{ return QualitySettings.shadowCascades; }
		set{ QualitySettings.shadowCascades = value; }
	}
	
	[ConsoleCommand("quality_shadowDistance", "Shadow drawing distance")]
	public static float ShadowDistance
	{
		get{ return QualitySettings.shadowDistance; }
		set{ QualitySettings.shadowDistance = value; }
	}
	
	[ConsoleCommand("quality_masterTextureLimit", "A texture size limit applied to all textures")]
	public static int MasterTextureLimit
	{
		get{ return QualitySettings.masterTextureLimit; }
		set{ QualitySettings.masterTextureLimit = value; }
	}
	
	[ConsoleCommand("quality_anisoFiltering", "Global anisotropic filtering mode")]
	public static AnisotropicFiltering AnisoMode
	{
		get{ return QualitySettings.anisotropicFiltering; }
		set{ QualitySettings.anisotropicFiltering = value; }
	}
	
	[ConsoleCommand("quality_lodBias", "Global multiplier for LOD's switching distance")]
	public static float LODBias
	{
		get{ return QualitySettings.lodBias; }
		set{ QualitySettings.lodBias = value; }
	}
	
	[ConsoleCommand("quality_maxLodLevel", "Maximum LOD level for all LOD groups")]
	public static int MaxLodLevel
	{
		get{ return QualitySettings.maximumLODLevel; }
		set{ QualitySettings.maximumLODLevel = value; }
	}
	
	[ConsoleCommand("quality_softVegetation", "Use a two-pass shader for the vegetation in the terrain engine.")]
	public static bool SoftVegetation
	{
		get{ return QualitySettings.softVegetation; }
		set{ QualitySettings.softVegetation = value; }	
	}
	
	[ConsoleCommand("quality_maxQueuedFrames", "Max number of frames queued up by graphics driver.")]
	public static int MaxQueuedFrames
	{
		get{ return QualitySettings.maxQueuedFrames; }
		set{ QualitySettings.maxQueuedFrames = value; }
	}
	
	[ConsoleCommand("quality_vSyncCount", "The number of VSyncs that should pass between each frame")]
	public static int VSyncCount
	{
		get{ return QualitySettings.vSyncCount; }
		set{ QualitySettings.vSyncCount = value; }
	}
	
	[ConsoleCommand("quality_aa", "Set the AA filtering option. Can be 0, 2, 4, or 8")]
	public static int AntiAliasing
	{
		get{ return QualitySettings.antiAliasing; }
		set{ QualitySettings.antiAliasing = value; }
	}
	
	[ConsoleCommand("quality_blendWeights", "Blend weights")]
	public static BlendWeights BlendWeights
	{
		get{ return QualitySettings.blendWeights; }
		set{ QualitySettings.blendWeights = value; }
	}
}
