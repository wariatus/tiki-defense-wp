using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Linq;
using VConsoleMessages;

[ConsoleCategory("Builtin")]
public static class VConsole_BuiltinCommands
{
	[ConsoleCommand("platform", "Displays the current platform.")]
	public static void LogPlatformInfo ()
	{
		if(Application.isEditor)
		{
			VConsole.LogSecondary("Running in Editor");
		}
		else
		{
			VConsole.LogSecondary("Platform: " + Enum.GetName(typeof(RuntimePlatform), Application.platform));
		}
	}
	
	[ConsoleCommand("open", "Opens the console")]
	public static void Open ()
	{
		VConsole.instance.open = true;
	}
	
	[ConsoleCommand("close", "Closes the console")]
	public static void Close ()
	{
		VConsole.instance.open = false;
	}
	
	[ConsoleCommand("clear", "Clears the console")]
	public static void Clear ()
	{
		VConsole.messageCache.Clear();
	}
	
	[ConsoleCommand("quit", "Quits the game")]
	public static void Quit ()
	{		
#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
#endif
		Application.Quit();
	}
	
	[ConsoleCommand("echo", "Logs something to the console")]
	[ConsoleCommand("print", "Logs something to the console")]
	[ConsoleCommand("log", "Logs something to the console")]
	public static void Echo (string message)
	{
		VConsole.LogSecondary(message);
	}
	
	[ConsoleCommand("hide_commands", "Whether to hide commands run in the console")]
	public static bool hideCommands { get; set; }
	
	[ConsoleCommand("console_max_commands", "The max commands to allow in the console")]
	public static int maxCommands { 
		get{ return VConsole.maxCommands; } 
		set{ 
			VConsole.maxCommands = (int)Mathf.Clamp((float)value, 0, Mathf.Infinity); 
		} 
	}
	
	[ConsoleCommand("macro_cmd", "Sets a macro command")]
	public static void CommandMacro (string macro, string command)
	{
		if(VConsole.commandMacros.ContainsKey(macro))
			VConsole.commandMacros[macro] = command;
		else
			VConsole.commandMacros.Add(macro, command);
	}
	[ConsoleCommand("macro_cmd_remove", "Removes a macro command")]
	public static void CommandMacroRemove (string macro)
	{
		if(VConsole.commandMacros.ContainsKey(macro))
			VConsole.commandMacros.Remove(macro);
	}
	
	[ConsoleCommand("macro_keydown", "Adds a macro to a keydown event", "The keycode of the macro", "The command to run from this macro")]
	public static void MacroKeyDown (KeyCode key, string command)
	{
		if(VConsole.keyDownMacros.ContainsKey(key))
			VConsole.keyDownMacros[key] = command;
		else
			VConsole.keyDownMacros.Add(key, command);
	}
	[ConsoleCommand("macro_keydown_remove", "Removes a macro from a keydown event", "The keycode of the macro to remove")]
	public static void MacroKeyDownRemove (KeyCode key)
	{
		if(VConsole.keyDownMacros.ContainsKey(key))
			VConsole.keyDownMacros.Remove(key);
	}
	
	[ConsoleCommand("macro_keyup", "Adds a macro to a keyup event", "The keycode of the macro", "The command to run from this macro")]
	public static void MacroKeyUp (KeyCode key, string command)
	{
		if(VConsole.keyUpMacros.ContainsKey(key))
			VConsole.keyUpMacros[key] = command;
		else
			VConsole.keyUpMacros.Add(key, command);
	}
	[ConsoleCommand("macro_keyup_remove", "Removes a macro from a keyup event", "The keycode of the macro to remove")]
	public static void MacroKeyUpRemove (KeyCode key)
	{
		if(VConsole.keyUpMacros.ContainsKey(key))
			VConsole.keyUpMacros.Remove(key);
	}
	
	[ConsoleCommand("run", "Runs a script.", "The name of the script. If only a filename is specified, the path will default to the run_path value.")]
	public static void RunScript (string name)
	{
		VConsole.RunScript(name);
	}
	
	[ConsoleCommand("reload_scripts", "Reloads the script library")]
	public static void LoadScripts ()
	{
		VConsole.scripts.Clear();
	
#if !UNITY_WEBPLAYER
		var dir = new DirectoryInfo(Application.dataPath + VConsole.runPath);
		
		if(dir.Exists)
		{
			foreach(var file in dir.GetFiles())
			{
				if(file.Extension.ToLower() != ".txt") continue;
				VConsole.scripts.Add(
					new VConsole.Script(){
						name = file.Name.Substring(0, file.Name.Length-4),
						contents = File.ReadAllText(file.FullName)
					}
				);
			}
		}
		else
		{
			VConsole.LogWarning("Run path was not found: " + VConsole.runPath);
		}
#endif
		
		VConsole.scripts.AddRange(VConsole.instance.GetCachedScripts());
	}
	
	[ConsoleCommand("wait", "Waits for a given amount of time before continuing to the next command.", "The amount of time in seconds to wait.")]
	public static IEnumerator Wait (float time)
	{
		float f = 0f;
		while(f < time)
		{
			yield return null;
			f += Time.deltaTime;
		}
	}
	
	[ConsoleCommand("help", "Prints out help command for a given command")]
	public static void HelpCommand ()
	{
		VConsole.Log(new VConsoleMessageComposite(
			new VConsoleMessageSingle("help commands", VConsole.primaryColor),
			new VConsoleMessageSingle("Lists all commands available.", VConsole.secondaryColor)
		));
		
		VConsole.Log(new VConsoleMessageComposite(
			new VConsoleMessageSingle("help [command name]", VConsole.primaryColor),
			new VConsoleMessageSingle("Displays help for a specific command", VConsole.secondaryColor)
		));
		
		VConsole.Log("");
		
		VConsole.Log(new VConsoleMessageComposite(
			new VConsoleMessageSingle("help enums", VConsole.primaryColor),
			new VConsoleMessageSingle("Lists all enumerations available.", VConsole.secondaryColor)
		));
		
		VConsole.Log(new VConsoleMessageComposite(
			new VConsoleMessageSingle("help [enumeration name]", VConsole.primaryColor),
			new VConsoleMessageSingle("Lists the options of a given enumeration", VConsole.secondaryColor)
		));
		
		VConsole.Log("");
		
		VConsole.Log(new VConsoleMessageComposite(
			new VConsoleMessageSingle("help categories", VConsole.primaryColor),
			new VConsoleMessageSingle("Lists all categories available available.", VConsole.secondaryColor)
		));
		
		VConsole.Log(new VConsoleMessageComposite(
			new VConsoleMessageSingle("help [category name]", VConsole.primaryColor),
			new VConsoleMessageSingle("Lists the options of a given category", VConsole.secondaryColor)
		));
	}
	
	[ConsoleCommand("help_all", "Prints out help for all commands")]
	public static void HelpAll ()
	{
		foreach(var kvp in VConsole.consoleCommands)
		{
			HelpCommand(kvp.Key);
		}
	}
	
	[ConsoleCommand("help", "Prints out help command for a given command")]
	public static void HelpCommand (string command)
	{		
		string lowerCommand = command.ToLower();		
		if(lowerCommand == "commands" || lowerCommand == "command")
		{			
			VConsole.Log("All Commands:");
			foreach(var kvp in VConsole.consoleCategories)
			{
				VConsole.Log("\n"+kvp.Key + ":");
				var list = kvp.Value.ToList();
				list.Sort((x,y)=>{
					int cmp = String.Compare(x.name, y.name);
					if(cmp == 0) cmp = x.signature.Length - y.signature.Length;
					return cmp;
				});
				
				HashSet<string> cmdNames = new HashSet<string>();
				foreach(var cmd in list)
				{
					cmdNames.Add(cmd.name);
				}
				foreach(var cmd in cmdNames)
				{
					VConsole.Log("\t"+cmd);
				}					
			}
			VConsole.Log(new VConsoleMessageSingle("Type help [command name] for help on a specific command.", VConsole.secondaryColor));
			return;
		}
		
		if(lowerCommand == "enums" || lowerCommand == "enumerations" || lowerCommand == "enum")
		{
			VConsole.Log("All Enumerations:");
			var enumList = new List<Type>(VConsole.enumTypes.Values);
			enumList.Sort((x,y)=>{return string.Compare(x.Name, y.Name);});
			foreach(var t in enumList)
			{
				VConsole.Log("\t" + t.Name);
			}
			return;
		}
	
		if(lowerCommand == "categories" || lowerCommand == "category")
		{
			VConsole.Log("All Categories:");
			foreach(var kvp in VConsole.consoleCategories)
			{
				VConsole.Log("\t" + kvp.Key);
			}
			return;
		}
		
		List<VConsoleCommand> categoryList = null;
		foreach(var kvp in VConsole.consoleCategories)
		{
			if(kvp.Key.ToLower() == lowerCommand)
			{
				categoryList = kvp.Value;
				break;
			}
		}
		if(categoryList != null)
		{
			var list = categoryList.ToList();
			list.Sort((x,y)=>{return string.Compare(x.name, y.name);});
			foreach(var c in list)
				DisplayHelpList(c);
			VConsole.Log(new VConsoleMessageSingle("Type help [command name] for help on a specific command.", VConsole.secondaryColor));
			return;
		}
		
		bool found = false;
		if(VConsole.consoleCommands.ContainsKey(lowerCommand))
		{
			var cmds = VConsole.consoleCommands[lowerCommand];
			HashSet<Type> enumTypes = new HashSet<Type>();
			foreach(var cmd in cmds)
			{
				DisplayHelp(cmd);
				foreach(var t in cmd.enumTypes)	enumTypes.Add(t);
			}
			foreach(var t in enumTypes)
			{
				DisplayHelpEnum(t);
			}
			found = true;
		}
		if(VConsole.enumTypes.ContainsKey(lowerCommand))
		{
			var t = VConsole.enumTypes[lowerCommand];
			DisplayHelpEnum(t);
			found = true;
		}
		if(!found)
		{
			VConsole.LogError(command + " is not a valid command.");
		}
	}
	
	static void DisplayHelpEnum (Type t)
	{
		VConsole.Log("\t" + t.Name + " Options");
		var names = Enum.GetNames(t);
		foreach(var n in names)
		{
			VConsole.LogSecondary("\t\t" + n);
		}
	}
	
	static void DisplayHelpList (VConsoleCommand cmd)
	{
		var msg = new VConsoleMessageSingle("\t" + cmd.name, VConsole.primaryColor);
				
		string s = "";
		foreach(var p in cmd.signature)
			s += " [" + p.Name + "]";
		var sig = new VConsoleMessageSingle(s, VConsole.secondaryColor);
		
		VConsole.Log(new VConsoleMessageComposite(msg, sig));
	}
	
	static void DisplayHelp (VConsoleCommand cmd)
	{
		string c = "\t"+cmd.name;
		
		VConsole.Log(new VConsoleMessageComposite(
			new VConsoleMessageSingle(c, VConsole.primaryColor),
			new VConsoleMessageSingle(cmd.helpMessage, VConsole.secondaryColor)));
		
		for(int i = 0; i < cmd.signature.Length; i++)
		{
			var p = cmd.signature[i];
			
			string s = "\t\t[" + p.Name + "]";
			string b = "";
			
			if(i < cmd.parameterInfo.Length)
			{
				b = cmd.parameterInfo[i];
			}
			
			if(p.ParameterType == typeof(float))
				b += " (number)";
			if(p.ParameterType == typeof(int))
				b += " (integer)";
			if(p.ParameterType.IsEnum)
				b += " (" + p.ParameterType.Name + ")";
			if(p.ParameterType == typeof(bool))
				b += " (boolean)";
			
			VConsole.Log(new VConsoleMessageComposite(
				new VConsoleMessageSingle(s, VConsole.primaryColor), 
				new VConsoleMessageSingle(b, VConsole.secondaryColor)));
		}
	}
	
	[ConsoleCommand("log_primaryColor", "Sets the primary log color.")]
	public static void SetPrimaryLogColor (float r, float g, float b, float a)
	{
		VConsole.primaryColor = new Color(r, g, b, a);
	}
	[ConsoleCommand("log_primaryColor", "Logs the primary log color.")]
	public static void LogPrimaryLogColor ()
	{
		VConsole.LogSecondary(VConsole.primaryColor.ToString());
	}
	
	[ConsoleCommand("log_secondarycolor", "Sets the secondary log color.")]
	public static void SetSecondaryLogColor (float r, float g, float b, float a)
	{
		VConsole.secondaryColor = new Color(r, g, b, a);
	}
	[ConsoleCommand("log_secondarycolor", "Logs the secondary log color.")]
	public static void LogSecondaryLogColor ()
	{
		VConsole.LogSecondary(VConsole.secondaryColor.ToString());
	}
	
	[ConsoleCommand("log_warningcolor", "Sets the warning log color.")]
	public static void SetWarningLogColor (float r, float g, float b, float a)
	{
		VConsole.warningColor = new Color(r, g, b, a);
	}
	[ConsoleCommand("log_warningcolor", "Logs the warning log color.")]
	public static void LogWarningLogColor ()
	{
		VConsole.LogSecondary(VConsole.warningColor.ToString());
	}
	
	[ConsoleCommand("log_errorcolor", "Sets the error log color.")]
	public static void SetErrorLogColor (float r, float g, float b, float a)
	{
		VConsole.errorColor = new Color(r, g, b, a);
	}
	[ConsoleCommand("log_errorcolor", "Logs the error log color.")]
	public static void LogErrorLogColor ()
	{
		VConsole.LogSecondary(VConsole.errorColor.ToString());
	}
	
	[ConsoleCommand("log_linkcolor", "Sets the link log color.")]
	public static void SetLinkLogColor (float r, float g, float b, float a)
	{
		VConsole.linkColor = new Color(r, g, b, a);
	}
	[ConsoleCommand("log_linkcolor", "Logs the link log color.")]
	public static void LogLinkLogColor ()
	{
		VConsole.LogSecondary(VConsole.linkColor.ToString());
	}
	
	public enum LogCategory
	{
		All,
		Errors,
		Warnings,
		Logs,
	}
	
	[ConsoleCommand("log", "Enables a log type to display in the console", "The type of log to enable")]
	public static void Log (LogCategory logCategory)
	{
		Log(logCategory, true);
	}
	
	[ConsoleCommand("log", "Toggle whether a log type displays in the console", "The type of log to toggle", "Whether the log is hidden or displayed")]
	public static void Log (LogCategory logCategory, bool status)
	{
		string term = status ? "displayed" : "hidden";
		switch(logCategory)
		{
			case LogCategory.All:
				Log(LogCategory.Logs, status);
				Log(LogCategory.Warnings, status);
				Log(LogCategory.Errors, status);
				break;
			case LogCategory.Errors:
				VConsole.displayUnityErrors = status;
				VConsole.LogSecondary("Errors are now " + term);
				break;
			case LogCategory.Logs:
				VConsole.displayUnityWarnings = status;
				VConsole.LogSecondary("Warnings are now " + term);
				break;
			case LogCategory.Warnings:
				VConsole.displayUnityLogs = status;
				VConsole.LogSecondary("Logs are now " + term);
				break;
		}
	}
	
	[ConsoleCommand("log_resources", "Logs all resources currently loaded in memory.", "The type of resource to log")]
	public static void LogResources (string typeName)
	{		
		var allResources = Resources.FindObjectsOfTypeAll(typeof(UnityEngine.Object));
		
		if(typeName == null)
		{
			var list =allResources.ToList();
			list.Sort((x,y)=>{ return string.Compare(x.GetType().Name, y.GetType().Name); });
			allResources = list.ToArray();
			VConsole.Log("DISPLAYING ALL LOADED RESOURCES:");
		}
		else
		{
			VConsole.Log("DISPLAYING ("+typeName+") RESOURCES:");
		}
		
		if(typeName != null) typeName = typeName.ToLower();
		foreach(var obj in allResources)
		{
			if(typeName != null && obj.GetType().Name.ToLower() != typeName) continue;
			VConsole.Log(obj.name + "\t\t\t\t("+obj.GetType().Name+")");
		}
	}
	
	[ConsoleCommand("log_resources", "Logs all resources currently loaded in memory.")]
	public static void LogResources ()
	{
		LogResources(null);
	}
	
	[ConsoleCommand("log_resources_types", "Logs all resource types loaded in memory.")]
	public static void LogResourceTypes ()
	{
		var allResources = Resources.FindObjectsOfTypeAll(typeof(UnityEngine.Object));
		HashSet<string> names = new HashSet<string>();
		foreach(var obj in allResources)
		{
			names.Add(obj.GetType().Name);
		}
		foreach(var name in names)
		{
			VConsole.Log(name);
		}
	}
	
#if !UNITY_WEBPLAYER
	[ConsoleCommand("log_to_file", "Prints out the current log to a file", "The file name to print to")]
	public static void LogToFile (string filename)
	{
		var path = Application.dataPath + "/" + filename;
		File.WriteAllText(path, VConsole.GetConsoleLog());
	}
#endif
}