using UnityEngine;

[ConsoleCategory("PlayerPrefs")]
public static class VConsole_PlayerPrefs
{
	[ConsoleCommand("prefs_set_int", "Sets an int to the player prefs")]
	public static void SetInt (string key, int value)
	{
		PlayerPrefs.SetInt(key, value);
	}
	[ConsoleCommand("prefs_log_int", "Logs an int value from the player prefs")]
	public static void LogInt (string key)
	{
		VConsole.LogSecondary(PlayerPrefs.GetInt(key).ToString());
	}
	
	[ConsoleCommand("prefs_set_float", "Sets an float to the player prefs")]
	public static void SetFloat (string key, float value)
	{
		PlayerPrefs.SetFloat(key, value);
	}
	[ConsoleCommand("prefs_log_float", "Logs a float value from the player prefs")]
	public static void LogFloat (string key)
	{
		VConsole.LogSecondary(PlayerPrefs.GetFloat(key).ToString());
	}
	
	[ConsoleCommand("prefs_set_string", "Sets a string to the player prefs")]
	public static void SetString (string key, string value)
	{
		PlayerPrefs.SetString(key, value);
	}
	[ConsoleCommand("prefs_log_string", "Logs a string value from the player prefs")]
	public static void LogString (string key)
	{
		VConsole.LogSecondary(PlayerPrefs.GetString(key));
	}
	
	[ConsoleCommand("prefs_hasKey", "Logs whether a given key is set")]
	public static void HasKey (string key)
	{
		VConsole.LogSecondary(PlayerPrefs.HasKey(key).ToString());
	}
	
	[ConsoleCommand("prefs_deleteKey", "Deletes a given key from the prefs")]
	public static void DeleteKey (string key)
	{
		PlayerPrefs.DeleteKey(key);
	}
	
	[ConsoleCommand("prefs_deleteAll", "Deletes all player prefs")]
	public static void DeleteAll ()
	{
		PlayerPrefs.DeleteAll();
	}
	
	[ConsoleCommand("prefs_save", "Saves the player prefs to disk.")]
	public static void Save ()
	{
		PlayerPrefs.Save();
	}
}
