using UnityEngine;

[ConsoleCategory("Scene")]
public static class VConsole_SceneCommands
{
	[ConsoleCommand("load_level", "Loads a level of given name", "The name of the level")]
	public static void LoadLevel (string name)
	{
		Application.LoadLevel(name);
	}
	
	[ConsoleCommand("load_levelID", "Loads a level of given id", "The id of the level")]
	public static void LoadLevel (int id)
	{
		Application.LoadLevel(id);
	}
	
	[ConsoleCommand("add_level", "Loads a level of a given name additively", "The name of the level")]
	public static void AddLevel (string name)
	{
		Application.LoadLevelAdditive(name);
	}
	
	[ConsoleCommand("add_levelID", "Loads a level of a given id additively", "The id of the level")]
	public static void AddLevel (int id)
	{
		Application.LoadLevelAdditive(id);
	}
	
	[ConsoleCommand("level", "Logs the current level")]
	public static void LogLevel ()
	{
		VConsole.LogSecondary(Application.loadedLevelName);
	}
	
	[ConsoleCommand("levelID", "Logs the current level id")]
	public static void LogLevelID ()
	{
		VConsole.LogSecondary(Application.loadedLevel.ToString());
	}
	
	[ConsoleCommand("set_position", "Sets the position of an object of a given name", "The name of the object")]
	public static void SetPosition (string objectName, float x, float y, float z)
	{
		var go = GameObject.Find(objectName);
		if(!go)
		{
			VConsole.LogError("Could not find object with name '" + objectName + "'");
			return;
		}
		go.transform.position = new Vector3(x, y, z);
	}
	
	[ConsoleCommand("set_rotation", "Sets the rotation of an object of a given name", "The name of the object")]
	public static void SetRotation (string objectName, float x, float y, float z)
	{
		var go = GameObject.Find(objectName);
		if(!go)
		{
			VConsole.LogError("Could not find object with name '" + objectName + "'");
			return;
		}
		go.transform.eulerAngles = new Vector3(x,y,z);
	}
	
	[ConsoleCommand("set_scale", "Sets the scale of an object of a given name", "The name of the object")]
	public static void SetScale (string objectName, float x, float y, float z)
	{
		var go = GameObject.Find(objectName);
		if(!go)
		{
			VConsole.LogError("Could not find object with name '" + objectName + "'");
			return;
		}
		go.transform.localScale = new Vector3(x,y,z);
	}
	
	[ConsoleCommand("set_scale", "Sets the scale of an object of a given name", "The name of the object", "Uniform Scale")]
	public static void SetScale (string objectName, float scale)
	{
		var go = GameObject.Find(objectName);
		if(!go)
		{
			VConsole.LogError("Could not find object with name '" + objectName + "'");
			return;
		}
		go.transform.localScale = new Vector3(scale,scale,scale);
	}
	
	[ConsoleCommand("destroy", "Destroys an object of a given name", "The name of the object")]
	public static void DestroyObject (string objectName)
	{
		var go = GameObject.Find(objectName);
		if(!go)
		{
			VConsole.LogError("Could not find object with name '" + objectName + "'");
			return;
		}
		GameObject.Destroy(go);
	}
	
	[ConsoleCommand("destroy_tag", "Destroys all objects with a given tag", "The tag to destroy")]
	public static void DestroyTag (string tag)
	{
		var gos = GameObject.FindGameObjectsWithTag(tag);
		if(gos.Length == 0)
		{
			VConsole.LogError("There were no objects with tag '" + tag + "'");
			return;
		}
		foreach(var go in gos)
		{
			GameObject.Destroy(go);
		}
	}
	
	[ConsoleCommand("spawn", "Spawns a prefab from the resources folder", "The name of the prefab")]
	public static void SpawnPrefabResource (string resourcePath)
	{
		var go = Resources.Load(resourcePath, typeof(GameObject)) as GameObject;
		if(!go)
		{
			VConsole.LogError("Resource '" + resourcePath + "' could not be loaded or is not a Game Object.");
			return;
		}
		GameObject.Instantiate(go);
	}
	
	[ConsoleCommand("spawn", "Spawns a prefab from the resources folder", "The name of the prefab", "The name to assign the instance")]
	public static void SpawnPrefabResource (string resourcePath, string instanceName)
	{
		var go = Resources.Load(resourcePath, typeof(GameObject)) as GameObject;
		if(!go)
		{
			VConsole.LogError("Resource '" + resourcePath + "' could not be loaded or is not a Game Object.");
			return;
		}
		go = GameObject.Instantiate(go) as GameObject;
		go.name = instanceName;
	}
	
	[ConsoleCommand("spawn", "Spawns a prefab from the resources folder at a given position", "The name of the prefab", "", "", "")]
	public static void SpawnPrefabResource (string resourcePath, float x, float y, float z)
	{
		var go = Resources.Load(resourcePath, typeof(GameObject)) as GameObject;
		if(!go)
		{
			VConsole.LogError("Resource '" + resourcePath + "' could not be loaded or is not a Game Object.");
			return;
		}
		go = GameObject.Instantiate(go) as GameObject;
		go.transform.position = new Vector3(x,y,z);
	}
	
	[ConsoleCommand("hide_layer", "Hides a given layer on the Main Camera", "The layer to hide")]
	public static void HideLayer (string layer)
	{
		if(!Camera.main)
		{
			VConsole.LogError("Main Camera could not be found.");
			return;
		}
		int id = LayerMask.NameToLayer(layer);
		Camera.main.cullingMask = ~((~Camera.main.cullingMask) | (1<<id));
	}
	
	[ConsoleCommand("show_layer", "Shows a given layer id on the Main Camera", "The layer to show")]
	public static void ShowLayer (string layer)
	{
		if(!Camera.main)
		{
			VConsole.LogError("Main Camera could not be found.");
			return;
		}
		int id = LayerMask.NameToLayer(layer);
		Camera.main.cullingMask = Camera.main.cullingMask | (1<<id);
	}
	
	[ConsoleCommand("show_main", "Shows the main camera")]
	public static void ShowMain ()
	{
		VConsole.Log(Camera.main);
	}
}