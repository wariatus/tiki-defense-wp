using UnityEngine;

[ConsoleCategory("Physics")]
public static class VConsole_Physics
{
	[ConsoleCommand("phy_gravity", "Logs the gravity setting")]
	public static void PhysGravity ()
	{
		VConsole.LogSecondary(Physics.gravity.ToString());
	}
	
	[ConsoleCommand("phy_gravity", "Overrides the physics gravity", "", "", "")]
	public static void PhysGravity (float x, float y, float z)
	{
		Physics.gravity = new Vector3(x,y,z);
	}
	
	[ConsoleCommand("phy_gravity", "Overrides the physics gravity", "The y value of gravity (x & z will be 0")]
	public static void PhysGravity (float y)
	{
		PhysGravity(0,y,0);
	}
	
	[ConsoleCommand("phy_minPenetration", "The minimum contact penetration value in order to apply a penalty force (default 0.05). Must be positive.")]
	public static float PhysMinPenetration
	{
		get{ return Physics.minPenetrationForPenalty; }
		set{ Physics.minPenetrationForPenalty = value; }
	}
	
	[ConsoleCommand("phy_bounceThreshhold", "Two colliding objects with a relative velocity below this will not bounce (default 2). Must be positive.")]
	public static float PhysBounceTreshhold
	{
		get{ return Physics.bounceThreshold; }
		set{ Physics.bounceThreshold = value; }
	}
	
	[ConsoleCommand("phy_sleepVelocity", "The default linear velocity, below which objects start going to sleep (default 0.15). Must be positive.")]
	public static float PhysSleepVelocity
	{
		get{ return Physics.sleepVelocity; }
		set{ Physics.sleepVelocity = value; }
	}
	
	[ConsoleCommand("phy_sleepAngularVelocity", "The default angular velocity, below which objects start sleeping (default 0.14). Must be positive.")]
	public static float PhysSleepAngularVelocity
	{
		get{ return Physics.sleepAngularVelocity; }
		set{ Physics.sleepAngularVelocity = value; }
	}
	
	[ConsoleCommand("phy_maxAngularVelocity", "The default maximimum angular velocity permitted for any rigid bodies (default 7). Must be positive.")]
	public static float PhysMaxAngularVelocity
	{
		get{ return Physics.maxAngularVelocity; }
		set{ Physics.maxAngularVelocity = value; }
	}
	
	[ConsoleCommand("phy_solverIterationCount", "The default solver iteration count permitted for any rigid bodies (default 7). Must be positive.")]
	public static int PhysSolverIterationCount
	{
		get{ return Physics.solverIterationCount; }
		set{ Physics.solverIterationCount = value; }
	}
}
