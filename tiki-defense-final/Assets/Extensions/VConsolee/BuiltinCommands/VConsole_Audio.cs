using UnityEngine;

[ConsoleCategory("Audio")]
public static class VConsole_Audio
{
	[ConsoleCommand("audio_sampleRate", "The mixer's current output rate")]
	public static int OutputSampleRate
	{
		get{ return AudioSettings.outputSampleRate; }
		set{ AudioSettings.outputSampleRate = value; }
	}
	
	[ConsoleCommand("audio_speakerMode", "Current speaker mode")]
	public static AudioSpeakerMode SpeakerMode
	{
		get{ return AudioSettings.speakerMode; }
		set{ AudioSettings.speakerMode = value; }
	}
	
	[ConsoleCommand("audio_volume", "Global volume setting")]
	public static float Volume
	{
		get{ return AudioListener.volume; }
		set{ AudioListener.volume = value; }
	}
}
