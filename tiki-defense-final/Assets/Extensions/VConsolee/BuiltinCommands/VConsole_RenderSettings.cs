using UnityEngine;

[ConsoleCategory("RenderSettings")]
public static class VConsole_RenderSettings
{
	[ConsoleCommand("fog", "Enables or disables the builtin fog")]
	public static bool Fog {
		get{
			return RenderSettings.fog;
		}
		set{
			RenderSettings.fog = value;
		}
	}
	
	[ConsoleCommand("fog_mode", "Sets the fog mode")]
	public static FogMode FogMode {
		get{
			return RenderSettings.fogMode;
		}
		set{
			RenderSettings.fogMode = value;
		}
	}
	
	[ConsoleCommand("fog_color", "Logs the fog color")]
	public static void FogColor ()
	{
		VConsole.LogSecondary(RenderSettings.fogColor.ToString());
	}
	[ConsoleCommand("fog_color", "Sets the fog color")]
	public static void FogColor (float r, float g, float b, float a)
	{
		RenderSettings.fogColor = new Color(r, g, b, a);
	}
	[ConsoleCommand("fog_density", "The density of the fog")]
	public static float FogDensity {
		get{
			return RenderSettings.fogDensity;
		}
		set{
			RenderSettings.fogDensity = value;
		}
	}
	[ConsoleCommand("fog_startDist", "The starting distance of linear fog")]
	public static float FogStartDist
	{
		get{
			return RenderSettings.fogStartDistance;
		}
		set{
			RenderSettings.fogStartDistance = value;
		}
	}
	[ConsoleCommand("fog_endDist", "The end distance of linear fog")]
	public static float FogEndDist
	{
		get{
			return RenderSettings.fogEndDistance;
		}
		set{
			RenderSettings.fogEndDistance = value;
		}
	}
	[ConsoleCommand("ambient_light", "The ambient light of the current scene")]
	public static void AmbientLight ()
	{
		VConsole.LogSecondary(RenderSettings.ambientLight.ToString());
	}
	[ConsoleCommand("ambient_light", "Sets the ambient light of the current scene")]
	public static void AmbientLight (float r, float g, float b, float a)
	{
		RenderSettings.ambientLight = new Color(r, g, b, a);
	}
	
}
