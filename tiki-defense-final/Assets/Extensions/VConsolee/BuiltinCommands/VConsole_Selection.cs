using UnityEngine;
using System.Collections.Generic;
using VConsoleMessages;
using System;

[ConsoleCategory("Selection")]
public static class VConsoleSelection
{
	public static List<UnityEngine.Object> selection = new List<UnityEngine.Object>();
	
	[ConsoleCommand("select_clear", "Clears the current selection")]
	public static void Clear ()
	{
		selection.Clear();
	}
	
	[ConsoleCommand("select", "Selects an object with a given name")]
	public static void SelectName (string name)
	{
		SelectName(name, true);
	}
	
	[ConsoleCommand("select_add", "Adds an object with a given name to the selection")]
	public static void SelectNameAdd (string name)
	{
		SelectName(name, false);
	}
	
	static void SelectName (string name, bool clearSelection)
	{
		ValidateSelection();
		if(clearSelection) selection.Clear();
		var obj = GameObject.Find(name);
		if(!obj)
		{
			VConsole.LogError("Game Object with given name could not be found.");
			return;
		}
		
		selection.Add(obj);
	}
	
	[ConsoleCommand("select_tag", "Sets the current selection to all game objects of a given tag")]
	public static void SelectTag (string tag)
	{
		selection.Clear();
		selection.AddRange(GameObject.FindGameObjectsWithTag(tag));
	}
	
	[ConsoleCommand("select_addtag", "Adds all game objects with a given tag to the selection")]
	public static void SelectTagAdd (string tag)
	{
		ValidateSelection();
		selection.AddRange(GameObject.FindGameObjectsWithTag(tag));
	}
	
	[ConsoleCommand("select_list", "Lists the current selection")]
	public static void List ()
	{
		ValidateSelection();
		for(int i = 0; i < selection.Count; i++)
		{
			var obj = selection[i];
			var cmp = new VConsoleMessageComposite(
				new VConsoleMessageSingle("\t" + (i+1) + ":", VConsole.primaryColor),
				new VConsoleMessageObject(obj),
				new VConsoleMessageSingle("[" + obj.GetType().Name + "]", VConsole.secondaryColor)
			);
			VConsole.Log(cmp);
		}
	}
	
	[ConsoleCommand("select_isolate", "Isolates an object from the current selection")]
	public static void Isolate (int index)
	{
		ValidateSelection();
		index--;
		if(index < 0 || selection.Count <= index)
		{
			VConsole.LogError("Index " + index + " is out of range.");
			return;
		}
		var obj = selection[index];
		selection.Clear();
		selection.Add(obj);
	}
	
	[ConsoleCommand("select_isolate", "Isolates a group of objects from the current selection", 
		"A list of indices to isolate. Indices should be wrapped in quotes and separated by commas. Example: select_isolate \"1,5,8,17\"")]
	public static void Isolate (string names)
	{
		ValidateSelection();
		var split = names.Split(',');
		var newSelection = new List<UnityEngine.Object>();
		foreach(var s in split)
		{
			int i;
			if(int.TryParse(s, out i))
			{
				i--;
				if(i < 0 || selection.Count <= i)
				{
					VConsole.LogError("Index " + i + " is out of range.");
					continue;
				}
				newSelection.Add(selection[i]);
			}
		}
		selection.Clear();
		selection.AddRange(newSelection);
		VConsole.LogSecondary("Isolated " + selection.Count + " objects.");
	}
	
	[ConsoleCommand("select_type", "Selects all objects of a given type")]
	public static void SelectType (string typeName)
	{
		SelectType(typeName, true);
	}
	
	[ConsoleCommand("select_addtype", "Adds all objects of a given type to the selection")]
	public static void SelectTypeAdd (string typeName)
	{
		SelectType(typeName, false);
	}
	
	static void SelectType (string typeName, bool clearSelection)
	{
		ValidateSelection();
		var type = Type.GetType(typeName,false, true);
		if(type == null)
		{
			foreach(var assembly in AppDomain.CurrentDomain.GetAssemblies())
			{
				var str = assembly.GetName().Name + "." + typeName;
				type = assembly.GetType(str,false, true);
				if(type != null) break;
			}
		}
		
		if(type == null)
		{
			VConsole.LogError("Type '" + typeName + "' does not exist.");
			return;
		}
		
		if(clearSelection) selection.Clear();
		var objs = GameObject.FindObjectsOfType(type);
		selection.AddRange(GameObject.FindObjectsOfType(type));
		VConsole.LogSecondary("Selected " + objs.Length + " objects.");
	}
	
	[ConsoleCommand("select_message", "Sends a message to the selection")]
	public static void SendMessage (string message)
	{
		ValidateSelection();
		foreach(var obj in selection)
		{
			var go = obj as GameObject;
			if(!go)
			{
				var comp = obj as Component;
				if(comp) go = comp.gameObject;
			}
			if(!go) continue;
			go.SendMessage(message, SendMessageOptions.DontRequireReceiver);
		}
	}
	
	[ConsoleCommand("select_broadcast", "Broadcasts a message to the selection")]
	public static void BroadcastMessage (string message)
	{
		ValidateSelection();
		foreach(var obj in selection)
		{
			var go = obj as GameObject;
			if(!go)
			{
				var comp = obj as Component;
				if(comp) go = comp.gameObject;
			}
			if(!go) continue;
			go.BroadcastMessage(message, SendMessageOptions.DontRequireReceiver);
		}
	}
	
	[ConsoleCommand("select_destroy", "Destroys all objects in the current selection")]
	public static void DestroySelection ()
	{
		ValidateSelection();
		foreach(var obj in selection)
		{
			GameObject.Destroy(obj);
		}
		selection.Clear();
	}
	
	[ConsoleCommand("select_gameObjects", "Converts the current selection to game objects")]
	public static void SelectGameObjects ()
	{
		ValidateSelection();
		var newSelection = new List<UnityEngine.Object>();
		foreach(var obj in selection)
		{
			var go = obj as GameObject;
			if(!go)
			{
				var comp = obj as Component;
				if(comp) go = comp.gameObject;
			}
			if(!go) continue;
			if(newSelection.Contains(go)) continue;
			newSelection.Add(go);
		}
		selection.Clear();
		selection.AddRange(newSelection);
	}
	
	static void ValidateSelection ()
	{
		for(int i = 0; i < selection.Count; i++)
		{
			if(selection[i]) continue;
			selection.RemoveAt(i--);
		}
	}
}
