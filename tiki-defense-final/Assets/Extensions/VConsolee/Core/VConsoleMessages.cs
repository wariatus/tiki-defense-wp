using UnityEngine;
using System.Collections.Generic;
using System.Diagnostics;
using System;

namespace VConsoleMessages
{
	public class VConsoleMessage
	{
		public virtual void DrawGUI (){}
	}
	
	public class VConsoleMessageSingle : VConsoleMessage
	{
		public string message;
		public Color color;
		
		public VConsoleMessageSingle (string message, Color color)
		{
			this.message = message;
			this.color = color;
		}
		
		public override void DrawGUI ()
		{
			GUIStyle label = new GUIStyle(GUI.skin.label);
			label.wordWrap = true;
			label.normal.textColor = color;
			GUILayout.Label(message, label);
		}
		
		public override string ToString ()
		{
			return message;
		}
	}
	
	public class VConsoleMessageComposite : VConsoleMessage
	{
		public List<VConsoleMessage> messages;
		public VConsoleMessageComposite (params VConsoleMessage[] messages)
		{
			this.messages = new List<VConsoleMessage>(messages);
		}
		
		public void Add (params VConsoleMessage[] messages)
		{
			this.messages.AddRange(messages);
		}
		
		public override void DrawGUI ()
		{
			GUILayout.BeginHorizontal();
			foreach(var m in messages) m.DrawGUI();
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();			
		}
		
		public override string ToString ()
		{
			var str = "";
			foreach(var msg in messages)
			{
				str += msg + " ";
			}
			return str;
		}
	}
	
	public class VConsoleMessageProgress : VConsoleMessage
	{
		public float progress = 0;
		public string message = "";
		public bool error = false;
		
		public VConsoleMessageProgress(){}
		public VConsoleMessageProgress (string message)
		{
			this.message = message;
		}
		
		public override void DrawGUI ()
		{
			GUILayout.BeginHorizontal();
			GUIStyle style = new GUIStyle(GUI.skin.label);
			style.normal.textColor = error ? VConsole.errorColor : VConsole.primaryColor;
			GUILayout.Label((progress * 100f).ToString("N0")+"%", style);
			style.normal.textColor = error ? VConsole.errorColor : VConsole.secondaryColor;
			style.wordWrap = true;
			InteriorGUI(style);
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();
		}
		
		protected virtual void InteriorGUI (GUIStyle style)
		{
			GUILayout.Label(message, style);
		}
		
		public override string ToString ()
		{
			return message;
		}
	}
	
	public class VConsoleMessageTimedProgress : VConsoleMessageProgress
	{
		public Stopwatch stopwatch;
		public bool displayTime = false;
		
		public VConsoleMessageTimedProgress () {}
		public VConsoleMessageTimedProgress (string message) : this (message, false) {}
		public VConsoleMessageTimedProgress (string message, bool start)
		{
			this.message = message;
			if(start) StartTimer();
		}
		
		public void StartTimer ()
		{
			if(stopwatch == null) stopwatch = Stopwatch.StartNew();
			stopwatch.Start();
		}
		public void StopTimer ()
		{
			stopwatch.Stop();
		}
		public void FinishTimer ()
		{
			StopTimer();
			displayTime = true;
		}
		protected override void InteriorGUI (GUIStyle style)
		{
			base.InteriorGUI (style);
			if(displayTime)
			{
				GUILayout.Label("(" + stopwatch.Elapsed.TotalSeconds.ToString("N2") + " seconds)", style);
			}
		}
	}
	
	public class VConsoleMessageObject : VConsoleMessage
	{
		public UnityEngine.Object target;
		
		public VConsoleMessageObject (UnityEngine.Object target)
		{
			this.target = target;
		}
		
		Rect labelRect = new Rect();
		public override void DrawGUI ()
		{			
			var style = new GUIStyle(GUI.skin.label);
			style.fontStyle = FontStyle.Italic;
			style.normal.textColor = VConsole.linkColor;
			
			string name = target ? target.name : "NULL";
			
			GUILayout.Label(name, style);
			
			if(Event.current.type == EventType.Repaint) labelRect = GUILayoutUtility.GetLastRect();
			bool mouseOver = labelRect.Contains(Event.current.mousePosition);
			
			if(Event.current.type == EventType.MouseDown && Event.current.button == 0 && mouseOver)
			{
#if UNITY_EDITOR
				UnityEditor.EditorGUIUtility.PingObject(target);
#endif
			}
		}
	}
}
