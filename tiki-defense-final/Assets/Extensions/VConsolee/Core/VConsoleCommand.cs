using System;
using System.Reflection;
using UnityEngine;

public class VConsoleCommand
{	
	public string name;
	public string helpMessage;
	public string[] parameterInfo;
	public Type parentType;
	
	public MethodInfo method;
	public ParameterInfo[] signature;
	public Type[] enumTypes;
	
	public bool MatchSignature (ParameterInfo[] sig)
	{			
		if(sig.Length != signature.Length) return false;
		
		for(int i = 0; i < signature.Length; i++)
		{
			var p1 = signature[i].ParameterType;
			var p2 = sig[i].ParameterType;
			
			if(p1 == p2) continue;
			
			if(p1.IsEnum && p2.IsEnum)
			{
				var names1 = Enum.GetNames(p1);
				var names2 = Enum.GetNames(p2);
				bool same = false;
				for(int n1 = 0; n1 < names1.Length; n1++)
				{
					for(int n2 = 0; n2 < names2.Length; n2++)
					{
						if(names1[n1].ToLower() == names2[n2].ToLower())
						{
							n1 = 10000;//hopefully p1 doesn't have 10k options lul
							same = true;
							break;
						}
					}
				}
				if(same) continue;
			}
			
			//floats and ints are too hard to tell apart from strings
			if(p1 == typeof(float)
				&& p2 == typeof(int)) 
					continue;
			
			if(p1 == typeof(int)
				&& p2 == typeof(float)) 
					continue;
			
			return false;
		}
		
		return true;
	}
	
	public bool TryGetSignature (string[] vars, out object[] result)
	{
		result = new object[0];
		
		if(vars.Length != signature.Length) return false;
		
		var objs = new object[vars.Length];
		
		for(int i = 0; i < signature.Length; i++)
		{
			Type t = signature[i].ParameterType;
			
			string v = vars[i].ToLower();
			
			if(t.IsEnum)
			{
				string[] names = Enum.GetNames(t);
				for(int b = 0; b < names.Length; b++)
				{
					var n = names[b].ToLower();
					if(v == n) break;
					if(b == names.Length-1)
						return false;
				}
				objs[i] = Enum.Parse(t, v, true);
				continue;
			}
			
			//everything's a string!
			if(t == typeof(string))
			{
				objs[i] = vars[i];
				continue;
			}
			
			if(t == typeof(float))
			{
				float f;
				if(!float.TryParse(v, out f)) return false;
				objs[i] = f;
				continue;
			}
			
			if(t == typeof(int))
			{
				float f;
				//parse as float so that decimals can still be read
				if(!float.TryParse(v, out f)) return false;
				objs[i] = (int)f;
				continue;
			}
			
			if(t == typeof(bool))
			{
				float f = 0;
				if(float.TryParse(v, out f))
				{
					if(f <= 0) objs[i] = false;
					else objs[i] = true;
					continue;
				}
				
				if(v == "t" || v == "y" || v == "on" || v == "yes" || v == "true")
				{
					objs[i] = true;
					continue;
				}
				
				if(v == "f" || v == "n" || v == "no" || v == "off" || v == "false")
				{
					objs[i] = false;
					continue;
				}
				
				return false;
			}

		}
		
		result = objs;
		return true;
	}
}