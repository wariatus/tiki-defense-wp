using System;

[AttributeUsage(AttributeTargets.Method | AttributeTargets.Property | AttributeTargets.Class, AllowMultiple = true)]
public class ConsoleCategoryAttribute : Attribute
{
	public string name = "";
	public ConsoleCategoryAttribute (string name)
	{
		this.name = name;
	}
}
