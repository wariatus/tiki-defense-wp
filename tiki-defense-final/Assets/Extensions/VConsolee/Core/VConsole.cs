using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using VConsoleMessages;
using System.Text;
using System.Linq;
using System.IO;

[AddComponentMenu("Miscellaneous/VConsole")]
public class VConsole : MonoBehaviour
{
	
	[Serializable]
	public class Script
	{
#if UNITY_EDITOR
		[NonSerialized] public bool foldout;
#endif
		
		public string name = "";
		public string contents = "";
		
		public Script () {}
		public Script (string name) { this.name = name; }
	}
	
	public static VConsole instance;
	public static int maxCommands = 1000;
	public static string runPath = "/VConsole/Scripts/";
	
	public static Color primaryColor = Color.white;
	public static Color secondaryColor = new Color(0.8f, 0.8f, 0.8f, 1.0f);	
	public static Color warningColor = new Color(1f, 1f, 0f, 1.0f);
	public static Color errorColor = new Color(0.95f, 0.3f, 0.3f, 1f);
	public static Color linkColor = new Color(0.7f, 0.8f, 1f, 1f);
	
	public static bool displayUnityLogs = true;	
	public static bool displayUnityWarnings = true;	
	public static bool displayUnityErrors = true;
	
	public static List<VConsoleMessage> messageCache = new List<VConsoleMessage>();
	public static bool initialized = false;
	
	public static Dictionary<string, List<VConsoleCommand>> consoleCategories = new Dictionary<string, List<VConsoleCommand>> ();
	public static Dictionary<string, List<VConsoleCommand>> consoleCommands = new Dictionary<string, List<VConsoleCommand>> ();
	public static Dictionary<string, Type> enumTypes = new Dictionary<string, Type>();	
	
	public static Dictionary<string, string> commandMacros = new Dictionary<string, string>();
	public static Dictionary<KeyCode, string> keyDownMacros = new Dictionary<KeyCode, string>();
	public static Dictionary<KeyCode, string> keyUpMacros = new Dictionary<KeyCode, string>();
	
	public static bool hideCommands = false;
	
	public static void Evaluate (string commands, bool logCommands = true)
	{
		instance.Parse(commands, logCommands);
	}
	
	public static string GetConsoleLog ()
	{
		StringBuilder builder = new StringBuilder();
		foreach(var msg in messageCache)
		{
			builder.Append(msg.ToString());
			builder.Append("\n");
		}
		return builder.ToString();
	}
	
	public static string GetScriptPath (string name)
	{
		var path = Application.dataPath + runPath + name;
		if(name[0] == '/') path = Application.dataPath + name;
		if(name.Length <= 4 || name.Substring(name.Length-4, 4) != ".gcp") path += ".gcp";
		return path;
	}
	
	public static List<Script> scripts = new List<Script>();
		
	public static VConsoleMessageSingle LogError (string r)
	{
		return Log(r, errorColor);
	}
	public static VConsoleMessageSingle LogWarning (string r)
	{
		return Log(r, warningColor);
	}
	public static VConsoleMessageSingle LogSecondary (string r)
	{
		return Log(r, secondaryColor);
	}
	public static VConsoleMessageSingle Log (string r)
	{
		return Log(r, primaryColor);
	}
	public static VConsoleMessageSingle Log (string r, Color color)
	{
		var msg = new VConsoleMessageSingle(r, color);
		Log(msg);
		return msg;
	}
	public static void Log (VConsoleMessage msg)
	{
		messageCache.Add(msg);
		while(maxCommands < messageCache.Count)
		{
			messageCache.RemoveAt(0);
		}
		if(instance) instance.scrollPosition.y = Mathf.Infinity+1;
	}
	public static void Log (UnityEngine.Object obj)
	{
		Log(new VConsoleMessageObject(obj));
	}
	
	public static void RunScript (string name)
	{
		for(int i = 0; i < scripts.Count; i++)
		{
			var script = scripts[i];
			if(script.name == name)
			{
				instance.Parse(script.contents, false);
				return;
			}
		}
		
		LogError("Script " + name + " does not exist.");
	}
	
	[HideInInspector]
	public Script startup = new Script("Startup");
	
	[HideInInspector]
	public Script startup_editor = new Script("Startup_Editor");
	
	[HideInInspector]
	public Script startup_runtime = new Script("Startup_Runtime");
	
	[HideInInspector]
	public Script startup_android = new Script("Startup_Android");
	
	[HideInInspector]
	public Script startup_flash = new Script("Startup_Flash");
	
	[HideInInspector]
	public Script startup_iOS = new Script("Startup_iOS");
	
	[HideInInspector]
	public Script startup_web = new Script("Startup_Web");
	
	[HideInInspector]
	public Script startup_linux = new Script("Startup_Linux");
	
	[HideInInspector]
	public Script startup_nacl = new Script("Startup_NaCl");
	
	[HideInInspector]
	public Script startup_windows = new Script("Startup_Windows");
	
	[HideInInspector]
	public Script startup_mac = new Script("Startup_Mac");
	
	[HideInInspector]
	public Script startup_pc = new Script("Startup_PC");
	
	[HideInInspector]
	public List<Script> cachedScripts = new List<Script>();
	
	public GUISkin guiSkin;
	
	[HideInInspector] public Vector2 scrollPosition = new Vector2(0, Mathf.Infinity);
	[HideInInspector] public int arrowIndex = 0;	
	[HideInInspector] public bool open = false;
	[HideInInspector] public string inputText = "";
	[HideInInspector] public List<string> commandCache = new List<string>();
	
	void OnEnable ()
	{
		if(instance)
		{
			GameObject.Destroy(this);
			return;
		}
		
		instance = this;
		
		foreach(var assembly in AppDomain.CurrentDomain.GetAssemblies())
		{
			var types = assembly.GetTypes();
			
			foreach(var t in types)
			{
				List<ConsoleCategoryAttribute> baseCategories = new List<ConsoleCategoryAttribute>();
				foreach(var a in t.GetCustomAttributes(false))
				{
					var category = a as ConsoleCategoryAttribute;
					if(category == null) continue;
					baseCategories.Add(category);
				}
				
				var methods = t.GetMethods(BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				foreach(var m in methods)
				{
					var attributes = m.GetCustomAttributes(false);
					foreach(var a in attributes)
					{
						var consoleCommand = a as ConsoleCommandAttribute;
						if(consoleCommand == null) continue;
						
						if(!ValidCommandName(consoleCommand.name))
						{
							LogWarning("Method " + m.Name + " on Type " + t.Name + " is marked as a Console Command but has a name that contains invalid characters or is empty.");
							continue;
						}
						
						var c = AddMethod(m, consoleCommand, t);					
						
						foreach(var cat in baseCategories) AddCommandToCategory(cat, c);
						
						bool categorized = CategorizeConsoleMethod(attributes, c) 
							|| baseCategories.Count != 0;
						
						if(!categorized) AddCommandToCategory("Uncategorized", c);
					}
				}
				
				var properties = t.GetProperties(BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				foreach(var p in properties)
				{
					var attributes = p.GetCustomAttributes(false);
					foreach(var a in attributes)
					{
						var consoleCommand = a as ConsoleCommandAttribute;
						if(consoleCommand == null) continue;
						
						if(!ValidCommandName(consoleCommand.name))
						{
							LogWarning("Property " + p.Name + " on Type " + t.Name + " is marked as a Console Command but has a name that contains invalid characters or is empty.");
							break;
						}
						
						var getMethod = p.GetGetMethod(false);
						if(getMethod != null)
						{
							var c = AddMethod(getMethod, consoleCommand, t);
							foreach(var cat in baseCategories) AddCommandToCategory(cat, c);
							
							bool categorized = CategorizeConsoleMethod(attributes, c)
								|| baseCategories.Count != 0;
							
							if(!categorized) AddCommandToCategory("Uncategorized", c);
						}
						
						var setMethod = p.GetSetMethod(false);
						if(setMethod != null)
						{
							var c = AddMethod(setMethod, consoleCommand, t);
							if(getMethod != null)
							{
								c.helpMessage = "Set the value of " + c.name + ".";
							}
							if(c.parameterInfo.Length < 1)
							{
								c.parameterInfo = new string[]{ "The value to set " + c.name + " to." };
							}
							
							foreach(var cat in baseCategories) AddCommandToCategory(cat, c);
							
							bool categorized = CategorizeConsoleMethod(attributes, c)
								|| baseCategories.Count != 0;
							
							if(!categorized) AddCommandToCategory("Uncategorized", c);
						}
						
						break;
					}
				}
			}
		}
		
		foreach(var kvp in consoleCommands)
		{
			kvp.Value.Sort((x,y)=>{
				return x.signature.Length - y.signature.Length;
			});
		}
		
		Application.RegisterLogCallback(UnityLogCallback);
		
		initialized = true;
		
		VConsole_BuiltinCommands.LoadScripts();
		
		RunScript(startup.name);
		
		if(Application.platform == RuntimePlatform.OSXEditor
		|| Application.platform == RuntimePlatform.WindowsEditor)
		{
			RunScript(startup_editor.name);
		}
		else
		{
			RunScript(startup_runtime.name);
		}
		if(Application.platform == RuntimePlatform.Android)
		{
			RunScript(startup_android.name);
		}
		if(Application.platform == RuntimePlatform.FlashPlayer)
		{
			RunScript(startup_flash.name);
		}
		if(Application.platform == RuntimePlatform.IPhonePlayer)
		{
			RunScript(startup_iOS.name);
		}
		if(Application.platform == RuntimePlatform.OSXWebPlayer
		|| Application.platform == RuntimePlatform.WindowsWebPlayer)
		{
			RunScript(startup_web.name);
		}
		if(Application.platform == RuntimePlatform.LinuxPlayer)
		{
			RunScript(startup_linux.name);
		}
		if(Application.platform == RuntimePlatform.NaCl)
		{
			RunScript(startup_nacl.name);
		}
		if(Application.platform == RuntimePlatform.WindowsPlayer
		|| Application.platform == RuntimePlatform.WindowsEditor)
		{
			RunScript(startup_windows.name);
		}
		if(Application.platform == RuntimePlatform.OSXPlayer
		|| Application.platform == RuntimePlatform.OSXEditor)
		{
			RunScript(startup_mac.name);
		}
		if(Application.platform == RuntimePlatform.OSXPlayer
		|| Application.platform == RuntimePlatform.OSXEditor
		|| Application.platform == RuntimePlatform.WindowsEditor
		|| Application.platform == RuntimePlatform.WindowsPlayer)
		{
			RunScript(startup_pc.name);
		}
	}
	
	bool CategorizeConsoleMethod (object[] attributes, VConsoleCommand c)
	{
		bool found = false;
		foreach(var cat in attributes)
		{			
			var category = cat as ConsoleCategoryAttribute;
			if(category == null) continue;
			
			found = true;
			AddCommandToCategory(category, c);
		}
		return found;
	}
	
	void AddCommandToCategory (ConsoleCategoryAttribute category, VConsoleCommand c)
	{
		AddCommandToCategory(category.name, c);
	}
	void AddCommandToCategory (string category, VConsoleCommand c)
	{
		List<VConsoleCommand> catList = null;
		if(!consoleCategories.ContainsKey(category))
		{
			catList = new List<VConsoleCommand>();
			consoleCategories.Add(category, catList);
		}else catList = consoleCategories[category];
		
		if(catList.Contains(c)) return;
		
		catList.Add(c);
	}
	
	bool ValidCommandName (string s)
	{
		if(s == "" || s == " " || s.Split(' ').Length != 1)
			return false;
		return true;
	}
	
	VConsoleCommand AddMethod (MethodInfo m, ConsoleCommandAttribute consoleCommand, Type t)
	{
		VConsoleCommand c = new VConsoleCommand();
		c.method = m;
		c.parentType = t;
		c.name = consoleCommand.name;
		c.helpMessage = consoleCommand.helpMessage;
		c.parameterInfo = consoleCommand.parameterInfo;
		
		var pmtrs = m.GetParameters();
		c.signature = new ParameterInfo[pmtrs.Length];
		
		List<Type> cEnumTypes = new List<Type>();
		for(int i = 0; i < pmtrs.Length; i++)
		{
			ParameterInfo p = pmtrs[i];
			Type pType = p.ParameterType;
			
			if(!(pType.IsEnum
				|| pType == typeof(float)
				|| pType == typeof(int)
				|| pType == typeof(string)
				|| pType == typeof(bool)))
			{
				VConsole.LogWarning("Method " + m.Name + " on Type " + t.Name + " is marked as a Console Command but accepts parameters other than Enum, float, int, or string");
				c.signature = null;
				break;
			}
			
			if(pType.IsEnum)
			{
				string en = pType.Name.ToLower();
				if(!enumTypes.ContainsKey(en))
				{
					enumTypes.Add(en, pType);
				}
				cEnumTypes.Add(pType);
			}
			
			c.signature[i] = p;
			
		}
		c.enumTypes = cEnumTypes.ToArray();
		
		if(c.signature == null) return null;
		
		string n = consoleCommand.name.ToLower();
		if(!consoleCommands.ContainsKey(n))
		{
			var list = new List<VConsoleCommand>();
			list.Add(c);
			consoleCommands.Add(n, list);
			return c;
		}
		
		var cmds = consoleCommands[n];
		bool broken = false;
		foreach(var cmd in cmds)
		{
			if(cmd.MatchSignature(c.signature))
			{
				VConsole.LogWarning("Method " + m.Name + " on Type " + t.Name + " is marked as a Console Command but has the same name and method signature as Method " + cmd.method.Name + " on Type " + cmd.parentType.Name);
				broken = true;
				break;
			}
		}
		
		if(broken)
		{
			return null;
		}
		
		cmds.Add(c);
		return c;
	}
	
	void OnGUI () {
		var prevSkin = GUI.skin;
		if(guiSkin) GUI.skin = guiSkin;
		
		if(open)
		{
			bool enterPressed = Event.current.type == EventType.KeyDown && Event.current.keyCode == KeyCode.Return;
			bool tildePress = Event.current.type == EventType.KeyDown && Event.current.keyCode == KeyCode.BackQuote;
			bool upPress = Event.current.type == EventType.KeyDown && Event.current.keyCode == KeyCode.UpArrow;
			bool downPress = Event.current.type == EventType.KeyDown && Event.current.keyCode == KeyCode.DownArrow;
			
			if(tildePress)
			{
				open = false;
				inputText = "";	
			}
			
			Rect textViewRect = new Rect(0, 0, Screen.width, Screen.height/2);
			Rect textInputRect = new Rect(0, (Screen.height/2 + 1), Screen.width, Screen.height/4);
			GUI.Box(textViewRect, GUIContent.none);
			
			GUILayout.BeginArea(textViewRect);
			
			scrollPosition = GUILayout.BeginScrollView(scrollPosition, GUILayout.Width(Screen.width), GUILayout.Height(Screen.height/2));
				
			for(int i = 0; i < messageCache.Count; i++)
			{
				var msg = messageCache[i];
				msg.DrawGUI();
			}
		
			GUILayout.EndScrollView();
			
			GUILayout.EndArea();
			
			GUILayout.BeginArea(textInputRect);
			GUILayout.BeginHorizontal();
			GUI.SetNextControlName("input");
			inputText = GUILayout.TextField(inputText);
			GUI.FocusControl("input");
	
			if(upPress && commandCache.Count != 0)
			{
				if(arrowIndex < commandCache.Count)
					arrowIndex++;
			}
	
			if(downPress)
			{
				if(arrowIndex > 0)
					arrowIndex--;
			}
	
			if((upPress || downPress) && arrowIndex != 0)
			{
				int i = (commandCache.Count - 1) - (arrowIndex -1 );
				inputText = commandCache[i];
			}
	
			if(GUILayout.Button("Send", GUILayout.Width(100)) || enterPressed)
			{
				arrowIndex = 0;
				if(inputText != "")
				{
					commandCache.Add(inputText);
					
					Parse(inputText);
					
					inputText = "";
					scrollPosition = new Vector2(0, Mathf.Infinity + 1);
				}
			}
			else if(inputText == "`")
			{
				inputText = "";
			}
			if(GUILayout.Button("Close", GUILayout.Width(64)))
			{
				open = false;
			}
			GUILayout.EndHorizontal();		
			GUILayout.EndArea();
			
		}	
		
		GUI.skin = prevSkin;
		
	}
	
	public void Parse (string inputText, bool logCommands = true)
	{
		StartCoroutine(ParseRoutine(inputText, logCommands));
	}
	IEnumerator ParseRoutine (string inputText, bool logCommands)
	{
		foreach(var s in SplitCommands(inputText))
		{
			if(s.Length == 0) continue;

			var inputs = SplitInputs(s).ToArray();
			
			if(inputs.Length == 0) continue;
			
			if(logCommands && !hideCommands) Log("> " + s);
			
			string command = inputs[0].ToLower().Trim();
			string[] vars = new string[inputs.Length-1];
			
			for(int i = 1; i < inputs.Length; i++)
				vars[i-1] = inputs[i];
			
			List<VConsoleCommand> cmds = null;
			if(!consoleCommands.TryGetValue(command, out cmds))
			{
				if(vars.Length == 0)
				{
					if(commandMacros.ContainsKey(command))
					{
						Parse(commandMacros[command], logCommands);
						continue;
					}
				}
				LogError("Command '" + command + "' is not valid.");
				continue;
			}
			
			bool found = false;
			foreach(var cmd in cmds)
			{
				object[] result;
				if(cmd.TryGetSignature(vars, out result))
				{
					var obj = cmd.method.Invoke(null, result);
					if(obj != null)
					{
						var routine = obj as IEnumerator;
						if(routine != null) while(routine.MoveNext()) yield return null;
						else
						{
							LogSecondary(obj.ToString());
						}
					}
					found = true;
					break;
				}
			}
			if(found) continue;
			
			LogError("Command '" + command + "' contained invalid parameters. Try 'help " + command + "' for more information.");
		}
	}
	bool IsEscaped (string inputText, int c)
	{
		if(c == 0) return false;
		if(inputText[c-1] == '\\') return true;
		return false;
	}
	
	IEnumerable<string> SplitCommands (string inputText)
	{
		StringBuilder builder = new StringBuilder();
		
		bool insideQuotes = false;
		bool commented = false;
		for(int i = 0; i < inputText.Length; i++)
		{
			var c = inputText[i];
			if(!commented)
			{
				if(c == '"')
				{
					insideQuotes = !insideQuotes;
				}
				if(c ==';')
				{
					if(!insideQuotes)
					{
						yield return builder.ToString();
						builder = new StringBuilder();
						continue;
					}
				}
				if(i != inputText.Length-1 && c == '/' && inputText[i+1] == '/')
				{
					commented = true;
					continue;
				}
				if(c=='\n')	continue;
				builder.Append(c);
			}
			else
			{
				if(c=='\n')
				{
					commented = false;
				}
			}
		}
		
		yield return builder.ToString();
	}
	IEnumerable<string> SplitInputs (string command)
	{
		StringBuilder builder = new StringBuilder();
		
		bool insideQuotes = false;
		bool commented = false;
		for(int i = 0; i < command.Length; i++)
		{
			var c = command[i];
			if(!commented)
			{
				if(!IsEscaped(command, i))
				{
					if(c == '\\') continue;
					if(c == '"')
					{
						insideQuotes = !insideQuotes;
						continue;
					}
					if(c == ' ')
					{
						if(!insideQuotes)
						{
							yield return builder.ToString();
							builder = new StringBuilder();
							continue;
						}
					}
				}
				if(i != command.Length-1 && c == '/' && command[i+1] == '/')
				{
					commented = true;
					continue;
				}
				if(c=='\n') continue;
				builder.Append(c);
			}
			else
			{
				if(c=='\n')
				{
					commented = false;
				}
			}
		}
		
		yield return builder.ToString();
	}
	
	void Update()
	{
		if(Input.GetKeyDown(KeyCode.BackQuote))
		{
			open = !open;	
			return;
		}
		
		foreach(var kvp in keyDownMacros.ToArray())
		{
			if(Input.GetKeyDown(kvp.Key))
			{
				Parse(kvp.Value);
			}
		}
		
		foreach(var kvp in keyUpMacros.ToArray())
		{
			if(Input.GetKeyUp(kvp.Key))
			{
				Parse(kvp.Value);
			}
		}
	}
	
	void UnityLogCallback (string condition, string stacktrace, LogType logtype)
	{
		Color c = primaryColor;
		bool isValid;
		switch(logtype)
		{
			case LogType.Assert:
			case LogType.Error:
			case LogType.Exception:
				c = errorColor;
				isValid = displayUnityErrors;
				break;
			default:
			case LogType.Log:
				c = primaryColor;
				isValid = displayUnityLogs;
				break;
			case LogType.Warning:
				c = warningColor;
				isValid = displayUnityWarnings;
				break;
		}
		if(isValid){
			var msg = new VConsoleMessageComposite(
				new VConsoleMessageSingle("[Unity]", secondaryColor),
				new VConsoleMessageSingle(condition, c)
			);
			Log(msg);
		}
	}
	
	public IEnumerable<Script> GetBuiltinScripts ()
	{
		yield return startup;		
		yield return startup_editor;
		
		foreach(var script in GetPlatformScripts())
			yield return script;
	}
	
	public IEnumerable<Script> GetPlatformScripts ()
	{
		yield return startup_runtime;
		yield return startup_android;
		yield return startup_flash;
		yield return startup_iOS;
		yield return startup_web;
		yield return startup_linux;
		yield return startup_nacl;
		yield return startup_windows;
		yield return startup_mac;
		yield return startup_pc;
	}
	
	public IEnumerable<Script> GetCachedScripts ()
	{
		foreach(var script in GetBuiltinScripts()) 
			yield return script;
		
		foreach(var script in cachedScripts) 
			yield return script;
	}

}
