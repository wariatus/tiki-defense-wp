using UnityEngine;
using System.Collections;
using System;

[AttributeUsage(AttributeTargets.Method | AttributeTargets.Property, AllowMultiple = true)]
public class ConsoleCommandAttribute : Attribute
{
	public string name;
	public string helpMessage;
	public string[] parameterInfo;
	public ConsoleCommandAttribute (string name, string helpMessage, params string[] parameterInfo)
	{
		this.name = name;
		this.helpMessage = helpMessage;
		this.parameterInfo = parameterInfo;
	}
}
