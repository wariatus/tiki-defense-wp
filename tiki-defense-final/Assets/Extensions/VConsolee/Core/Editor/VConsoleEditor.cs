using UnityEngine;
using UnityEditor;
using System;
using System.Collections.Generic;

[CustomEditor(typeof(VConsole))]
public class VConsoleEditor : Editor
{	
	bool platformsFoldout;
	bool customsFoldout;
	
	public override void OnInspectorGUI ()
	{	
		EditorGUIUtility.LookLikeInspector();
		DrawDefaultInspector();
		
		GUILayout.Space(10);
		
		EditorGUI.BeginChangeCheck();
		
		var console = target as VConsole;
		
		ScriptFoldoutBox(console.startup);
		ScriptFoldoutBox(console.startup_editor);
		
		platformsFoldout = FoldoutBox(platformsFoldout,
		()=>{
			GUILayout.Label("Platforms");
		},
		()=>{
			foreach(var script in console.GetPlatformScripts())
			{
				ScriptFoldoutBox(script);
			}
		});
		
		customsFoldout = FoldoutBox(customsFoldout,
		()=>{
			GUILayout.Label("Custom Scripts");
			if(customsFoldout)
			{
				GUILayout.FlexibleSpace();
				if(GUILayout.Button("Add New"))
				{
					console.cachedScripts.Add(new VConsole.Script("New Script"));
				}
			}
		},
		()=>{
			for(int i = 0; i < console.cachedScripts.Count; i++)
			{
				var script = console.cachedScripts[i];
				script.foldout = FoldoutBox(script.foldout,
				()=>{
					GUILayout.Label(script.name);
					if(GUILayout.Button("X", GUILayout.Width(23)))
					{
						if(EditorUtility.DisplayDialog("Remove Script", "Are you sure you wish to remove this script?", "Confirm", "Cancel"))
						{
							console.cachedScripts.RemoveAt(i--);
						}
					}
				},
				()=>{
					script.name = GUILayout.TextField(script.name);
					ScriptContentsEditor(script);
				});
			}
		});
		
		if(EditorGUI.EndChangeCheck())
		{
			EditorUtility.SetDirty(console);
		}
	}
			
	void ScriptFoldoutBox (VConsole.Script script)
	{
		script.foldout = FoldoutBox(script.foldout,
		()=>{
			GUILayout.Label(script.name);
		},
		()=>{
			ScriptContentsEditor(script);	
		});
	}
	void ScriptContentsEditor (VConsole.Script script)
	{
		EditorGUILayout.BeginHorizontal();
		
		if(GUILayout.Button("Copy", GUILayout.Height(15)))
		{
			EditorGUIUtility.systemCopyBuffer = script.contents;
		}
		
		if(GUILayout.Button("Paste", GUILayout.Height(15)))
		{
			script.contents = EditorGUIUtility.systemCopyBuffer;
		}
		
		EditorGUILayout.EndHorizontal();
		script.contents = GUILayout.TextArea(script.contents, GUILayout.Height(120));
	}
	
	public static bool FoldoutBox (bool foldoutState, Action title, Action contents)
	{
		//left margin
		EditorGUILayout.BeginHorizontal();
		GUILayout.Space(4);
		
		//start drawing toggle area
		Rect r = EditorGUILayout.BeginHorizontal();
		GUI.Box(r, GUIContent.none);
		
		//left margin
		GUILayout.Space(5);
		
		//begin drawing downward
		EditorGUILayout.BeginVertical();
		
		GUILayout.Space(5);
		
		//title
		Rect titleRect = EditorGUILayout.BeginHorizontal();
		GUILayout.Space(2);
		title();
		EditorGUILayout.EndHorizontal();
		
		if(Event.current.type == EventType.MouseDown && Event.current.button == 0 && titleRect.Contains(Event.current.mousePosition))
		{
			foldoutState = !foldoutState;
			Event.current.Use();
		}
		
		GUILayout.Space(5);
		
		if(foldoutState)
		{			
			EditorGUILayout.BeginVertical();
			contents();
			EditorGUILayout.EndVertical();
			
			GUILayout.Space(5);
		}
		
		GUILayout.Space(5);
		
		EditorGUILayout.EndVertical();
		GUILayout.Space(5);
		EditorGUILayout.EndHorizontal();
		
		EditorGUILayout.EndHorizontal();
		
		return foldoutState;
	}
}
