log "This is an example script provided by VConsole.";
log "This script runs automatically when \"Run Example\" is sent to the console.";
log "The next log will be executed with a delay of 3 seconds.";
wait 3;
log "I hope this helps demonstrate how VConsole scripts can be used and written.";
log "This example script should be located in VConsole/Scripts/Example.txt";