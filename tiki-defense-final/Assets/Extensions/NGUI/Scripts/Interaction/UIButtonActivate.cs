//----------------------------------------------
//            NGUI: Next-Gen UI kit
// Copyright � 2011-2014 Tasharen Entertainment
//----------------------------------------------

using UnityEngine;

/// <summary>
/// Very basic script that will activate or deactivate an object (and all of its children) when clicked.
/// </summary>

[AddComponentMenu("NGUI/Interaction/Button Activate")]
public class UIButtonActivate : MonoBehaviour
{
    public GameObject target;
    public bool state = true;

	void OnClick () 
	{ 
		if(Application.loadedLevel == 5)
		{
			if(gameObject.name == "Charges Button" || gameObject.name == "Child")
			{
				if(target != null)
				{
					if(target.gameObject.name != "4. Play" && target.gameObject.name != "3. Pausa")
						NGUITools.SetActive(target, state); 
				}
			}
			else
				NGUITools.SetActive(target, state); 
		}
		else if (target != null) 
			NGUITools.SetActive(target, state); 
	}
}