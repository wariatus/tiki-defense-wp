////////////////////////////////////////////////////////////////////////////////
//  
// @module <module_name>
// @author Osipov Stanislav lacost.st@gmail.com
//
////////////////////////////////////////////////////////////////////////////////

using UnityEngine;
using UnityEditor;
using System.Collections;

public class IOSNativeEditor : MonoBehaviour {

	//--------------------------------------
	// INITIALIZE
	//--------------------------------------

	[MenuItem("GameObject/Create Other/IOS Native")]
	public static void CreateMesh()  {
		IOSNativeCreateWindow wnd = EditorWindow.GetWindow<IOSNativeCreateWindow>();
		wnd.appId = IOSNativeCreateWindow.ios.appId;
	}

	//--------------------------------------
	//  PUBLIC METHODS
	//--------------------------------------
	
	//--------------------------------------
	//  GET/SET
	//--------------------------------------
	
	//--------------------------------------
	//  EVENTS
	//--------------------------------------
	
	//--------------------------------------
	//  PRIVATE METHODS
	//--------------------------------------
	
	//--------------------------------------
	//  DESTROY
	//--------------------------------------

}
