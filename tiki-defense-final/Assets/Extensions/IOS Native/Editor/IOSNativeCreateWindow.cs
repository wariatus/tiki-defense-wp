////////////////////////////////////////////////////////////////////////////////
//  
// @module <module_name>
// @author Osipov Stanislav lacost.st@gmail.com
//
////////////////////////////////////////////////////////////////////////////////

using UnityEngine;
using UnityEditor;
using System.Collections;

public class IOSNativeCreateWindow : EditorWindow {

	public string appId;

	//--------------------------------------
	// INITIALIZE
	//--------------------------------------

	//--------------------------------------
	//  PUBLIC METHODS
	//--------------------------------------

	void OnGUI() {
		title = "IOS Native";
		minSize = new Vector2 (300f, 100f);
		maxSize = new Vector2 (300f, 100f);

		GUILayout.Label ("Application Settings", EditorStyles.boldLabel);
		EditorGUILayout.Separator();
		appId = EditorGUILayout.TextField ("Application Id", appId);


		if(GUI.Button (new Rect(205, 65, 90, 20), "Done")) {
			ios.appId = appId;
			Close ();
		}

		if(GUI.Button (new Rect(110, 65, 90, 20), "iTunes")) {
			Application.OpenURL ("https://itunesconnect.apple.com");
		}

		if(GUI.Button (new Rect(10, 65, 95, 20), "Documentation")) {
			Application.OpenURL ("http://bit.ly/19mKULI");
		}

	}
	
	//--------------------------------------
	//  GET/SET
	//--------------------------------------

	public static IOSNative ios {
		get {
			object[] obj = Resources.FindObjectsOfTypeAll(typeof(IOSNative));
			if(obj.Length == 0) {
				GameObject o = new GameObject ("IOSNative");
				return o.AddComponent<IOSNative> ();
			} else {
				return obj [0] as IOSNative;
			}
		}
	}

	
	//--------------------------------------
	//  EVENTS
	//--------------------------------------
	
	//--------------------------------------
	//  PRIVATE METHODS
	//--------------------------------------
	
	//--------------------------------------
	//  DESTROY
	//--------------------------------------

}
