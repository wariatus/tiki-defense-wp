//
//  ALInterstitialCache.h
//  Unity-iPhone
//
//  Created by Matt Szaro on 1/17/14.
//
//

#import <Foundation/Foundation.h>
#import "ALAdLoadDelegate.h"
#import "ALInterstitialCache.h"
#import "ALAdDelegateWrapper.h"

@interface ALInterstitialCache : NSObject  <ALAdLoadDelegate>

+(instancetype) shared;

@property (strong, nonatomic) ALAd* lastAd;
@property (strong, nonatomic) ALAdDelegateWrapper* wrapperToNotify;

@end
