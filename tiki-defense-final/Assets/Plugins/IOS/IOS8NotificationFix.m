//
//  IOS8NotificationFix.m
//  Unity-iPhone
//
//  Created by Łukasz Boruń on 13/10/14.
//
//

#import <Foundation/Foundation.h>

void _EnableLocalNotificationIOS8()
{
    UIApplication *app = [UIApplication sharedApplication];
    
    if ([app respondsToSelector:@selector(registerUserNotificationSettings:)])
    {
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound categories:nil];
        [app registerUserNotificationSettings:settings];
        [app registerForRemoteNotifications];
    }
}