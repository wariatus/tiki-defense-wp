{
  "iOSSKU": "com.roscostudios.tikidefense",
  "macAppStoreSKU": "com.roscostudios.tikidefensemacos",
  "androidBillingPlatform": "GooglePlay",
  "GooglePlayPublicKey": "[Your key]",
  "useAmazonSandbox": false,
  "UseWP8MockingFramework": false,
  "useHostedConfig": false,
  "hostedConfigUrl": null,
  "UseWin8_1Sandbox": true,
  "samsungAppsMode": "PRODUCTION",
  "samsungAppsItemGroupId": "",
  "unibillAnalyticsAppId": "",
  "unibillAnalyticsAppSecret": "",
  "purchasableItems": {
    "com.roscostudios.tikidefense.tikimasks": {
      "@id": "com.roscostudios.tikidefense.tikimasks",
      "@purchaseType": "NonConsumable",
      "name": "All Tiki Masks in existence",
      "description": "A pack containing all the tiki masks",
      "downloadableContentId": null,
      "platforms": {
        "GooglePlay": {
          "priceInLocalCurrency": 1.0,
          "defaultLocale": "en_US"
        },
        "AmazonAppstore": {},
        "SamsungApps": {},
        "AppleAppStore": {
          "appleAppStorePriceTier": 3,
          "screenshotPath": ""
        },
        "MacAppStore": {
          "MacAppStore.Id": "com.roscostudios.tikidefensemacos.tikimasks"
        },
        "WindowsPhone8": {},
        "Windows8_1": {},
        "UnityEditor": {}
      }
    },
    "com.roscostudios.tikidefense.1000gems": {
      "@id": "com.roscostudios.tikidefense.1000gems",
      "@purchaseType": "Consumable",
      "name": "1000 gems",
      "description": "1000 gems",
      "downloadableContentId": null,
      "platforms": {
        "GooglePlay": {},
        "AmazonAppstore": {},
        "SamsungApps": {},
        "AppleAppStore": {
          "appleAppStorePriceTier": 3,
          "screenshotPath": ""
        },
        "MacAppStore": {
          "MacAppStore.Id": "com.roscostudios.tikidefensemacos.1000gems"
        },
        "WindowsPhone8": {},
        "Windows8_1": {},
        "UnityEditor": {}
      }
    },
    "com.roscostudios.tikidefense.2000gems": {
      "@id": "com.roscostudios.tikidefense.2000gems",
      "@purchaseType": "Consumable",
      "name": "2000 gems",
      "description": "2000 gems",
      "downloadableContentId": null,
      "platforms": {
        "GooglePlay": {},
        "AmazonAppstore": {},
        "SamsungApps": {},
        "AppleAppStore": {
          "appleAppStorePriceTier": 5,
          "screenshotPath": ""
        },
        "MacAppStore": {
          "MacAppStore.Id": "com.roscostudios.tikidefensemacos.2000gems"
        },
        "WindowsPhone8": {},
        "Windows8_1": {},
        "UnityEditor": {}
      }
    },
    "com.roscostudios.tikidefense.5000gems": {
      "@id": "com.roscostudios.tikidefense.5000gems",
      "@purchaseType": "Consumable",
      "name": "5000 gems",
      "description": "5000 gems",
      "downloadableContentId": null,
      "platforms": {
        "GooglePlay": {},
        "AmazonAppstore": {},
        "SamsungApps": {},
        "AppleAppStore": {
          "appleAppStorePriceTier": 10,
          "screenshotPath": ""
        },
        "MacAppStore": {
          "MacAppStore.Id": "com.roscostudios.tikidefensemacos.5000gems"
        },
        "WindowsPhone8": {},
        "Windows8_1": {},
        "UnityEditor": {}
      }
    },
    "com.roscostudios.tikidefense.15000gems": {
      "@id": "com.roscostudios.tikidefense.15000gems",
      "@purchaseType": "Consumable",
      "name": "15000 gems",
      "description": "15000 gems",
      "downloadableContentId": null,
      "platforms": {
        "GooglePlay": {},
        "AmazonAppstore": {},
        "SamsungApps": {},
        "AppleAppStore": {
          "appleAppStorePriceTier": 18,
          "screenshotPath": ""
        },
        "MacAppStore": {
          "MacAppStore.Id": "com.roscostudios.tikidefensemacos.15000gems"
        },
        "WindowsPhone8": {},
        "Windows8_1": {},
        "UnityEditor": {}
      }
    },
    "com.roscostudios.tikidefense.40000gems": {
      "@id": "com.roscostudios.tikidefense.40000gems",
      "@purchaseType": "Consumable",
      "name": "40000 gems",
      "description": "40000 gems",
      "downloadableContentId": null,
      "platforms": {
        "GooglePlay": {},
        "AmazonAppstore": {},
        "SamsungApps": {},
        "AppleAppStore": {
          "appleAppStorePriceTier": 45,
          "screenshotPath": ""
        },
        "MacAppStore": {
          "MacAppStore.Id": "com.roscostudios.tikidefensemacos.40000"
        },
        "WindowsPhone8": {},
        "Windows8_1": {},
        "UnityEditor": {}
      }
    }
  },
  "currencies": {
    "gems": {
      "com.roscostudios.tikidefense.5000gems": 5000.0,
      "com.roscostudios.tikidefense.2000gems": 2000.0,
      "com.roscostudios.tikidefense.1000gems": 1000.0,
      "com.roscostudios.tikidefense.15000gems": 15000.0,
      "com.roscostudios.tikidefense.40000gems": 40000.0
    }
  }
}