SKU	Product ID	Reference Name	Type	Cleared For Sale	Wholesale Price Tier	Displayed Name	Description	Screenshot Path
com.roscostudios.tikidefense	com.roscostudios.tikidefense.tikimasks	com.roscostudios.tikidefense.tikimasks	Non-Consumable	yes	3	All Tiki Masks in existence	A pack containing all the tiki masks	
com.roscostudios.tikidefense	com.roscostudios.tikidefense.1000gems	com.roscostudios.tikidefense.1000gems	Consumable	yes	3	1000 gems	1000 gems	
com.roscostudios.tikidefense	com.roscostudios.tikidefense.2000gems	com.roscostudios.tikidefense.2000gems	Consumable	yes	5	2000 gems	2000 gems	
com.roscostudios.tikidefense	com.roscostudios.tikidefense.5000gems	com.roscostudios.tikidefense.5000gems	Consumable	yes	10	5000 gems	5000 gems	
com.roscostudios.tikidefense	com.roscostudios.tikidefense.15000gems	com.roscostudios.tikidefense.15000gems	Consumable	yes	18	15000 gems	15000 gems	
com.roscostudios.tikidefense	com.roscostudios.tikidefense.40000gems	com.roscostudios.tikidefense.40000gems	Consumable	yes	45	40000 gems	40000 gems	
