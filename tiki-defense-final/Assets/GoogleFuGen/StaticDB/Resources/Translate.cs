//----------------------------------------------
//    GoogleFu: Google Doc Unity integration
//         Copyright © 2013 Litteratus
//
//        This file has been auto-generated
//              Do not manually edit
//----------------------------------------------

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace GoogleFu
{
	[System.Serializable]
	public class TranslateRow 
	{
		public string _PL;
		public string _EN;
		public string _FR;
		public string _DE;
		public string _SP;
		public string _BR;
		public TranslateRow(string __PL, string __EN, string __FR, string __DE, string __SP, string __BR) 
		{
			_PL = __PL;
			_EN = __EN;
			_FR = __FR;
			_DE = __DE;
			_SP = __SP;
			_BR = __BR;
		}

		public string GetStringData( string colID )
		{
			string ret = String.Empty;
			switch( colID.ToUpper() )
			{
				case "PL":
					ret = _PL.ToString();
					break;
				case "EN":
					ret = _EN.ToString();
					break;
				case "FR":
					ret = _FR.ToString();
					break;
				case "DE":
					ret = _DE.ToString();
					break;
				case "SP":
					ret = _SP.ToString();
					break;
				case "BR":
					ret = _BR.ToString();
					break;
			}

			return ret;
		}
	}
	public sealed class Translate
	{
		public enum rowIds {
			Language, ID_ENGLISH, ID_FRENCH, ID_GERMAN, ID_POLISH, ID_INFORMATION, ID_SOUND, ID_MUSIC, ID_DESCRIPTION, ID_ENEMY, ID_CHANGE_LANGUAGE, ID_JUNGLE_LOCKED, ID_VILLAGE_LOCKED, ID_HILLS_LOCKED, ID_VOLCANO_LOCK, ID_BUILD_TURRET_POPUP_1, ID_BUILD_TURRET_POPUP_2, ID_BUILD_TURRET_POPUP_3, ID_BUILD_TURRET_POPUP_4, ID_OBSTACLES_POPUP, 
			ID_BACK, ID_DEFEAT, ID_VICTORY, ID_EARTH_TOTEM, ID_EARTH_TOTEM_INFO, ID_ALCHEMY_VIAL, ID_ALCHEMY_VIAL_INFO, ID_FIRE_TOTEM, ID_FIRE_TOTEM_INFO, ID_AIR_TOTEM, ID_AIR_TOTEM_INFO, ID_MAGIC_SHOT, ID_MAGIC_SHOT_INFO, ID_ICE_TOTEM, ID_ICE_TOTEM_INFO, ID_DRUMS, ID_DRUMS_INFO, ID_THUNDERCLOUD, ID_THUNDERCLOUD_INFO, ID_CALUDRON, 
			ID_CAULDRON_INFO, ID_THREE_BIG_STONES, ID_THREE_BIG_STONES_INFO, ID_LIZARD, ID_LIZARD_INFO, ID_NECKLACE, ID_NECKLACE_INFO, ID_PAN_FLUTE, ID_PAN_FLUTE_INFO, ID_ANVIL, ID_ANVIL_INFO, ID_RUNESTONE, ID_RUNESTONE_INFO, ID_TURRET_16, ID_TURRET_16_INFO, ID_TIKI_MASK_FIRE, ID_TIKI_MASK_FIRE_INFO, ID_TIKI_MASK_ICE, ID_TIKI_MASK_ICE_INFO, ID_TIKI_MASK_EARTH, 
			ID_TIKI_MASK_EARTH_INFO, ID_TIKI_MASK_AIR, ID_TIKI_MASK_AIR_INFO, ID_TUTORIAL_TIKI_MASK_1, ID_TUTORIAL_TIKI_MASK_2, ID_TUTORIAL_TIKI_MASK_3, ID_TUTORIAL_TIKI_MASK_4, ID_TUTORIAL_TIKI_MASK_5, ID_TUTORIAL_TIKI_MASK_6, ID_TUTORIAL_TIKI_MASK_7, ID_TUTORIAL_TIKI_MASK_8, ID_TUTORIAL_TIKI_MASK_9, ID_BLOCK_1, ID_BLOCK_2, ID_BLOCK_3, ID_BLOCK_4, ID_BLOCK_5, ID_BLOCK_6, ID_BLOCK_7, ID_BLOCK_8, 
			ID_BLOCK_9, ID_BLOCK_10, ID_BLOCK_11, ID_BLOCK_12, ID_NO_BLOCKS, ID_ENCYCLO_MASKS, ID_ENCYCLO_TURRETS, ID_NEW_TOWER, ID_BEACH, ID_JUNGLE, ID_VILLAGE, ID_HILLS, ID_VOLCANO, ID_OR, ID_SELECT_PROFILE, ID_NEW_TIKI_MASK, ID_SHOP_GEMS_1, ID_SHOP_GEMS_2, ID_SHOP_GEMS_3, ID_SHOP_GEMS_4, 
			ID_SHOP_GEMS_5, ID_SHOP_GEMS_6, ID_SHOP_GEMS_7, ID_SHOP_GEMS_8, ID_SHOP_TAB_GEMS, ID_SHOP_TAB_UNLOCKS, ID_SHOP_MASKS, ID_SHOP_JUNGLE, ID_SHOP_VILLAGE, ID_SHOP_HILLS, ID_SHOP_VOLCANO, ID_SHOP_ALL_CONTENT, ID_POPUP_TOTEM_BLOCK, ID_TIKI_MASKS_UNLOCK, ID_JUNGLE_LOCK_AD_1, ID_JUNGLE_LOCK_AD_2, ID_JUNGLE_LOCK_AD_3, ID_VILLAGE_LOCK_AD_1, ID_VILLAGE_LOCK_AD_2, ID_VILLAGE_LOCK_AD_3, 
			ID_HILLS_LOCK_AD_1, ID_HILLS_LOCK_AD_2, ID_HILLS_LOCK_AD_3, ID_VOLCANO_LOCK_AD_1, ID_VOLCANO_LOCK_AD_2, ID_VOLCANO_LOCK_AD_3, ID_JUNGLE_LOCK_AD_4, ID_VILLAGE_LOCK_AD_4, ID_HILLS_LOCK_AD_4, ID_VOLCANO_LOCK_AD_4, ID_BUY_NOW, ID_PAUSE, ID_CIRCLE_TUTORIAL_1, ID_CIRCLE_TUTORIAL_2, ID_CIRCLE_TUTORIAL_3, ID_CIRCLE_TUTORIAL_4, ID_CIRCLE_TUTORIAL_5, ID_SHOP_WOOD_TITLE, ID_SHOP_WOOD_DESCRIPTION, ID_SHOP_BONUS_TITLE, 
			ID_SHOP_BONUS_DESCRIPTION, ID_MONSTER_BADYL, ID_MONSTER_BANANA, ID_MONSTER_BUTELKA, ID_MONSTER_GRZYBEK, ID_MONSTER_ISKRA, ID_MONSTER_KWIATEK, ID_MONSTER_VOODOO, ID_MONSTER_KOKOS, ID_MONSTER_LAWA, ID_MONSTER_DRZEWO, ID_MONSTER_MUSZLA, ID_MONSTER_WUNG, ID_MONSTER_HEAD, ID_MONSTER_SHIELD, ID_MONSTER_DYM, ID_MONSTER_MUCHA, ID_MONSTER_PARROT, ID_MONSTER_SWIETLIK, ID_MONSTER_ZOLW, 
			ID_MONSTER_SNAKE, ID_MONSTER_ANANAS, ID_MONSTER_DUCH, ID_MONSTER_LAVAGHOST, ID_MONSTER_MASKA, ID_MONSTER_NORMAL, ID_MONSTER_NORMAL_INFO, ID_MONSTER_HEAVY, ID_MONSTER_HEAVY_INFO, ID_MONSTER_FAST, ID_MONSTER_FAST_INFO, ID_MONSTER_ARMOUR, ID_MONSTER_ARMOUR_INFO, ID_MONSTER_GHOST, ID_MONSTER_GHOST_INFO, ID_MONSTER_ELITE, ID_MONSTER_BOSS, ID_ENCYCLO_MONSTERS, ID_MONSTER_JUMPER, ID_MONSTER_JUMPER_INFO, 
			ID_MONSTER_SPECIAL, ID_MONSTER_SPECIAL_INFO, ID_VOLCANO_LOCKED, ID_GAME_COMPLETE, ID_ARMOUR_OBSTACLES, ID_SHOP_GEMS_1_INFO, ID_SHOP_GEMS_2_INFO, ID_SHOP_GEMS_3_INFO, ID_SHOP_GEMS_4_INFO, ID_SHOP_GEMS_5_INFO, ID_SHOP_GEMS_6_INFO, ID_SHOP_GEMS_7_INFO, ID_SHOP_GEMS_8_INFO, ID_PLAY, ID_EMBLEM, ID_SHOP_INFO, ID_VICTORY_TAP, ID_POWERS, ID_BONUS_LEVELS, ID_RESTORE, 
			ID_TERMS, ID_WAVE_COMPLETE_1, ID_WAVE_COMPLETE_2, ID_WAVE_COMPLETE_3, ID_INTRO_TUTORIAL_1, ID_INTRO_TUTORIAL_2, ID_SHOP_WATCH_AD, ID_SHOP_WATCH_AD_INFO, ID_AD_WATCHED, ID_LOADING, ID_SHOP_MASKS_INFO, ID_REKLAMA, ID_CONNECT, ID_ENERGY, ID_DOWNLOAD
		};
		public string [] rowNames = {
			"Language", "ID_ENGLISH", "ID_FRENCH", "ID_GERMAN", "ID_POLISH", "ID_INFORMATION", "ID_SOUND", "ID_MUSIC", "ID_DESCRIPTION", "ID_ENEMY", "ID_CHANGE_LANGUAGE", "ID_JUNGLE_LOCKED", "ID_VILLAGE_LOCKED", "ID_HILLS_LOCKED", "ID_VOLCANO_LOCK", "ID_BUILD_TURRET_POPUP_1", "ID_BUILD_TURRET_POPUP_2", "ID_BUILD_TURRET_POPUP_3", "ID_BUILD_TURRET_POPUP_4", "ID_OBSTACLES_POPUP", 
			"ID_BACK", "ID_DEFEAT", "ID_VICTORY", "ID_EARTH_TOTEM", "ID_EARTH_TOTEM_INFO", "ID_ALCHEMY_VIAL", "ID_ALCHEMY_VIAL_INFO", "ID_FIRE_TOTEM", "ID_FIRE_TOTEM_INFO", "ID_AIR_TOTEM", "ID_AIR_TOTEM_INFO", "ID_MAGIC_SHOT", "ID_MAGIC_SHOT_INFO", "ID_ICE_TOTEM", "ID_ICE_TOTEM_INFO", "ID_DRUMS", "ID_DRUMS_INFO", "ID_THUNDERCLOUD", "ID_THUNDERCLOUD_INFO", "ID_CALUDRON", 
			"ID_CAULDRON_INFO", "ID_THREE_BIG_STONES", "ID_THREE_BIG_STONES_INFO", "ID_LIZARD", "ID_LIZARD_INFO", "ID_NECKLACE", "ID_NECKLACE_INFO", "ID_PAN_FLUTE", "ID_PAN_FLUTE_INFO", "ID_ANVIL", "ID_ANVIL_INFO", "ID_RUNESTONE", "ID_RUNESTONE_INFO", "ID_TURRET_16", "ID_TURRET_16_INFO", "ID_TIKI_MASK_FIRE", "ID_TIKI_MASK_FIRE_INFO", "ID_TIKI_MASK_ICE", "ID_TIKI_MASK_ICE_INFO", "ID_TIKI_MASK_EARTH", 
			"ID_TIKI_MASK_EARTH_INFO", "ID_TIKI_MASK_AIR", "ID_TIKI_MASK_AIR_INFO", "ID_TUTORIAL_TIKI_MASK_1", "ID_TUTORIAL_TIKI_MASK_2", "ID_TUTORIAL_TIKI_MASK_3", "ID_TUTORIAL_TIKI_MASK_4", "ID_TUTORIAL_TIKI_MASK_5", "ID_TUTORIAL_TIKI_MASK_6", "ID_TUTORIAL_TIKI_MASK_7", "ID_TUTORIAL_TIKI_MASK_8", "ID_TUTORIAL_TIKI_MASK_9", "ID_BLOCK_1", "ID_BLOCK_2", "ID_BLOCK_3", "ID_BLOCK_4", "ID_BLOCK_5", "ID_BLOCK_6", "ID_BLOCK_7", "ID_BLOCK_8", 
			"ID_BLOCK_9", "ID_BLOCK_10", "ID_BLOCK_11", "ID_BLOCK_12", "ID_NO_BLOCKS", "ID_ENCYCLO_MASKS", "ID_ENCYCLO_TURRETS", "ID_NEW_TOWER", "ID_BEACH", "ID_JUNGLE", "ID_VILLAGE", "ID_HILLS", "ID_VOLCANO", "ID_OR", "ID_SELECT_PROFILE", "ID_NEW_TIKI_MASK", "ID_SHOP_GEMS_1", "ID_SHOP_GEMS_2", "ID_SHOP_GEMS_3", "ID_SHOP_GEMS_4", 
			"ID_SHOP_GEMS_5", "ID_SHOP_GEMS_6", "ID_SHOP_GEMS_7", "ID_SHOP_GEMS_8", "ID_SHOP_TAB_GEMS", "ID_SHOP_TAB_UNLOCKS", "ID_SHOP_MASKS", "ID_SHOP_JUNGLE", "ID_SHOP_VILLAGE", "ID_SHOP_HILLS", "ID_SHOP_VOLCANO", "ID_SHOP_ALL_CONTENT", "ID_POPUP_TOTEM_BLOCK", "ID_TIKI_MASKS_UNLOCK", "ID_JUNGLE_LOCK_AD_1", "ID_JUNGLE_LOCK_AD_2", "ID_JUNGLE_LOCK_AD_3", "ID_VILLAGE_LOCK_AD_1", "ID_VILLAGE_LOCK_AD_2", "ID_VILLAGE_LOCK_AD_3", 
			"ID_HILLS_LOCK_AD_1", "ID_HILLS_LOCK_AD_2", "ID_HILLS_LOCK_AD_3", "ID_VOLCANO_LOCK_AD_1", "ID_VOLCANO_LOCK_AD_2", "ID_VOLCANO_LOCK_AD_3", "ID_JUNGLE_LOCK_AD_4", "ID_VILLAGE_LOCK_AD_4", "ID_HILLS_LOCK_AD_4", "ID_VOLCANO_LOCK_AD_4", "ID_BUY_NOW", "ID_PAUSE", "ID_CIRCLE_TUTORIAL_1", "ID_CIRCLE_TUTORIAL_2", "ID_CIRCLE_TUTORIAL_3", "ID_CIRCLE_TUTORIAL_4", "ID_CIRCLE_TUTORIAL_5", "ID_SHOP_WOOD_TITLE", "ID_SHOP_WOOD_DESCRIPTION", "ID_SHOP_BONUS_TITLE", 
			"ID_SHOP_BONUS_DESCRIPTION", "ID_MONSTER_BADYL", "ID_MONSTER_BANANA", "ID_MONSTER_BUTELKA", "ID_MONSTER_GRZYBEK", "ID_MONSTER_ISKRA", "ID_MONSTER_KWIATEK", "ID_MONSTER_VOODOO", "ID_MONSTER_KOKOS", "ID_MONSTER_LAWA", "ID_MONSTER_DRZEWO", "ID_MONSTER_MUSZLA", "ID_MONSTER_WUNG", "ID_MONSTER_HEAD", "ID_MONSTER_SHIELD", "ID_MONSTER_DYM", "ID_MONSTER_MUCHA", "ID_MONSTER_PARROT", "ID_MONSTER_SWIETLIK", "ID_MONSTER_ZOLW", 
			"ID_MONSTER_SNAKE", "ID_MONSTER_ANANAS", "ID_MONSTER_DUCH", "ID_MONSTER_LAVAGHOST", "ID_MONSTER_MASKA", "ID_MONSTER_NORMAL", "ID_MONSTER_NORMAL_INFO", "ID_MONSTER_HEAVY", "ID_MONSTER_HEAVY_INFO", "ID_MONSTER_FAST", "ID_MONSTER_FAST_INFO", "ID_MONSTER_ARMOUR", "ID_MONSTER_ARMOUR_INFO", "ID_MONSTER_GHOST", "ID_MONSTER_GHOST_INFO", "ID_MONSTER_ELITE", "ID_MONSTER_BOSS", "ID_ENCYCLO_MONSTERS", "ID_MONSTER_JUMPER", "ID_MONSTER_JUMPER_INFO", 
			"ID_MONSTER_SPECIAL", "ID_MONSTER_SPECIAL_INFO", "ID_VOLCANO_LOCKED", "ID_GAME_COMPLETE", "ID_ARMOUR_OBSTACLES", "ID_SHOP_GEMS_1_INFO", "ID_SHOP_GEMS_2_INFO", "ID_SHOP_GEMS_3_INFO", "ID_SHOP_GEMS_4_INFO", "ID_SHOP_GEMS_5_INFO", "ID_SHOP_GEMS_6_INFO", "ID_SHOP_GEMS_7_INFO", "ID_SHOP_GEMS_8_INFO", "ID_PLAY", "ID_EMBLEM", "ID_SHOP_INFO", "ID_VICTORY_TAP", "ID_POWERS", "ID_BONUS_LEVELS", "ID_RESTORE", 
			"ID_TERMS", "ID_WAVE_COMPLETE_1", "ID_WAVE_COMPLETE_2", "ID_WAVE_COMPLETE_3", "ID_INTRO_TUTORIAL_1", "ID_INTRO_TUTORIAL_2", "ID_SHOP_WATCH_AD", "ID_SHOP_WATCH_AD_INFO", "ID_AD_WATCHED", "ID_LOADING", "ID_SHOP_MASKS_INFO", "ID_REKLAMA", "ID_CONNECT", "ID_ENERGY", "ID_DOWNLOAD"
		};
		public List<TranslateRow> Rows = new List<TranslateRow>();

		public static Translate Instance
		{
			get { return NestedTranslate.instance; }
		}

		private class NestedTranslate
		{
			static NestedTranslate() { }
			internal static readonly Translate instance = new Translate();
		}

		private Translate()
		{
			Rows.Add( new TranslateRow("Polish",
														"English",
														"French",
														"German",
														"Spain",
														"Brasilian Portuguese"));
			Rows.Add( new TranslateRow("angielski",
														"English",
														"Anglais",
														"Englisch",
														"Ingl\u00e9s",
														"Ingl\u00eas"));
			Rows.Add( new TranslateRow("francuski",
														"French",
														"Fran\u00e7ais",
														"Franz\u00f6sisch",
														"Franc\u00e9s",
														"Franc\u00eas"));
			Rows.Add( new TranslateRow("niemiecki",
														"German",
														"Allemand",
														"Deutsch",
														"Alem\u00e1n",
														"Alem\u00e3o"));
			Rows.Add( new TranslateRow("polski",
														"Polish",
														"Polonais",
														"Polnisch",
														"Polaco",
														"Polon\u00eas"));
			Rows.Add( new TranslateRow("",
														"",
														"",
														"",
														"",
														""));
			Rows.Add( new TranslateRow("D\u017awi\u0119k",
														"Sound",
														"Son",
														"Ton",
														"Sonido",
														"Som"));
			Rows.Add( new TranslateRow("Muzyka",
														"Music",
														"Musique",
														"Musik",
														"M\u00fasica",
														"M\u00fasica"));
			Rows.Add( new TranslateRow("Opis",
														"Description",
														"Description",
														"Beschreibung",
														"Descripci\u00f3n",
														"Descri\u00e7\u00e3o"));
			Rows.Add( new TranslateRow("Przeciwnik",
														"Enemy",
														"Ennemi",
														"Gegner",
														"Enemigo",
														"Inimigo"));
			Rows.Add( new TranslateRow("Zmie\u0144 j\u0119zyk",
														"Change language",
														"Changer langue",
														"Sprache \u00e4ndern",
														"Cambiar idioma",
														"Alterar idioma"));
			Rows.Add( new TranslateRow("",
														"",
														"",
														"",
														"",
														""));
			Rows.Add( new TranslateRow("",
														"",
														"",
														"",
														"",
														""));
			Rows.Add( new TranslateRow("",
														"",
														"",
														"",
														"",
														""));
			Rows.Add( new TranslateRow("",
														"",
														"",
														"",
														"",
														""));
			Rows.Add( new TranslateRow("Tapnij na zaznaczone pole, by otworzy\u0107 menu budowania!",
														"Tap the marked field to open the building menu!",
														"Touche la grille pour ouvrir le menu de construction\u00a0!",
														"Tippe auf das Feld, um das Bau-Men\u00fc zu \u00f6ffnen.",
														"\u00a1Toca la parrilla para abrir el men\u00fa de construcci\u00f3n!",
														"Toque na grade para abrir o menu de constru\u00e7\u00e3o!"));
			Rows.Add( new TranslateRow("\u015awietnie! Teraz zbuduj wie\u017cyczk\u0119!",
														"Great! Now build the Forest Idol!",
														"Super\u00a0! B\u00e2tis \u00e0 pr\u00e9sent l'Idole sylvestre\u00a0!",
														"Sehr gut! Jetzt baue das Wald-Idol!",
														"\u00a1Genial! \u00a1Ahora debes construir el \u00cddolo del bosque!",
														"\u00d3timo! Agora construa o \u00cddolo da Floresta!"));
			Rows.Add( new TranslateRow("Wie\u017cyczki mo\u017cna ulepsza\u0107. Ulepsz totem ziemi!",
														"Each tower can be improved. Upgrade the Forest Idol now!",
														"Chaque tour peut \u00eatre am\u00e9lior\u00e9e. Am\u00e9liore l'Idole sylvestre maintenant\u00a0!",
														"Jeder Turm kann upgegradet werden. Verbessere das Wald-Idol jetzt!",
														"Las torres se pueden actualizar. \u00a1Ahora puedes mejorar el \u00cddolo del bosque!",
														"\u00c9 poss\u00edvel aprimorar cada torre. Melhore o \u00cddolo da Floresta agora!"));
			Rows.Add( new TranslateRow("Nadci\u0105gaj\u0105 wrogowie. Bro\u0144 si\u0119!",
														"Enemies are coming! Defend the island!",
														"Des vagues d'ennemis sont en approche ! Tu dois d\u00e9fendre l'\u00eele !",
														"Horden von Gegnern im Anmarsch! Verteidige die Insel!",
														"\u00a1Se acercan oleadas de enemigos! \u00a1Defiende la isla!",
														"Est\u00e3o vindo multid\u00f5es de inimigos! Defenda a ilha!"));
			Rows.Add( new TranslateRow("Niszcz przeszkody, by zdobywa\u0107 surowce i zwalnia\u0107 miejsce na wie\u017cyczki!",
														"Tap on the obstacles to target them. Destroy the obstacles to earn more coins!",
														"Touche les obstacles pour les cibler. D\u00e9truis-les pour gagner des pi\u00e8ces en plus !",
														"Tippe auf die Hindernisse, um sie anzuvisieren. Zerst\u00f6re die Hindernisse, um mehr M\u00fcnzen zu sammeln!",
														"\u00a1Toca los obst\u00e1culos para centrarte en ellos! \u00a1Destruye los obst\u00e1culos para ganar m\u00e1s monedas!",
														"Toque nos obst\u00e1culos para fazer deles alvo. Destrua os obst\u00e1culos para ganhar mais moedas!"));
			Rows.Add( new TranslateRow("Powr\u00f3t",
														"Back",
														"Retour",
														"Zur\u00fcck",
														"Atr\u00e1s",
														"Voltar"));
			Rows.Add( new TranslateRow("Przegrana",
														"Defeat!",
														"D\u00e9faite !",
														"Niederlage!",
														"\u00a1Derrota!",
														"Derrota!"));
			Rows.Add( new TranslateRow("Jeste\u015b zwyci\u0119zc\u0105!",
														"Victory!",
														"Victoire !",
														"Sieg!",
														"\u00a1Victoria!",
														"Vit\u00f3ria!"));
			Rows.Add( new TranslateRow("IDOL LASU",
														"FOREST IDOL",
														"IDOLE SYLVESTRE",
														"WALD-IDOL",
														"\u00cdDOLO DEL BOSQUE",
														"\u00cdDOLO DA FLORESTA"));
			Rows.Add( new TranslateRow("Trafia jednego przeciwnika energi\u0105 natury.",
														"Strikes the enemies with force of nature.",
														"Frappe les ennemis avec la force de la nature.",
														"Schl\u00e4gt die Gegner mit der Kraft der Natur.",
														"Ataca a los enemigos con la fuerza de la naturaleza.",
														"Ataca os inimigos com a for\u00e7a da natureza."));
			Rows.Add( new TranslateRow("MIKSTURA ALCHEMICZNA",
														"ALCHEMICAL POTION",
														"POTION ALCHIMIQUE",
														"ALCHEMISTISCHER TRANK",
														"POCI\u00d3N DE ALQUIMIA",
														"PO\u00c7\u00c3O ALQU\u00cdMICA"));
			Rows.Add( new TranslateRow("Spowalnia trafionego przeciwnika",
														"Slows down the enemies with its poisonous liquid.",
														"Liquide toxique ralentissant les ennemis.",
														"Verlangsamt die Feinde mit giftiger Fl\u00fcssigkeit.",
														"Ralentiza a los enemigos con su fluido venenoso.",
														"Abranda os inimigos com seu l\u00edquido venenoso."));
			Rows.Add( new TranslateRow("OGNISKO",
														"CAMPFIRE",
														"FEU DE CAMP",
														"LAGERFEUER",
														"HOGUERA DEL CAMPAMENTO",
														"FOGUEIRA"));
			Rows.Add( new TranslateRow("Uderza w cel i w obszar wok\u00f3\u0142 trafionego celu.",
														"Hits the area around a target with destructive firepower.",
														"Frappe la zone autour de la cible avec une puissance de feu destructrice.",
														"Trifft den Bereich um das Ziel herum mit zerst\u00f6rerischer Feuerkraft.",
														"Impacta contra la zona de alrededor del objetivo con fuego destructor.",
														"Atinge a \u00e1rea em volta do alvo com pot\u00eancia de fogo destrutiva."));
			Rows.Add( new TranslateRow("CHMURA BURZOWA",
														"THUNDERCLOUD",
														"CUMULONIMBUS",
														"GEWITTERWOLKE",
														"NUBE REL\u00c1MPAGO",
														"NUVENS DE TROVOADA"));
			Rows.Add( new TranslateRow("Zsy\u0142a tornado, kt\u00f3re nie zatrzymuje si\u0119 na swojej drodze.",
														"Sends an unstoppable tornado, wreaking havoc until it leaves the board.",
														"D\u00e9clenche une redoutable tornade d\u00e9truisant tout sur le plateau avant de dispara\u00eetre.",
														"Sendet einen unaufhaltsamen Tornado, der alles verw\u00fcstet, bis er das Spielfeld verl\u00e4sst.",
														"Env\u00eda un tornado imparable, creando el caos hasta que abandona el tablero.",
														"Envia um tornado impar\u00e1vel, causando devasta\u00e7\u00e3o at\u00e9 sair do tabuleiro."));
			Rows.Add( new TranslateRow("MAGICZNA R\u00d3\u017bD\u017bKA",
														"MAGIC WAND",
														"BAGUETTE MAGIQUE",
														"ZAUBERSTAB",
														"VARITA M\u00c1GICA",
														"VARINHA M\u00c1GICA"));
			Rows.Add( new TranslateRow("Uderza w cel i wszystko inne, co mi\u0119dzy ni\u0105, a celem.",
														"Hits a target and everything in the line between the tower and a target.",
														"Frappe une cible et tout ce qui se trouve dans la ligne de mire depuis la tour.",
														"Trifft ein Ziel und alles in der Linie zwischen dem Turm und dem Ziel.",
														"Impacta contra un objetivo y todo lo que tenga en l\u00ednea entre la torre y un objetivo.",
														"Atinge um alvo e tudo na linha entre a torre e um alvo."));
			Rows.Add( new TranslateRow("LODOWY PIER\u015aCIE\u0143",
														"ICE RING",
														"ANNEAU DE GLACE",
														"EISRING",
														"ANILLO DE HIELO",
														"ANEL DE GELO"));
			Rows.Add( new TranslateRow("Spowalnia cel i przeciwnik\u00f3w na obszarze wok\u00f3\u0142 celu.",
														"Slows down the enemy and the other enemies near the target.",
														"Ralentit l'ennemi et les adversaires se trouvant \u00e0 proximit\u00e9 de la cible.",
														"Verlangsamt das Ziel und alle anderen Gegner im Umkreis.",
														"Ralentiza al enemigo al que atacas y a los que est\u00e9n cerca.",
														"Abranda o inimigo e os outros inimigos perto do alvo."));
			Rows.Add( new TranslateRow("UTALENTOWANY TAMTAM",
														"TALENTED TAMTAM",
														"TAM-TAM DE TALENT",
														"TALENTIERTES TAMTAM",
														"TAM-TAM TALENTOSO",
														"TANT\u00c3 TALENTOSO"));
			Rows.Add( new TranslateRow("Gra tak pi\u0119knie, \u017ce wrogowie obok zatrzymuj\u0105 si\u0119 na chwil\u0119, by pos\u0142ucha\u0107.",
														"Its sound stuns the nearby foes for a short while.",
														"Le son du tam-tam envo\u00fbte et \u00e9tourdit les ennemis \u00e0 proximit\u00e9 pendant un court instant.",
														"Sein Klang fasziniert die Zuh\u00f6rer und bet\u00e4ubt die Gegner in der N\u00e4he f\u00fcr eine kurze Weile.",
														"Su sonido fascina a quien lo escucha, atontando a los enemigos cercanos durante un tiempo.",
														"Seu som fascina os ouvintes, atordoando os advers\u00e1rios pr\u00f3ximos por um momento."));
			Rows.Add( new TranslateRow("",
														"",
														"",
														"",
														"",
														""));
			Rows.Add( new TranslateRow("",
														"",
														"",
														"",
														"",
														""));
			Rows.Add( new TranslateRow("KOCIO\u0141EK",
														"VOODOO CAULDRON",
														"CHAUDRON VAUDOU",
														"VOODOO-KESSEL",
														"CALDERO VUD\u00da",
														"CALDEIR\u00c3O DE VODU"));
			Rows.Add( new TranslateRow("Zaraza zgotowana przeciwnikom. Przeskakuje z jednego na drugiego.\n",
														"Infects the target with the plague which jumps between several enemies.",
														"La cible est infect\u00e9e par le fl\u00e9au qui contamine ensuite d'autres ennemis.",
														"Infiziert das Ziel mit der Pest, die zwischen mehreren Gegnern hin- und herspringt.",
														"Infecta al objetivo con la plaga que salta entre varios enemigos.",
														"Infeta o alvo com a praga que salta entre v\u00e1rios inimigos."));
			Rows.Add( new TranslateRow("CI\u0118\u017bKIE KAMOLE",
														"BIG ANCIENT STONES",
														"PIERRES ANTIQUES",
														"GROSSE ALTE STEINE",
														"ENORMES PIEDRAS MILENARIAS",
														"GRANDES PEDRAS SECULARES"));
			Rows.Add( new TranslateRow("Uderzaj\u0105 w cele znajduj\u0105ce si\u0119 obok.",
														"Emit a deadly sonic wave which causes area damage.",
														"\u00c9mettent une onde sonore mortelle infligeant des d\u00e9g\u00e2ts de zone.",
														"Sendet eine t\u00f6dliche Schallwelle, die Fl\u00e4chenschaden verursacht.",
														"Emiten una mort\u00edfera onda expansiva que causa da\u00f1os en toda la zona.",
														"Emite uma onda s\u00f4nica mort\u00edfera que causa danos."));
			Rows.Add( new TranslateRow("KWASOWA JASZCZURKA",
														"ACIDIC LIZARD",
														"L\u00c9ZARD ACIDE",
														"S\u00c4UREHALTIGE EIDECHSE",
														"LAGARTO \u00c1CIDO",
														"LAGARTO \u00c1CIDO"));
			Rows.Add( new TranslateRow("Pluje substancj\u0105, kt\u00f3ra topi pancerz przeciwnika.",
														"Spits the substance which melts armour of the target.",
														"Crache une substance capable de faire fondre l'armure de la cible.",
														"Spuckt eine Substanz, die die R\u00fcstung des Ziels schmelzen l\u00e4sst.",
														"Escupe una sustancia que hace que se derrita la armadura del objetivo.",
														"Cospe a subst\u00e2ncia que derrete a armadura do alvo."));
			Rows.Add( new TranslateRow("NASZYJNIK",
														"NECKLACE",
														"COLLIER",
														"HALSKETTE",
														"COLLAR",
														"COLAR"));
			Rows.Add( new TranslateRow("Pociski z ko\u015bci s\u0142oniowej, kt\u00f3re strzelaj\u0105 w kilka stron naraz.",
														"Ivory bullets which strike in multiple directions at once.",
														"Salve de balles d'ivoire projet\u00e9es dans plusieurs directions.",
														"Elfenbeinkugeln, die in verschiedene Richtungen gleichzeitig geschossen werden.",
														"Bala de marfil que se disparan en diferentes direcciones simult\u00e1neamente.",
														"Balas de marfim que atacam em diferentes dire\u00e7\u00f5es de uma vez."));
			Rows.Add( new TranslateRow("FLETNIA",
														"PANFLUTE",
														"FL\u00dbTE DE PAN",
														"PANFL\u00d6TE",
														"ZAMPO\u00d1A",
														"FLAUTA DE P\u00c3"));
			Rows.Add( new TranslateRow("Ze sk\u0142onno\u015bci\u0105 do d\u0142ugich sol\u00f3wek. Im d\u0142u\u017cej strzela w jeden cel, tym silniej uderza.\n",
														"Deadly tribal rifle. The longer it shoots the more damage it causes. ",
														"Redoutable fusil tribal dont les d\u00e9g\u00e2ts augmentent proportionnellement \u00e0 la dur\u00e9e de tir.",
														"T\u00f6dliches Stammesgewehr: je l\u00e4nger es schie\u00dft, desto gr\u00f6\u00dfer der Schaden.",
														"Un mort\u00edfero rifle tribal que causa m\u00e1s da\u00f1os si se dispara durante m\u00e1s tiempo.",
														"Rifle tribal mort\u00edfera que causa maiores danos quanto mais longe disparar."));
			Rows.Add( new TranslateRow("KOWAD\u0141O",
														"ANVIL",
														"ENCLUME",
														"AMBOSS",
														"YUNQUE",
														"BIGORNA"));
			Rows.Add( new TranslateRow("Niszczy pancerz wrog\u00f3w znajduj\u0105cych si\u0119 w pobli\u017cu kowad\u0142a.",
														"Destroys an armour of the enemies around it.",
														"D\u00e9truit l'armure des ennemis se trouvant \u00e0 proximit\u00e9.",
														"Zerst\u00f6rt die R\u00fcstung der Feinde um ihn herum.",
														"Destruye la armadura de los enemigos que tiene cerca.",
														"Destr\u00f3i a armadura dos inimigos \u00e0 sua volta."));
			Rows.Add( new TranslateRow("KAMIE\u0143 RUNICZNY",
														"RUNESTONE",
														"RUNE",
														"RUNENSTEIN",
														"RUNA",
														"PEDRA R\u00daNICA"));
			Rows.Add( new TranslateRow("Dzia\u0142a inaczej na ka\u017cdym poziomie rozbudowy. Odkryj sam!",
														"Each level of the upgrade adds a whole new feature to this turret. Discover by yourself!",
														"Chaque niveau d'am\u00e9lioration ajoute une nouvelle fonctionnalit\u00e9 \u00e0 cette tourelle. D\u00e9couvre-les par toi-m\u00eame\u00a0!",
														"Jedes Upgrade-Level verleiht diesem Gesch\u00fctzturm eine neue Funktion. Sieh selbst!",
														"Cada nivel de la mejora a\u00f1ade una nueva funci\u00f3n a esta torreta. \u00a1Desc\u00fabrelo t\u00fa mismo!\n",
														"Cada n\u00edvel da aprimoramento adiciona uma caracter\u00edstica completamente nova a esta torre pequena. Descubra voc\u00ea mesmo!\n"));
			Rows.Add( new TranslateRow("",
														"",
														"",
														"",
														"",
														""));
			Rows.Add( new TranslateRow("",
														"",
														"",
														"",
														"",
														""));
			Rows.Add( new TranslateRow("TIKI MASKA OGNIA",
														"TIKI MASK OF FIRE",
														"MASQUE TIKI DE FEU",
														"TIKI-FEUERMASKE",
														"M\u00c1SCARA DE FUEGO TIKI",
														"M\u00c1SCARA TIKI DO FOGO"));
			Rows.Add( new TranslateRow("Chwilowo zwi\u0119ksza si\u0142\u0119 uderzeniow\u0105 wie\u017cyczki.",
														"Increases a tower\u2019s damage for a short period of time.",
														"Augmente les d\u00e9g\u00e2ts inflig\u00e9s par une tour pendant un court instant.",
														"Erh\u00f6ht den Schaden eines Turms f\u00fcr einen kurzen Zeitraum.",
														"Aumenta el da\u00f1o que provoca la torre durante un breve periodo de tiempo.\n",
														"Aumenta o dano de uma torre por um curto per\u00edodo de tempo.\n"));
			Rows.Add( new TranslateRow("TIKI MASKA LODU",
														"TIKI MASK OF ICE",
														"MASQUE TIKI DE GLACE",
														"TIKI-EISMASKE",
														"M\u00c1SCARA DE HIELO TIKI",
														"M\u00c1SCARA TIKI DO GELO"));
			Rows.Add( new TranslateRow("Sprawia, \u017ce nast\u0119pny strza\u0142 wie\u017cyczki dodatkowo spowolni wroga.",
														"Adds a slow effect to the next shot of the selected tower.",
														"Conf\u00e8re un effet de ralentissement au prochain tir de la tour s\u00e9lectionn\u00e9e.",
														"Verleiht dem n\u00e4chsten Schuss des ausgew\u00e4hlten Turms eine verlangsamende Wirkung.",
														"A\u00f1ade el efecto de ralentizaci\u00f3n al siguiente disparo a la torre seleccionada.",
														"Adiciona um efeito de abrandamento ao ataque da torre selecionada."));
			Rows.Add( new TranslateRow("TIKI MASKA ZIEMI",
														"TIKI MASK OF EARTH",
														"MASQUE TIKI DE TERRE",
														"TIKI-ERDMASKE",
														"M\u00c1SCARA DE TIERRA TIKI",
														"M\u00c1SCARA TIKI DA TERRA"));
			Rows.Add( new TranslateRow("Sprawia, \u017ce nast\u0119pny strza\u0142 wie\u017cyczki dodatkowo zatrzyma wroga.",
														"Adds a stun effect to the next shot of the selected tower.",
														"Conf\u00e8re un effet d'\u00e9tourdissement au prochain tir de la tour s\u00e9lectionn\u00e9e.",
														"Verleiht dem n\u00e4chsten Schuss des ausgew\u00e4hlten Turms eine bet\u00e4ubende Wirkung.",
														"A\u00f1ade el efecto de aturdir al siguiente disparo a la torre seleccionada.",
														"Adiciona um efeito de atordoamento ao ataque da torre selecionada."));
			Rows.Add( new TranslateRow("TIKI MASKA POWIETRZA",
														"TIKI MASK OF AIR",
														"MASQUE TIKI D'AIR",
														"TIKI-LUFTMASKE",
														"M\u00c1SCARA DE AIRE TIKI",
														"M\u00c1SCARA TIKI DO AR"));
			Rows.Add( new TranslateRow("Chwilowo zwi\u0119ksza szybko\u015b\u0107 strza\u0142u wie\u017cyczki.",
														"Increases a tower\u2019s speed for a short period of time.",
														"Augmente la puissance d'une tour pendant un court instant.",
														"Erh\u00f6ht die Geschwindigkeit eines Turms f\u00fcr einen kurzen Zeitraum.",
														"Aumenta la velocidad de la torre durante un breve periodo de tiempo.\n",
														"Aumenta a velocidade de uma torre por um curto per\u00edodo de tempo.\n"));
			Rows.Add( new TranslateRow("\nA teraz wa\u017cna lekcja. Niekt\u00f3rzy przeciwnicy nosz\u0105 ze sob\u0105 co\u015b cennego!",
														"And now, an important lesson. A certain inhabitant of the island drops elemental essences!",
														"Tu dois savoir qu'un certain habitant de l'\u00eele laisse tomber des essences \u00e9l\u00e9mentaires\u00a0!",
														"Und jetzt eine wichtige Lektion. Ein gewisser Bewohner der Insel l\u00e4sst elementare Essenzen fallen!",
														"Y ahora, una importante lecci\u00f3n. \u00a1Cierto habitante de la isla deja caer esencias elementales!",
														"E agora, uma li\u00e7\u00e3o importante. Um certo habitante da ilha deixa cair ess\u00eancias elementares!"));
			Rows.Add( new TranslateRow("Podnie\u015b esencj\u0119 magiczn\u0105!",
														"Tap the essence to collect it!",
														"Touche l'essence pour la ramasser\u00a0!",
														"Tippe auf die Essenz, um sie einzusammeln!",
														"\u00a1Toca la esencia para recogerla!",
														"Toque na ess\u00eancia para a coletar!"));
			Rows.Add( new TranslateRow("Inni przeciwnicy s\u0105 tak mocni, \u017ce przyda Ci si\u0119 dodatkowe wsparcie!",
														"Meanwhile, some opponents are so strong that you could use extra support!",
														"Et certains adversaires sont si puissants qu'il te faudra un peu d'aide !",
														"In der Zwischenzeit sind einige Gegner so stark geworden, dass du zus\u00e4tzliche Unterst\u00fctzung gebrauchen k\u00f6nntest!",
														"Mientras tanto, algunos oponentes son tan fuertes que necesitar\u00e1s algo de ayuda contra ellos.",
														"Entretanto, alguns advers\u00e1rios s\u00e3o t\u00e3o fortes que voc\u00ea podia beneficiar de um apoio extra!"));
			Rows.Add( new TranslateRow("Tapnij tiki mask\u0119, by przywo\u0142a\u0107 energi\u0119 \u017cywio\u0142u!",
														"Tap the tiki mask, to call down the elemental force!",
														"Touche le masque tiki pour invoquer la force \u00e9l\u00e9mentaire !",
														"Tippe auf die Tiki-Maske, um die elementare Kraft herbeizurufen!",
														"\u00a1Toca la m\u00e1scara tiki para invocar la fuerza de los elementos!",
														"Toque na m\u00e1scara tiki para invocar a for\u00e7a elementar!"));
			Rows.Add( new TranslateRow("\u015awietnie! A teraz tapnij wie\u017cyczk\u0119!",
														"Great! And now unleash it by tapping the Forest Idol!",
														"Super ! Lib\u00e8re-la \u00e0 pr\u00e9sent en touchant l'Idole sylvestre !",
														"Gut Gemacht! Und nun setze sie durch Antippen des Wald-Idols frei!",
														"\u00a1Genial! \u00a1Y ahora des\u00e1tala tocando el \u00cddolo del bosque!",
														"\u00d3timo! E agora libera-a tocando no \u00cddolo da Floresta!"));
			Rows.Add( new TranslateRow("Oho, nadchodzi kolejny mocny przeciwnik! Potrzebujesz kolejnych esencji, dzi\u0119ki kt\u00f3rym wezwiesz \u017cywio\u0142y. Tapnij plusik.",
														"To use the tiki masks, you need elemental essences. Let's get an additional one. Tap the \"+\" button.",
														"Pour utiliser les masques tiki, des essences \u00e9l\u00e9mentaires sont n\u00e9cessaires. Obtiens-en une suppl\u00e9mentaire. Touche le bouton \"+\".",
														"Um die Tiki-Masken zu verwenden, brauchst du elementare Essenzen. Lass uns noch eine holen. Tippe auf die Schaltfl\u00e4che \"+\".",
														"Para usar las m\u00e1scaras tiki necesitas esencias elementales. Vamos a conseguir una extra. Toca el bot\u00f3n \"+\".",
														"Para usar as m\u00e1scaras tiki, voc\u00ea precisa de ess\u00eancias elementares. Obtenhamos mais uma. Toque no bot\u00e3o \"+\"."));
			Rows.Add( new TranslateRow("A teraz zdob\u0105d\u017a esencj\u0119!",
														"Now acquire the essence!",
														"Prends l'essence\u00a0!",
														"Jetzt hol dir die Essenz!",
														"\u00a1Ahora coge la esencia!",
														"Agora adquira a ess\u00eancia!"));
			Rows.Add( new TranslateRow("Doskonale. U\u017cyj tiki maski jeszcze raz i pokonaj przeciwnika!",
														"Well done. Now use the tiki mask again and defeat the enemy!",
														"Bien jou\u00e9. Sers-toi \u00e0 nouveau du masque tiki pour terrasser l'ennemi\u00a0!",
														"Bravo! Verwende nun die Tiki-Maske erneut und besiege den Feind!",
														"Bien hecho. \u00a1Ahora usa la m\u00e1scara tiki otra vez y derrota al enemigo!",
														"Muito bem. Agora use a m\u00e1scara tiki de novo e derrote o inimigo!"));
			Rows.Add( new TranslateRow("Korzystaj z masek w krytycznych momentach podczas gry. U\u017cywaj ich m\u0105drze!",
														"Invoke the elements with the Masks in critical moments. Spending the essences wisely is the key to success!",
														"En cas de probl\u00e8me, invoque les \u00e9l\u00e9ments \u00e0 l'aide des masques. D\u00e9penser ses essences judicieusement est la cl\u00e9 du succ\u00e8s\u00a0!",
														"Rufe die Elemente mit den Masken in kritischen Momenten herbei. Die Essenzen mit Bedacht zu verbrauchen ist der Schl\u00fcssel zum Erfolg!",
														"Invoca a los elementos con las m\u00e1scaras en los momentos clave. \u00a1Saber administrar las esencias es clave para triunfar!",
														"Invoque os elementos com as M\u00e1scaras em momentos cr\u00edticos. Usar as ess\u00eancias sensatamente \u00e9 a chave para o sucesso!"));
			Rows.Add( new TranslateRow("Dodaje 20 monet na start poziomu.",
														"Adds 20 coins at start of the level.",
														"Ajoute 20 pi\u00e8ces au d\u00e9but du niveau.",
														"F\u00fcgt 20 M\u00fcnzen zu Beginn des Levels hinzu.",
														"A\u00f1ade 20 monedas al comenzar el nivel.",
														"Adiciona 20\u00a0moedas no in\u00edcio do n\u00edvel."));
			Rows.Add( new TranslateRow("Zwi\u0119ksza czas zatrzymania wrog\u00f3w przez Tiki Mask\u0119 Ziemi.",
														"Increases the duration of the stun by Tiki Mask of Earth.",
														"Augmente la dur\u00e9e de l'\u00e9tourdissement du Masque tiki de Terre.",
														"Erh\u00f6ht die Dauer der Bet\u00e4ubung durch die Tiki-Erdmaske.",
														"Aumenta la duraci\u00f3n de la conmoci\u00f3n por la M\u00e1scara Tiki de la Tierra.",
														"Aumenta a dura\u00e7\u00e3o do atordoamento com a M\u00e1scara Tiki da Terra."));
			Rows.Add( new TranslateRow("Zwi\u0119ksza czas spowolnienia wrog\u00f3w przez Tiki Mask\u0119 Lodu.",
														"Increases the duration of the slow by Tiki Mask of Ice.",
														"Augmente la dur\u00e9e de ralentissement du Masque tiki de Glace.",
														"Erh\u00f6ht die Dauer der Verlangsamung durch die Tiki-Eismaske.",
														"Aumenta la duraci\u00f3n de la ralentizaci\u00f3n por la M\u00e1scara Tiki del Hielo.",
														"Aumenta a dura\u00e7\u00e3o do abrandamento com a M\u00e1scara Tiki do Gelo."));
			Rows.Add( new TranslateRow("Dodaje 30 monet na start poziomu.",
														"Adds 30 more coins at start of the level.",
														"Ajoute 30 pi\u00e8ces au d\u00e9but du niveau.",
														"F\u00fcgt 30 M\u00fcnzen zu Beginn des Levels hinzu.",
														"A\u00f1ade 30 monedas m\u00e1s al comenzar el nivel.",
														"Adiciona mais 30\u00a0moedas no in\u00edcio do n\u00edvel."));
			Rows.Add( new TranslateRow("Uczy szamank\u0119 czaru ochronnego, kt\u00f3ry chroni j\u0105 przed atakiem jednego wroga.",
														"Teaches shaman a defensive spell which she casts upon low health.",
														"Permet \u00e0 la chamane de lancer un sort d\u00e9fensif lorsque son niveau de sant\u00e9 est bas.",
														"Bringt der Schamanin einen Verteidigungszauber bei, den sie bei schlechter Gesundheit anwendet.",
														"Ense\u00f1a al cham\u00e1n un hechizo defensivo que puede lanzar cuando tiene poca salud.",
														"Ensina um feiti\u00e7o defensivo \u00e0 xam\u00e3 que ela lan\u00e7ar\u00e1 quando estiver com pouca energia."));
			Rows.Add( new TranslateRow("Ostatni fragment totemu. Wymagany do wej\u015bcia do wulkanu.\n",
														"The final piece of the totem. Required to enter the volcano.",
														"La derni\u00e8re pi\u00e8ce du totem. N\u00e9cessaire pour p\u00e9n\u00e9trer dans le volcan.",
														"Das letzte St\u00fcck des Totems. Erforderlich, um den Vulkan zu betreten.",
														"La pieza final del t\u00f3tem. Es necesaria para entrar en el volc\u00e1n.",
														"A pe\u00e7a final do totem. Obrigat\u00f3ria para entrar no vulc\u00e3o."));
			Rows.Add( new TranslateRow("Zwi\u0119ksza ceny sprzeda\u017cy wie\u017cyczek.",
														"Increases towers' sell price.",
														"Augmente le prix de vente des tours.",
														"Erh\u00f6ht den Verkaufspreis der T\u00fcrme.",
														"Aumenta el precio de venta de la torre.",
														"Aumenta o pre\u00e7o de venda das torres."));
			Rows.Add( new TranslateRow("Zwi\u0119ksza bonus szybko\u015bci Tiki Maski Powietrza.",
														"Gives Tiki Mask of Air even more haste.",
														"Octroie une vitesse accrue au Masque tiki d'Air.",
														"Verleiht der Tiki-Luftmaske noch mehr Eile.",
														"Proporciona a la M\u00e1scara Tiki del Aire a\u00fan m\u00e1s poder.",
														"D\u00e1 \u00e0 M\u00e1scara Tiki do Ar ainda mais rapidez."));
			Rows.Add( new TranslateRow("Dodaje czwarty pojemnik na esencj\u0119 magiczn\u0105.",
														"Adds additional essence slot.",
														"Ajoute un 4e emplacement d'essence.",
														"F\u00fcgt 4. Slot f\u00fcr Essenzen hinzu.",
														"A\u00f1ade 4 ranuras de esencia.",
														"Adiciona o 4.\u00ba entalhe de ess\u00eancia."));
			Rows.Add( new TranslateRow("Zwi\u0119ksza bonus si\u0142y Tiki Maski Ognia.",
														"Gives Tiki Mask of Fire even bigger damage bonus.",
														"Octroie un bonus de d\u00e9g\u00e2ts accru au Masque tiki de Feu.",
														"Verleiht der Tiki-Feuermaske einen noch gr\u00f6\u00dferen Schadensbonus.",
														"Proporciona a la M\u00e1scara Tiki del Fuego a\u00fan m\u00e1s poder destructivo.",
														"D\u00e1 \u00e0 M\u00e1scara Tiki do Fogo um b\u00f4nus de dano ainda maior."));
			Rows.Add( new TranslateRow("Ostatni fragment totemu. Wymagany do wej\u015bcia do wulkanu.\n",
														"The final piece of the totem. Required to enter the volcano.",
														"La derni\u00e8re pi\u00e8ce du totem. N\u00e9cessaire pour p\u00e9n\u00e9trer dans le volcan.",
														"Das letzte St\u00fcck des Totems. Erforderlich, um den Vulkan zu betreten.",
														"La pieza final del t\u00f3tem. Es necesaria para entrar en el volc\u00e1n.",
														"A pe\u00e7a final do totem. Obrigat\u00f3ria para entrar no vulc\u00e3o."));
			Rows.Add( new TranslateRow("Dodaje jedn\u0105 esencj\u0119 na start poziomu.",
														"Provides one elemental essence at the start of each level.",
														"Conf\u00e8re une essence \u00e9l\u00e9mentaire au d\u00e9part.",
														"F\u00fcgt eine elementare Essenz beim Levelstart hinzu.",
														"Proporciona una esencia elementar al comenzar.",
														"Fornece uma ess\u00eancia elementar no in\u00edcio."));
			Rows.Add( new TranslateRow("Potrzebujesz drewna do zbudowania totemu.",
														"You need more wood to build a totem.",
														"Il te faut plus de bois pour b\u00e2tir le totem.",
														"Du brauchst mehr Holz, um ein Totem zu bauen.",
														"Necesitas m\u00e1s madera para construir un t\u00f3tem.",
														"Voc\u00ea precisa de mais madeira para construir um totem."));
			Rows.Add( new TranslateRow("Tiki maski",
														"Tiki Masks",
														"Masques tiki",
														"Tiki-Masken",
														"M\u00e1scaras Tiki",
														"M\u00e1scaras Tiki"));
			Rows.Add( new TranslateRow("Wie\u017cyczki",
														"Towers & Masks",
														"Tours et masques",
														"T\u00fcrme & Masken",
														"Torres y M\u00e1scaras",
														"Torres e M\u00e1scaras"));
			Rows.Add( new TranslateRow("Nowa wie\u017cyczka!",
														"New Tower!",
														"Nouvelle tour !",
														"Neuer Turm!",
														"\u00a1Nueva torre!",
														"Nova Torre!"));
			Rows.Add( new TranslateRow("Nadbrze\u017ce",
														"Shoreline",
														"Littoral",
														"Strand",
														"Orilla",
														"Litoral"));
			Rows.Add( new TranslateRow("Dzika d\u017cungla",
														"Wild Jungle",
														"Jungle sauvage",
														"Wilder Dschungel",
														"Jungla salvaje",
														"Selva Selvagem"));
			Rows.Add( new TranslateRow("Wioska szaman\u00f3w",
														"Shamans' Village",
														"Village des chamanes",
														"Schamanen-Dorf",
														"Poblado del cham\u00e1n",
														"Vila dos Xam\u00e3s"));
			Rows.Add( new TranslateRow("Wzg\u00f3rza duch\u00f3w",
														"Ghostly Hills",
														"Collines spectrales",
														"Geisterh\u00fcgel",
														"Colinas fantasmales",
														"Colinas Fantasmag\u00f3ricas"));
			Rows.Add( new TranslateRow("Staro\u017cytny krater",
														"Ancient Volcano",
														"Volcan antique",
														"Alter Vulkan",
														"Volc\u00e1n milenario",
														"Vulc\u00e3o Secular"));
			Rows.Add( new TranslateRow("lub",
														"or",
														"ou",
														"oder",
														"o",
														"ou"));
			Rows.Add( new TranslateRow("Wybierz profil:",
														"Select Profile:",
														"Choisir profil\u00a0:",
														"Profil w\u00e4hlen:",
														"Elije un perfil:",
														"Selecionar perfil:"));
			Rows.Add( new TranslateRow("Nowa Tiki Maska!",
														"New Tiki Mask!",
														"Nouveau masque tiki\u00a0!",
														"Neue Tiki-Maske!",
														"\u00a1Nueva m\u00e1scara tiki!",
														"Nova M\u00e1scara Tiki!"));
			Rows.Add( new TranslateRow("X rubin\u00f3w",
														"X gems",
														"X gemmes",
														"X Edelsteine",
														"X gemas",
														"X gemas"));
			Rows.Add( new TranslateRow("X rubin\u00f3w",
														"X gems",
														"X gemmes",
														"X Edelsteine",
														"X gemas",
														"X gemas"));
			Rows.Add( new TranslateRow("X rubin\u00f3w",
														"X gems",
														"X gemmes",
														"X Edelsteine",
														"X gemas",
														"X gemas"));
			Rows.Add( new TranslateRow("X rubin\u00f3w",
														"X gems",
														"X gemmes",
														"X Edelsteine",
														"X gemas",
														"X gemas"));
			Rows.Add( new TranslateRow("X rubin\u00f3w",
														"X gems",
														"X gemmes",
														"X Edelsteine",
														"X gemas",
														"X gemas"));
			Rows.Add( new TranslateRow("X rubin\u00f3w",
														"X gems",
														"X gemmes",
														"X Edelsteine",
														"X gemas",
														"X gemas"));
			Rows.Add( new TranslateRow("X rubin\u00f3w",
														"X gems",
														"X gemmes",
														"X Edelsteine",
														"X gemas",
														"X gemas"));
			Rows.Add( new TranslateRow("X rubin\u00f3w",
														"X gems",
														"X gemmes",
														"X Edelsteine",
														"X gemas",
														"X gemas"));
			Rows.Add( new TranslateRow("",
														"",
														"",
														"",
														"",
														""));
			Rows.Add( new TranslateRow("",
														"",
														"",
														"",
														"",
														""));
			Rows.Add( new TranslateRow("Komplet Masek Tiki",
														"All Tiki Masks",
														"Tous masques tiki",
														"Alle Tiki-Masken",
														"Todas las m\u00e1scaras tiki",
														"Todas as M\u00e1scaras Tiki"));
			Rows.Add( new TranslateRow("",
														"",
														"",
														"",
														"",
														""));
			Rows.Add( new TranslateRow("",
														"",
														"",
														"",
														"",
														""));
			Rows.Add( new TranslateRow("",
														"",
														"",
														"",
														"",
														""));
			Rows.Add( new TranslateRow("",
														"",
														"",
														"",
														"",
														""));
			Rows.Add( new TranslateRow("",
														"",
														"",
														"",
														"",
														""));
			Rows.Add( new TranslateRow("Zdobywasz drewno! Czy chcesz odwiedzi\u0107 Kr\u0105g R\u00f3wnowagi, by zleci\u0107 budow\u0119 totemu?",
														"You receive a pile of wood! Would you like to visit the Circle of Balance and build the totem?",
														"Tu as re\u00e7u une pile de bois ! Veux-tu te rendre au Cercle de l'\u00c9quilibre pour b\u00e2tir le totem\u00a0?",
														"Du bekommst einen Holzstapel! M\u00f6chtest du den Kreis des Gleichgewichts besuchen und das Totem bauen?",
														"\u00a1Has recibido un mont\u00f3n de madera! \u00bfQuieres visitar el C\u00edrculo del Equilibrio y construir el t\u00f3tem?",
														"Voc\u00ea recebeu uma pilha de madeira! Gostaria de visitar o C\u00edrculo de Equil\u00edbrio e construir o totem?"));
			Rows.Add( new TranslateRow("",
														"",
														"",
														"",
														"",
														""));
			Rows.Add( new TranslateRow("",
														"",
														"",
														"",
														"",
														""));
			Rows.Add( new TranslateRow("",
														"",
														"",
														"",
														"",
														""));
			Rows.Add( new TranslateRow("",
														"",
														"",
														"",
														"",
														""));
			Rows.Add( new TranslateRow("",
														"",
														"",
														"",
														"",
														""));
			Rows.Add( new TranslateRow("",
														"",
														"",
														"",
														"",
														""));
			Rows.Add( new TranslateRow("",
														"",
														"",
														"",
														"",
														""));
			Rows.Add( new TranslateRow("",
														"",
														"",
														"",
														"",
														""));
			Rows.Add( new TranslateRow("",
														"",
														"",
														"",
														"",
														""));
			Rows.Add( new TranslateRow("",
														"",
														"",
														"",
														"",
														""));
			Rows.Add( new TranslateRow("",
														"",
														"",
														"",
														"",
														""));
			Rows.Add( new TranslateRow("",
														"",
														"",
														"",
														"",
														""));
			Rows.Add( new TranslateRow("",
														"",
														"",
														"",
														"",
														""));
			Rows.Add( new TranslateRow("",
														"",
														"",
														"",
														"",
														""));
			Rows.Add( new TranslateRow("",
														"",
														"",
														"",
														"",
														""));
			Rows.Add( new TranslateRow("",
														"",
														"",
														"",
														"",
														""));
			Rows.Add( new TranslateRow("",
														"",
														"",
														"",
														"",
														""));
			Rows.Add( new TranslateRow("Kup!",
														"Buy Now!",
														"Acheter\u00a0!",
														"Jetzt kaufen!",
														"\u00a1C\u00f3mpralo ya!",
														"Comprar agora!"));
			Rows.Add( new TranslateRow("Pauza",
														"Pause",
														"Pause",
														"Pause",
														"Pausa",
														"Pausa"));
			Rows.Add( new TranslateRow("Witaj w Kr\u0119gu R\u00f3wnowagi. Dzi\u0119ki tutejszym totemom otrzymasz b\u0142ogos\u0142awie\u0144stwa przodk\u00f3w i otworzysz przej\u015bcie do wulkanu.",
														"Welcome to the Circle of Balance! The totems which you can build here will grant you an entrance to the volcano as well as some permanent buffs.",
														"Bienvenue au Cercle de l'\u00c9quilibre ! Les totems que tu peux b\u00e2tir ici t'octroient des augmentations permanentes et te permettent d'entrer dans le volcan.",
														"Willkommen beim Kreis des Gleichgewichts! Die Totems, die du hier bauen kannst, verleihen dir dauerhafte Booster und erm\u00f6glichen es dir, den Vulkan zu betreten.",
														"\u00a1Bienvenido al C\u00edrculo del Equilibrio! Los t\u00f3tems que puedes construir aqu\u00ed te dar\u00e1n m\u00e1s potenciadores permanentes y te permitir\u00e1n entrar en el volc\u00e1n.",
														"Bem-vindo ao C\u00edrculo de Equil\u00edbrio! Os totens que voc\u00ea pode construir aqui dar-lhe-\u00e3o incrementos permanentes, bem como permitir\u00e3o a entrada no vulc\u00e3o."));
			Rows.Add( new TranslateRow("By zbudowa\u0107 fragment totemu b\u0119dziesz potrzebowa\u0107 drewna. ",
														"To build one block of the totem, you will need a pile of enchanted wood.",
														"Pour b\u00e2tir un bloc de totem, il te faut une Pile de bois enchant\u00e9.",
														"Um einen Totemblock zu bauen, ben\u00f6tigst du einen Stapel Zauberholz.",
														"Para construir un bloque de un t\u00f3tem, necesitar\u00e1s un mont\u00f3n de madera encantada.",
														"Para construir um bloco de totem, voc\u00ea precisa de uma pilha de madeira encantada."));
			Rows.Add( new TranslateRow("Mo\u017cesz budowa\u0107 Totem \u017bycia i Totem \u015amierci naraz. Zastan\u00f3w si\u0119, od czego zacz\u0105\u0107 i postaw pierwszy element!",
														"You can build Totem of Life and Totem of Death at the same time. Choose by yourself! Build one block now!",
														"Tu peux b\u00e2tir le Totem de la Vie et le Totem de la Mort en m\u00eame temps. \u00c0 toi de d\u00e9cider\u00a0! B\u00e2tis un bloc maintenant\u00a0!",
														"Du kannst gleichzeitig das Totem des Lebens und das Totem des Todes ausbauen. Such es dir aus! Erstelle jetzt einen Block!",
														"Puedes construir un T\u00f3tem de Vida y un T\u00f3tem de Muerte al mismo tiempo. \u00a1Elige t\u00fa mismo! \u00a1Construye un bloque ahora!",
														"Voc\u00ea pode construir o Totem da Vida e o Totem da Morte em simult\u00e2neo. Escolha voc\u00ea mesmo! Construa um bloco agora!"));
			Rows.Add( new TranslateRow("\u015awietnie! Tapnij przycisk, by opu\u015bci\u0107 Kr\u0105g R\u00f3wnowagi.",
														"Well done! Tap the button to leave the Circle of Balance.",
														"Bravo\u00a0! Touche le bouton pour quitter le Cercle de l'\u00c9quilibre.",
														"Bravo! Tippe auf die Schaltfl\u00e4che, um den Kreis des Gleichgewichts zu verlassen.",
														"\u00a1Muy bien! Toca el bot\u00f3n para abandonar el C\u00edrculo del Equilibrio.",
														"Muito bem! Toque no bot\u00e3o para sair do C\u00edrculo de Equil\u00edbrio."));
			Rows.Add( new TranslateRow("Drewno zdob\u0119dziesz za uko\u0144czenie bonusowych poziom\u00f3w. Kr\u0105g R\u00f3wnowagi znajdziesz na p\u00f3\u0142wyspie. Mo\u017cesz tu wst\u0105pi\u0107 zawsze. ",
														"Collect wood by completing bonus levels. Every time you want to visit Circle of Balance, you will find it on the rocky peninsula.",
														"Obtiens du bois en terminant les niveaux bonus. Le Cercle de l'\u00c9quilibre est situ\u00e9 sur la p\u00e9ninsule rocheuse.",
														"Sammle Holz, indem du Bonus-Levels abschlie\u00dft. Jedes Mal wenn du den Kreis des Gleichgewichts besuchen willst, findest du ihn auf der felsigen Halbinsel.",
														"Consigue madera para completar los niveles extra. Cuando quieras visitar el C\u00edrculo del Equilibrio, lo encontrar\u00e1s en la pen\u00ednsula rocosa.",
														"Colete madeira completando os n\u00edveis b\u00f4nus. Sempre que quiser visitar o C\u00edrculo de Equil\u00edbrio, voc\u00ea encontr\u00e1-lo-\u00e1 na pen\u00ednsula rochosa."));
			Rows.Add( new TranslateRow("Drewno",
														"Enchanted Wood",
														"Bois enchant\u00e9",
														"Zauberholz",
														"Madera encantada",
														"Madeira Encantada"));
			Rows.Add( new TranslateRow("Wykorzystaj do budowy totem\u00f3w!",
														"Use to build the totems!",
														"Sers-t'en pour b\u00e2tir les totems\u00a0!",
														"Benutzen, um die Totems zu bauen!",
														"\u00a1\u00dasala para construir los t\u00f3tems!",
														"Use para construir os totens!"));
			Rows.Add( new TranslateRow("Drewno",
														"A Pile of Enchanted Wood",
														"Pile de bois enchant\u00e9",
														"Ein Stapel Zauberholz",
														"Un mont\u00f3n de madera encantada.",
														"Uma Pilha de Madeira Encantada"));
			Rows.Add( new TranslateRow("Wykorzystaj do budowy totem\u00f3w!",
														"Use to build the totems!",
														"Sers-t'en pour b\u00e2tir les totems\u00a0!",
														"Benutzen, um die Totems zu bauen!",
														"\u00a1\u00dasala para construir los t\u00f3tems!",
														"Use para construir os totens!"));
			Rows.Add( new TranslateRow("SPALONE DRZEWO",
														"BURNED TREE",
														"ARBRE BR\u00dbL\u00c9",
														"VERBRANNTER BAUM",
														"\u00c1RBOL QUEMADO",
														"\u00c1RVORE QUEIMADA"));
			Rows.Add( new TranslateRow("BANAN",
														"BANANA",
														"BANANE",
														"BANANE",
														"PL\u00c1TANO",
														"BANANA"));
			Rows.Add( new TranslateRow("BUTELKA Z LISTEM",
														"LOST BOTTLE",
														"BOUTEILLE PERDUE",
														"VERLORENE FLASCHE",
														"BOTELLA PERDIDA",
														"GARRAFA PERDIDA"));
			Rows.Add( new TranslateRow("GRZYBEK",
														"MUSHROOM",
														"CHAMPIGNON",
														"PILZ",
														"SETA",
														"COGUMELO"));
			Rows.Add( new TranslateRow("ISKRA",
														"SPARK",
														"\u00c9TINCELLE",
														"FUNKE",
														"CHISPA",
														"FA\u00cdSCA"));
			Rows.Add( new TranslateRow("KWIAT",
														"FLOWER",
														"FLEUR",
														"BLUME",
														"FLOR",
														"FLOR"));
			Rows.Add( new TranslateRow("LALECZKA VOODOO",
														"VOODOO DOLL",
														"POUP\u00c9E VAUDOU",
														"VOODOO-PUPPE",
														"MU\u00d1ECO DE VUD\u00da",
														"BONECO DE VODU"));
			Rows.Add( new TranslateRow("KOKOS",
														"COCONUT",
														"NOIX DE COCO",
														"KOKOSNUSS",
														"COCO",
														"COCO"));
			Rows.Add( new TranslateRow("LAWA",
														"LAVA",
														"LAVE",
														"LAVA",
														"LAVA",
														"LAVA"));
			Rows.Add( new TranslateRow("DRZEWO",
														"TREE",
														"ARBRE",
														"BAUM",
														"\u00c1RBOL",
														"\u00c1RVORE"));
			Rows.Add( new TranslateRow("MUSZLA",
														"SEASHELL",
														"COQUILLAGE",
														"MUSCHELSCHALE",
														"CONCHA",
														"CONCHA"));
			Rows.Add( new TranslateRow("W\u0118GIEL",
														"COAL PIECE",
														"MORCEAU DE CHARBON",
														"ST\u00dcCK KOHLE",
														"TROZO DE CARB\u00d3N",
														"PEDA\u00c7O DE CARV\u00c3O"));
			Rows.Add( new TranslateRow("STATUA",
														"STATUE FRAGMENT",
														"FRAGMENT DE STATUE",
														"STATUENFRAGMENT",
														"FRAGMENTO DE ESTATUA",
														"FRAGMENTO DE EST\u00c1TUA"));
			Rows.Add( new TranslateRow("TARCZA",
														"SHIELD",
														"BOUCLIER",
														"SCHILD",
														"ESCUDO",
														"ESCUDO"));
			Rows.Add( new TranslateRow("DUCH PARY",
														"STEAM GHOST",
														"FANT\u00d4ME VAPOREUX",
														"DAMPFGEIST",
														"FANTASMA VAPOROSO",
														"FANTASMA A VAPOR"));
			Rows.Add( new TranslateRow("TSE TSE",
														"TSETSE",
														"TS\u00c9-TS\u00c9",
														"TSETSEFLIEGE",
														"MOSCA TSE-TSE",
														"TS\u00c9-TS\u00c9"));
			Rows.Add( new TranslateRow("PAPUGA",
														"PARROT",
														"PERROQUET",
														"PAPAGEI",
														"LORO",
														"PAPAGAIO"));
			Rows.Add( new TranslateRow("\u015aWIETLIK",
														"FIREFLY",
														"LUCIOLE",
														"LEUCHTK\u00c4FER",
														"LIB\u00c9LULA",
														"VAGA-LUME"));
			Rows.Add( new TranslateRow("SZALONY \u017b\u00d3\u0141W",
														"CRAZY TURTLE",
														"TORTUE FOLLE",
														"VERR\u00dcCKTE SCHILDKR\u00d6TE",
														"TORTUGA LOCA",
														"TARTARUGA LOUCA"));
			Rows.Add( new TranslateRow("W\u0104\u017b",
														"SNAKE",
														"SERPENT",
														"SCHLANGE",
														"SERPIENTE",
														"COBRA"));
			Rows.Add( new TranslateRow("ANANAS",
														"PINEAPPLE",
														"ANANAS",
														"ANANAS",
														"PI\u00d1A",
														"ABACAXI"));
			Rows.Add( new TranslateRow("DUCH PRZODK\u00d3W",
														"ANCESTRAL GHOST",
														"FANT\u00d4ME ANCESTRAL",
														"SPUKENDER VORFAHRE",
														"FANTASMA ANCESTRAL",
														"FANTASMA ANCESTRAL"));
			Rows.Add( new TranslateRow("DUCH LAWY",
														"LAVA GHOST",
														"FANT\u00d4ME DE LAVE",
														"LAVAGEIST",
														"FANTASMA DE LAVA",
														"FANTASMA DE LAVA"));
			Rows.Add( new TranslateRow("MASKA",
														"MASK",
														"MASQUE",
														"MASKE",
														"M\u00c1SCARA",
														"M\u00c1SCARA"));
			Rows.Add( new TranslateRow("Zwyk\u0142y przeciwnik",
														"Normal Monster",
														"Monstre normal",
														"Normales Monster",
														"Monstruo normal",
														"Monstro Normal"));
			Rows.Add( new TranslateRow("\u015arednia pr\u0119dko\u015b\u0107, \u015brednia wytrzyma\u0142o\u015b\u0107",
														"Moderate speed and health",
														"Vitesse et sant\u00e9 mod\u00e9r\u00e9es",
														"M\u00e4\u00dfige Geschwindigkeit und Gesundheit",
														"Velocidad y salud moderadas",
														"Velocidade e energia moderadas"));
			Rows.Add( new TranslateRow("Ci\u0119\u017cki przeciwnik",
														"Heavy Monster",
														"Monstre imposant",
														"Schweres Monster",
														"Monstruo pesado",
														"Monstro Forte"));
			Rows.Add( new TranslateRow("Niska pr\u0119dko\u015b\u0107, du\u017ca wytrzyma\u0142o\u015b\u0107",
														"Low speed, big health",
														"Lent. Sant\u00e9 \u00e9lev\u00e9e",
														"Niedrige Geschwindigkeit, hohe Gesundheit",
														"Velocidad m\u00ednima, mucha salud",
														"Baixa velocidade, muita energia"));
			Rows.Add( new TranslateRow("Szybki przeciwnik",
														"Fast Monster",
														"Monstre rapide",
														"Schnelles Monster",
														"Monstruo r\u00e1pido",
														"Monstro R\u00e1pido"));
			Rows.Add( new TranslateRow("Wysoka pr\u0119dko\u015b\u0107, niska wytrzyma\u0142o\u015b\u0107",
														"High speed. Low health",
														"Rapide. Sant\u00e9 basse",
														"Schnelle Geschwindigkeit. Schlechte Gesundheit",
														"Alta velocidad. Poca salud.",
														"Alta velocidade. Baixa energia"));
			Rows.Add( new TranslateRow("Opancerzony przeciwnik",
														"Armoured Monster",
														"Monstre en armure",
														"Gepanzertes Monster",
														"Monstruo con armadura",
														"Monstro com Armadura"));
			Rows.Add( new TranslateRow("Odporny na zwyk\u0142e ataki przed stopieniem pancerza.",
														"Immune to standard damage before the armour is melted",
														"Insensible aux d\u00e9g\u00e2ts normaux si l'armure n'est pas fondue.",
														"Immun gegen Standardsch\u00e4den, bevor die R\u00fcstung geschmolzen wird",
														"Inmune al da\u00f1o normal antes de que la armadura se derrita.",
														"Imune a dano padr\u00e3o antes de a armadura derreter"));
			Rows.Add( new TranslateRow("Przeciwnik z za\u015bwiat\u00f3w",
														"Ghostly Monster",
														"Monstre spectral",
														"Spukendes Monster",
														"Monstruo fantasmal",
														"Monstro Fantasmag\u00f3rico"));
			Rows.Add( new TranslateRow("Omija \u015bcie\u017ck\u0119",
														"Traveling directly to the shaman, ignoring the path.",
														"Atteint directement la chamane sans emprunter le chemin.",
														"Reist direkt zum Schamanen, ohne auf den Weg zu achten.",
														"Va directo al cham\u00e1n, ignorando la ruta.",
														"Viajar diretamente at\u00e9 \u00e0 xam\u00e3, ignorando o caminho."));
			Rows.Add( new TranslateRow("Elitki",
														"Elites",
														"\u00c9lites",
														"Eliten",
														"\u00c9lites",
														"Elites"));
			Rows.Add( new TranslateRow("Bossy",
														"Bosses",
														"Boss",
														"Bosse",
														"Enemigos finales",
														"Chefes"));
			Rows.Add( new TranslateRow("Potwory",
														"Monsters",
														"Monstres",
														"Monster",
														"Monstruos",
														"Monstros"));
			Rows.Add( new TranslateRow("Skoczki",
														"Jumping Monster",
														"Monstre bondissant",
														"Springendes Monster",
														"Monstruo saltar\u00edn",
														"Monstro Saltitante"));
			Rows.Add( new TranslateRow("Czasami wybiera drog\u0119 na skr\u00f3ty",
														"Taking shortcuts sometimes",
														"Prend parfois des raccourcis.",
														"Nimmt manchmal Abk\u00fcrzungen",
														"A veces toma atajos",
														"Tomar atalhos por vezes"));
			Rows.Add( new TranslateRow("Przeciwnik specjalny",
														"Special Monster",
														"Monstre sp\u00e9cial",
														"Besonderes Monster",
														"Monstruos especiales",
														"Monstro Especial"));
			Rows.Add( new TranslateRow("Umo\u017cliwia zbieranie esencji do tiki masek",
														"Drops essences for tiki masks.",
														"D\u00e9pose des essences pour les masques tiki.",
														"L\u00e4sst Essenzen f\u00fcr die Tiki-Masken fallen.",
														"Suelta esencias para las m\u00e1scaras tiki.",
														"Deixa cair ess\u00eancias para m\u00e1scaras tiki."));
			Rows.Add( new TranslateRow("Potrzebujemy wsparcia przodk\u00f3w, by przetrwa\u0107 w wulkanie. Wybuduj najpierw oda totemy!",
														"We need ancestors' help to survive in the volcano. Build both totems in the Circle of Balance first!",
														"Il nous faut l'aide des anc\u00eatres pour survivre dans le volcan. B\u00e2tis d'abord les deux totems dans le Cercle de l'\u00c9quilibre\u00a0!",
														"Wir brauchen die Hilfe der Vorfahren, um im Vulkan zu \u00fcberleben. Baue beide Totems im Kreis des Gleichgewichts zuerst!",
														"Necesitamos la ayuda de los ancestros para sobrevivir en el volc\u00e1n. \u00a1Antes debes construir ambos t\u00f3tems en el C\u00edrculo del Equilibrio!",
														"Precisamos da ajuda dos antepassados para sobreviver no vulc\u00e3o. Construa ambos os totens no C\u00edrculo de Equil\u00edbrio primeiro!"));
			Rows.Add( new TranslateRow("Jeste\u015b bohaterem plemienia Tiki! Uda\u0142o ci si\u0119 powstrzyma\u0107 ducha wulkanu i przywr\u00f3ci\u0107 spok\u00f3j na wyspie!",
														"You are the hero of the Tiki tribe! You have stopped the menace of volcano spirit! Well done!",
														"Tu es le h\u00e9ros de la tribu Tiki\u00a0! Tu as mis un terme \u00e0 la menace de l'esprit du volcan ! Bien jou\u00e9\u00a0!",
														"Du bist der Held des Tiki-Stammes! Du hast die Bedrohung durch den Vulkangeist gestoppt! Bravo!",
														"\u00a1Eres el h\u00e9roe de la tribu Tiki! \u00a1Has detenido la amenaza del esp\u00edritu del volc\u00e1n! \u00a1Muy bien!",
														"Voc\u00ea \u00e9 o her\u00f3i da tribo Tiki! Voc\u00ea parou a amea\u00e7a do esp\u00edrito do vulc\u00e3o! Muito bem!"));
			Rows.Add( new TranslateRow("Uwa\u017caj na opancerzonych przeciwnik\u00f3w! S\u0105 odporni na zwyk\u0142e ataki zanim stopisz im pancerz!",
														"Some enemies are armoured! They are invulnerable before you melt armour with dedicated towers.",
														"Certains ennemis sont prot\u00e9g\u00e9s par une armure\u00a0! Pour leur infliger des d\u00e9g\u00e2ts, tu dois d'abord faire fondre leur armure avec les tours appropri\u00e9es.",
														"Einige Gegner sind gepanzert! Sie sind unverwundbar, bevor du ihre R\u00fcstung mit den jeweiligen T\u00fcrmen schmilzt.",
														"\u00a1Algunos enemigos tienen armaduras! Son invulnerables hasta que derrites su armadura con las torres especiales.",
														"Alguns inimigos est\u00e3o armados! Eles s\u00e3o invulner\u00e1veis antes de voc\u00ea derreter a armadura com torres dedicadas."));
			Rows.Add( new TranslateRow("",
														"",
														"",
														"",
														"",
														""));
			Rows.Add( new TranslateRow("",
														"",
														"",
														"",
														"",
														""));
			Rows.Add( new TranslateRow("",
														"",
														"",
														"",
														"",
														""));
			Rows.Add( new TranslateRow("",
														"",
														"",
														"",
														"",
														""));
			Rows.Add( new TranslateRow("",
														"",
														"",
														"",
														"",
														""));
			Rows.Add( new TranslateRow("",
														"",
														"",
														"",
														"",
														""));
			Rows.Add( new TranslateRow("",
														"",
														"",
														"",
														"",
														""));
			Rows.Add( new TranslateRow("",
														"",
														"",
														"",
														"",
														""));
			Rows.Add( new TranslateRow("Graj!",
														"Play!",
														"Jouer\u00a0!",
														"Spiel!",
														"\u00a1Juega!",
														"Jogue!"));
			Rows.Add( new TranslateRow("Usuni\u0119to wszystkie przeszkody!",
														"All obstacles cleared!",
														"Tous les obstacles sont \u00e9limin\u00e9s\u00a0!",
														"Alle Hindernisse entfernt!",
														"\u00a1Se han eliminado todos los obst\u00e1culos!",
														"Todos os obst\u00e1culos removidos!"));
			Rows.Add( new TranslateRow("Du\u017co kasy!",
														"All levels and tiki masks unlocks will be available for all the game profiles. The gems will be available for your current profile.",
														"Tous les niveaux et les masques tiki d\u00e9bloqu\u00e9s sont disponibles pour l'ensemble des profils de jeu. Les gemmes sont d\u00e9bloqu\u00e9es pour ton profil actuel uniquement.",
														"Alle Levels und Tiki-Masken-Entsperrungen werden f\u00fcr alle Spielprofile zur Verf\u00fcgung stehen. Die Edelsteine \u200b\u200bwerden f\u00fcr dein aktuelles Profil zur Verf\u00fcgung stehen.",
														"Todos los niveles y las m\u00e1scaras tiki desbloqueados estar\u00e1n disponibles para todos los perfiles de juego. Las gemas estar\u00e1n disponibles para tu perfil actual.",
														"Todos os desbloqueios de n\u00edveis e m\u00e1scaras tiki estar\u00e3o dispon\u00edveis para todos os perfis de jogo. As gemas estar\u00e3o dispon\u00edveis para seu perfil atual."));
			Rows.Add( new TranslateRow("Tapnij by kontynuowa\u0107...",
														"Tap to continue...",
														"Touche pour continuer...",
														"Zum Fortfahren tippen...",
														"Toca para continuar...",
														"Toque para continuar..."));
			Rows.Add( new TranslateRow("Bronie",
														"Powers",
														"Pouvoirs",
														"Kr\u00e4fte",
														"Poderes",
														"Poderes"));
			Rows.Add( new TranslateRow("Bonusowe poziomy",
														"Bonus levels",
														"Niveaux bonus",
														"Bonus-Levels",
														"Niveles extra",
														"N\u00edveis b\u00f4nus"));
			Rows.Add( new TranslateRow("Restore Purchases",
														"Restore Purchases",
														"R\u00e9tablir les achats",
														"K\u00e4ufe wiederherstellen",
														"Restablecer compras",
														"Restaurar compras"));
			Rows.Add( new TranslateRow("Licencje",
														"Licenses",
														"Licences",
														"Lizenzen",
														"Licencias",
														"Licen\u00e7as"));
			Rows.Add( new TranslateRow("\u015awietnie!",
														"Great!",
														"Super\u00a0!",
														"Prima!",
														"\u00a1Genial!",
														"\u00d3timo!"));
			Rows.Add( new TranslateRow("Doskonale!",
														"Blimey!",
														"G\u00e9nial\u00a0!",
														"O je!",
														"\u00a1Vaya tela!",
														"Caramba!"));
			Rows.Add( new TranslateRow("Zrobione!",
														"Done!",
														"Termin\u00e9\u00a0!",
														"Fertig!",
														"\u00a1Hecho!",
														"Conclu\u00eddo!"));
			Rows.Add( new TranslateRow("Witaj na wyspie szaman\u00f3w. Pojawiasz si\u0119 w odpowiednim momencie. W wulkanie przebudzi\u0142o si\u0119 staro\u017cytne z\u0142o! Udajmy si\u0119 p\u0119dem na nadbrze\u017ce. ",
														"Welcome Hero to our island. You arrived in the right moment. An ancient loom, spirit of the volcano has awakened. Let's hurry to the shoreline!",
														"Bienvenue sur notre \u00eele\u00a0! Tu arrives \u00e0 point nomm\u00e9. Une menace antique, le d\u00e9mon du volcan, s'est r\u00e9veill\u00e9e. Tu dois nous aider\u00a0! Rejoins vite le littoral\u00a0!",
														"Willkommen auf unserer Insel, Held. Du bist im richtigen Moment angekommen. Eine alte Bedrohung, der Geist des Vulkans, ist erwacht. Wir brauchen deine Hilfe! Lass uns zum Strand eilen!",
														"Bienvenido a nuestra isla, h\u00e9roe. Has llegado en el momento oportuno. Una maldici\u00f3n ancestral, el demonio del volc\u00e1n se ha despertado. \u00a1Necesitamos tu ayuda! \u00a1Apresur\u00e9monos a llegar a la orilla!",
														"Bem-vindo \u00e0 nossa ilha, her\u00f3i. Voc\u00ea chegou no momento certo. Um antigo motivo de preocupa\u00e7\u00e3o, o dem\u00f4nio do vulc\u00e3o, acordou. Precisamos de sua ajuda! Apressemo-nos para o litoral!"));
			Rows.Add( new TranslateRow("Moja c\u00f3rka broni nadbrze\u017ca przed szale\u0144stwem, kt\u00f3re rozp\u0119ta\u0142 duch. Pom\u00f3\u017c jej prosz\u0119!",
														"My daughter is defending the shoreline from the corruption spread by the awakened volcano spirit. Please help her!",
														"C'est ma fille qui prot\u00e8ge le littoral de la corruption du d\u00e9mon du volcan. Je t'en prie, aide-la\u00a0!",
														"Meine Tochter verteidigt das Ufer vor dem Verderben, das durch den erwachten Vulkangeist verbreitet wird. Bitte hilf ihr!",
														"Mi hija est\u00e1 defendiendo la orilla de la corrupci\u00f3n que ha desatado el despertar del demonio del volc\u00e1n. \u00a1Por favor, ay\u00fadala!",
														"Minha filha est\u00e1 defendendo o litoral da corrup\u00e7\u00e3o espalhada pelo dem\u00f4nio do vulc\u00e3o acordado. Por favor, ajude-a!"));
			Rows.Add( new TranslateRow("Worek rubin\u00f3w",
														"A sack of gems",
														"Sac de gemmes",
														"Ein Sack Edelsteine",
														"Un saco de gemas",
														"Um saco de gemas"));
			Rows.Add( new TranslateRow("Przydatne do zakupu esencji lub drewna",
														"Use to buy essences or wood",
														"Sers-t'en pour acheter des essences ou du bois.",
														"Benutzen, um Essenzen und Holz zu kaufen",
														"\u00dasalo para comprar esencias o madera",
														"Use para comprar ess\u00eancias ou madeira"));
			Rows.Add( new TranslateRow("Otrzymujesz rubiny za obejrzenie reklamy.",
														"Gems added for watching the ad!",
														"Gemmes ajout\u00e9es pour avoir regard\u00e9 la publicit\u00e9\u00a0!",
														"Edelsteine werden \u200b\u200bf\u00fcr das Ansehen der Werbung hinzugef\u00fcgt!",
														"\u00a1Has conseguido m\u00e1s gemas por ver la publicidad!",
														"Gemas adicionadas por ver o an\u00fancio!"));
			Rows.Add( new TranslateRow("Wczytuj\u0119...",
														"Loading...",
														"Chargement...",
														"Wird geladen...",
														"Cargando...",
														"Carregando..."));
			Rows.Add( new TranslateRow("Graj z czterema naraz!",
														"Play with all masks at once!",
														"Joue avec tous les masques \u00e0 la fois\u00a0!",
														"Spiele mit allen Masken auf einmal!",
														"\u00a1Juega con todas las m\u00e1scaras a la vez!",
														"Jogue com todas as m\u00e1scaras de uma vez!"));
			Rows.Add( new TranslateRow("Reklama",
														"Watch Ad",
														"Voir pub",
														"Werbung",
														"Ver publicidad",
														"Ver an\u00fancio"));
			Rows.Add( new TranslateRow("Po\u0142\u0105czenie internetowe wymagane do otwarcia sklepu.",
														"Internet connection required to open the store.",
														"Connexion Internet requise pour ouvrir la boutique.",
														"Erforderliche Internetverbindung, um den Laden zu \u00f6ffnen.",
														"Conexi\u00f3n a Internet necesaria para abrir la tienda.",
														"Conex\u00e3o com a Internet requerida para abrir a loja."));
			Rows.Add( new TranslateRow("",
														"",
														"",
														"",
														"",
														""));
			Rows.Add( new TranslateRow("Pobieram zawarto\u015b\u0107",
														"Downloading content",
														"T\u00e9l\u00e9chargement du contenu",
														"Ich lade den Inhalt herunter",
														"Descarga de contenidos",
														"Baixando o conte\u00fado"));
		}
		public TranslateRow GetRow(rowIds rowID)
		{
			TranslateRow ret = null;
			try
			{
				ret = Rows[(int)rowID];
			}
			catch( KeyNotFoundException ex )
			{
				Debug.LogError( rowID + " not found: " + ex.Message );
			}
			return ret;
		}
		public TranslateRow GetRow(string rowString)
		{
			TranslateRow ret = null;
			try
			{
				ret = Rows[(int)Enum.Parse(typeof(rowIds), rowString)];
			}
			catch(ArgumentException) {
				Debug.LogError( rowString + " is not a member of the rowIds enumeration.");
			}
			return ret;
		}

	}

}
