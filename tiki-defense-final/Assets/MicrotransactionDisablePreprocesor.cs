﻿using UnityEngine;
using System.Collections;

public class MicrotransactionDisablePreprocesor : MonoBehaviour
{
#if BLOCK_MICROTRANSACTION
    public bool ChangePositionOnly;
    public Vector3 ChangePosition;
    public bool DisableOnlyUIButton;
    public bool Disable = true;
    public bool DisableCollider;
    public Collider Collider;

    void Awake()
    {

        if (DisableCollider)
            Collider.enabled = false;

        if (ChangePositionOnly)
        {
            transform.localPosition = ChangePosition;
            return;
        }

        if (DisableOnlyUIButton)
        {
            GetComponent<UIButton>().enabled = false;
            return;
        }

        if(Disable)
            gameObject.SetActive(false);


    }


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

    }
#endif
}
