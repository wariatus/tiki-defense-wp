﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

public class TIKITools   {

    public static string[] scenes;

    private static string[] ReadNames()
    {
        List<string> temp = new List<string>();
        foreach (UnityEditor.EditorBuildSettingsScene S in UnityEditor.EditorBuildSettings.scenes)
        {
            if (S.enabled)
            {
                string name = S.path.Substring(S.path.LastIndexOf('/') + 1);
                name = name.Substring(0, name.Length - 6);
                temp.Add(name);
            }
        }
        return temp.ToArray();
    }

    [MenuItem("TIKI Defense/FIX Grid position")]
    private static void FixGridPosition(UnityEditor.MenuCommand command)
    {
        scenes = ReadNames();
      
        foreach (var scene in scenes)
        {
            EditorApplication.OpenScene("Assets/Scenes/" + scene+".unity");
            
            
            var path = EditorApplication.currentScene.Split(char.Parse("/"));
           
            var grid = GameObject.FindObjectOfType<tk2dCameraAnchor>();

            if (grid == null) continue;
            if (grid.GetComponent<tk2dCameraAnchor>() == null) continue;

            grid.GetComponent<tk2dCameraAnchor>().AnchorCamera = null;
            grid.GetComponent<tk2dCameraAnchor>().transform.localPosition = new Vector3(1, 3.038889f, 10);


            EditorApplication.SaveScene(String.Join("/",path));

            Debug.Log("Scena: " + scene + " poprawiona");
        }
       

    }

   


}
