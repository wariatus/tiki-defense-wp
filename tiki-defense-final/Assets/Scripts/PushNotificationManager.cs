﻿using UnityEngine;
using System.Collections;

public class PushNotificationManager : MonoBehaviour {

	// Use this for initialization
	void Start () {


	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	private void AddNotification()
	{
        #if !UNITY_STANDALONE
			// Tworzymy nowy obiekt notyfikacji lokalnej
			LocalNotification noti = new LocalNotification();
			
			// Przypisujemy date (DateTime), o której ma się notyfikacja odpalić
			noti.fireDate = System.DateTime.Now.AddMinutes(5);
			
			// Dodajemy treść notyfikacji
			noti.alertBody = "Hey! Come back!";
			
			// Sprawdzamy czy notyfikacja nie jest już dodana do listy oczekującyh na wykonanie
			if(!this.CheckIfNotificationIsScheduled(noti.alertBody))
			{
				// Dodajemy utworzony obiekt notyfikacji lokalnej do listy oczekujących na wykonanie
				NotificationServices.ScheduleLocalNotification(noti);
				
				if(Debug.isDebugBuild)
					Debug.Log("[NotificationService]: Rejestracja notyfikacji " + noti.alertBody);
				
			}
#endif
	}

	/// <summary>
	/// Metoda spradza czy notyfikacja znajduje się już w liście oczekujących na wywołanie
	/// </summary>
	/// <returns><c>true</c>, Gdy jest już dodana <c>false</c> Gdy nie jest dodana.</returns>
	private bool CheckIfNotificationIsScheduled(string _alertBody)
	{
        #if !UNITY_STANDALONE
		foreach(LocalNotification noti in NotificationServices.scheduledLocalNotifications)
		{
			if(noti.alertBody.Equals(_alertBody))
				return true;
		}
#endif
        return false;

	}
}
