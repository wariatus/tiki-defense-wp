﻿using UnityEngine;
using System.Collections;

namespace Projectiles
{
		public class JumpHit : ProjectileBase {
		
		public int howManyJump;
		public LayerMask mask;

		private float range;

		protected override void Start ()
		{
			base.Start();
		}
		
		protected override void Awake ()
		{
			base.Awake ();
			
			
		}
		
		protected override void Update ()
		{	
			if (GameMaster.gameState == GameState.RUNNING)
	        {
				if(target != null && target.isSpawned)
				{
                    MyTransform.position = Vector3.MoveTowards(MyTransform.position, target.MyTransform.position, moveSpeed * Time.deltaTime);

				}
				else
					ReturnToPool();
			}   
		}

		protected virtual void OnDrawGizmosSelected()
		{
			Gizmos.color = Color.red;
			Gizmos.DrawSphere(transform.position, range);
		
		}	

		
		protected override void OnTriggerEnter2D (Collider2D c)
		{


            if (c.gameObject.tag == "TowerCampfire")
            {
                Towers.Campfire t = c.GetComponent<TowerBaseReference>().tower as Towers.Campfire;
                if (t != null)
                {
                    BurnProjectile();
                    Debug.Log("Podpalam pocisk: " + this.name + " DMG: " + damage);
                }
            }
            else
            {

                UnitBase u = c.GetComponent<UnitBaseReference>().unitBase;

                if (u == target)
                {

                    Execute(u);
					howManyJump--;

                    if (howManyJump >= 0)
                    {
                        Collider2D[] hit = Physics2D.OverlapCircleAll(MyTransform.position, range, mask);
						Debug.Log(hit.Length);
                        if (hit.Length > 0)
                        {
													

                            
                        }
                        else
                        {
                            ReturnToPool();
                            Debug.Log("Brak Celow");
                        }
                    }
                    else
                    {
                        ReturnToPool();
						Debug.Log("Koniec skokow");
                    }
                }
                
            }
			
		}
	
		
		public override void Setup (ProjectileSetting settings, Vector3 moveDirection)
		{
			base.Setup (settings, moveDirection);
			howManyJump = settings.jumpHit;
			range = settings.radiusRange;

		}
		
		protected override void OnDisable ()
		{
			base.OnDisable ();

			
		}
		
		protected override void Execute (UnitBase u)
		{
			DamageInfo damageInfo = new DamageInfo(damage, this.settings.armoreDamage, Element.None, null, false, MyTransform.position, false);
			u.RecieveDamage(damageInfo);
		}
		
		
	}
		
}
