﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Projectiles
{
    public class Laser : ProjectileBase
    {
		
		public Transform laser;
		public LineRenderer beam;			// REferenca do LineRenderera
		public float timeActive;
		public LayerMask mask;

		private List<UnitBase> targetsHit;		// Lista celow, ma na celu blokade zadawania podwojnych obrazen
		
        protected override void Awake()
        {
             base.Awake();
        }

        protected override void Start()
        {
            base.Start();

			targetsHit = new List<UnitBase>();
        }

        protected override void Update()
        {
//			if(enabled && target != null)
//			{
//				RaycastHit2D[] hits =  Physics2D.RaycastAll(MyTransform.position, target.MyTransform.position + ((MyTransform.position - target.MyTransform.position) * -100f), Mathf.Infinity, mask);
//				{
//					Debug.Log("Trafilem: " + hits.Length);
//					for(int i = 0; i < hits.Length; i++)
//					{
//						UnitBase u = hits[i].transform.GetComponent<UnitBase>();
//
//						if(u != null)
//						{
//							bool takeDamage = true;
//							if(targetsHit.Count > 0)
//							{
//								for(int j = 0; j < targetsHit.Count; j++)
//								{
//									if(targetsHit[j] == u)
//										takeDamage = false;
//								}
//							}
//
//							if(takeDamage)
//							{
//								Execute(u);
//								targetsHit.Add(u);
//							}
//						}
//					}
//					
//				}	
//			}

        }
		
//		protected override void Execute (UnitBase u)
//		{
//			Splash(u);
//
//			DamageInfo damageInfo = new DamageInfo(this.settings.damage, this.settings.armoreDamage, Element.None, null, false, MyTransform.position, false);
//				u.RecieveDamage(damageInfo);
//		}

		public override void Setup (ProjectileSetting settings,  Vector3 moveDirection)
		{
			base.Setup (settings, moveDirection);
//
//			beam.SetVertexCount(2);
//			beam.SetPosition(0, MyTransform.position);
//			Vector3 position =  (MyTransform.position - target.MyTransform.position) * -10f;
//			Vector3 fixedPosition = target.MyTransform.position + position;
//			//	fixedPosition.x = MyTransform.position.x;
//			// fixujemy strzal w poziomie i pionie
//			if(Vector3.Angle(MyTransform.position, target.MyTransform.position) < 7  && Vector3.Angle(MyTransform.position, target.MyTransform.position) > 4.5f)
//				fixedPosition.y = MyTransform.position.y;	
//
//			beam.SetPosition(1, fixedPosition);
//
//			//Debug.Log (Vector3.Angle(MyTransform.position, target.MyTransform.position));


		}
		
		void OnSpawned()
		{

			Invoke("ReturnToPool", timeActive);
		}
		
		protected virtual void OnTriggerEnter2D(Collider2D c)
		{
			UnitBase u = c.GetComponent<UnitBaseReference>().unitBase;
			Execute(u);
		}
		
		protected override void OnDisable ()
		{
			base.OnDisable ();


		}
		
		protected override void OnEnable ()
		{
			base.OnEnable ();
			//laser.gameObject.SetActive(true);
		}
    }
}
