﻿using UnityEngine;
using System.Collections;

namespace Projectiles
{

	public class Instant : AreaOfEffectHit {

		protected override void Awake ()
		{
			base.Awake ();
		}

		protected override void Start ()
		{
			base.Start ();
		}

		protected override void Update ()
		{
			base.Update ();
		}

		protected override void OnTriggerEnter2D (Collider2D c)
		{
			// Nie robimy nic
		}

		protected override void OnEnable ()
		{
			base.OnEnable ();


		}

		public override void Setup (ProjectileSetting settings, Vector3 moveDirection)
		{
			base.Setup (settings, moveDirection);

			Execute(null);
		}

		protected override void SetEffect (Unit unit)
		{
			if(settings.slowAmount > 0 && unit != null)
			{
				MoveSlowData slowData = new MoveSlowData(){ modifier = settings.slowAmount, ttl = settings.timeEffect };
				unit.SetSpeedModifier(slowData);
				
				if(settings.affectedByTiki && unit.gameObject.layer != LayerMask.NameToLayer("Blockers"))
				{
					string effectName = slowAmount < 100 ? "HitEffect_IceFrozen" : "HitEffect_StunEffect";
					ProjectileHitEffect sEffect = 	PathologicalGames.PoolManager.Pools["HitEffects"].Spawn(effectName).GetComponent<ProjectileHitEffect>();
					sEffect.MyTransform.position = unit.MyTransform.position;
					sEffect.SetTarget(unit.MyTransform, settings.timeEffect);
				}
			}
		}

		protected override void Splash (UnitBase u)
		{
			if(hitEffect != PoolObjectType.Null && u != null)
			{
				ProjectileHitEffect effect = 	PathologicalGames.PoolManager.Pools["HitEffects"].Spawn(hitEffect.ToString()).GetComponent<ProjectileHitEffect>();
				effect.MyTransform.position = u.MyTransform.position;	
				effect.SetTarget(u.MyTransform);
			
			}
		}

		protected override void Execute (UnitBase u)
		{
			if(u != null)
				base.Execute(u);
			
			Collider2D[] hits = Physics2D.OverlapCircleAll(MyTransform.position, settings.radiusRange, mask);
			//Debug.Log("Trafilem: " + hits.Length + " Radius " + radius + " DmgTargers: " + radiusDamage );
			if(hits.Length > 0)
			{
				DamageInfo damageInfo = new DamageInfo(settings.radiusDamage, this.settings.armoreDamage, Element.None, null, false, MyTransform.position, false);
				for(int i = 0; i < hits.Length; i++)	
				{
					//Debug.Log(hits[i].transform.name);
					unitBase = hits[i].transform.GetComponent<UnitBase>();
					if(unitBase == null)
						unitBase = hits[i].transform.GetComponent<UnitBaseReference>().unitBase;
					
					unitBase.RecieveDamage(damageInfo);
					//Debug.Log(hits[i].name);
					SetEffect(unitBase as Unit);
					
					Splash(unitBase);
				}
			}


			
			ReturnToPool();

		}
	}
}
