﻿using System;
using UnityEngine;
using Towers;


[Serializable]
public class ProjectileSetting  {


	public float damage;
	public float armoreDamage;

	public float additiveDamage;
    [Range(0, 100)]
    public float campfireDamageBurst;

    public float radiusDamage;
    [Range(0, 100)]
    public float campfireRadiusDamageBurst;
    public float radiusRange;

	[HideInInspector]
	public UnitBase unit;
	
    [Range(0, 100)]
	public int slowAmount;
	public float timeEffect;
	public int jumpHit;

	public TowerBase tower;
	public bool affectedByTiki;
	
}
