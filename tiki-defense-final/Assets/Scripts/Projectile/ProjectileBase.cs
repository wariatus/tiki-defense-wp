﻿using UnityEngine;  
using System.Collections;

namespace Projectiles
{

	public class ProjectileBase : ExtendedMonoBehavior {
		
		public float moveSpeed;
		public PoolObjectType poolObjectType;
		
		public float damage { set; get; }
		public UnitBase target;

		protected int slowAmount;
		protected float slowTime;

		protected bool moveDirection;
		protected Vector3 moveVector;
		protected tk2dSprite spriteBase;

		protected ProjectileSetting settings;

		/// <summary>
		/// Prefab efektu trafienia
		/// </summary>
		public PoolObjectType hitEffect = PoolObjectType.Null;

        private float timeStart;
        private float timeStartUnscaled;

		protected override void Awake ()
		{
			base.Awake ();
			if(MyTransform.FindChild("Sprite") != null)
				spriteBase = MyTransform.FindChild("Sprite").GetComponent<tk2dSprite>();
		}



		protected override void Start ()
		{
			base.Start ();

			//NotificationCenter.defaultCenter.addListener(OnBackToPoolRequest, NotificationType.BackToPool);
		}
		
		protected override void Update ()
		{
			base.Update ();
			
			if (GameMaster.gameState == GameState.RUNNING)
	        {
				if(target != null && target.isSpawned)
				{
					Vector3 pos = target.transform.position;
					pos.z = MyTransform.position.z;

					MyTransform.LookAt(pos);
                    MyTransform.position = Vector3.MoveTowards(MyTransform.position, target.MyTransform.position, (moveSpeed * Unit.speedScale) * Time.deltaTime);

				}
				else if(moveDirection)
				{
					MyTransform.position += moveVector * (moveSpeed * Unit.speedScale) * Time.deltaTime;
					if(!spriteBase.renderer.isVisible)
						ReturnToPool();

				}
				else
					ReturnToPool();
			}        
		}
		
		protected virtual void ReturnToPool()
		{
			//Debug.Log("asdasdasd");

			MyTransform.rotation = Quaternion.identity;

			if(PathologicalGames.PoolManager.Pools["Bullets"].IsSpawned(MyTransform))
				PathologicalGames.PoolManager.Pools["Bullets"].Despawn(MyTransform);
			//PoolManager.AddItem(MyGameObject, poolObjectType);
			
		}
		
		protected virtual void OnTriggerEnter2D(Collider2D c)
		{
        	UnitBase u = c.GetComponent<UnitBaseReference>().unitBase;

			if(target != null)
			{
                if (u.type == UnitType.Enemy && target == u)
                {
                    Execute(u);
                    ReturnToPool();
                }
                else if (target.type == UnitType.Blocker && target == u)
                {
                    Execute(u);
                    ReturnToPool();
                }
			}
			else
			{
				Execute(u);
				ReturnToPool();
			}
           
			
		}
		
		public virtual void Setup(ProjectileSetting settings, Vector3 _moveVector)
		{   
            this.settings = settings;

			if(_moveVector == Vector3.zero)
				this.target = settings.unit;
			else
			{
				moveDirection = true;
				moveVector = _moveVector;
				
				if(_moveVector == Vector3.up)
					MyTransform.Rotate( new Vector3(0, 0, 90), Space.Self);
				else if(_moveVector == Vector3.down)
					MyTransform.Rotate( new Vector3(0, 0, -90), Space.Self);
				else if(_moveVector == Vector3.left)
					MyTransform.Rotate( new Vector3(0, 0, -180), Space.Self);
				else if(_moveVector == Vector3.right)
					MyTransform.Rotate(new Vector3(0, 0, 0), Space.Self);
				else if(_moveVector == new Vector3(1, 1, 0))
					MyTransform.Rotate(new Vector3(0, 0, 45), Space.Self);
				else if(_moveVector == new Vector3(-1, -1, 0))
					MyTransform.Rotate(new Vector3(0, 0, -125), Space.Self);
				else if(_moveVector == new Vector3(1, -1, 0))
					MyTransform.Rotate(new Vector3(0, 0, -45), Space.Self);
				else if(_moveVector == new Vector3(-1, 1, 0))
					MyTransform.Rotate(new Vector3(0, 0, 125), Space.Self);


			}

			this.damage = settings.damage;
			this.slowAmount = settings.slowAmount;
			this.slowTime = settings.timeEffect;
		}
		
		protected virtual void Execute(UnitBase u)
		{

         //  Debug.Log((Time.time - timeStart) + " () " + (Time.unscaledTime - timeStartUnscaled));
			Splash(u);
          //  UnityEditor.EditorApplication.isPaused = true;
            DamageInfo damageInfo = new DamageInfo(damage, this.settings.armoreDamage, Element.None, null, false, MyTransform.position, false);

			if(settings.affectedByTiki && u.gameObject.layer != LayerMask.NameToLayer("Blockers"))
			{
				string effectName = slowAmount < 100 ? "HitEffect_IceFrozen" : "HitEffect_StunEffect";
				ProjectileHitEffect sEffect = 	PathologicalGames.PoolManager.Pools["HitEffects"].Spawn(effectName).GetComponent<ProjectileHitEffect>();
				sEffect.MyTransform.position = u.MyTransform.position;
				sEffect.SetTarget(u.MyTransform, settings.timeEffect);
			}

			if(this.slowAmount > 0)
			{
				Unit unit = u as Unit;
				if(unit != null)
				{
					MoveSlowData slowData = new MoveSlowData(){modifier = slowAmount, ttl = slowTime};
					unit.SetSpeedModifier(slowData);
					unit.RecieveDamage(new DamageInfo(damage, this.settings.armoreDamage, Element.None, null, false, MyTransform.position));
				}
				else
					u.RecieveDamage(damageInfo);
			}
			else
				u.RecieveDamage(damageInfo);

		}

		protected virtual void OnSpawned()
		{
			NotificationCenter.defaultCenter.addListener(OnBackToPoolRequest, NotificationType.BackToPool);

         
		}

		protected virtual void OnDespawned()
		{
			NotificationCenter.defaultCenter.removeListener(OnBackToPoolRequest, NotificationType.BackToPool);
			moveDirection = false;
			target = null;
		}

		protected virtual void Splash(UnitBase u)
		{
			if(hitEffect != PoolObjectType.Null && u != null)
			{
				ProjectileHitEffect effect = 	PathologicalGames.PoolManager.Pools["HitEffects"].Spawn(hitEffect.ToString()).GetComponent<ProjectileHitEffect>();

				effect.MyTransform.position = u.MyTransform.position;

				effect.SetTarget(u.MyTransform);
				
				
			}
		}
		
		protected virtual void OnDisable()
		{
		
			
			target = null;
		}
		
		protected virtual void OnEnable()
		{
            timeStart = Time.time;
            timeStartUnscaled = Time.unscaledTime;
		}

        protected virtual void BurnProjectile()
        {
            damage += damage * (settings.campfireDamageBurst / 100);
            
        }

		/// <summary>
		/// Sets the effect.
		/// </summary>
		/// <param name="unit">Unit.</param>
		protected virtual void SetEffect(Unit unit)
		{
			if(this.slowAmount > 0)
			{
				if(unit != null)
				{
					MoveSlowData slowData = new MoveSlowData(){ modifier = slowAmount, ttl = slowTime };
					unit.SetSpeedModifier(slowData);

                    if (settings.affectedByTiki && unit.gameObject.layer != LayerMask.NameToLayer("Blockers"))
					{
						string effectName = slowAmount < 100 ? "HitEffect_IceFrozen" : "HitEffect_StunEffect";
						ProjectileHitEffect sEffect = 	PathologicalGames.PoolManager.Pools["HitEffects"].Spawn(effectName).GetComponent<ProjectileHitEffect>();
						sEffect.MyTransform.position = unit.MyTransform.position;
						sEffect.SetTarget(unit.MyTransform, settings.timeEffect);
					}
				}
			}
		}
		
		void OnBackToPoolRequest(Notification n)
		{
			if(PathologicalGames.PoolManager.Pools["Bullets"].IsSpawned(MyTransform))
				ReturnToPool();
			
		}
		
		void OnLevelWasLoaded(int level)
		{

		}
	}
}
