﻿using UnityEngine;
using System.Collections;
using Projectiles;

public class StunDown : Instant {

	public PoolObjectType stunEffect;
	
	protected override void Start ()
	{
		base.Start();
	}
	
	protected override void Awake ()
	{
		base.Awake ();
	}
	
	protected override void Update ()
	{
		base.Update ();
	}
	
	protected override void Splash (UnitBase u)
	{
		
		if(u.MyTransform.CompareTag("Blocker")) return;
		
		ProjectileHitEffect sEffect = 	PathologicalGames.PoolManager.Pools["HitEffects"].Spawn(stunEffect.ToString()).GetComponent<ProjectileHitEffect>();
		sEffect.MyTransform.position = u.MyTransform.position;
		sEffect.SetTarget(u.MyTransform, settings.timeEffect);
		
		base.Splash (u);
	}


}
