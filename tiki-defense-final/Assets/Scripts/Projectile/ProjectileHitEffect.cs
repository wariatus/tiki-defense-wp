﻿using UnityEngine;
using System.Collections;

public class ProjectileHitEffect : ExtendedMonoBehavior {

	private tk2dSprite sprite;
	private tk2dSpriteAnimator animation;
	public float ttl;

	public bool fadeOut;

	private Color originalColor;
	private Color bufferedColor;


	private Transform target;
	
	private GameObject targetGameObject;

	private float ttlOriginal;

	// Use this for initialization
	protected override void Awake () {

		sprite = transform.FindChild("sprite").GetComponent<tk2dSprite>();
		animation = transform.FindChild("sprite").GetComponent<tk2dSpriteAnimator>();

		originalColor = sprite.color;
		bufferedColor = originalColor;
		ttlOriginal = ttl;

	}

	protected override void Start ()
	{
		base.Start ();
	}

	private IEnumerator SplashColapse(float radius)
	{
		float scaleFix = radius / 3;
		Vector3 scale = Vector3.zero;
		float t = 0;

		while(sprite.scale.x < scaleFix)
		{
			if(GameMaster.gameState == GameState.RUNNING)
			{
				t += Time.deltaTime / ttl;

				scale.x = Mathf.Lerp(0, scaleFix,t);
				scale.y = Mathf.Lerp(0, scaleFix,t);

				sprite.scale = scale;
			}


			yield return null;
		}
	}

	public void SetTarget(Transform target, bool isSplashColapse, float radius, float _ttl = 0)
	{
		StartCoroutine("SplashColapse", radius);
		sprite.scale = Vector3.zero;

		SetTarget(target, _ttl);
	}

	public void SetTarget(Transform target, float _ttl = 0, float scale = 1)
	{
		if(_ttl != 0)
			ttl = _ttl;
			
		MyTransform.localScale = MyTransform.localScale * scale;

		this.target = target;
		targetGameObject = target.gameObject;
	}
	// Update is called once per frame
	protected override void Update () {

		if(target != null && target.gameObject.activeSelf)
		{
			MyTransform.position = target.position;

			if(animation != null)
				if(!animation.IsPlaying("Colapse"))
					if(PathologicalGames.PoolManager.Pools["HitEffects"].IsSpawned(this.transform))
						PathologicalGames.PoolManager.Pools["HitEffects"].Despawn(this.transform);
		}
		else
		{

			if(animation != null)
			{
				if(!animation.IsPlaying("Colapse"))
					if(PathologicalGames.PoolManager.Pools["HitEffects"].IsSpawned(this.transform))
						PathologicalGames.PoolManager.Pools["HitEffects"].Despawn(this.transform);
			}
			else
			{

				if(PathologicalGames.PoolManager.Pools["HitEffects"].IsSpawned(this.transform))
					PathologicalGames.PoolManager.Pools["HitEffects"].Despawn(this.transform);
			}
				
		}
	
	}

	private IEnumerator StartTTL()
	{
		if(fadeOut)
		{
			while(sprite.color.a > 0)
			{
				bufferedColor.a -= Time.deltaTime;
				sprite.color = bufferedColor;
				yield return null;
			}
		}
		else
		{
			while(ttl > 0)
			{
				if(GameMaster.gameState == GameState.RUNNING)
				{
					ttl -= Time.deltaTime;
				}

				yield return null;
			}
		}

		if(PathologicalGames.PoolManager.Pools["HitEffects"].IsSpawned(this.transform))
			PathologicalGames.PoolManager.Pools["HitEffects"].Despawn(this.transform);
	}


	void OnSpawned()
	{


		StartCoroutine("StartTTL");
		if(animation != null)
			animation.Play("Colapse");

	}

	void OnDespawned()
	{
		target = null;
		targetGameObject = null;
		StopCoroutine("StartTTL");
		StopCoroutine("SplashColapse");
		sprite.color = originalColor;
		MyTransform.localScale = new Vector3(1, 1, 1);
		ttl = ttlOriginal;


	}
}
