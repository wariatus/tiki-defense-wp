﻿using UnityEngine;
using System.Collections;

namespace Projectiles
{

	public class SlowDownAOE : AreaOfEffectHit {

		public PoolObjectType slowEffect;

		/// <summary>
		/// Start this instance.
		/// </summary>
		protected override void Start ()
		{
			base.Start();
		}
		
		protected override void Awake ()
		{
			base.Awake ();
		}
		
		protected override void Update ()
		{
			base.Update ();
		}
		
		protected override void SetEffect (Unit unit)
		{
			if(unit != null)
			{
				MoveSlowData slowData = new MoveSlowData(){ modifier = slowAmount, ttl = slowTime };
				unit.SetSpeedModifier(slowData);
	
			}
		}

		protected override void Splash (UnitBase u)
		{
			base.Splash (u);

			if(u.MyTransform.CompareTag("Blocker")) return;

			ProjectileHitEffect effect = 	PathologicalGames.PoolManager.Pools["HitEffects"].Spawn(slowEffect.ToString()).GetComponent<ProjectileHitEffect>();
			effect.MyTransform.position = u.MyTransform.position;
			
			float scale = 1;
			
			if(u.isBoss || u.isElite)
				scale = 1.2f;
			
			effect.SetTarget(u.MyTransform, settings.timeEffect, scale);
		}
		
		protected override void OnDespawned ()
		{
			base.OnDespawned ();
			
			
		}
		
		
	}
}
