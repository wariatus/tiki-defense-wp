﻿using UnityEngine;
using System.Collections;

namespace Projectiles
{
		public class AreaOfEffectHit : ProjectileBase {
		
		public LayerMask mask;
	
		private float radius;
        private float radiusDamage;

		private Unit unit;
		protected UnitBase unitBase;
		protected override void Start ()
		{
			base.Start();
		}
		
		protected override void Awake ()
		{
			base.Awake ();
			
		}
		
	
		protected override void OnTriggerEnter2D (Collider2D c)
		{
            if (c.gameObject.tag == "TowerCampfire")
            {
                Towers.Campfire t = c.GetComponent<TowerBaseReference>().tower as Towers.Campfire;
                if (t != null)
                {
                    BurnProjectile();
                    Debug.Log("Podpalam pocisk: " + this.name + " DMG: " + damage + " DMGRadius: " + radiusDamage);
                }
            }
            else
            {

                UnitBase u = c.GetComponent<UnitBaseReference>().unitBase;
                if (target.type == u.type)
				{
					MyTransform.position = target.MyTransform.position;
                    Execute(u);
				}
            }
			
		
		}
	
		protected override void Execute (UnitBase u)
		{
			if(u != null)
				base.Execute(u);

			Collider2D[] hits = Physics2D.OverlapCircleAll(MyTransform.position, radius, mask);
			//Debug.Log("Trafilem: " + hits.Length + " Radius " + radius + " DmgTargers: " + radiusDamage );
			if(hits.Length > 0)
			{
				DamageInfo damageInfo = new DamageInfo(radiusDamage, this.settings.armoreDamage, Element.None, null, false, MyTransform.position, false);
				for(int i = 0; i < hits.Length; i++)	
				{
					//Debug.Log(hits[i].transform.name);
					 unitBase = hits[i].transform.GetComponent<UnitBase>();
					if(unitBase == null)
						unitBase = hits[i].transform.GetComponent<UnitBaseReference>().unitBase;



					unitBase.RecieveDamage(damageInfo);
                   
					//Debug.Log(hits[i].name);
					SetEffect(unitBase as Unit);

					Splash(unitBase);
				}
			}


			
			ReturnToPool();
			
			
		}
		
		
		protected override void OnDisable ()
		{
			base.OnDisable ();

		}
		
		
		public override void Setup (ProjectileSetting settings, Vector3 moveDirection)
		{
			base.Setup (settings, moveDirection);
		
			this.radius = settings.radiusRange;
            this.radiusDamage = settings.radiusDamage;
			
		}
		
		protected override void BurnProjectile ()
		{
			base.BurnProjectile ();
			 
       		radiusDamage += radiusDamage * (settings.campfireRadiusDamageBurst / 100);
		}
		
	}
		
}
