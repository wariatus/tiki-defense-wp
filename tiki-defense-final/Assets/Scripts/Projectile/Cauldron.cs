using UnityEngine;
using System.Collections;
using Projectiles;

public class Cauldron : ProjectileBase {

	/// <summary>
	/// The damage.
	/// </summary>
	public float dps;

	/// <summary>
	/// Co ile beda zadawne obrazenia
	/// </summary>
	public float damageTime;

	public LayerMask enemyLayer;

	private bool isAttached;
	private Renderer myRender;

	protected override void Awake ()
	{
		base.Awake ();
		
		myRender = GetComponentInChildren<Renderer>();
	}

	protected override void Start ()
	{
		base.Start ();
	}

	protected override void Update ()
	{
		if (GameMaster.gameState == GameState.RUNNING)
		{
			if(!isAttached)
			{
				if(target != null && target.isSpawned)
				{
					Move();
					
				}
				else
				{
					ReturnToPool();
				}
			}
			else
			{
                if (myRender.enabled)
                    myRender.enabled = false;

				Move();

				if(!target.isSpawned)
				{

					StopCoroutine("StartDPS");
					
					isAttached = false;
					if(!SeekTarget())
					{
						ReturnToPool();

					}
				}
			}
		}        
	}

	private void Move()
	{
		Vector3 pos = target.transform.position;
		pos.z = MyTransform.position.z;
	
		MyTransform.LookAt(pos);
		MyTransform.position = Vector3.MoveTowards(MyTransform.position, target.MyTransform.position, moveSpeed * Time.deltaTime);
		
	}

	private bool SeekTarget()
	{
		Collider2D[] hit = Physics2D.OverlapCircleAll(MyTransform.position, settings.radiusRange, enemyLayer );
		
		if(hit.Length > 0)
		{
			float distanse = 100000000;
			int targetIndex = 0;
			for(int i = 0; i < hit.Length; i++)
			{
				if(target != hit[i].GetComponent<UnitBase>())
				{
					float vecDis = Vector3.Distance(target.MyTransform.position, MyTransform.position);
					if(vecDis < distanse)
					{
						distanse = vecDis;
						targetIndex = i;
					}

				}
			}
			if(target != hit[targetIndex].GetComponent<UnitBase>())
			{
				target = hit[targetIndex].GetComponent<UnitBaseReference>().unitBase;
				//Debug.Log("Chuj" + " " + i + " " + hit.Length);
				
				if(!myRender.enabled)
					myRender.enabled = true;
				
                return true;
			}
			else
			{
				//Debug.Log("Chuj 2");
				target = null;
				//target = hit[0].GetComponent<UnitBase>();
			}
            
            return false;
		}

		//Debug.Log("Chuj 3");
		return false;
	}

	public override void Setup (ProjectileSetting settings, Vector3 _moveVector)
	{
		base.Setup (settings, _moveVector);
	}

	protected override void OnTriggerEnter2D (Collider2D c)
	{
		UnitBase u = c.GetComponent<UnitBaseReference>().unitBase;
		
		if(target != null)
		{
			if (u.type == UnitType.Enemy && target == u)
			{
				if(myRender.enabled)
					myRender.enabled = false;
				Execute(u);
			}
		}
	}


	protected override void Execute (UnitBase u)
	{
		base.Execute (u);

		isAttached = true;
			
		StartCoroutine("StartDPS");

	}
	
	protected override void ReturnToPool ()
	{
		myRender.enabled = true;
		target = null;
		base.ReturnToPool ();
	}
	

	private IEnumerator StartDPS()
	{
		while(true)
		{
			yield return new WaitForSeconds(damageTime);

			if(GameMaster.gameState == GameState.RUNNING)
			{
				Splash(target);
				DamageInfo damageInfo = new DamageInfo(dps, this.settings.armoreDamage, Element.None, null, false, MyTransform.position, false);
				target.RecieveDamage(damageInfo);
				
			}
		}
	}
}
