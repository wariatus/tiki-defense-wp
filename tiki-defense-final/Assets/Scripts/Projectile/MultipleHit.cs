﻿using UnityEngine;
using System.Collections;

namespace Projectiles
{
		public class MultipleHit : ProjectileBase {

		public float ttl;						// Czas, ktory musi minac aby pocisk pwrocil do poola
		public tk2dSprite sprite;
		private Vector3 targetDirection;
		
		protected override void Start ()
		{
			base.Start();
		}
		
		protected override void Awake ()
		{
			base.Awake ();
			
			targetDirection = Vector3.zero;
		}
		
		protected override void Update ()
		{	
			if (GameMaster.gameState == GameState.RUNNING)
	        {
                MyTransform.Translate(targetDirection * moveSpeed * Time.deltaTime, Space.World);
				if(!sprite.renderer.isVisible)
					ReturnToPool();
				
			}       
		}
		
		protected override void OnTriggerEnter2D (Collider2D c)
		{
			//Debug.Log(c.name);
            if (c.gameObject.tag == "TowerCampfire")
            {
                Towers.Campfire t = c.GetComponent<TowerBaseReference>().tower as Towers.Campfire;
                if (t != null)
                {
                    BurnProjectile();
                    Debug.Log("Podpalam pocisk: " + this.name + " DMG: " + damage);
                }
            }
            else
            {
				if(c != null)
				{
	                UnitBase u = c.GetComponent<UnitBaseReference>().unitBase;
	                Execute(u);
				}
            }
		}
	
		
		protected override void OnDisable ()
		{
			base.OnDisable ();
			
			targetDirection = Vector3.zero;
		}


		public override void Setup (ProjectileSetting settings, Vector3 moveDirection)
		{
			base.Setup (settings, moveDirection);

			if(moveDirection == Vector3.zero)
			{

				if(target == null) return;

	            targetDirection = target.MyTransform.position - MyTransform.position;
				targetDirection.Normalize();
				//float angle = Vector3.Angle(MyTransform.position, target.MyTransform.position);

			

				if(targetDirection.x <= -0.5f && targetDirection.x >= -0.95f && targetDirection.y >= 0.5f && targetDirection.y <= 0.95f)
				{
					//targetDirection.x = -0.5f;
					//targetDirection.y = 0.5f;
				}
				else if(targetDirection.x >= 0.5f && targetDirection.x <= 0.95f && targetDirection.y >= 0.3f && targetDirection.y <= 0.95f)
				{
					//targetDirection.x = 0.5f;
					//targetDirection.y = 0.5f;
				}
				else if(targetDirection.x >= 0.5f && targetDirection.x <= 0.95f && targetDirection.y <= -0.5f && targetDirection.y <= 0.95f)
				{
					//targetDirection.x = 0.5f;
					//targetDirection.y = -0.5f;
				}
				else if(targetDirection.x <= -0.5f && targetDirection.x >= -0.95f && targetDirection.y <= -0.5f && targetDirection.y >= -0.95f)
				{
					//targetDirection.x = -0.5f;
					//targetDirection.y = -0.5f;
				}
				else if(targetDirection.x >= -0.1f && targetDirection.x <= 0.1f && targetDirection.y >= 0.95f && targetDirection.y <= 1.1f)
				{
					//Debug.Log("Before: " + targetDirection);

					targetDirection.x = 0;
					targetDirection.y = 1;

				}
				else if(targetDirection.x >= -0.1f && targetDirection.x <= 0.1f && targetDirection.y <= -0.95f && targetDirection.y >= -1.1f)
				{
					//Debug.Log("Before: " + targetDirection);

					targetDirection.x = 0;
					targetDirection.y = -1;

					
				}
				else if(targetDirection.x >= -1.1f && targetDirection.x <= 0.1f && targetDirection.y >= -0.2f && targetDirection.y <= 0.35f)
				{
					//Debug.Log("Before: " + targetDirection);

					targetDirection.x = -1;
					targetDirection.y = 0;
				
					
				}
				else if(targetDirection.x <= 1.1f && targetDirection.x >= 0.1f && targetDirection.y >= -0.2f && targetDirection.y <= 0.4f)
				{
					//Debug.Log("Before: " + targetDirection);

					targetDirection.x = 1;
					targetDirection.y = 0;
					
				}
			}
			else
			{
				targetDirection = moveDirection;
				//Debug.Log("chuj");
			}
			//Debug.Log("After: " + targetDirection);

		

		}



	}
		
}
