﻿ 	using UnityEngine;
using System.Collections;

public class UIButtonToggleActivate : MonoBehaviour {

	#region PUBLIC VARIABLE
	public GameObject target;

	
	#endregion

	#region PRIVATE VARIABLE

	#endregion

	#region INIT


	
	#endregion


	#region PUBLIC METHODS

	public void OnClick () 
	{ 
		if (target != null) 
		{
			if(target.activeInHierarchy)
				target.SetActive(false);
			else
				target.SetActive(true);
		}
	}
	
	#endregion

	#region PROTECTED METHODS
	
	#endregion

	#region PRIVATE METHODS

	#endregion

	#region EVENTS & NOTIFICATIONS


	#endregion
	
}
