﻿using UnityEngine;
using System.Collections;

public class UIToggleColliderActivate : MonoBehaviour {

	#region PUBLIC VARIABLE

	public Collider collider;
	
	#endregion

	#region PRIVATE VARIABLE

	#endregion

	#region INIT
	
	#endregion

	#region PUBLIC METHODS

	public void Toggle()
	{
		if(collider != null)
		{

			if(collider.enabled)
				collider.enabled = false;
			else
				collider.enabled = true;
		}

	}

    public void ActiveCollider()
    {
        collider.enabled = true;
    }

	#endregion

	#region PROTECTED METHODS
	
	
	#endregion

	#region PRIVATE METHODS

	#endregion

	#region EVENTS & NOTIFICATIONS
	

	#endregion
	
}
