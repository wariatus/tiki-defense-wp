﻿using UnityEngine;
using System.Collections;

public class Ghost : Unit {


	private Transform tikiShamanPosition;
	protected override void Awake ()
	{
		base.Awake ();
	}

	protected override void Start ()
	{
		base.Start ();
	}

	protected override void Update ()
	{
        if (this._slowsModifier.Count > 0)
        {
        

            speed.speedModifier = this._slowsModifier[0].modifier;

            for (int i = 0; i < this._slowsModifier.Count; i++)
            {
                this._slowsModifier[i].ttl -= Time.deltaTime;

                if (this._slowsModifier[i].ttl <= 0)
                    this._slowsModifier.RemoveAt(i);
            }


        }
        else if (speed.speedModifier != 0)
            speed.speedModifier = 0;

		if(GameMaster.gameState == GameState.RUNNING)
		{
			moveVector = Vector3.MoveTowards(MyTransform.position, tikiShamanPosition.position, speed.GetCurrentSpeed * Time.deltaTime);
			moveVector.z = 0;
			//moveVector.y += Mathf.Sin(0.75f * Time.deltaTime);
			MyTransform.position = moveVector;
		}
	}


	protected override void OnSpawned ()
	{
		base.OnSpawned ();

		tikiShamanPosition = GetReferences.tiki.MyTransform;
	}


}
