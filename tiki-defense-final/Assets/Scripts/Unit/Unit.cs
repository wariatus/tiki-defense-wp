using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Unit : UnitBase {
	
	public float damage;
    public UnitSpeed speed;
    public PoolObjectType poolObjectType;
 
    public Path currentPath;
	public Transform enemyIconType;
	private Waypoint currentWaypoint;
    private int counterWaypoint;

	private Vector3 jumpPosition;					// Pozycja w ktora strone powinien poruszac sie obiekt111
	protected Vector3 moveVector = Vector3.zero; 		// Wektor wyliczeniowy do poruszania
	private bool isDualDie;
	private int dieCount;
	protected List<MoveSlowData> _slowsModifier;
	/// <summary>
	/// The creep counter.
	/// </summary>
	[HideInInspector]
	public int creepCounter;
    public float distanseToFinish;
    public static float speedScale = 1;

    private float timeStart;
    private float timeStartUnscaled;
    protected override void Awake()
    {
        base.Awake();


		this._slowsModifier = new List<MoveSlowData>();
		
		if(this.isBoss || this.isElite)
		{
			tk2dSprite icon = (Instantiate(enemyIconType, Vector3.zero, Quaternion.identity) as Transform).GetComponent<tk2dSprite>();
			
			if(this.isBoss)
				icon.SetSprite("crown_gold");
			else
				icon.SetSprite("crown_silver");
				
				
			icon.transform.parent = MyTransform.FindChild("HUD/HP").transform;
			icon.transform.localPosition = new Vector3(0.6f, 0.5f, 1);
			icon.scale = new Vector3(0.5f, 0.5f, 1);
			
			BoxCollider2D col = MyTransform.FindChild("Colliders/Enemy Hit collider").GetComponent<BoxCollider2D>();
			col.center = new Vector2(0, -0.69f);
			BoxCollider2D col2d = MyTransform.FindChild("Colliders/Enemy Waypoint collider").GetComponent<BoxCollider2D>();
			col2d.size = new Vector2(0.2f, 0.2f);
		
			if(poolObjectType == PoolObjectType.Enemy_Boss_Final)
				this.isDualDie = true;

		}

    }

    protected override void Start()
    {
        base.Start();
		NotificationCenter.defaultCenter.addListener(OnBackToPoolRequest, NotificationType.BackToPool);

	


    }

    protected override void Update()
    {
        base.Update();

        if (GameMaster.gameState == GameState.RUNNING)
        {

            if (this._slowsModifier.Count > 0)
            {
                //if(this._slowsModifier.Count == 1)
                //	Debug.Log("Modfikator: " + speed.speedModifier + " Ilosc slowow: "  + this._slowsModifier.Count + " TTL1: " + this._slowsModifier[0].ttl);
                //else 
                //	Debug.Log("Modfikator: " + speed.speedModifier + " Ilosc slowow: "  + this._slowsModifier.Count + " TTL1: " + this._slowsModifier[0].ttl + "  TTL2: " + this._slowsModifier[1].ttl);


                speed.speedModifier = this._slowsModifier[0].modifier;

                for (int i = 0; i < this._slowsModifier.Count; i++)
                {
                    this._slowsModifier[i].ttl -= Time.deltaTime;

                    if (this._slowsModifier[i].ttl <= 0)
                        this._slowsModifier.RemoveAt(i);
                }


            }
            else if (speed.speedModifier != 0)
                speed.speedModifier = 0;

            if (state == UnitState.Moving && currentWaypoint != null)
            {

                moveVector = Vector3.MoveTowards(MyTransform.position, currentWaypoint.MyTransform.position, (speed.GetCurrentSpeed * speedScale) * Time.deltaTime);
        
                moveVector.z = 0;
                MyTransform.position = moveVector;
                // MyTransform.Translate(Vector3.right * speed.GetCurrentSpeed * Time.deltaTime, Space.Self);


            }
            else if (state == UnitState.Jumping && currentWaypoint != null)
            {
                jumpPosition.z = 0;
                moveVector = Vector3.MoveTowards(MyTransform.position, jumpPosition, speed.GetCurrentSpeed * Time.deltaTime);
                moveVector.z = 0;
                MyTransform.position = moveVector;

            }

        }

    }

    void FixedUpdate()
    {

        //if (state == UnitState.Moving && currentWaypoint != null)
        //{

        //    //moveVector = Vector3.MoveTowards(MyTransform.position, currentWaypoint.MyTransform.position, speed.GetCurrentSpeed * Time.fixedDeltaTime);
        //    //moveVector.z = 0;
        //    //MyTransform.position = moveVector;
        //    // MyTransform.Translate(Vector3.right * speed.GetCurrentSpeed * Time.deltaTime, Space.Self);
        //    MyRigidbody.MovePosition(MyRigidbody.position + new Vector2(speed.GetCurrentSpeed * Time.deltaTime, 0));


        //}
    }

    protected void CalculateCreepCounter()
    {
        distanseToFinish = Path.Instance.GetFinalWaypointDistanse(transform.position, currentWaypoint);
    }

    public override void Setup(Path path, Vector3 position, float healthMulti)
    {
		base.Setup(path, position, healthMulti);
		state = UnitState.Moving;
        speed.Setup(MyTransform);

        currentPath = path;
        currentWaypoint = path.waypoints[0];

		MyGameObject.SetActive(true);
		
    }

    public virtual void OnDisable()
    {
		base.OnDisable();
		speed.speedModifier = 0;
		_slowsModifier.Clear();

		state = UnitState.InPool;
        speed.Reset();
		
		currentPath = null;
        currentWaypoint = null;
        counterWaypoint = 0;
		
    }
	
	public void SetSpeedModifier(MoveSlowData slow)
	{
		if (MyGameObject.activeSelf)
		{
			if(this._slowsModifier.Count == 0)
					this._slowsModifier.Add(slow);
			else
			{
				if(this._slowsModifier.Count == 1)
				{
					if(slow.modifier >= this._slowsModifier[0].modifier)
					{
						this._slowsModifier.Add(this._slowsModifier[0]);
						this._slowsModifier[0] = slow;
					}
					else
					{
						this._slowsModifier.Add(slow);
					}
				}
				else if(this._slowsModifier.Count == 2)
				{
					if(slow.modifier >= this._slowsModifier[0].modifier)
					{
						if(this._slowsModifier[0].modifier >= this._slowsModifier[1].modifier)
							this._slowsModifier[1] = this._slowsModifier[0];

						this._slowsModifier[0] = slow;
						return;
					}
					else if(slow.modifier >= this._slowsModifier[1].modifier)
					{
						this._slowsModifier[1] = slow;
					}
				}

			}
		}

//        if (MyGameObject.activeSelf)
//        {
//			// W przypadku, gdy jest juz nalozony efekt. Sprawdzamy, czy nowy jest silniejszy
//			if(speed.speedModifier != 0)
//			{
//				if(modifier >= speed.speedModifier)
//				{
//					//Debug.Log("Chuj 1");
//					speed.speedModifier = modifier;
//					StopCoroutine("SetTTL");
//					StartCoroutine("SetTTL",  time);
//					return;
//				}
//
//			}
//			else
//			{
//				//Debug.Log("Chuj 2");
//	            speed.speedModifier = modifier;
//	            //Debug.Log(speed.speedModifier);
//	            StartCoroutine(SetTTL(time));
//			}
//        }
		
	}
	
	private IEnumerator SetTTL(float time)
	{
		yield return new WaitForSeconds(time);
		
		speed.speedModifier = 0;
	}

    public override void RecieveDamage(DamageInfo damage)
    {
     
       // Debug.Log((Time.time - timeStart) + " () " + (Time.unscaledTime - timeStartUnscaled));
        if (state == UnitState.Dying)
            return;

        //Debug.Log("UNIT " + myGameObject.name + " HIT FOR " + damage.value + " DAMAGE");

        if (damage.value > 0)
        {
            if (damage.element == Element.None)
            {
                health.ApplyDamage(damage);
            }
            else
            {
                for (int i = 0; i < elementalResists.Length; i++)
                {
                    if (elementalResists[i].element == damage.element)
                    {
                        damage.value = elementalResists[i].ReduceDamage(damage.value);
                        health.ApplyDamage(damage);
                        break;
                    }
                }
            }
        }
        else
        {
            health.ApplyDamage(damage);
        }

        if (health.CurrentHealth <= 0f)
        {


#if UNITY_IPHONE

			AchievmentManager.RaportEnemyKill();
#endif

			if(!isDualDie)
			{
				PathologicalGames.PoolManager.Pools["ParticleEffects"].Spawn(ParticleEffectType.Effect_Enemy_Die.ToString(), MyTransform.position, Quaternion.identity);
				NotificationCenter.defaultCenter.postNotification(new EnemyKilledNotification(NotificationType.EnemyKilled, this, moneyForKill));
	            Die();

			}
			else
			{
				if(dieCount == 1)
				{
					PathologicalGames.PoolManager.Pools["ParticleEffects"].Spawn(ParticleEffectType.Effect_Enemy_Die.ToString(), MyTransform.position, Quaternion.identity);
					Die();
					NotificationCenter.defaultCenter.postNotification(new EnemyKilledNotification(NotificationType.EnemyKilled, this, moneyForKill));
				}
				else
				{
					dieCount = 1;
					health.Reset();
					health.HealthBar.Reset();
					health.HealthBar.Refresh();

					Color color = this.sprite.color;
					color.a = 0.5f;
					this.sprite.color = color;
				}

			}
			#if UNITY_IPHONE
			AchievmentManager.RaportEnemyKill();
			#endif
        }


       // Debug.Log(health.CurrentHealth);
    }

    
    public void ResetAndReturnToPool()
    {
		if(PathologicalGames.PoolManager.Pools["Enemies"].IsSpawned(MyTransform))
			PathologicalGames.PoolManager.Pools["Enemies"].Despawn(MyTransform);

        sprite.scale = new Vector3(1, 1, 1);
       // PoolManager.AddItem(MyGameObject, poolObjectType);
    }

    public virtual void Die()
    {
        state = UnitState.Dying;
		SoundDieManager.Play();
        ResetAndReturnToPool();
        
    }

	public void StartJump(Vector3 jumpPositionToMove)
	{
		jumpPosition = jumpPositionToMove;
		state = UnitState.Jumping;

	}

	public void StopJump()
	{
		state = UnitState.Moving;
		jumpPosition = Vector3.zero;
	}

	public void SwitchWaypoint(int waypointID)
	{
		counterWaypoint = waypointID;
		currentWaypoint = currentPath.waypoints[waypointID];
	}

	public void SwitchWaypoint(Waypoint waypoint)
	{
    
		if (counterWaypoint < currentPath.waypoints.Length - 1)
		{
			counterWaypoint++;
			currentWaypoint = currentPath.waypoints[counterWaypoint];
        }

        if (waypoint.useFlip)
        {
            if (sprite.scale.x > 0)
                sprite.scale = new Vector3(-1, 1, 1);
            else
                sprite.scale = new Vector3(1, 1, 1);
        }

    }

	void OnBackToPoolRequest(Notification n)
	{
		if(PathologicalGames.PoolManager.Pools["Enemies"].IsSpawned(MyTransform))
			ResetAndReturnToPool();

	}

	void OnLevelWasLoaded(int level)
	{
		// Nasluchujemy czy bloker zostal klikniety
		NotificationCenter.defaultCenter.addListener(OnBackToPoolRequest, NotificationType.BackToPool);
	}

    protected override void OnSpawned()
    {
        base.OnSpawned();
        timeStart = Time.time;
        timeStartUnscaled = Time.unscaledTime;

        InvokeRepeating("CalculateCreepCounter", 0.25f, 0.25f);
    }

    protected override void OnDespawned()
    {
        base.OnDespawned();

        CancelInvoke("CalculateCreepCounter");
    }



}
