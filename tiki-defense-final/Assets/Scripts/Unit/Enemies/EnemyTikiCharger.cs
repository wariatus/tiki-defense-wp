﻿using UnityEngine;
using System.Collections;
using Drops;

public class EnemyTikiCharger : Unit {

	protected override void Awake ()
	{
		base.Awake ();
	}

	protected override void Start ()
	{
		base.Start ();
	}

	protected override void Update ()
	{
		base.Update ();
	}

	public override void Die ()
	{
		//Drop drop = PoolManager.GetItem(PoolObjectType.Drop_Tiki_Charger).GetComponent<Drop>();
		TIKICharger drop = PathologicalGames.PoolManager.Pools["Drops"].Spawn("Drop_Tiki_Charger").GetComponent<TIKICharger>();
		Vector3 pos = MyTransform.position;
		pos.y -= 0.75f;
		drop.MyTransform.position = pos;

        drop.Init();

	   

		if(GameMaster.onTutorial)
			NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.TutorialTikiUseProgress));

		base.Die ();
	}
}
