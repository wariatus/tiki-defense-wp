﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class UnitSpeed {

    public float speed;

    [HideInInspector]
    public float currentSpeed;

    [HideInInspector]
    public bool currentMoveLeft;

    [HideInInspector]
    public float speedModifier;

    public float GetCurrentSpeed
    {
        get
        {
            if (speedModifier != 0)
                return currentSpeed - (currentSpeed * (speedModifier / 100));
            else
                return currentSpeed;
        }
    }

    [HideInInspector]
    public Transform ownerTransform;
    
    public void Setup(Transform owner)
    {
        ownerTransform = owner;
        Reset();
    }

    public void Reset()
    {
        currentSpeed = speed;
        speedModifier = 0;
    }
	
	

}
