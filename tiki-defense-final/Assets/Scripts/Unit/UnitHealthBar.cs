﻿using UnityEngine;
using System.Collections;

public class UnitHealthBar : MonoBehaviour {
	
	private tk2dSlicedSprite health;
	private tk2dSlicedSprite armore;
	private tk2dSlicedSprite background;
	private float ttl = 5f;
	private float ttlOriginal;
	public GameObject contener;
	
	private float healthMaxWidth;
	private float armoreMaxWidth;
	
	private Vector2 healthDimension;
	private Vector3 armoreDimension;
	
	private UnitBase unit;

	void Awake()
	{
		health = transform.Find("Components/Health").GetComponent<tk2dSlicedSprite>();
		armore = transform.Find("Components/Armore").GetComponent<tk2dSlicedSprite>();
		background = transform.Find("Components/Background").GetComponent<tk2dSlicedSprite>();

		health.color = Color.white;
		armore.color = Color.white;
		background.color = Color.white;

		health.SetSprite("LifeBar_red");
		armore.SetSprite("LifeBar_blue");
		background.SetSprite("LifeBar_white");




		health.dimensions = new Vector2(health.dimensions.x, 5);
		armore.dimensions = new Vector2(armore.dimensions.x, 5);
		background.dimensions = new Vector2(background.dimensions.x, 5);



		health.SortingOrder = 10;
		armore.SortingOrder = 10;
		background.SortingOrder = 10;

		ttlOriginal = ttl;

	}

	// Use this for initialization
	void Start () {



		unit = transform.parent.parent.GetComponent<UnitBase>();
		transform.parent.localPosition = Vector3.zero;
		Vector3 pos = Vector3.zero;
		pos.y += unit.sprite.GetBounds().size.y / 2;
		pos.x -= 0.5f;
		transform.localPosition = pos;

		health.SetBorder(1.6f/36f, 1.78f/6f, 1.6f/36f, 1.33f/6f);
		//health.borderLeft = 0f;
		//health.borderBottom = 0f;
		//health.borderRight = 0f;
		//health.borderTop = 0f;


	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void Set(float currentHealth, float currentArmore)
	{
		// Wlaczamy paski zycia
		contener.SetActive(true);

		ttl = ttlOriginal;
		
		// Obliczamy aktualny stan hp wzgledem rozmiaru paska zycia
		healthDimension.x = currentHealth * healthMaxWidth;
		//Debug.Log(healthDimension.x + " " + currentHealth);
		healthDimension.x = Mathf.Clamp(healthDimension.x, 3.2f, healthMaxWidth);
		
		health.dimensions = healthDimension;

		if(currentHealth <= 0)
			health.gameObject.SetActive(false);
		
		// Obliczamy aktualny stan amore wzgledem rozmiaru paska armore
		armoreDimension.x = currentArmore * armoreMaxWidth;
		armoreDimension.x = Mathf.Clamp(armoreDimension.x, 0f, armoreMaxWidth);
		
		if(armoreDimension.x <= 0)
			armore.gameObject.SetActive(false);
		
		armore.dimensions = armoreDimension;
		
	}

	/// <summary>
	/// Turns the off delay. //BUGFIX: 0000047
	/// </summary>
	/// <returns>The off delay.</returns>
	private IEnumerator TurnOffDelay()
	{
		while(ttl > 0)
		{
			if(GameMaster.gameState == GameState.RUNNING)
			{
				ttl -= Time.deltaTime;
			}

			yield return new WaitForEndOfFrame();
		}

		contener.SetActive(false);
	}
	
	void OnDisable()
	{
		Reset ();
		
		
	}
	public void Reset()
	{
		// Przywracamy ustawienia defaulotwe 
		healthDimension.x = healthMaxWidth;
		health.dimensions = healthDimension;
		
		// Przywracamy ustawienia defaulotwe 
		armoreDimension.x = armoreMaxWidth;
		armore.dimensions = armoreDimension;
		
		StopCoroutine("TurnOffDelay");
	}
	public void Refresh()
	{
		health.gameObject.SetActive(true);
		healthMaxWidth = health.dimensions.x;
		healthDimension = health.dimensions;
		
		armore.gameObject.SetActive(true);
		armoreMaxWidth = armore.dimensions.x;
		armoreDimension = armore.dimensions;
		
		
		// Wylaczamy paski zycia
		contener.SetActive(false);
		
		//BUGFIX: 0000047
		StartCoroutine("TurnOffDelay");

	}

	void OnEnable()
	{
		Refresh();
	}
	
}
