using UnityEngine;
using System.Collections;

public class UnitBase : ExtendedMonoBehavior {
	
	
    public UnitHealth health;
	public int moneyForKill;
	public UnitElementalResist[] elementalResists;
	public tk2dSpriteAnimator animation;
    public tk2dSprite sprite;
	[HideInInspector]
 	public bool isSpawned;
    public Collider2D[] colliders;
	public bool runSetupOnStart;
	public UnitType type;
	public UnitState state;

    public bool isElite;
    public bool isBoss;
    public AudioClip soundOnTarget;
    
	private int clickedCount;
	protected bool useShield = true ;
	public Transform targetSpriteTag;
	private Transform targetSprite;

	protected override void Awake()
    {
        base.Awake();

		targetSprite = Instantiate(targetSpriteTag, Vector3.zero, Quaternion.identity) as Transform;
		targetSprite.transform.parent = MyTransform;
		targetSprite.transform.position = Vector3.zero;
		targetSprite.transform.localPosition = Vector3.zero;
		targetSprite.gameObject.SetActive(false);

    }

    protected override void Start()
    {
        base.Start();
		
		if(runSetupOnStart)
            Setup(null, MyTransform.position, 1);

    }

    protected override void Update()
    {
		
	}
	
	public virtual void Setup(Path path, Vector3 position, float multi)
    {
		position.y -= 0.1f;
        MyTransform.position = position;

        state = UnitState.Stopped;
        health.Setup(MyTransform, multi);
     

        for (int i = 0; i < elementalResists.Length; i++)
        {
            elementalResists[i].Setup(MyTransform);
        }

        for (int i = 0; i < colliders.Length; i++)
        {
            colliders[i].enabled = true;
        }
		
		isSpawned = true;
        
    }
	
	public virtual void OnDisable()
    {
		health.Reset();
        
        for (int i = 0; i < colliders.Length; i++)
        {
            colliders[i].enabled = false;
        }
		targetSprite.gameObject.SetActive(false);
		isSpawned = false;
	}

	protected virtual void OnSpawned()
	{
		NotificationCenter.defaultCenter.addListener(OnUnitClick, NotificationType.BlockerClick);
		NotificationCenter.defaultCenter.addListener(OnUnitClick, NotificationType.EnemyClick);

	}

	protected virtual void OnDespawned()
	{
		NotificationCenter.defaultCenter.removeListener(OnUnitClick, NotificationType.BlockerClick);
		NotificationCenter.defaultCenter.removeListener(OnUnitClick, NotificationType.EnemyClick);
	}
	
	public virtual void RecieveDamage(DamageInfo damage)
    {

       
        if (state == UnitState.Dying)
            return;

       //Debug.Log(" HIT FOR " + damage.value + " DAMAGE");

        if (damage.value > 0)
        {
            if (damage.element == Element.None)
            {
             

                health.ApplyDamage(damage);
				//Debug.Log(damage.value + " " + health.CurrentHealth + " " + health.maxHealth + " " + health.health);
            }
            else
            {
                for (int i = 0; i < elementalResists.Length; i++)
                {
                    if (elementalResists[i].element == damage.element)
                    {
                        damage.value = elementalResists[i].ReduceDamage(damage.value);
                        health.ApplyDamage(damage);
                        break;
                    }
                }
            }
        }
        else
        {
            health.ApplyDamage(damage);
        }

        if (health.CurrentHealth <= 0f)
        {

			//SaveData.tikiTotemLeft_ID5 = 1;
			if(SaveData.tikiTotemLeft_ID5 == 1 && useShield)
			{
				if(MyTransform.CompareTag("Shaman") && !damage.instantDeath)
				{
					health.currentHealth = 1;
					useShield = false;
                    Shield();
					NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.ShamanTakeDamage));
				}
				else
				{
					//Debug.Log(health.CurrentHealth + " " + health.maxHealth);
					Die();
				}


			}
			else
			{
	            //Debug.Log(health.CurrentHealth + " " + health.maxHealth);
	            Die();
			}
        }

    }
	
    public virtual void Die()
    {
        state = UnitState.Dying;

		// Tymczasowo
        Destroy(MyTransform.gameObject);
    }

    protected virtual void Shield()
    {
    }

	void OnLevelWasLoaded(int level)
	{
		//NotificationCenter.defaultCenter.addListener(OnUnitClick, NotificationType.BlockerClick);
		//NotificationCenter.defaultCenter.addListener(OnUnitClick, NotificationType.EnemyClick);
	}

	protected void OnUnitClick(Notification n)
	{
		if(!isSpawned || type == UnitType.Tiki) return;
		//Debug.Log("chuj 1");
		BlockerClickNotification noti = n as BlockerClickNotification;

		if(noti.unit == this)
		{
			//Debug.Log("chuj 2");
			clickedCount++;
			if(clickedCount == 1)
			{
				//Debug.Log("chuj 3");
				NGUITools.PlaySound(soundOnTarget, 1 ,1);
				targetSprite.localPosition = Vector3.zero; 
				targetSprite.position = Vector3.zero;
				Vector3 pos = targetSprite.position;
				pos.y += sprite.GetBounds().size.y;
				//pos.x -= sprite.GetBounds().size.x
				targetSprite.localPosition = pos;


				targetSprite.gameObject.SetActive(true);
			}
			else
			{
				//Debug.Log("chuj 4");
				clickedCount = 0;
				targetSprite.gameObject.SetActive(false);
			}
		}
		else
		{
			//Debug.Log("chuj 5");
			targetSprite.gameObject.SetActive(false);
			clickedCount = 0;
		}
	}

	
	

}
