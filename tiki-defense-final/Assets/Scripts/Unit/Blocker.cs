﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Blocker : UnitBase {

	private List<GridCell> grids;

    protected override void Awake()
    {
        base.Awake();
    }

    protected override void Start()
    {
        base.Start();

		grids = new List<GridCell>();

		MyTransform.localPosition = new Vector3(MyTransform.localPosition.x, MyTransform.localPosition.y, -14);

        BadgeWorkflow.Blockers++;

		isSpawned = true;

		NotificationCenter.defaultCenter.addListener(OnUnitClick, NotificationType.BlockerClick);
		NotificationCenter.defaultCenter.addListener(OnUnitClick, NotificationType.EnemyClick);

		StartCoroutine("BlinkCollider");

    }



	private IEnumerator BlinkCollider()
	{
		foreach(Collider2D c in colliders)
		{
			c.enabled = false;
		}

		yield return new WaitForSeconds(0.2f);

		foreach(Collider2D c in colliders)
		{
			c.enabled = true;
		}

		yield return new WaitForSeconds(1f);

		MyTransform.localPosition = new Vector3(MyTransform.localPosition.x, MyTransform.localPosition.y, -24);

	}

    protected override void Update()
    {
        base.Update();
    }

    public override void Die()
    {
		EnableGrids();

        BadgeWorkflow.Blockers--;
        NotificationCenter.defaultCenter.postNotification(new EnemyKilledNotification(NotificationType.BlockerDestroy, this, moneyForKill));
        base.Die();
    }

	private void EnableGrids()
	{
		foreach(GridCell g in grids)
		{
			g.haveBlockerOnStart = false;
		    g.isClickable = true;
		    g.StateCheckEnable = true;
		    g.ResetTagColor();
		}
	}

	private void OnTriggerEnter2D(Collider2D c)
	{
		if(c.CompareTag("Grid"))
		{
			//Debug.Log(Vector2.Distance(MyTransform.position, c.transform.position));
			if(Vector2.Distance(MyTransform.position, c.transform.position) < 1.65f)
			{
				GridCell grid = c.GetComponent<GridCell>();
				grid.haveBlockerOnStart = true;
				grids.Add(grid);
			}
		}
	}


}
