﻿using UnityEngine;

[System.Serializable]
public class UnitElementalResist {

    public Element element;

    [Range(0,100)]
    public float resistance;

    [HideInInspector]
    public float currentResistance;

    [HideInInspector]
    public Transform ownerTransform;

    public void Setup(Transform owner)
    {
        ownerTransform = owner;
        Reset();
    }

    public void Reset()
    {
        currentResistance = resistance;
    }

    public float ReduceDamage(float damage)
    {
        return damage - (damage * (int)currentResistance / 100);
    }
    
	
}
