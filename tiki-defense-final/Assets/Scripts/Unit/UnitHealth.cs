﻿using UnityEngine;

[System.Serializable]
public class UnitHealth  {

    public float health;
	public float CurrentHealth { get { return currentHealth / baseHealth; } }
    public float currentHealth;
	private float baseHealth;
    
	
	public float armoreHealth;
	public float CurrentArmoreHealth { get { return currentArmoreHealth / baseArmoreHealth; } }
	public float currentArmoreHealth;
	private float baseArmoreHealth;
	
    private float currentArmoreBuffor;
	public UnitHealthBar HealthBar
	{
		get { return healthBar;}
	}
	private UnitHealthBar healthBar;

	private float multi;

    [HideInInspector]
    public Transform ownerTransform;


   // private HealthBar healthBar;

    public void Setup(Transform owner, float _multi)
    {
        multi = _multi;
        
		SetupHealth();
		ownerTransform = owner;
		
		Reset();

        if (health > 0 && owner.FindChild("HUD/HP") != null)
        {
            healthBar = owner.FindChild("HUD/HP").GetComponent<UnitHealthBar>();

        }
        

    }

	public void RefreshHealthBar()
	{
		healthBar.Set(CurrentHealth > 0 ? CurrentHealth : 0, CurrentArmoreHealth > 0 ? CurrentArmoreHealth : 0);
	}

	public void SetupHealth()
	{
		baseHealth = health * multi;
		baseArmoreHealth = armoreHealth * multi;
	}

    public void Reset()
    {
        currentHealth = baseHealth;
        currentArmoreHealth = baseArmoreHealth;

        //if (health > 0)
          //  healthBar.Reset();
    }

    /// <summary>
    /// Dodatnia wartosc to obrazenia, ujemna to leczenie
    /// </summary>
    /// <param name="value"></param>
    public void ApplyDamage(DamageInfo info)
    {
        
		// Jezeli armore nie jest mniejszy od 0 sciagamy najpierw amore
		if(CurrentArmoreHealth > 0)
		{
			
			// Do buffora przypisujemy aktualny poziom zycia
			currentArmoreBuffor = currentArmoreHealth;

			if(info.damageValue > 0)
				// Odejmujemy armore
				currentArmoreHealth -= info.damageValue;
			else
				currentArmoreHealth -= info.value / GlobalConfiguration.armoreNormalDamageReduct;
				
			if(healthBar != null)
			{
				// Aktualizujemy ArmoreBar
				//healthBar.Set(CurrentHealth > 0 ? CurrentHealth : 0, CurrentArmoreHealth > 0 ? CurrentArmoreHealth : 0);
				RefreshHealthBar();
			}
			else if(ownerTransform.CompareTag("Shaman"))
			{
				NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.ShamanTakeDamage));
			}
		
				
			// Odejmujemy poziom dmg od zaobsorwanego armore
			info.value -=  currentArmoreBuffor;
	
		}
		/// Jezeli w dlaszym ciagu dmg ma jakas wartosc to sciagamy hp
		else if(info.value > 0)
		{	

			currentHealth -= info.value;
            
			if(healthBar != null)
			{
				// Aktualizujemy HealthBar
				//healthBar.Set(CurrentHealth > 0 ? CurrentHealth : 0, CurrentArmoreHealth > 0 ? CurrentArmoreHealth : 0);
				RefreshHealthBar();
			}
			else if(ownerTransform.CompareTag("Shaman"))
			{
				NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.ShamanTakeDamage));
			}
		}
		
		
        //ClampHealth();
		//ClampArmore();

       
    }


    private void ClampHealth()
    {
        currentHealth = Mathf.Clamp(currentHealth, 0f, baseHealth);
    }
	
	private void ClampArmore()
    {
        currentArmoreHealth = Mathf.Clamp(currentArmoreHealth, 0f, armoreHealth);
    }
}
