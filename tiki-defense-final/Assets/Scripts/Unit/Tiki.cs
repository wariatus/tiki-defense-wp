﻿using UnityEngine;
using System.Collections;

public class Tiki : UnitBase {

    
	public enum HealthSite
	{
		TOP,
		BOTTOM,
		LEFT,
		RIGHT
	}

	public HealthSite healthSite;
    public ParticleSystem particle;
    private float test;

    private float timeStart;
    private float timeStartUnscaled;
    protected override void Awake()
    {
        base.Awake();
    }

    protected override void Start()
    {
        base.Start();

        GetReferences.tiki = this;
		useShield = true;


        timeStart = Time.time;
        timeStartUnscaled = Time.unscaledTime;

    }



	protected override void Update()
    {
        base.Update();
    }

    public override void Die()
    {

        Debug.Log((Time.time - timeStart) + " () " + (Time.unscaledTime - timeStartUnscaled));
        if (type == UnitType.Tiki)
        {
            NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.GameDefeated));
        }
     
        state = UnitState.Dying;

        // Tymczasowo
        Destroy(MyTransform.gameObject);

    }

	public virtual void OnTriggerEnter2D(Collider2D c)
	{
		if(c.tag == "Enemy")
		{

           // Debug.Log((Time.realtimeSinceStartup));
           // UnityEditor.EditorApplication.isPaused = true;

			Unit u = c.gameObject.GetComponent<UnitBaseReference>().unitBase as Unit;
			DamageInfo damageInfo = new DamageInfo(u.damage, 0, Element.None, null, false, MyTransform.position, false);
			if(u.poolObjectType == PoolObjectType.Enemy_Boss_Final)
				damageInfo.instantDeath = true;

			RecieveDamage(damageInfo);
			//Debug.Log("damage");

			// W przypadku gdy tiki zginal, nie chcemy aby byl problem z wyborem pomiedzy wygrana a przegrana,gdy w tiki trafil osatni mob ze sceny
			if(health.CurrentHealth <= 0)
				return;

			// Taki maly bajer
			CameraShake.Shake();
			
			// Informjemy ze mob zostaj zdjety ze sceny po zderzyl sie z Tiki. W tym wypadku pieniedzy nie otrzymujemy
			NotificationCenter.defaultCenter.postNotification(new EnemyKilledNotification(NotificationType.EnemyKilled, u as UnitBase, 0));
			
			// Wrzucamy moba spowrotem do poola
			u.ResetAndReturnToPool();
		}

	}

    protected override void Shield()
    {
        particle.Play(true);
    }
}
