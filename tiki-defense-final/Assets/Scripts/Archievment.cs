﻿using UnityEngine;
using System.Collections;

public enum AchievmentType
{
	EnemyKill,
	TotemElementBuild,
	TotemLeftFullBuild,
	TotemRightFullBuild,
	TikiMaskUse,
}

[System.Serializable]
public class Archievment
{
	public string id;
	public AchievmentType type;
	public float valueNeeded;
	public float currentValue
	{
		get { return this._currentValue;}
		set
		{
			if(!isCompleted)
			{
				this._currentValue = value;
				this._percent = (this._currentValue / valueNeeded) * 100f;
				GameCenterManager.submitAchievement(this._percent, id, true);
			}
		}
	}

	public bool isCompleted
	{
		get { return this._percent == 100 ? true : false; }
	}

	private float _currentValue;
	private float _percent;
	
	
}
