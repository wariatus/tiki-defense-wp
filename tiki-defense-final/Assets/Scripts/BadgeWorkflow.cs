﻿using UnityEngine;
using System.Collections;

public class BadgeWorkflow : MonoBehaviour {

    /// <summary>
    /// GET/SET ilosc blokerow
    /// </summary>
    public static int Blockers
    {
        set
        {
            blockers = value;
            
            if(GameMaster.gameState == GameState.RUNNING)
            {
	            if(blockers <= 0)
	            {
	            	GUIEmblem.Instance.Show();
	            	
	    		}
	    	}
           
        }
        get
        {
            return blockers;
        }

    }

    /// <summary>
    /// Ilosc blokerow na scenie od poczatku
    /// </summary>
    private static int blockers;
  
	void Awake ()
	{
		BadgeWorkflow.Blockers = 0;
        
	}

	void Start () {

	
	}
	
	// Update is called once per frame
	void Update () {

        
	}
}
