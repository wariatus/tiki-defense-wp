﻿using UnityEngine;
using System.Collections;

public class Path : Singleton<Path> {

    public Waypoint[] waypoints;
    
    [HideInInspector]
    public Waypoint firstWaypoint;

    [HideInInspector]
    public Waypoint lastWaypoint;


    protected override void Awake()
    {
        base.Awake();

        firstWaypoint = waypoints[0];
        lastWaypoint = waypoints[waypoints.Length - 1];
    }

    protected override void Start()
    {
        base.Start();

		for(int i = 0; i < waypoints.Length; i++)
		{
			if(waypoints[i].grid != null)
			{
				Vector3 position = waypoints[i].grid.MyTransform.position;
				position.z = 0;
				switch(waypoints[i].type)
				{
					case PathType.RIGHT:
						position.y += GridCell.spriteBound.y / 2 - (GlobalConfiguration.PATH_TOP_OFFSET - 0.1f);
						waypoints[i].MyTransform.position = position;
						
					break;
				}

				waypoints[i].MyTransform.position = position;
			}
		}

    }

  	// Update is called once per frame
	void Update () {
	
	}

    void OnDrawGizmosSelected()
    {
        for (int i = 0; i < waypoints.Length; i++)
        {
            if (i + 1 < waypoints.Length)
            {
                Gizmos.color = Color.blue;
                Gizmos.DrawLine(waypoints[i].transform.position, waypoints[i + 1].transform.position);
            }
        }
    }


    private int GetWaypointIndex(Waypoint _waypoint)
    {
        for(int i = 0; i < waypoints.Length; i++)
        {
            if (waypoints[i] == _waypoint)
                return i;
        }

        throw new System.NullReferenceException("Waypoint nie zostal odnaleziony");
    }

    public float GetFinalWaypointDistanse(Vector3 _creepPosition, Waypoint _creepCurrentWaypoint)
    {

        float distanse = 0.0f;
        int creepWaypointIndex = GetWaypointIndex(_creepCurrentWaypoint);
       
        if (creepWaypointIndex != waypoints.Length - 1)
        {
            distanse += Vector3.Distance(_creepPosition, waypoints[creepWaypointIndex + 1 ].MyTransform.position);
            for (int i = creepWaypointIndex + 1; i < waypoints.Length - 1; i++)
            {

                distanse += Vector3.Distance(waypoints[i].MyTransform.position, waypoints[i + 1].MyTransform.position);
                
            }

            return distanse;
        }
        return Vector3.Distance(_creepPosition, waypoints[waypoints.Length - 1].MyTransform.position) ;
    }
     
    
}
