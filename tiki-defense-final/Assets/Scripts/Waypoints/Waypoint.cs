﻿using UnityEngine;
using System.Collections;

public class Waypoint : ExtendedMonoBehavior {

	public PathType type;
	public GridCell grid;
    public bool useFlip;
    private static string enemyTag;
    // Use this for initialization
	void Start () {

        if (string.IsNullOrEmpty(enemyTag))
            enemyTag = "Enemy";
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public virtual void OnTriggerEnter2D(Collider2D c)
	{
		if(c.CompareTag(enemyTag))
		{
			Unit u = c.GetComponent<UnitBaseReference>().unitBase as Unit;
			if(u.state == UnitState.Moving)
				u.SwitchWaypoint(this);
		}
	}
}
