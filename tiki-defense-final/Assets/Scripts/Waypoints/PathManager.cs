﻿using UnityEngine;
using System.IO;
using System.Xml.Serialization;
using System.Text;
using System.Security.Cryptography;
using System;
using System.Collections.Generic;
using System.Collections;
using PathologicalGames;

public class PathManager : Singleton<PathManager> {

    public TextAsset wavesData;

    /// <summary>
    /// Czas jaki musi uplynac aby wskoczyla kolejna fala
    /// </summary>
    public float timeBeetwenWaves;

    /// <summary>
    /// Stan PathManagera
    /// </summary>
    public PathManagerState state = PathManagerState.Stoped;

    /// <summary>
    ///  Aktualnie wrogowie na scenie
    /// </summary>
    public List<UnitBase> currentEnemies;
	
	/// <summary>
	/// Czy dany PathManager jest primary
	/// </summary>
	public bool isPrimary = true;

    /// <summary>
    /// Obiekt do ktorego przypisujemy dane z ToolWave'a
    /// </summary>
    private DTO_Waves waves;

    /// <summary>
    /// Czas liczony od rozpoczęcia gry
    /// </summary>
    private float currentTime;

    /// <summary>
    /// Aktualna fala
    /// </summary>
    public DTO_Wave currentWave;

    /// <summary>
    /// Ilosc wrogow  aktualnej fali;
    /// </summary>
    private int currentWaveEnemyCount;

    /// <summary>
    /// Referencja do sciezki, w ktorej znajduej sie PathManager
    /// </summary>
    private Path path;
	
	/// <summary>
	/// The creep coutner.
	/// </summary>
	private int creepCoutner;

	/// <summary>
	/// GET aktualny ID fali
	/// </summary>
	private int currentWaveId;


	private int enemyCountForPortal;
	/// <summary>
	/// GET maksymalna ilosc fali
	/// </summary>
	private int maxWave;

	private ParticleSystem portalParticle;

    public bool GlobalDisable;

	// Use this for initialization
	void Start () {
        
		if(GlobalConfiguration.USE_FTP_DTO_WAVES)
		{
			if(Debug.isDebugBuild)
				Debug.Log("[PathManager] Loading from WWW");

			StartCoroutine("LoadDataFromFTP");

		}
		else
		{
			// Odczytujemy dane z pliku fal
			waves = Deserialize(wavesData);

			if(Application.loadedLevelName == "Level5x09")
			{
				DTO_Wave newWave = new DTO_Wave();
				newWave.multiplier = 1;
				newWave.id = waves.wave.Count;

				DTO_Enemy newEnemy = new DTO_Enemy();
				newEnemy.type = PoolObjectType.Enemy_Boss_Final;
				newEnemy.secound = 1;
				newEnemy.isSpawned = false;

				newWave.enemy.Add(newEnemy);
				waves.wave.Add(newWave);

			}


			Setup();
		}

		GameObject particle = GameObject.FindWithTag("PortalParticle");
		if(particle != null)
			portalParticle = particle.GetComponent<ParticleSystem>();


        state = PathManagerState.Pause;
		
		// Przypisywanie referencji
		path = GetComponent<Path>();

        // Nasluchujemy, kiedy ginie jakis wrog
        NotificationCenter.defaultCenter.addListener(PathManager_OnEnemyKilled, NotificationType.EnemyKilled);

    }

	// Update is called once per frame
	void Update ()
	{

	    if (GlobalDisable) return;

        if (GameMaster.gameState == GameState.RUNNING)
        {
            if (state == PathManagerState.Running)
            {

				if(creepCoutner >= currentWave.enemy.Count)
				{
					if(portalParticle != null)
						if(portalParticle.isPlaying)
							portalParticle.Stop(true);
				}

				if (currentEnemies.Count < currentWaveEnemyCount)
                {
                    currentTime += Time.deltaTime * Unit.speedScale;
                    
                    for (int i = 0; i < currentWave.enemy.Count; i++)
                    {
						//Debug.Log("Petla: " + currentWave.enemy[i].secound + " " + currentTime);
                        if (!currentWave.enemy[i].isSpawned && (currentWave.enemy[i].secound <= currentTime))
                        {
                            creepCoutner++;


#if BLOCK_MICROTRANSACTION
                            if ( currentWaveId == 4 && Application.loadedLevelName == "Level1x03-tutorial-tiki")
                            {
                                NotificationCenter.defaultCenter.postNotification(
                                    new Notification(NotificationType.GameWon));
                                return;
                            }
#endif

                            //GameObject go = PoolManager.GetItem(currentWave.enemy[i].type);
                            //Unit enemy = go.GetComponent<Unit>();
							Unit enemy = PathologicalGames.PoolManager.Pools["Enemies"].Spawn(currentWave.enemy[i].type.ToString()).GetComponent<Unit>();
							enemy.sprite.SortingOrder = i;
							enemy.Setup(path, path.firstWaypoint.MyTransform.position, currentWave.multiplier * LevelConfigurations.GetConfiguration(Application.loadedLevelName).HPMultiplier);
							enemy.creepCounter = creepCoutner;
							
                            currentWave.enemy[i].isSpawned = true;

                            /// Dodajemy obiekt do listy aktualnych wrogow
                            currentEnemies.Add(enemy);

							//Debug.Log("List: "  + NotificationCenter.defaultCenter.GetListenersCount());

							NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.OnEnemySpawn));

                            // Notyfikacja: Nowy wrog na scenie
							if(Application.loadedLevelName == "Level1x03-tutorial-tiki" && enemy.poolObjectType == PoolObjectType.Enemy_Special_0_1)
							{
								NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.TutorialTikiUseProgress));
								GameMaster.guiOperation.TikiMaskTutorialShow();
							}
							else if(Application.loadedLevelName == "Level1x03-tutorial-tiki" && enemy.poolObjectType == PoolObjectType.Enemy_Boss_1_1)
							{
								if(currentWaveId == 3)
								{
									GameMaster.guiOperation.TikiMaskTutorialShow();
									NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.TutorialTikiUseProgress));



								}
								else if(currentWaveId == 2)
								{
									GameMaster.guiOperation.TikiMaskTutorialShow();
									NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.TutorialTikiUseProgress));
								}

								else if(currentWaveId == 4)
								{


									NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.ResetTikiEffect));
									NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.TutorialTikiUseProgress));

								
								}

							}

                        }
                    }
                }
					
            }
        }

	}

	private IEnumerator LoadDataFromFTP()
	{
		string url = "http://foreth.linuxpl.info/TikiDefense/" + Application.loadedLevelName + ".txt";

		WWW www = new WWW(url);
		//Debug.Log(url);

		yield return www;

		// Odczytujemy dane z pliku fal
		waves = Deserialize(www.text, true);
		
		Setup();

	}
    
    private void Setup()
    {
        
        StartCoroutine("StartFirstWave");	
		// TEMP
		//state = PathManagerState.Running;

        maxWave = waves.wave.Count;

        currentWaveId = 1;
		
        ConfigureWaveData(true);

        if (Debug.isDebugBuild && isPrimary)
            Debug.Log("[PathManager]: Głowny Path: " + isPrimary + " Maksymnalna ilość fal: " + waves.wave.Count);
   
    }

    private void ConfigureWaveData(bool firstWave = true)
    {
        if (firstWave)
            currentWave = waves.wave[0];
        else
		{
            currentWave = waves.wave[currentWaveId - 1];
		}

        if(Debug.isDebugBuild)
		    Debug.Log("[PathManager]: Aktualny numer fali: " + currentWave.id + " Liczba wrogów w fali: " + currentWave.enemy.Count);
		
        currentWaveEnemyCount = currentWave.enemy.Count;

        currentEnemies.Clear();
        currentTime = 0;
    }

    private void PathManager_OnEnemyKilled(Notification note)
    {
        EnemyKilledNotification n = note as EnemyKilledNotification;

		for(int i = 0; i < currentEnemies.Count; i++)
		{
			if(currentEnemies[i] == n.unitBase)
			{
				currentEnemies.Remove(n.unitBase);

				if (currentEnemies.Count == 0 && creepCoutner  == currentWaveEnemyCount)
				{
					creepCoutner = 0;
					if(currentWaveId < waves.wave.Count)
					{
						//Debug.Log(currentWaveId);
						currentWaveId++;
						ConfigureWaveData(false);
						state = PathManagerState.Pause;
						
						StartCoroutine("NewWaveStart");	
					}
					else 
					{
						// Wysylamy informacje o wygranej dla interesujacych obiektow
						if(isPrimary)
							NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.GameWon));
					}	
					
				}

				return;
			}

		}
   }

    private IEnumerator NewWaveStart()
    {


   	   float time = timeBeetwenWaves;
   	   
   	   while(time > 0)
   	   {
   	   		if(GameMaster.gameState == GameState.RUNNING)
   	   		{
   	   			time -= Time.deltaTime;
   	   		}
   	   		
   	   		yield return new WaitForEndOfFrame();
   	   }
   	   
        //yield return new WaitForSeconds(timeBeetwenWaves);
     
        state = PathManagerState.Running;
		
		if(isPrimary)
		{
			if(portalParticle != null)
				this.portalParticle.Play (true);

			// Powiadamiamy wszystkie obiekty interesujace sie zmiana fali
			NotificationCenter.defaultCenter.postNotification(new SuperNotification(NotificationType.WaveIncreased, 
																0,
																currentWaveId));

            if (currentWaveId >= waves.wave.Count)
                NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.OnFinalWave));
		}
    }

    private IEnumerator StartFirstWave()
    {

        while (GameMaster.gameState != GameState.RUNNING)
        {
            yield return new WaitForEndOfFrame();
        }



		float time = 4f;
		while(time > 0)
		{
			if(GameMaster.gameState == GameState.RUNNING)
			{
				time -= Time.deltaTime;
			}
			
			yield return new WaitForEndOfFrame();
		}
        //yield return new WaitForSeconds(4f);
       
        state = PathManagerState.Running;

       
    }

    public DTO_Waves Deserialize(TextAsset asset)
    {
        TextReader reader = new StringReader(asset.text);
        string encryptedSaveData = reader.ReadToEnd();
        string decryptedSaveData = Decrypt(encryptedSaveData);
        reader = new StringReader(decryptedSaveData);
        XmlSerializer deserializer = new XmlSerializer(typeof(DTO_Waves));

		return (DTO_Waves)deserializer.Deserialize(reader);
	}
	
	public void Deserialize(string filepath)
    {
		TextReader textReader = new StreamReader(filepath, new UTF8Encoding(false));
        string encryptSaveData = textReader.ReadToEnd();
        string decryptSaveData = Decrypt(encryptSaveData);
        //Debug.Log(decryptSaveData);
        TextReader newTextReader = new StringReader(decryptSaveData);
        XmlSerializer deserializer = new XmlSerializer(typeof(DTO_Waves));
        waves = (DTO_Waves)deserializer.Deserialize(newTextReader);

        textReader.Close();
        newTextReader.Close();
    }

	public DTO_Waves Deserialize(string filepath, bool isFromWWW)
	{
		TextReader reader = new StringReader(filepath);
		string encryptSaveData = reader.ReadToEnd();
		string decryptSaveData = Decrypt(encryptSaveData);
		//Debug.Log(decryptSaveData);
		TextReader newTextReader = new StringReader(decryptSaveData);

		XmlSerializer deserializer = new XmlSerializer(typeof(DTO_Waves));

		return (DTO_Waves)deserializer.Deserialize(newTextReader);

	}
	
	
	private string Decrypt(string data)
	{
		byte[] encryptedBytes = Convert.FromBase64String(data);

        MemoryStream ms = new MemoryStream();
        SymmetricAlgorithm alg = SymmetricAlgorithm.Create();
        byte[] rgbIV = Encoding.ASCII.GetBytes("3werdgff56sda4g9"); //16 bajtów
        byte[] key = Encoding.ASCII.GetBytes("enro3pnrneorfnseiofpnoweir2d67c1"); //32 bajty
        CryptoStream cs = new CryptoStream(ms, alg.CreateDecryptor(key, rgbIV), CryptoStreamMode.Write);
        cs.Write(encryptedBytes, 0, encryptedBytes.Length);
        cs.Close();
        return new UTF8Encoding(false).GetString(ms.ToArray());

    }
}
