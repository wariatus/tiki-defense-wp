﻿using UnityEngine;
using System.Collections;

public class UIInstantWin : ExtendedMonoBehavior {

	#region Variables
	
	#endregion

	#region Initialization

	protected override void Awake () 
	{
		base.Awake();
	}

	protected override void Start () 
	{
		base.Start();
	}

	#endregion
	
	// Update is called once per frame
	protected override void Update () {
	
		base.Update();
	}

	#region Public methods

	#endregion

	#region Protected methods

	#endregion
	
	#region Private methods

	#endregion
	
	#region Events & Notifications
	
	private void OnClick()
	{
	
		NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.GameWon));
	}	

	#endregion



}
