﻿using UnityEngine;
using System.Collections;

public class UIArrowTutorial : MonoBehaviour {

	public string keyToCheck;
	public GameObject container;
	public GameObject[] gameObjectToState;
	public bool stateForTrue  = true;
	public bool stateforFalse = false;
	// Use this for initialization
	void Start () {

		Debug.Log(EncryptedPlayerPrefs.GetInt(this.keyToCheck, 0, 1));
		if(EncryptedPlayerPrefs.GetInt(this.keyToCheck, 0, 1) == 0)
		{
			this.container.SetActive(true);
			this.SetState(stateForTrue);

		}
		else
		{
			this.container.SetActive(false);
			this.SetState(stateforFalse);
		}

	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	private void SetState(bool state)
	{
		if(gameObjectToState.Length > 0)
		{
			for(int i = 0; i < this.gameObjectToState.Length; i++)
			{
				this.gameObjectToState[i].SetActive(state);
			}
		}
	}
}
