﻿using UnityEngine;
using System.Collections;

public class GUITikiMask : ExtendedMonoBehavior {

	public TikiMaskType type;				// Typ maski
	public GameObject activeSprite;			// Referencja do aktywnego sprita
	public GameObject unactiveSprite;		// Referencja do nie aktywnego sprita
	public UIGrid grid;
	public AudioClip soundOnActive;
	private int price;
	private bool activate;
	private TIKIMaskConfiguration configuration;

	private int useCount = 0;
	public GUIWindow windowTikimaskTutoria;
    public ParticleSystem particle;

	protected override void Awake ()
	{
		base.Awake ();

        /// Pobieramy konfiguracje dla danej maski tiki
        configuration = LevelConfigurations.GetTikiMaskConfiguration(type);

        /// Przypisujemy jej cene
        price = configuration.price;

        if (SaveData.tikiMaskUnlockAll == 0 || Application.loadedLevel == 5)
        {
            switch (type)
            {
                case TikiMaskType.FIRE:
                    if (!LevelConfigurations.GetConfiguration(Application.loadedLevelName).fireTiki)
                    {
                        gameObject.SetActive(false);
                        return;
                    }
                    break;

                case TikiMaskType.EARTH:
                    if (!LevelConfigurations.GetConfiguration(Application.loadedLevelName).earthTiki)
                    {
                        gameObject.SetActive(false);
                        return;
                    }
                    break;

                case TikiMaskType.FROST:
                    if (!LevelConfigurations.GetConfiguration(Application.loadedLevelName).frostTiki)
                    {
                        gameObject.SetActive(false);
                        return;
                    }
                    break;

                case TikiMaskType.WIND:
                    if (!LevelConfigurations.GetConfiguration(Application.loadedLevelName).windTiki)
                    {
                        gameObject.SetActive(false);
                        return;
                    }
                    break;
            }
        }
	}

	// Use this for initialization
	void Start () {

		NotificationCenter.defaultCenter.addListener(OnElseClicked, NotificationType.BlockerClick);
		NotificationCenter.defaultCenter.addListener(OnElseClicked, NotificationType.EnemyClick);
		NotificationCenter.defaultCenter.addListener(OnElseClicked, NotificationType.GridClicked);

		/// Podpinamy sie pod interesujace nas eventy
		NotificationCenter.defaultCenter.addListener(OnTikiMaskUsed, NotificationType.TikimaskUsed);
		NotificationCenter.defaultCenter.addListener(OnTikiMaskClicked, NotificationType.TikiMaskClicked);

		

		if(Application.loadedLevelName == "Level1x03-tutorial-tiki")
		{
			//tikiMaskBarSmall.localPosition = new Vector3(-67, -40, 0);
			//tikiMaskChargerButton.localPosition = new Vector3(151, -81, 0);
			MyTransform.localPosition = new Vector3(0, -53, 0);
			
		}
		else
		{
			Invoke("Reposition", 1.5f);

		}

	}

	void Reposition()
	{
		grid.Reposition();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void ActivateMask()
	{
		if(SaveData.tikiMaskCharge <= 0)
		{
			///TODO: wlaczyc okno GUI gdy brakuje chargy a gracz chce uzyc maski
			return;
		}

		/// Jezeli nie jest aktywna, to aktywujemy
        if (!activate)
        {
            activate = true;
            particle.Play(true);
            NGUITools.PlaySound(soundOnActive);
        }
        else
        {

            if(!Application.loadedLevelName.Equals("Level1x03-tutorial-tiki"))
            {
                NGUITools.SetActive(activeSprite, false);
                NGUITools.SetActive(unactiveSprite, true);
                GameMaster.tikiActieType = TikiMaskType.NONE;
                /// W przeciwnym razie deaktywujemy
                activate = false;
                return;
            }
        }
			


		switch(type)
		{
			case TikiMaskType.FIRE:

				/// ustawiamy referencje do aktywnej maski tiki
				/// uzywanie tej zmiennej powinno byc poprzedzane zmiennymi wewnetrznymi
				/// np czy maska zostala kliknieta korzystajac z eventu TikiMaskClicked
				GameMaster.tikiActieType = TikiMaskType.FIRE;

			if(useCount == 0)
			{
				useCount++;

				if(GameMaster.onTutorial)
					NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.TutorialTikiUseProgress));
			}

				
			///TODO: zmiana sprite;
			/// 
			break;


			case TikiMaskType.FROST:
				GameMaster.tikiActieType = TikiMaskType.FROST;

			break;

			case TikiMaskType.EARTH:
				GameMaster.tikiActieType = TikiMaskType.EARTH;
			break;

			case TikiMaskType.WIND:
				GameMaster.tikiActieType = TikiMaskType.WIND;
			break;
		}

		/// Informujemy obiekty o tym, ze maska zostala kliknieta
		NotificationCenter.defaultCenter.postNotification(new TikiMaskNotification(NotificationType.TikiMaskClicked, type));

	}

	/// <summary>
	/// Raises the tiki mask used event.
	/// </summary>
	/// <param name="note">Note.</param>
	private void OnTikiMaskUsed(Notification note)
	{
		TikiMaskNotification n = note as TikiMaskNotification;
		activate = false;

		if(n.maskType == type)
		{

			if(GameMaster.onTutorial && windowTikimaskTutoria.tutorialTikiProgressCount == 10)
				NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.TutorialTikiUseProgress));


			// Jezeli maska zostala uzyta, uzuswamy jeden charge
			if(SaveData.tikiMaskCharge > 0)
				SaveData.tikiMaskCharge--;
					
			GameMaster.tikiActieType = TikiMaskType.NONE;

			//NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.TikiMaskChargeChanged));

		}

		NGUITools.SetActive(activeSprite, false);
		NGUITools.SetActive(unactiveSprite, true);
	}

	/// <summary>
	/// Jezeli klikniete zostanie cos innego niz wieza/tiki wylaczamytiki
	/// </summary>
	/// <param name="n">N.</param>
	private void OnElseClicked(Notification n)
	{
		NGUITools.SetActive(activeSprite, false);
		NGUITools.SetActive(unactiveSprite, true);
		GameMaster.tikiActieType = TikiMaskType.NONE;
		//Debug.Log(GameMaster.tikiActieType);
	}

	/// <summary>
	/// Raises the tiki mask clicked event.
	/// </summary>
	/// <param name="note">Note.</param>
	private void OnTikiMaskClicked(Notification note)
	{
		TikiMaskNotification n = note as TikiMaskNotification;

		/// Jezeli kliknieta zostala jak kolwiek inna maska nalezy
		/// zresetowac stany pozostalych 
		if(n.maskType  != type)
		{
			activate = false;
			NGUITools.SetActive(activeSprite, false);
			NGUITools.SetActive(unactiveSprite, true);


		}

		else
		{
			NGUITools.SetActive(activeSprite, true);
			NGUITools.SetActive(unactiveSprite, false);
		
		}


	}

	public void Unlock()
	{
		MyGameObject.SetActive(true);
		grid.repositionNow = true;
	}
}
