﻿using UnityEngine;
using System.Collections;

public class GUIEmblem : Singleton<GUIEmblem> {

	#region Variables
	
	public UITweener[] startTweeners;
	
	private UITweener[] _tweeners;
	private bool isShow;
	#endregion

	#region Initialization

	protected override void Awake () 
	{
		base.Awake();
		
		this._tweeners = GetComponentsInChildren<UITweener>(true);		
	}

	protected override void Start () 
	{
		base.Start();
	}

	#endregion
	
	// Update is called once per frame
	protected override void Update () {
	
		base.Update();
		
		if(isShow)
		{
			if(GameMaster.gameState != GameState.RUNNING)
				gameObject.SetActive(false);
		}
	}

	#region Public methods
	
	public void Show()
	{
		if(GameMaster.gameState != GameState.RUNNING) return;
	
		this.startTweeners[0].gameObject.SetActive(true);
		this.transform.localPosition = Vector3.zero;
		
		isShow = true;
		for(int i = 0; i < this._tweeners.Length; i++)
		{
			this._tweeners[i].ResetToBeginning();
			
		}
		
		for(int i =0; i < this.startTweeners.Length; i++)
		{
			this.startTweeners[i].PlayForward();
		}

     
	}

	public void Hide()
	{
		this.transform.localPosition = new Vector3(-1000, 1050, 1);
	}

	#endregion

	#region Protected methods

	#endregion
	
	#region Private methods

	#endregion
	
	#region Events & Notifications

	#endregion



}
