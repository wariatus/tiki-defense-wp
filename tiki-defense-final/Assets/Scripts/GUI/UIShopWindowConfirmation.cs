﻿using UnityEngine;
using System.Collections;

public class UIShopWindowConfirmation : Singleton<UIShopWindowConfirmation> {

	public GameObject container;
	public UILabel label;
	public UISprite detalSprite;
	private UITweener[] tweeners;

	public GameObject containerFade;
	public UILabel labelFade;
	public UISprite detalSpriteFade;
	public UITweener tweenersFade;


	// Use this for initialization
	void Start () {

		container.SetActive(true);
		tweeners = GetComponentsInChildren<UITweener>();
		container.SetActive(false);
	}
	

	public void Show()
	{
	 
		this.container.SetActive(true);
		this.ResetTweeners();
		this.RunTweeners();

	}

	public void Show(string message)
	{
		label.text = message;
		Show();
	}

	public void Show(string message, bool showDetal, string detailSpriteName =  "")
	{
		if (showDetal)
		{
			detalSprite.gameObject.SetActive(showDetal);
			detalSprite.spriteName = detailSpriteName;
		}
		Show(message);
	}

	public void ShowFade(string message)
	{
	    labelFade.text = message;

		this.containerFade.SetActive(true);
		this.RunTweenersFade();
	}

    public void CloseFade()
    {
        RunTweenersFade();
        GameMaster.gameState = GameState.RUNNING;

        Invoke("EnableMissionPanel", tweenersFade.duration);
    }

    public void EnableMissionPanel()
    {
        GameObject panel = GameObject.FindGameObjectWithTag("MissionPanel");
        if (panel != null)
        {
            foreach (Transform child in panel.transform)
            {
               if(child.name == "Mission Panel")
                   child.gameObject.SetActive(true);
            }

        }
        
    }


	public void Close()
	{
		this.container.SetActive(false);
		//GameMaster.gameState = GameState.RUNNING;
	}

	private void ResetTweeners()
	{
		foreach(UITweener tweener in tweeners)
		{
			tweener.ResetToBeginning();
		}
		
	}

	private void RunTweeners()
	{
		foreach(UITweener tweener in tweeners)
		{
			tweener.PlayForward();
		}
	}

	public void RunTweenersFade()
	{
		tweenersFade.Toggle();
	}
}
