using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GoogleFu;

public class GUIOperations : MonoBehaviour {

	public Camera guiCamera;	// Camera gui

	//Lista referencji do dostepnych okien
	public List<GUIWindow> windows;

	public GameObject tikiBarBig;
	public GameObject tikiBarSmall;
	public Transform fireTikiMask;

	public UILabel musicLabel;
    
	public Transform tikiMaskBarSmall;
	public Transform tikiMaskChargerButton;
	public Transform[] leftCoverTikiMask;
	public Transform[] rightCoverTikiMask;

	public GameObject languageBar;
	public GameObject mianMenuBar;
    public bool isBuyChargeButtonVisible;
    public GUIBuyChargeButton buyChargeButton;
	public UIPanel slotPanel;
    public GameObject mainMenuBackground;
    
    public ExtendedMonoBehavior mainPanel;

    public GameObject[] turnOffForEncyclopedy;

	private bool isPausa;
	private bool isMusic;
	private bool tutorialFirstChargeBuy;


	void Awake ()
	{
		// Przypisujemy referencje do gamemastera
		GameMaster.guiOperation = this;

		if(Application.loadedLevel >= 3 && Application.loadedLevelName != "MainMenu_After")
		{
			if(SaveData.tikiMaskUnlockAll == 1 && Application.loadedLevel != 5)
			{
				tikiBarBig.SetActive(true);
				tikiBarSmall.SetActive(false);


				for(int cover = 0; cover < 2; cover++)
				{
#if !BLOCK_MICROTRANSACTION
					leftCoverTikiMask[cover].gameObject.SetActive(true);
#endif
                    rightCoverTikiMask[cover].gameObject.SetActive(false);
				}

			}
			else
			{

				if(Application.loadedLevel > 6 && Application.loadedLevelName != "MainMenu_After")
				{

					for(int cover = 0; cover < 2; cover++)
					{
#if !BLOCK_MICROTRANSACTION
						leftCoverTikiMask[cover].gameObject.SetActive(false);
#endif
                        rightCoverTikiMask[cover].gameObject.SetActive(false);
					}

				}
				else if(Application.loadedLevel == 6)
				{

					Invoke ("DelayLeftCover", 1.5f);


				}
			}
		}
//		else
//		{
//			if(tikiBarSmall != null)
//			{
//				tikiBarSmall.SetActive(false);
//				tikiBarBig.SetActive(false);
//			}
//		}
	}

	void DelayLeftCover()
	{
		for(int cover = 0; cover < 2; cover++)
		{
#if !BLOCK_MICROTRANSACTION
			leftCoverTikiMask[cover].GetComponent<UITweener>().Toggle();
#endif
            rightCoverTikiMask[cover].gameObject.SetActive(false);
		}
	}

	// Use this for initialization
	void Start () {
	
		if(Application.loadedLevelName == "Main Menu")
		{
			NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.MainThemeMusicOn));

			SaveData.lastLevelSettingShow = 0;

            //if(EncryptedPlayerPrefs.GetInt("BACK_TO_MAIN_MENU", 0, GlobalConfiguration.saveGameSlot) != 0)
            //{
            //    languageBar.SetActive(false);
            //    mianMenuBar.SetActive(true);
            //}
		}

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void BuyFullLive()
	{

	}

	public void OnClick_ADColony()
	{
		ADManager.Instance.PlayAdColony();
		Debug.Log("Colony");
	}

	public void OnClick_ADLovin()
	{
		ADManager.Instance.PlayAdAppLovin();
		Debug.Log("AppLovin");
	}

	public void OnClick_AdVungle()
	{
		ADManager.Instance.PlayAdVungle();
		Debug.Log("Vungle");
	}

	public void OnClick_AdGlobal()
	{
		ADManager.Instance.PlayAdVideo();
		Debug.Log("Global");
	}

	public void OnClick_PausaUnapusa()
	{
		if(!isPausa)
		{
			GameMaster.gameState = GameState.PAUSA;
			isPausa = true;
		}
		else
		{
			GameMaster.gameState = GameState.RUNNING;
			isPausa = false;
		}
	}

    public void OnClick_Pausa()
    {
		if(GameMaster.gameState == GameState.GUIWINDOW) return;

		if(GameMaster.onTutorial) return;

        if(!isPausa)
        {
            GameMaster.gameState = GameState.PAUSA;
            isPausa = true;
        }
        else if(GameMaster.gameState != GameState.PAUSA)
        {
            GameMaster.gameState = GameState.RUNNING;
            isPausa = false;
        }

		if(Debug.isDebugBuild)
			Debug.Log("[GUIOperations] Game OnPause. Status: " + GameMaster.gameState);
    }



	public void OnClick_MusicOn()
	{
		
		GameMaster.canPlayMusic = true;
	
		if(GameMaster.canPlayFXSounds)
		{
			if(GameMaster.levelMusic != null)
				GameMaster.levelMusic.Play();
		
			NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.MainThemeMusicOn));
		}
	}
	
	public void OnClick_MusicOff()
	{
		GameMaster.canPlayMusic = false;
	
		if(GameMaster.levelMusic != null)
			GameMaster.levelMusic.Stop();
		
		NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.MainThemeMusicOff));
	}
	
	public void OnClick_SoundOn()
	{
		GameMaster.canPlayFXSounds = true;
	
		if(GameMaster.canPlayMusic)
			OnClick_MusicOn();
			
		
		
	}
	
	public void OnClick_SoundOff()
	{
		GameMaster.canPlayFXSounds = false;	
		
		if(GameMaster.levelMusic != null)
			GameMaster.levelMusic.Stop();
		
		NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.MainThemeMusicOff));
	}

    public void OnClick_PlayNext()
    {

		GameMaster.NextLevel();
    }

	#region GUI WINDOWS

	#region GUI TUTORIAL BUILD TOTEM
	public void BuildTotemWindowShow()
	{
		for(int i = 0; i < windows.Count; i++)
		{
			if(windows[i].type == GUIWindowType.TUTORIALBUILDTOWER)
				windows[i].MyGameObject.SetActive(true);
		}

		NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.TutorialBuildTowerOn));
	}

	public void BuildTotemWindowHide()
	{
		for(int i = 0; i < windows.Count; i++)
		{
			if(windows[i].type == GUIWindowType.TUTORIALBUILDTOWER)
				windows[i].MyGameObject.SetActive(false);
		}
	}
	
	#endregion

	#region TUTORIAL TIKI MASK

	public void TikiMaskTutorialShow()
	{
		for(int i = 0; i < windows.Count; i++)
		{
			if(windows[i].type == GUIWindowType.TUTORIALTIKIMASK)
				windows[i].MyGameObject.SetActive(true);
		}

		NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.TutorialBuildTowerOn));
		//Debug.Log("Chuj");
	}
	
	public void TikiMaskTutorialHide()
	{
		for(int i = 0; i < windows.Count; i++)
		{
			if(windows[i].type == GUIWindowType.TUTORIALTIKIMASK)
				windows[i].MyGameObject.SetActive(false);
		}
	}

	#endregion

	#region New Stuff
	public void OnCLick_NewWeaponShow()
	{
		if(GameMaster.gameState == GameState.PAUSA)
		{
			for(int i = 0; i < windows.Count; i++)
			{
				if(windows[i].type == GUIWindowType.NEWEAPONG)
				{
					windows[i].MyGameObject.SetActive(true);
					//windows[i].tweener.Toggle();

				}
			}
			
			GameMaster.gameState = GameState.GUIWINDOW;
		}
		
	}
	
	public void OnCLick_NewWeaponHide()
	{
        Invoke("CountStart", 0.75f);
	}

    private void CountStart()
    {
        for (int i = 0; i < windows.Count; i++)
        {
            if (windows[i].type == GUIWindowType.NEWEAPONG)
            {
                windows[i].MyGameObject.SetActive(false);
                //windows[i].tweener.Toggle();
            }
        }

        NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.OnCountStart));
    }

	#endregion

	#region GAME_MENU
	public void OnCLick_GAMEMENU()
	{
		if(GameMaster.gameState == GameState.RUNNING || GameMaster.gameState == GameState.PAUSA)
		{
			this.OnClick_Pausa();

			for(int i = 0; i < windows.Count; i++)
			{
				if(windows[i].type == GUIWindowType.MENUGAME)
					windows[i].MyGameObject.SetActive(true);
			}
			
			NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.OnLevelMainMenuOpen));

			GameMaster.gameState = GameState.GUIWINDOW;
		}


	}

    public void TurnOffForEncyclopedy()
    {
        for (int i = 0; i < this.turnOffForEncyclopedy.Length; i++)
        {
            this.turnOffForEncyclopedy[i].SetActive(false);            
        }
    }

    public void TurnOnForEncyclopedy()
    {
        for (int i = 0; i < this.turnOffForEncyclopedy.Length; i++)
        {
            this.turnOffForEncyclopedy[i].SetActive(true);
        }
    }
	
	public void OnClick_GAMEMENU_CLOSE()
	{
		for(int i = 0; i < windows.Count; i++)
		{
			if(windows[i].type == GUIWindowType.MENUGAME)
				windows[i].MyGameObject.SetActive(false);
		}
		//NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.PauseClicked));

	
		if(!GameMaster.onTutorial)
		{
			GameMaster.gameState = GameState.RUNNING;
			OnClick_Pausa();

		}
		
	}

	#endregion

	#region ENCYCLOPEDY
	public void OnCLick_ENCYCLOPEDY()
	{
	
        //for(int i = 0; i < windows.Count; i++)
        //{
        //    if(windows[i].type == GUIWindowType.ENCYCLOPEDY)        
        //        windows[i].MyGameObject.SetActive(true);
        //}

        //GameObject.FindGameObjectWithTag("EncyclopedyBackground").GetComponent<MeshRenderer>().enabled = true;			
        GUIEncyclopedyWindow.Instance.Show();
		GameMaster.gameState = GameState.GUIWINDOW;
		
		
		
	}
	
	public void OnClick_ENCYCLOPEDY_CLOSE()
	{
        //for(int i = 0; i < windows.Count; i++)
        //{
        //    if(windows[i].type == GUIWindowType.ENCYCLOPEDY)
        //        windows[i].MyGameObject.SetActive(false);
        //}

        //GameObject.FindGameObjectWithTag("EncyclopedyBackground").GetComponent<MeshRenderer>().enabled = false;

        GUIEncyclopedyWindow.Instance.Hide();
		GameMaster.gameState = GameState.RUNNING;
		
	}
	#endregion
	
	#region POPUP_OBSTACLE
	public void OnClick_POPUPOBSTACLE()
	{
		for(int i = 0; i < windows.Count; i++)
		{
			if(windows[i].type == GUIWindowType.OBSTACLES)
			windows[i].MyGameObject.SetActive(true);
		}
		GameMaster.gameState = GameState.GUIWINDOW;
	
	}
	
	public void OnClick_POPUPOBSTACLE_CLOSE()
	{
		for(int i = 0; i < windows.Count; i++)
		{
			if(windows[i].type == GUIWindowType.OBSTACLES)
				windows[i].MyGameObject.SetActive(false);
		}
		
		GameMaster.gameState = GameState.RUNNING;
		
	}
	#endregion
	#region POPUP_OBS
	public void OnClick_POPUPARMORED()
	{
		for(int i = 0; i < windows.Count; i++)
		{
			if(windows[i].type == GUIWindowType.ARMORED)
				windows[i].MyGameObject.SetActive(true);
		}
		GameMaster.gameState = GameState.GUIWINDOW;
	}
	
	public void OnClick_POPUPARMORED_CLOSE()
	{
		for(int i = 0; i < windows.Count; i++)
		{
			if(windows[i].type == GUIWindowType.ARMORED)
				windows[i].MyGameObject.SetActive(false);
		}
		
		GameMaster.gameState = GameState.RUNNING;
		
	}
	#endregion
	
	#region Global Shop
	public void OnClick_SHOW_GLOBAL_SHOP()
	{
        if (InternetStatus.internetAvailable)
        {

            GUIShop.Instance.ShowGlobalShop();
            NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.OnEnterToShop));
            for (int i = 0; i < this.turnOffForEncyclopedy.Length; i++)
            {
                this.turnOffForEncyclopedy[i].SetActive(false);
            }

            GameMaster.gameState = GameState.GUIWINDOW;
        }
        else
        {
            string message = Translate.Instance.GetRow(Translate.rowIds.ID_CONNECT).GetStringData(GameMaster.language);
            UIShopWindowConfirmation.Instance.ShowFade(message);
            GameMaster.gameState = GameState.GUIWINDOW;
        }
		
		
	}
	
	
	public void OnClick_CLOSE_GLOBAL_SHOP()
	{
		//GUIShop.Instance.CloseGlobalShop();
		
		for (int i = 0; i < this.turnOffForEncyclopedy.Length; i++)
		{
			this.turnOffForEncyclopedy[i].SetActive(true);            
		}
		

		GameMaster.gameState = GameState.RUNNING;
	}
	
	#endregion
	
	#region TIKIMASK_UNLOCKALL
	public void OnClick_WINDOW_TIKIMASK_UNLOCKALL()
	{
	    if (InternetStatus.internetAvailable)
	    {

	        if (GameMaster.gameState == GameState.RUNNING || GameMaster.gameState == GameState.PAUSA)
	        {
	            //for(int i = 0; i < windows.Count; i++)
	            //{
	            //	if(windows[i].type == GUIWindowType.TIKIMASKUNLOCK)
	            //	windows[i].MyGameObject.SetActive(true);
	            //}
                this.OnClick_Pausa();
	            GUIShop.Instance.ShowUnlocks();

	            GameMaster.gameState = GameState.GUIWINDOW;
	        }
	    }
	    else
	    {
            string message = Translate.Instance.GetRow(Translate.rowIds.ID_CONNECT).GetStringData(GameMaster.language);
            UIShopWindowConfirmation.Instance.ShowFade(message);
            GameMaster.gameState = GameState.GUIWINDOW;
           
	    }
	}

	public void OnClick_Window_TIKIMASK_CLOSE()
	{
		for(int i = 0; i < windows.Count; i++)
		{
			if(windows[i].type == GUIWindowType.TIKIMASKUNLOCK)
				windows[i].MyGameObject.SetActive(false);
		}

        GUIShop.Instance.CloseUnlocks();
        GameMaster.gameState = GameState.RUNNING;

	}
    //public void OnClick_Window_TIKIMASK_BUY()
    //{
    //    for(int i = 0; i < windows.Count; i++)
    //    {
    //        if(windows[i].type == GUIWindowType.TIKIMASKUNLOCK)
    //            windows[i].MyGameObject.SetActive(false);
    //    }

    //    SaveData.tikiMaskUnlockAll = 1;

    //    tikiBarBig.SetActive(true);
    //    tikiBarSmall.SetActive(false);

    //    for(int cover = 0; cover < 2; cover++)
    //    {
    //        leftCoverTikiMask[cover].gameObject.SetActive(true);
    //        leftCoverTikiMask[cover].GetComponent<UITweener>().PlayReverse();

    //    }
    //}
	#endregion

	#region TIKIMASK_CHARGE_ADD

	public void OnClick_WINDOW_TIKIMASK_CHARGE_SHOW_BUTTON()
	{
	}

    public void OnClikc_WINDOW_TIKIMASK_CHARGE_SHOW_SHOP()
    {
        if (InternetStatus.internetAvailable)
        {
            if (!GameMaster.onTutorial)
            {
                this.OnClick_Pausa();
                GUIShop.Instance.ShowGems();

            }
        }
        else
        {
            GameMaster.gameState = GameState.GUIWINDOW;
            string message = Translate.Instance.GetRow(Translate.rowIds.ID_CONNECT).GetStringData(GameMaster.language);
            UIShopWindowConfirmation.Instance.ShowFade(message);
        }
    }

	public void OnClick_WINDOW_TIKIMASK_CHARGE_ADD()
	{
       // Debug.Log(GameMaster.onTutorial + " " +  tutorialFirstChargeBuy);
		if(GameMaster.onTutorial && !tutorialFirstChargeBuy)
		{
			tutorialFirstChargeBuy = true;
			NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.TutorialTikiUseProgress));
			NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.TutorialTikiUseProgress));
			return;
		}
		else if (!GameMaster.onTutorial && SaveData.tikiMaskCharge < (SaveData.tikiTotemRight_ID3 == 1 ? 4 : 3))
        {
            //if(SaveData.premium >= GlobalConfiguration.chargeCost)
            if (Unibiller.DebitBalance("gems", GlobalConfiguration.chargeCost))
            {
                if (GameMaster.gameState == GameState.RUNNING || GameMaster.gameState == GameState.PAUSA)
                {
                    //				for(int i = 0; i < windows.Count; i++)
                    //				{
                    //					if(windows[i].type == GUIWindowType.TIKIMASKCHARGEADD)
                    //						windows[i].MyGameObject.SetActive(true);
                    //					else
                    //						windows[i].MyGameObject.SetActive(false);
                    //				}

                    //SaveData.premium -= GlobalConfiguration.chargeCost;
                    NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.PremiumChanged));
                    SaveData.tikiMaskCharge++;
                    

                }
            }
            else
            {
                if (Debug.isDebugBuild)
                    Debug.Log("[GUIOperations]: brak dostepnej waluty premium");


                if(InternetStatus.internetAvailable)
                    GUIShop.Instance.ShowGems();
                else
                {
                    GameMaster.gameState = GameState.GUIWINDOW;
                    string message = Translate.Instance.GetRow(Translate.rowIds.ID_CONNECT).GetStringData(GameMaster.language);
                    UIShopWindowConfirmation.Instance.ShowFade(message);
                }
                //IOSMessage dialog = IOSMessage.Create("Nie masz monet", "Zabij wrogow, aby pozyskac zloto!");
            }
        }
	}
	
	public void OnClick_WINDOW_TIKIMASK_CHARGE_CLOSE()
	{
		for(int i = 0; i < windows.Count; i++)
		{
			if(windows[i].type == GUIWindowType.TIKIMASKCHARGEADD)
				windows[i].MyGameObject.SetActive(false);
		}
		
	}
	public void OnClick_WINDOW_TIKIMASK_CHARGE_BUY()
	{
		for(int i = 0; i < windows.Count; i++)
		{
			if(windows[i].type == GUIWindowType.TIKIMASKCHARGEADD)
				windows[i].MyGameObject.SetActive(false);
		}

		//SaveData.premium -= GlobalConfiguration.chargeCost;
		Unibiller.DebitBalance("gems", GlobalConfiguration.chargeCost);
		SaveData.tikiMaskCharge++;
	}
	#endregion


	#endregion

    public void OnClick_Replay()
    {

//		if(!LiveManager.Instance.CanBePlayed())
//		{
//			UILiveManager.Instance.Open();
//			return;
//		}

		// Na wszelki wypadek ustawiamy TimeScale na 1
		Time.timeScale = 1;

		GA.API.Design.NewEvent("Level: " + Application.loadedLevelName + " restarted");
        GameMaster.ResetLevel();

    }

    public void OnClick_SlotSelect1()
    {
        GlobalConfiguration.saveGameSlot = 1;
		if(Debug.isDebugBuild)
        	Debug.Log("[GameWorkflow]: GameSlot: 1");
    }

    public void OnClick_SlotSelect2()
    {
        GlobalConfiguration.saveGameSlot = 2;
		if(Debug.isDebugBuild)
        	Debug.Log("[GameWorkflow]: GameSlot: 2");
    }

    public void OnClick_SlotSelect3()
    {
        GlobalConfiguration.saveGameSlot = 3;
		if(Debug.isDebugBuild)
        	Debug.Log("[GameWorkflow]: GameSlot: 3");
    }

	public void OnClick_PolishSelect()
	{
		SaveData.defaultLanguage = "PL";
        Invoke("OpenMainMenu", 0.5f);
		if(Debug.isDebugBuild)
			Debug.Log("[GameWorkflow]: Language set to: PL");


	}

	public void OnClick_EnglishSelect()
	{
		SaveData.defaultLanguage = "EN";
        Invoke("OpenMainMenu", 0.5f);
		if(Debug.isDebugBuild)
			Debug.Log("[GameWorkflow]: Language set to: EN");

	}

    public void ShowLanguageBar()
    {
        Invoke("OpenLanguageBar", 0.5f);

    }

    private void OpenLanguageBar()
    {
        languageBar.GetComponent<TweenAlpha>().Toggle();
        languageBar.SetActive(true);
    }
        

    private void OpenMainMenu()
    {
        mianMenuBar.GetComponent<TweenAlpha>().Toggle();
        mianMenuBar.SetActive(true);
    }

	public void OnClick_GermanSelect()
	{
		SaveData.defaultLanguage = "DE";
        Invoke("OpenMainMenu", 0.5f);
		if(Debug.isDebugBuild)
			Debug.Log("[GameWorkflow]: Language set to: DE");
	}

	public void OnClick_RunMainMenu()
	{
		SaveData.levelToLoad = "Main Menu";
		LoadingScreen.Instance.Show();
	}
	public void OnClick_RunMainMenuAfter()
	{
		SaveData.levelToLoad = "MainMenu_After";
		LoadingScreen.Instance.Show();
	}
	
	
	public void OnClick_RunMissionSelectScene()
	{
		GameMaster.DespawnPools();
		NotificationCenter.defaultCenter.ClearListener();

		if(Application.loadedLevelName == "Main Menu" || Application.loadedLevelName == "MainMenu_After")
		{
			EncryptedPlayerPrefs.SetInt("BACK_TO_MAIN_MENU", 1, GlobalConfiguration.saveGameSlot);
            SaveData.lastLevelSettingShow = 0;
            PreviewLabs.PlayerPrefs.Flush();
		}
		else if(Application.loadedLevel >= 3)
		{
			LiveManager.Instance.DecreaseLive();
		}


		SaveData.levelToLoad = "Mission Select";
		LoadingScreen.Instance.Show();
	
	}

	public void OpenSlotPanel()
	{
		//Camera.main.backgroundColor = Color.black;
		//mainMenuBackground.SetActive(tr);
		slotPanel.gameObject.SetActive(true);

		//StartCoroutine("RunAlphaPanel");
	}

	private IEnumerator RunAlphaPanel()
	{
		yield return new WaitForSeconds(1);
		float time = 2.5f;
		float currentTime = 0;
		float counter = time;
		while((counter -=  Time.deltaTime) > 0)
		{
			currentTime += Time.deltaTime;
			slotPanel.alpha = currentTime / time;

			yield return new WaitForEndOfFrame();
		}
	}

	public void OnClick_RunIslandScene()
	{
		Application.LoadLevel("Island");	
	}


	public void OnClick_MoveCameraRightBy1000px()
	{
		guiCamera.transform.localPosition = new Vector3(guiCamera.transform.localPosition.x  + 1000f, 0, 0);
	}

	public void OnClick_MoveCameraLeftBy1000px()
	{
		guiCamera.transform.localPosition = new Vector3(guiCamera.transform.localPosition.x  - 1000f, 0, 0);
	}

    public void AllTikiMaskUnlock()
    {
        SaveData.tikiMaskUnlockAll = 1;

        tikiBarBig.SetActive(true);
        tikiBarSmall.SetActive(false);

        for (int cover = 0; cover < 2; cover++)
        {
            leftCoverTikiMask[cover].gameObject.SetActive(true);
            leftCoverTikiMask[cover].GetComponent<UITweener>().PlayReverse();

        }
        
        NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.OnShopBuyTIKIUnlock));

        OnClick_Window_TIKIMASK_CLOSE();
    }

	void OnApplicationPause(bool pauseStatus) {

		if(pauseStatus)
		{
			if(Application.loadedLevel != 3 &&Application.loadedLevel != 5)
				this.OnCLick_GAMEMENU();
		}
	}
}
