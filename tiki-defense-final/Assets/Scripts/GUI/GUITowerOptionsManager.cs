﻿using UnityEngine;
using System.Collections;


/// <summary>
/// GUI tower options manager. TODO: ZUNIFIKOWAC~Z GRIDTOWERMANAGER
/// </summary>
public class GUITowerOptionsManager : ExtendedMonoBehavior {

	public float swiftOffset;
	
	public GUITowerOption[] options;
	public GUIGridManager gridManager;
	public Camera guiCamera;
	public AudioClip SoundOnClick;
    public GameObject firstArrowTutorial;
    public GameObject secoundArrowTutorial;

	public bool isShow;
	private Towers.TowerBase actualTower;			// Aktualnie wybrana wieza
	private bool enableButtons;
	private bool attackBlocker;
	private UnitBase blockerToAttack;
	
	[HideInInspector]
	public bool firstTutorialClickEnabled;
	
	protected override void Awake ()
	{
		base.Awake ();
	}
	
	// Use this for initialization
	protected override void Start () {
	
		base.Start();
		NotificationCenter.defaultCenter.addListener(OnTowerClicked, NotificationType.TowerClicked);
		NotificationCenter.defaultCenter.addListener(OnEnemyClicked, NotificationType.EnemyClick);
		NotificationCenter.defaultCenter.addListener(OnBlockerCLicked, NotificationType.BlockerClick);
		NotificationCenter.defaultCenter.addListener(OnCountStart, NotificationType.OnGameStateChanged);
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void DisableTowersGUI()
	{
		for(int i = 0; i < options.Length; i++)
		{
			options[i].isShow = false;
			options[i].tweener.ResetToBeginning();
			options[i].transform.localScale = Vector3.zero;
		}

		if(actualTower != null)
			actualTower.HideRange();
		GameMaster.guiButtonShowUp = false;

		isShow = false;
			
	}

	protected virtual void OnEnemyClicked(Notification n)
	{
		attackBlocker = false;
		blockerToAttack = null;
	}

	void OnBlockerCLicked(Notification n)
	{
		DisableTowersGUI();
		gridManager.DisableTowersGUI();


	}
	
	void OnTowerClicked(Notification n)
	{
		if(firstTutorialClickEnabled && GameMaster.onTutorial) return;
		if(!enableButtons) return;
	
		if(Application.loadedLevelName.Equals("Level1x03-tutorial-tiki"))
			return;
        else if (Application.loadedLevelName.Equals("Level1x01"))
        {
            firstArrowTutorial.SetActive(false);
            secoundArrowTutorial.SetActive(true);
        }

		if(isShow || GUIGridManager.isShow)
		{
			if(!GameMaster.onTutorial)
			{
				DisableTowersGUI();
				gridManager.DisableTowersGUI();
			}
            return;
		}

		TowerClickedNotification notification = n as TowerClickedNotification;
		

		
		if(notification.tower != null)
		{	
			actualTower = notification.tower;

			Vector3 pos = mainCamera.WorldToScreenPoint(notification.position);
			pos.z = 0;
			pos.y += 25;
            MyTransform.position = guiCamera.ScreenToWorldPoint(pos);
			
			NGUITools.PlaySound(SoundOnClick);
			
			StartCoroutine(ShowTowers(notification.tower, notification.position));
		}
		
	}
	
	private void EnableButtons()
	{
		this.enableButtons = true;
		
	}
	
	private void OnCountStart(Notification n)
	{
		if(Application.loadedLevelName == "Level1x01")
			enableButtons = true;
		else
			Invoke("EnableButtons", 3);
	}
	
	private IEnumerator ShowTowers(Towers.TowerBase tower, Vector3 position)
	{
		if(!isShow)
		{
			isShow = true;
			int optionCount = options.Length;

			tower.ShowRange();

			while(optionCount-- > 0)
			{
				options[optionCount].transform.localPosition = new Vector3(-(swiftOffset /2 - (optionCount * swiftOffset)), 0, 0);
				options[optionCount].gameObject.SetActive(true);
				options[optionCount].Show(tower);	
				
				yield return new WaitForSeconds(0.1f);
				
			}
		}
		
	}
}
