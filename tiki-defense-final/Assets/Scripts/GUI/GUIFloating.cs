﻿using UnityEngine;
using System.Collections;
using System.Text;

public class GUIFloating : MonoBehaviour {

	public string creepMoney;
	public string towerMoney;
	public string towerSellMoney;

	// Use this for initialization
	void Start () {

#if UNITY_EDITOR
		OnLevelWasLoaded(0);
#endif
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTowerManagment(Notification n)
	{
		SuperNotification noti = n as SuperNotification;
		//Debug.Log(" GUIFloating TOwer 0" );
		if(noti == null) return;
		//Debug.Log(" GUIFloating TOwer 1");
		if(noti.isTower)
		{
			if(noti.varInt < 0)
			{
				GUIFloatingItem item = PathologicalGames.PoolManager.Pools[creepMoney].Spawn(towerMoney).GetComponent<GUIFloatingItem>();
				item.Show(noti.position, noti.varInt);
			}
			else
			{
				GUIFloatingItem item = PathologicalGames.PoolManager.Pools[creepMoney].Spawn(towerSellMoney).GetComponent<GUIFloatingItem>();
				item.Show(noti.position, noti.varInt);
			}
		}

	}

	void OnEnemyKilled(Notification n)
	{
		EnemyKilledNotification noti = n as EnemyKilledNotification;
		//Debug.Log(" GUIFloating enemy 0");
		if(noti.moneyForKill == 0) return;
		//Debug.Log(" GUIFloating enemy 1");

		GUIFloatingItem item = PathologicalGames.PoolManager.Pools[creepMoney].Spawn(creepMoney).GetComponent<GUIFloatingItem>();

		item.Show(noti.unitBase.MyTransform.position, noti.moneyForKill);
	}

	void OnEnable()
	{

	}

	void OnDisable()
	{
	
	}

	void OnLevelWasLoaded(int level)
	{
		NotificationCenter.defaultCenter.removeListener(OnEnemyKilled, NotificationType.EnemyKilled);
		NotificationCenter.defaultCenter.removeListener(OnTowerManagment, NotificationType.MoneyCountChanged);
		NotificationCenter.defaultCenter.removeListener(OnEnemyKilled, NotificationType.BlockerDestroy);
		NotificationCenter.defaultCenter.addListener(OnEnemyKilled, NotificationType.EnemyKilled);
		NotificationCenter.defaultCenter.addListener(OnTowerManagment, NotificationType.MoneyCountChanged);
		NotificationCenter.defaultCenter.addListener(OnEnemyKilled, NotificationType.BlockerDestroy);
	}
}
