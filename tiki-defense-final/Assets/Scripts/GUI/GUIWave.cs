﻿using UnityEngine;
using System.Collections;

public class GUIWave : MonoBehaviour {
	
	public UILabel currentLabel;
	public UILabel maxLabel;
    public GameObject normalObject;
    public UILabel countLabel;
    public float time;

    private float _time;
	// Use this for initialization
	void Start () {
		
		maxLabel.text =  LevelConfigurations.GetConfiguration(Application.loadedLevelName).maxWaves.ToString("00");
		
		NotificationCenter.defaultCenter.addListener(OnNewWave, NotificationType.WaveIncreased);
        NotificationCenter.defaultCenter.addListener(OnCountStart, NotificationType.OnCountStart);

      
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnNewWave(Notification n)
	{
		SuperNotification notification = n as SuperNotification;
		currentLabel.text = notification.varInt.ToString("00");
	}

    private void OnCountStart(Notification n)
    {
       // normalObject.SetActive(false);
        //countLabel.gameObject.SetActive(true);

        StartCoroutine("CountStart");
    }

    private IEnumerator CountStart()
    {  _time = time;

        while (_time > 0)
        {
        	if(GameMaster.gameState == GameState.RUNNING)
				_time -= Time.deltaTime;

			countLabel.text = Mathf.FloorToInt(_time).ToString();
			yield return new WaitForEndOfFrame();
		}
		
		normalObject.SetActive(true);
        countLabel.gameObject.SetActive(false);

        _time = time;

    }
}
