﻿using UnityEngine;
using System.Collections;
using System.Text;

public class GUIFloatingItem : MonoBehaviour {

	public UILabel label;
	public UITweener[] tweener;



	private Transform _transform;

    private Camera cameraGUI
    {
        get
        {
            if(this._cameraGUI == null)
                this._cameraGUI = GameObject.FindWithTag("GUICamera").GetComponent<Camera>();

            return this._cameraGUI;
        }
    }
    private Camera cameraMain
    {
        get
        {
            if (this._cameraMain == null)
                this._cameraMain = Camera.main;

            return this._cameraMain;
        }
    }



    

    private Camera _cameraMain;
    private Camera _cameraGUI;

	void Awake()
	{
		_transform = GetComponent<Transform>();
	}

	// Use this for initialization
	void Start () {



	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	public void Show(Vector3 position, int money)
	{
		Vector3 pos = cameraMain.WorldToViewportPoint(position);
		pos.z = 0;
		Vector3 fixPos = cameraGUI.ViewportToWorldPoint(pos);

		_transform.localScale = new Vector3(1, 1, 1);
		_transform.position = fixPos;


		StringBuilder sb = new StringBuilder(money > 0 ? "+" : "");
		sb.Append(money);
        sb = sb.Replace(@"\t|\n|\r", "");
		label.text = sb.ToString();
		
		for(int i = 0; i < tweener.Length; i++)
		{
			tweener[i].ResetToBeginning();
			tweener[i].PlayForward();
		}

		//Debug.Log("Plakietka: " + pos);

		Invoke("BackToPool", 2f);
	}

	private void BackToPool()
	{
		PathologicalGames.PoolManager.Pools["Creep Money"].Despawn(_transform);
	}



}
