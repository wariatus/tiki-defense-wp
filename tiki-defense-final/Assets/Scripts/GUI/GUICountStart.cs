﻿using UnityEngine;
using System.Collections;

public class GUICountStart : ExtendedMonoBehavior {

	#region PUBLIC VARIABLE

	public UITweener firstTweener;
	public UITweener secoundTweener;
	public UITweener thirthTweener;
	
    public UISpriteAnimation sprite;
	public UISprite fade;
	#endregion

	#region PRIVATE VARIABLE

	#endregion

	#region INIT

	protected override void Awake ()
	{
		base.Awake ();
		NotificationCenter.defaultCenter.addListener(OnCountStart, NotificationType.OnCountStart);
	}


	protected override void Start ()
	{
		base.Start ();
	}

	protected override void Update ()
	{
		base.Update ();
	}

	#endregion


	#region PUBLIC METHODS

	#endregion

	#region PROTECTED METHODS
	
	
	
	#endregion

	#region PRIVATE METHODS

	private IEnumerator StartCount()
	{
		fade.gameObject.SetActive(true);
		
		firstTweener.PlayForward();
		
		yield return new WaitForSeconds(0.75f);

		secoundTweener.PlayForward();
		
		yield return new WaitForSeconds(0.75f);

		thirthTweener.PlayForward();

		yield return new WaitForSeconds(0.75f);

		gameObject.SetActive(false);
		fade.gameObject.SetActive(false);
        sprite.gameObject.SetActive(false);
	}

	#endregion

	#region EVENTS & NOTIFICATIONS

	private void OnCountStart(Notification n)
	{
		//label.enabled = true;
        sprite.gameObject.SetActive(true);
		StartCoroutine("StartCount");
		
	}


	#endregion
	
}
