﻿using UnityEngine;
using System.Collections;

public class GUILabelFinalWave : ExtendedMonoBehavior {

	#region Variables

    public UITweener tweener;
	#endregion

	#region Initialization

	protected override void Awake () 
	{
		base.Awake();

        NotificationCenter.defaultCenter.addListener(OnFinalWeave, NotificationType.OnFinalWave);
	}

	protected override void Start () 
	{
		base.Start();
	}

	#endregion
	
	// Update is called once per frame
	protected override void Update () {
	
		base.Update();
	}

	#region Public methods

	#endregion

	#region Protected methods

	#endregion
	
	#region Private methods

	#endregion
	
	#region Events & Notifications
    private void OnFinalWeave(Notification n)
    {
        if (Application.loadedLevelName.Equals("Level1x03-tutorial-tiki")) return;

        tweener.ResetToBeginning();
        tweener.PlayForward();
    }
	#endregion



}
