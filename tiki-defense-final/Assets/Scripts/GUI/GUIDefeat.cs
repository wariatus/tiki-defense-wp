﻿using UnityEngine;
using System.Collections;

public class GUIDefeat : MonoBehaviour {

	public GUIGridManager towerManager;
	public GUITowerOptionsManager optionTowerManager;

	// Use this for initialization
	void Start () {

		GetReferences.defeatWindow = this;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Show()
	{
		towerManager.DisableTowersGUI();
		optionTowerManager.DisableTowersGUI();

		transform.localPosition = new Vector3(0, -121f, 0);	



	}
}
