﻿using UnityEngine;
using System.Collections;
using GoogleFu;
using System.Collections.Generic;
using Prime31;

public class GUIWindow : ExtendedMonoBehavior {

	public GUIWindowType type;

	public GameObject tuturialBuildTowerDesc1;
	public GameObject tuturialBuildTowerDesc2;
	public GameObject tuturialBuildTowerDesc3;
	public GameObject tuturialBuildTowerDesc4;

	public GameObject[] tikiMaskTutorial;
	public UITweener tikiMaskBar;
	public UITweener tikiCharger;
	public UILabel gameCounterLabel;


	#region NewWeapon

	public GameObject newWeapon_spriteTowers;
	public GameObject newWeapon_spriteMask;

	public UISprite[] newWeapon_towerSprite;
	public UILabel newWeapon_title;
	public UILabel newWeapon_descriptio;

	public PoolObjectType newWeapon_PoolType;
	public UITweener tweener;

	#endregion

	public List<GameObject> deactivations;

	/// <summary>
	/// Oryginalna wartosc time scale
	/// </summary>
	private float timeScaleOriginal;
	private GameState gameStateOriginal;

	private int tutorialBuildProgressCount;

	public int tutorialTikiProgressCount;
	protected override void Awake ()
	{
		base.Awake ();
	}

	protected override void Start ()
	{
		base.Start ();

		if(type == GUIWindowType.TUTORIALBUILDTOWER)
			NotificationCenter.defaultCenter.addListener(OnTutorialProgress, NotificationType.TutorialBuildTowerProgress);
		else if(type == GUIWindowType.TUTORIALTIKIMASK)
			NotificationCenter.defaultCenter.addListener(OnTutorialTikiProgress, NotificationType.TutorialTikiUseProgress);


	}

	protected override void Update ()
	{

	}

	void OnEnable()
	{
		for(int i = 0; i < deactivations.Count; i++)
		{
			deactivations[i].SetActive(false);
		}

		gameStateOriginal = GameMaster.gameState;

		if(type != GUIWindowType.TUTORIALBUILDTOWER && type != GUIWindowType.TUTORIALTIKIMASK)
			GameMaster.gameState = GameState.GUIWINDOW;

		newWeapon_PoolType = LevelConfigurations.GetConfiguration(Application.loadedLevelName).newTypeOfWeapon;


		if(newWeapon_PoolType == PoolObjectType.Null || type != GUIWindowType.NEWEAPONG) return;

		if(newWeapon_PoolType == PoolObjectType.EarthTikiMask || newWeapon_PoolType == PoolObjectType.FireTikiMask || 
		   newWeapon_PoolType == PoolObjectType.AirTikiMask || newWeapon_PoolType == PoolObjectType.IceTikiMask)
		{


			newWeapon_spriteTowers.SetActive(false);
			newWeapon_spriteMask.SetActive(true);
			UISprite maskSprite = newWeapon_spriteMask.GetComponent<UISprite>();
			switch(newWeapon_PoolType)
			{


				case PoolObjectType.AirTikiMask:	
					maskSprite.spriteName = "maski_air_active";
					//newWeapon_towerSprite[1].spriteName = "ice_totem_1_off";
					//newWeapon_towerSprite[2].spriteName = "ice_totem_2_off";
					
					newWeapon_title.text = Translate.Instance.GetRow(Translate.rowIds.ID_TIKI_MASK_AIR).GetStringData(GameMaster.language);
					newWeapon_descriptio.text = Translate.Instance.GetRow(Translate.rowIds.ID_TIKI_MASK_AIR_INFO).GetStringData(GameMaster.language);
					
					break;
					
				case PoolObjectType.EarthTikiMask:	
					maskSprite.spriteName = "maski_earth_active";
					//newWeapon_towerSprite[1].spriteName = "ice_totem_1_off";
					//newWeapon_towerSprite[2].spriteName = "ice_totem_2_off";
					
					newWeapon_title.text = Translate.Instance.GetRow(Translate.rowIds.ID_TIKI_MASK_EARTH).GetStringData(GameMaster.language);
					newWeapon_descriptio.text = Translate.Instance.GetRow(Translate.rowIds.ID_TIKI_MASK_EARTH_INFO).GetStringData(GameMaster.language);
					
					break;
					
				case PoolObjectType.FireTikiMask:	
					maskSprite.spriteName = "maski_fire_active";
					//newWeapon_towerSprite[1].spriteName = "ice_totem_1_off";
					//newWeapon_towerSprite[2].spriteName = "ice_totem_2_off";
					
					newWeapon_title.text = Translate.Instance.GetRow(Translate.rowIds.ID_TIKI_MASK_FIRE).GetStringData(GameMaster.language);
					newWeapon_descriptio.text = Translate.Instance.GetRow(Translate.rowIds.ID_TIKI_MASK_FIRE_INFO).GetStringData(GameMaster.language);
					
					break;
					
				case PoolObjectType.IceTikiMask:	
					maskSprite.spriteName = "maski_water_active";

					//newWeapon_towerSprite[1].spriteName = "ice_totem_1_off";
					//newWeapon_towerSprite[2].spriteName = "ice_totem_2_off";
					
					newWeapon_title.text = Translate.Instance.GetRow(Translate.rowIds.ID_TIKI_MASK_ICE).GetStringData(GameMaster.language);
					newWeapon_descriptio.text = Translate.Instance.GetRow(Translate.rowIds.ID_TIKI_MASK_ICE_INFO).GetStringData(GameMaster.language);
					
					break;
			}
		}
		else
		{

			newWeapon_spriteTowers.SetActive(true);
			newWeapon_spriteMask.SetActive(false);

			switch(newWeapon_PoolType)
			{
				case PoolObjectType.Tower_Totem_level_1:	
					newWeapon_towerSprite[0].spriteName = "turret_earthtotem";
					newWeapon_towerSprite[1].spriteName = "turret_earthtotem_upgrade2";
					newWeapon_towerSprite[2].spriteName = "turret_earthtotem_upgrade3";

					newWeapon_title.text = Translate.Instance.GetRow(Translate.rowIds.ID_EARTH_TOTEM).GetStringData(GameMaster.language);
					newWeapon_descriptio.text = Translate.Instance.GetRow(Translate.rowIds.ID_EARTH_TOTEM_INFO).GetStringData(GameMaster.language);

					break;

			case PoolObjectType.Totem_AirTotem_level_1:	
					newWeapon_towerSprite[0].spriteName = "turret_thundercloud";
					newWeapon_towerSprite[1].spriteName = "turret_thundercloud_upgrade2";
					newWeapon_towerSprite[2].spriteName = "turret_thundercloud_upgrade3";
					
					newWeapon_title.text = Translate.Instance.GetRow(Translate.rowIds.ID_AIR_TOTEM).GetStringData(GameMaster.language);
					newWeapon_descriptio.text = Translate.Instance.GetRow(Translate.rowIds.ID_AIR_TOTEM_INFO).GetStringData(GameMaster.language);
				
				break;

				case PoolObjectType.Tower_VooDoo_level_1:	
					newWeapon_towerSprite[0].spriteName = "turret_alchemyvial";
					newWeapon_towerSprite[1].spriteName = "turret_alchemyvial_upgrade1";
					newWeapon_towerSprite[2].spriteName = "turret_alchemyvial_upgrade2";
					
					newWeapon_title.text = Translate.Instance.GetRow(Translate.rowIds.ID_ALCHEMY_VIAL).GetStringData(GameMaster.language);
					newWeapon_descriptio.text = Translate.Instance.GetRow(Translate.rowIds.ID_ALCHEMY_VIAL_INFO).GetStringData(GameMaster.language);
					
					break;

				case PoolObjectType.Tower_Tornado_level_1:	
					newWeapon_towerSprite[0].spriteName = "cloud_0_off";
					newWeapon_towerSprite[1].spriteName = "cloud_1_off";
					newWeapon_towerSprite[2].spriteName = "cloud_2_off";
					
					newWeapon_title.text = Translate.Instance.GetRow(Translate.rowIds.ID_THUNDERCLOUD).GetStringData(GameMaster.language);
			    	newWeapon_descriptio.text = Translate.Instance.GetRow(Translate.rowIds.ID_THUNDERCLOUD_INFO).GetStringData(GameMaster.language);
					
					break;

/*				case PoolObjectType.Totem_Cauldron_level_1:	
					newWeapon_towerSprite[0].spriteName = "cloud_0_off";
					newWeapon_towerSprite[1].spriteName = "cloud_1_off";
					newWeapon_towerSprite[2].spriteName = "cloud_2_off";
					
					newWeapon_title.text = Translate.Instance.GetRow(Translate.rowIds.ID_CALUDRON).GetStringData(GameMaster.language);
					newWeapon_descriptio.text = Translate.Instance.GetRow(Translate.rowIds.ID_CAULDRON_INFO).GetStringData(GameMaster.language);
					
					break;*/

				case PoolObjectType.Totem_FireTotem_level_1:	
					newWeapon_towerSprite[0].spriteName = "fire_totem_0";
					newWeapon_towerSprite[1].spriteName = "fire_totem_1";
					newWeapon_towerSprite[2].spriteName = "fire_totem_2";
					
					newWeapon_title.text = Translate.Instance.GetRow(Translate.rowIds.ID_FIRE_TOTEM).GetStringData(GameMaster.language);
					newWeapon_descriptio.text = Translate.Instance.GetRow(Translate.rowIds.ID_FIRE_TOTEM_INFO).GetStringData(GameMaster.language);
					
					break;

				case PoolObjectType.Totem_MagicShot_level_1:	
					newWeapon_towerSprite[0].spriteName = "magic_wand_u0_0";
					newWeapon_towerSprite[1].spriteName = "magic_wand_u1_0";
					newWeapon_towerSprite[2].spriteName = "magic_wand_u2_0";
					
					newWeapon_title.text = Translate.Instance.GetRow(Translate.rowIds.ID_MAGIC_SHOT).GetStringData(GameMaster.language);
					newWeapon_descriptio.text = Translate.Instance.GetRow(Translate.rowIds.ID_MAGIC_SHOT_INFO).GetStringData(GameMaster.language);
					
					break;

				case PoolObjectType.Totem_IceTotem_level_1:	
					newWeapon_towerSprite[0].spriteName = "ice_totem_0_off";
					newWeapon_towerSprite[1].spriteName = "ice_totem_1_off";
					newWeapon_towerSprite[2].spriteName = "ice_totem_2_off";
					
					newWeapon_title.text = Translate.Instance.GetRow(Translate.rowIds.ID_ICE_TOTEM).GetStringData(GameMaster.language);
					newWeapon_descriptio.text = Translate.Instance.GetRow(Translate.rowIds.ID_ICE_TOTEM_INFO).GetStringData(GameMaster.language);
					
					break;

				case PoolObjectType.Totem_Melter_level_1:	
					newWeapon_towerSprite[0].spriteName = "lizard_u0_0";
					newWeapon_towerSprite[1].spriteName = "lizard_u1_0";
					newWeapon_towerSprite[2].spriteName = "lizard_u2_0";
					
					newWeapon_title.text = Translate.Instance.GetRow(Translate.rowIds.ID_LIZARD).GetStringData(GameMaster.language);
					newWeapon_descriptio.text = Translate.Instance.GetRow(Translate.rowIds.ID_LIZARD_INFO).GetStringData(GameMaster.language);
					
					break;

				case PoolObjectType.Totem_RockHammer_level_1:	
					newWeapon_towerSprite[0].spriteName = "3bigstones_u0";
					newWeapon_towerSprite[1].spriteName = "3bigstones_u1";
					newWeapon_towerSprite[2].spriteName = "3bigstones_u2";
					
					newWeapon_title.text = Translate.Instance.GetRow(Translate.rowIds.ID_THREE_BIG_STONES).GetStringData(GameMaster.language);
					newWeapon_descriptio.text = Translate.Instance.GetRow(Translate.rowIds.ID_THREE_BIG_STONES_INFO).GetStringData(GameMaster.language);
					
					break;

				case PoolObjectType.Totem_Drum_level_1:	
					newWeapon_towerSprite[0].spriteName = "tamtam_u0_0";
					newWeapon_towerSprite[1].spriteName = "tamtam_u1_0";
					newWeapon_towerSprite[2].spriteName = "tamtam_u2_0";
					
					newWeapon_title.text = Translate.Instance.GetRow(Translate.rowIds.ID_DRUMS).GetStringData(GameMaster.language);
					newWeapon_descriptio.text = Translate.Instance.GetRow(Translate.rowIds.ID_DRUMS_INFO).GetStringData(GameMaster.language);
					
					break;

				case PoolObjectType.Tower_Cave_level_1:	
					newWeapon_towerSprite[0].spriteName = "flute_u0_1";
					newWeapon_towerSprite[1].spriteName = "flute_u1_1";
					newWeapon_towerSprite[2].spriteName = "flute_u2_1";
					
					newWeapon_title.text = Translate.Instance.GetRow(Translate.rowIds.ID_PAN_FLUTE).GetStringData(GameMaster.language);
					newWeapon_descriptio.text = Translate.Instance.GetRow(Translate.rowIds.ID_PAN_FLUTE_INFO).GetStringData(GameMaster.language);
					
					break;

				case PoolObjectType.Totem_Necklace_level_1:	
					newWeapon_towerSprite[0].spriteName = "necklace_u0";
					newWeapon_towerSprite[1].spriteName = "necklace_u1";
					newWeapon_towerSprite[2].spriteName = "necklace_u2";
					
					newWeapon_title.text = Translate.Instance.GetRow(Translate.rowIds.ID_NECKLACE).GetStringData(GameMaster.language);
					newWeapon_descriptio.text = Translate.Instance.GetRow(Translate.rowIds.ID_NECKLACE_INFO).GetStringData(GameMaster.language);
					
					break;

				case PoolObjectType.Totem_Anvil_level_1:	
					newWeapon_towerSprite[0].spriteName = "anvil_u0";
					newWeapon_towerSprite[1].spriteName = "anvil_u1";
					newWeapon_towerSprite[2].spriteName = "anvil_u2";
					
					newWeapon_title.text = Translate.Instance.GetRow(Translate.rowIds.ID_ANVIL).GetStringData(GameMaster.language);
					newWeapon_descriptio.text = Translate.Instance.GetRow(Translate.rowIds.ID_ANVIL_INFO).GetStringData(GameMaster.language);
					
					break;

				case PoolObjectType.Totem_Cauldron_level_1:	
					newWeapon_towerSprite[0].spriteName = "kociolek_u0_00";
					newWeapon_towerSprite[1].spriteName = "kociolek_u1_01";
					newWeapon_towerSprite[2].spriteName = "kociolek_u2_01";
					
					newWeapon_title.text = Translate.Instance.GetRow(Translate.rowIds.ID_CALUDRON).GetStringData(GameMaster.language);
					newWeapon_descriptio.text = Translate.Instance.GetRow(Translate.rowIds.ID_CAULDRON_INFO).GetStringData(GameMaster.language);
					
					break;
					
				case PoolObjectType.Tower_Runic_level_1:	
					newWeapon_towerSprite[0].spriteName = "runestone_u0";
					newWeapon_towerSprite[1].spriteName = "runestone_u1";
					newWeapon_towerSprite[2].spriteName = "runestone_u2";
						
					newWeapon_title.text = Translate.Instance.GetRow(Translate.rowIds.ID_RUNESTONE).GetStringData(GameMaster.language);
					newWeapon_descriptio.text = Translate.Instance.GetRow(Translate.rowIds.ID_RUNESTONE_INFO).GetStringData(GameMaster.language);
					
					break;
					

		



			
			}
		}

		//timeScaleOriginal = Time.timeScale;
		//Time.timeScale = 0;
	}

	void OnDisable()
	{
		for(int i = 0; i < deactivations.Count; i++)
		{
			deactivations[i].SetActive(true);
		}

		GameMaster.gameState = gameStateOriginal;

		if(type == GUIWindowType.TUTORIALBUILDTOWER)
			NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.TutorialBuildTowerOff));

		//Time.timeScale = timeScaleOriginal;
	}

	private void OnTutorialProgress(Notification n)
	{
		tutorialBuildProgressCount++;

		if(tutorialBuildProgressCount == 1)
		{
			tuturialBuildTowerDesc1.SetActive(false);
			tuturialBuildTowerDesc2.SetActive(true);
		}
		else if(tutorialBuildProgressCount == 2)
		{
			tuturialBuildTowerDesc1.SetActive(false);
			tuturialBuildTowerDesc2.SetActive(false);
			tuturialBuildTowerDesc3.SetActive(true);
		}
		else
		{
			tuturialBuildTowerDesc1.SetActive(false);
			tuturialBuildTowerDesc2.SetActive(false);
			tuturialBuildTowerDesc3.SetActive(false);
			tuturialBuildTowerDesc4.SetActive(true);
		}

	}

	private void OnTutorialTikiProgress(Notification n)
	{


		tutorialTikiProgressCount++;
		if(tutorialTikiProgressCount == 1)
			GameMaster.guiOperation.TikiMaskTutorialShow();

		if(tutorialTikiProgressCount == 2 || tutorialTikiProgressCount == 6 )
		{
			gameObject.SetActive(false);
			return;
		}
		else if(tutorialTikiProgressCount == 3)
		{
			GameMaster.gameState = gameStateOriginal;
		}

		if(tutorialTikiProgressCount == 4)
		{


			GameMaster.guiOperation.TikiMaskTutorialShow();
			GameMaster.guiOperation.fireTikiMask.GetComponent<UITweener>().Toggle();
			//tikiMaskBar.gameObject.SetActive(true);
			//tikiMaskBar.Toggle();
		}

	   

		if(tutorialTikiProgressCount == 7)
		{



            GameMaster.guiOperation.TikiMaskTutorialShow();
			//gameCounterLabel.text = "1000";
            //SaveData.addPremium = GlobalConfiguration.chargeCost;
			tikiCharger.Toggle();
			//Debug.Log("chuj 1");
		}

		if(tutorialTikiProgressCount == 8)
		{
			
			//Debug.Log("chuj 2");
		}

		if(tutorialTikiProgressCount == 9)
		{
            SaveData.removePremium = GlobalConfiguration.chargeCost;
			SaveData.tikiMaskCharge++;
			//Debug.Log("chuj 3");
		}
		if(tutorialTikiProgressCount == 11)
		{
			//Debug.Log("chuj 4");
			//gameCounterLabel.text = "0000";

			GameMaster.guiOperation.TikiMaskTutorialHide();
		}

		//Debug.Log(tutorialTikiProgressCount);

		if(tutorialTikiProgressCount >= 10)
			return;

		for(int i = 0; i < tikiMaskTutorial.Length; i++)
		{
		
			if(i == tutorialTikiProgressCount)
			{
				tikiMaskTutorial[i].SetActive(true);
			}
			else 
				tikiMaskTutorial[i].SetActive(false);
		}
	}

    public void TikiTutorialBuildOff()
    {

        NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.TutorialBuildTowerOff));
    }
	public void TikiTutorialNext()
	{
		OnTutorialTikiProgress(null);
	}
}
