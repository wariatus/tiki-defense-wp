﻿using UnityEngine;
using System.Collections;

public class UILiveManager : Singleton<UILiveManager> {

	public GameObject container;

	protected override void Awake ()
	{
		base.Awake ();
	}

	protected override void Start ()
	{
		base.Start ();
	}

	protected override void Update ()
	{
		base.Update ();
	}


	public void Open()
	{
		container.SetActive(true);
	}

	public void Close()
	{
		container.SetActive(false);
	}

	public void Buy()
	{
		if(Unibiller.DebitBalance("gems", GlobalConfiguration.LIVE_RESTART_COST))
		{
			NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.PremiumChanged));

			LiveManager.Instance.RestartLive();

			this.Close();
		}
		else
		{
			GUIShop.Instance.ShowGems();
		}
	}
}
