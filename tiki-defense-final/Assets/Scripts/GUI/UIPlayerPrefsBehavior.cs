﻿using UnityEngine;
using System.Collections;

public class UIPlayerPrefsBehavior : MonoBehaviour {

	public string keyToCheck;
	public UIButton button;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	void OnEnable()
	{
		if(EncryptedPlayerPrefs.GetInt(keyToCheck, 0, 1) == 0)
			button.enabled = false;
		else
			button.enabled = true;
	}
}
