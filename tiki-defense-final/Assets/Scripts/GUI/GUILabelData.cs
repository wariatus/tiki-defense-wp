﻿using UnityEngine;
using System.Collections;

public class GUILabelData : MonoBehaviour {

	public GUILabelDataType type;
	private UILabel label;
	// Use this for initialization
	void Awake () {

		/// Pobieramy referencje do labela
		label = GetComponent<UILabel>();


		switch(type)
		{
			case GUILabelDataType.CHARGES:
				NotificationCenter.defaultCenter.addListener(OnTIKIChargeChanged, NotificationType.TikiMaskChargeChanged);
				label.text = "Charges: " + SaveData.tikiMaskCharge;
			break;
			case GUILabelDataType.STARCOUNT:
				NotificationCenter.defaultCenter.addListener(OnStarCountChanged, NotificationType.StarCountChanged);
			break;
			case GUILabelDataType.SLOTSTARCOUNT:
				label.text = SaveData.GetSlotStar(1).ToString() + "/" + GlobalConfiguration.MAX_STARS.ToString();
			break;
			case GUILabelDataType.SLOTSTARCOUNT2:
				label.text = SaveData.GetSlotStar(2).ToString() + "/" + GlobalConfiguration.MAX_STARS.ToString();
			break;
			case GUILabelDataType.SLOTSTARCOUNT3:
				label.text = SaveData.GetSlotStar(3).ToString() + "/" + GlobalConfiguration.MAX_STARS.ToString();
			break;
			case GUILabelDataType.PREMIUMWOOD:
				NotificationCenter.defaultCenter.addListener(OnPremiumWoodChanged, NotificationType.PremiumWoodChange);
				label.text = SaveData.premiumWood.ToString();
			break;
			case GUILabelDataType.SHOWSTARSBEACH:
				label.text = SaveData.settingStarsBeach.ToString();
			break;
			case GUILabelDataType.SHOWSTARSJUNGLE:
				label.text = SaveData.settingStarsJungle.ToString();
			break;
			case GUILabelDataType.SHOWSTARSVILLAGE:
				label.text = SaveData.settingStarsVillage.ToString();
			break;
			case GUILabelDataType.SHOWSTARSHILLS:
				label.text = SaveData.settingStarsHills.ToString();
			break;
			case GUILabelDataType.SHOWSTARSVULCANO:
				label.text = SaveData.settingStarsVulcano.ToString();
			break;

		}

	
	}

	void Start()
	{
		switch(type)
		{
			case GUILabelDataType.PREMIUM_GEMS:
				label.text = Unibiller.GetCurrencyBalance("gems").ToString();
				NotificationCenter.defaultCenter.addListener(OnPremiumChanged, NotificationType.PremiumChanged);
			break;
			case GUILabelDataType.LIVE_DISPLAY:
			label.text = LiveManager.Instance.CurrentLiveCount.ToString();
			NotificationCenter.defaultCenter.addListener(OnLiveChanged, NotificationType.OnLiveChanged);
			break;
		}
	}


	// Update is called once per frame
	void Update () {

		switch(type)
		{
			case GUILabelDataType.LIVE_CURRENT_TIME:
			if(LiveManager.Instance.CurrentLiveCount < GlobalConfiguration.LIVE_MAX)
			{
				if(!label.enabled) label.enabled = true;

				System.TimeSpan time = System.TimeSpan.FromSeconds(LiveManager.Instance.GetTimeToFullLive());

				label.text = string.Format("{0:D2}M:{1:D2}S",
				                           time.Minutes,
				                           time.Seconds);
			}
			else
			{
				if(label.enabled) label.enabled = false;
			}
			break;
			case GUILabelDataType.LIVE_DISPLAY:
			label.text = LiveManager.Instance.CurrentLiveCount.ToString();
			break;

		}
	
	}

	void OnEnable() {
		if(type == GUILabelDataType.PREMIUM_GEMS)
			label.text = SaveData.premium.ToString();
	}

	private void OnPremiumChanged(Notification n)
	{
		label.text = SaveData.premium.ToString();
	}

	private void OnLiveChanged(Notification n)
	{
		label.text = LiveManager.Instance.CurrentLiveCount.ToString();
	}

	/// <summary>
	/// Raises the TIKI charge changed event.
	/// </summary>
	/// <param name="n">N.</param>
	private void OnTIKIChargeChanged(Notification n)
	{
		label.text = "Charges: " + SaveData.tikiMaskCharge;
	}

	private void OnStarCountChanged(Notification n )
	{
		SuperNotification note = n as SuperNotification;

		int star = int.Parse(label.text);
		star += note.varInt;
		label.text = star.ToString();	
		//Debug.Log(star);
		// Zapisujemy informacje o wszystkich wiazdach 
		SaveData.starsAllCount = star;

	}

	private void OnPremiumWoodChanged(Notification n)
	{
		SuperNotification note = n as SuperNotification;

		label.text = note.varInt.ToString();
	}

	void OnApplicationPause(bool pauseStatus)
	{
		switch(type)
		{
			case GUILabelDataType.LIVE_DISPLAY:
			label.text = LiveManager.Instance.CurrentLiveCount.ToString();
			break;
		}

	}

}
