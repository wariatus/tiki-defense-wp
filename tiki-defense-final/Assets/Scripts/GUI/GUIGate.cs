﻿using UnityEngine;
using System.Collections;

public class GUIGate : MonoBehaviour {

	public string previousLevel;

	public GameObject lockedGate;
	public GameObject unlockedGate;
	public bool requireTotems;
	// Use this for initialization
	void Start () {
	
		if(!requireTotems)
		{
			if(SaveData.GetLevelStars(previousLevel) > 0)
			{
				lockedGate.SetActive(false);
				unlockedGate.SetActive(true);
			}
		}
		else
		{
			InvokeRepeating("ShowTotems", 1f, 1f);	
				
		}
		

	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	private void ShowTotems()
	{
	
		if(SaveData.GetLevelStars(previousLevel) > 0)
		{
			if(SaveData.tikiTotemRight_ID6 == 1 && SaveData.tikiTotemLeft_ID6 == 1)
			{
				lockedGate.SetActive(false);
				unlockedGate.SetActive(true);
			}
		}
	}
}
