﻿using UnityEngine;
using System.Collections;
using GoogleFu;

public class GUILabelTranslate : MonoBehaviour {

	public Translate.rowIds id;
	private UILabel label;

	// Use this for initialization
	void Start () {

		NotificationCenter.defaultCenter.addListener(OnLanguageChanged, NotificationType.OnLanguageChanged);
		//Debug.Log(GameMaster.language + " " + Translate.Instance.GetRow(id).GetStringData(GameMaster.language) + " " + GlobalConfiguration.saveGameSlot);
		label = GetComponent<UILabel>();
		label.text = Translate.Instance.GetRow(id).GetStringData(GameMaster.language);

	
	}

	void OnLanguageChanged(Notification n)
	{
		label.text = Translate.Instance.GetRow(id).GetStringData(GameMaster.language);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
