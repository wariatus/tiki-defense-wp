﻿using UnityEngine;
using System.Collections;

public class GUITikiTotem : MonoBehaviour {

	public GUITikiTotemItem[] totemLevel;

	private GUISettingChange settingManager;
	private UIImageButton button;

	private int _level;

	void Awake ()
	{
		button = GetComponent<UIImageButton>();

	}


	// Use this for initialization
	void Start () {

		settingManager = GameObject.FindObjectOfType<GUISettingChange>();

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void SetLevel(int level)
	{
		if(level > _level)
			_level = level;

		if(_level >= 6)
			gameObject.SetActive(false);
	}

	void OnClick()
	{
		if(SaveData.premiumWood == 0)
		{
			//settingManager.ShowNoMoreWoodWindow();
            GUIShop.Instance.ShowWood();
			return;
		}

		if(_level <= 6)
		{
			SaveData.premiumWood--;
			
			totemLevel[_level == 0 ? 0 : _level ].Execute();
			_level++;
			if(_level >= 6)
				gameObject.SetActive(false);
			SaveData.Save();
		}
		else
		{
			gameObject.SetActive(false);
			Debug.Log("Maks poziom");
		}

	}
}
