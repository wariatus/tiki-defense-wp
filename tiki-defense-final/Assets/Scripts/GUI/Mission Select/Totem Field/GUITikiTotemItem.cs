﻿using UnityEngine;
using System.Collections;
using GoogleFu;

public class GUITikiTotemItem : MonoBehaviour {

	public TIKITotemBonusType type;
	public UILabel labelDescription;
	public UILabel labelDescriptionGlobal;
	public TweenScale tweenScale;
	public GUITikiTotem totem;

	private UISprite sprite;

	void Awake()
	{
		//SaveData.premiumWood = 12;

		sprite = GetComponent<UISprite>();

		switch(type)
		{
		case TIKITotemBonusType.LEFT_ID1:
			if(SaveData.tikiTotemLeft_ID1 == 0)
				Disable();
			else
				Enable(1);
			break;
		case TIKITotemBonusType.LEFT_ID2:
			if(SaveData.tikiTotemLeft_ID2 == 0)
				Disable();
			else
				Enable(2);
			break;
		case TIKITotemBonusType.LEFT_ID3:
			if(SaveData.tikiTotemLeft_ID3 == 0)
				Disable();
			else
				Enable(3);
			break;
		case TIKITotemBonusType.LEFT_ID4:
			if(SaveData.tikiTotemLeft_ID4 == 0)
				Disable();
			else
				Enable(4);
			break;
		case TIKITotemBonusType.LEFT_ID5:
			if(SaveData.tikiTotemLeft_ID5 == 0)
				Disable();
			else
				Enable(5);
			break;
		case TIKITotemBonusType.LEFT_ID6:
			if(SaveData.tikiTotemLeft_ID6 == 0)
				Disable();
			else
				Enable(6);
			break;
			
		case TIKITotemBonusType.RIGHT_ID1:
			if(SaveData.tikiTotemRight_ID1 == 0)
				Disable();
			else
				Enable(1);
			break;
		case TIKITotemBonusType.RIGHT_ID2:
			if(SaveData.tikiTotemRight_ID2 == 0)
				Disable();
			else
				Enable(2);
			break;
		case TIKITotemBonusType.RIGHT_ID3:
			if(SaveData.tikiTotemRight_ID3 == 0)
				Disable();
			else
				Enable(3);
			break;
		case TIKITotemBonusType.RIGHT_ID4:
			if(SaveData.tikiTotemRight_ID4 == 0)
				Disable();
			else
				Enable(4);
			break;
		case TIKITotemBonusType.RIGHT_ID5:
			if(SaveData.tikiTotemRight_ID5 == 0)
				Disable();
			else
				Enable(5);
			break;
		case TIKITotemBonusType.RIGHT_ID6:
			if(SaveData.tikiTotemRight_ID6 == 0)
				Disable();
			else
				Enable(6);
			break;
			
		}

		NotificationCenter.defaultCenter.addListener(OnTotemClick, NotificationType.TotemClick);
		
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	private string TranslateMe()
	{
		switch(type)
		{
		case TIKITotemBonusType.LEFT_ID1:
				return Translate.Instance.GetRow(Translate.rowIds.ID_BLOCK_1).GetStringData(GameMaster.language);
			break;
		case TIKITotemBonusType.LEFT_ID2:
			return Translate.Instance.GetRow(Translate.rowIds.ID_BLOCK_2).GetStringData(GameMaster.language);
			break;
		case TIKITotemBonusType.LEFT_ID3:
			return Translate.Instance.GetRow(Translate.rowIds.ID_BLOCK_3).GetStringData(GameMaster.language);
			break;
		case TIKITotemBonusType.LEFT_ID4:
			return Translate.Instance.GetRow(Translate.rowIds.ID_BLOCK_4).GetStringData(GameMaster.language);
			break;
		case TIKITotemBonusType.LEFT_ID5:
			return Translate.Instance.GetRow(Translate.rowIds.ID_BLOCK_5).GetStringData(GameMaster.language);
			break;
		case TIKITotemBonusType.LEFT_ID6:
			return Translate.Instance.GetRow(Translate.rowIds.ID_BLOCK_6).GetStringData(GameMaster.language);
			break;
		case TIKITotemBonusType.RIGHT_ID1:
			return Translate.Instance.GetRow(Translate.rowIds.ID_BLOCK_7).GetStringData(GameMaster.language);
			break;
		case TIKITotemBonusType.RIGHT_ID2:
			return Translate.Instance.GetRow(Translate.rowIds.ID_BLOCK_8).GetStringData(GameMaster.language);
			break;
		case TIKITotemBonusType.RIGHT_ID3:
			return Translate.Instance.GetRow(Translate.rowIds.ID_BLOCK_9).GetStringData(GameMaster.language);
			break;
		case TIKITotemBonusType.RIGHT_ID4:
			return Translate.Instance.GetRow(Translate.rowIds.ID_BLOCK_10).GetStringData(GameMaster.language);
			break;
		case TIKITotemBonusType.RIGHT_ID5:
			return Translate.Instance.GetRow(Translate.rowIds.ID_BLOCK_12).GetStringData(GameMaster.language);
			break;
		case TIKITotemBonusType.RIGHT_ID6:
			return Translate.Instance.GetRow(Translate.rowIds.ID_BLOCK_11).GetStringData(GameMaster.language);
			break;

		}

		return string.Empty;
	}

	void OnClick()
	{
		if(SaveData.plainTutorial == 0) return;

		NotificationCenter.defaultCenter.postNotification(new TotemNotification(NotificationType.TotemClick, this));
		this.labelDescriptionGlobal.text = this.TranslateMe();
		this.tweenScale.enabled = true;
		this.tweenScale.ResetToBeginning();
		this.tweenScale.Play();


	}

	private void OnTotemClick(Notification n)
	{
		TotemNotification note = n as TotemNotification;


		if( note.totemItem == null || note.totemItem != this)
		{
			this.transform.localScale = new Vector3(1, 1, 1);
			this.labelDescriptionGlobal.text = "";
			this.tweenScale.enabled = false;
		}
	}

	void Disable()
	{
		//sprite.enabled = false;
		sprite.color = new Color(1, 1, 1, 0.5f);
		labelDescription.color = new Color(0,0,0, 0.5f);
	}

	void Enable(int level)
	{
		Enable();

		totem.SetLevel(level);
	}

	void Enable()
	{
		//sprite.enabled = true;
		sprite.color = new Color(1, 1, 1, 1f);
		labelDescription.color = new Color(0,0 ,0, 1f);

	}

	public void Execute()
	{
		switch(type)
		{
			case TIKITotemBonusType.LEFT_ID1:
				SaveData.tikiTotemLeft_ID1 = 1;
			break;
			case TIKITotemBonusType.LEFT_ID2:
				SaveData.tikiTotemLeft_ID2 = 1;
			break;
			case TIKITotemBonusType.LEFT_ID3:
				SaveData.tikiTotemLeft_ID3 = 1;
			break;
			case TIKITotemBonusType.LEFT_ID4:
				SaveData.tikiTotemLeft_ID4 = 1;
			break;
			case TIKITotemBonusType.LEFT_ID5:
				SaveData.tikiTotemLeft_ID5 = 1;
			break;
			case TIKITotemBonusType.LEFT_ID6:
				SaveData.tikiTotemLeft_ID6 = 1;
			break;

			case TIKITotemBonusType.RIGHT_ID1:
				SaveData.tikiTotemRight_ID1 = 1;
			break;
			case TIKITotemBonusType.RIGHT_ID2:
				SaveData.tikiTotemRight_ID2 = 1;
			break;
			case TIKITotemBonusType.RIGHT_ID3:
				SaveData.tikiTotemRight_ID3 = 1;
			break;
			case TIKITotemBonusType.RIGHT_ID4:
				SaveData.tikiTotemRight_ID4 = 1;
			break;
			case TIKITotemBonusType.RIGHT_ID5:
				SaveData.tikiTotemRight_ID5 = 1;
			break;
			case TIKITotemBonusType.RIGHT_ID6:
				SaveData.tikiTotemRight_ID6 = 1;
			break;

		}

		PreviewLabs.PlayerPrefs.Flush();
		Enable();
		labelDescription.color = new Color(0, 0, 0, 1f);

	}
}
