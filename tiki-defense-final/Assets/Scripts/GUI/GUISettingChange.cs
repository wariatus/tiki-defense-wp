﻿using UnityEngine;
using System.Collections;

public class GUISettingChange : MonoBehaviour {

	public GameObject[] settings;		// Lista settingsow
	public GameObject[] backgrounds;	// Lista tel
	public GameObject settingsOptions;	// Referencja doopcjisettingow
	public GameObject islandSettingsButton; // Referencja
	public GameObject blockedSettingWindow;
	public GameObject windowNoMoreWood;
	public GameObject shopWindow;
	public GameObject WoodWindow;
	public GameObject plainTutorialOne;
	public GameObject plainTutorialfour;
	public GameObject volcanoLocketWindow;
    public GameObject buttons;
	public UILabel starCountLabel;		// Referencja do labela informujacego o posiadanych gwiazdkach

	private int index;					// Indeks reprezentujacy aktualnie wybrany settings

	// Use this for initialization
	void Start () {

		if(SaveData.lastLevelSettingShow != 0)
			Show (SaveData.lastLevelSettingShow);
	
	
		if(!string.IsNullOrEmpty(SaveData.lastMissionLoaded))
		{
			ShowNewWoodWindow();
			SaveData.lastMissionLoaded = string.Empty;
		}
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void ShowJungle()
	{
		//if(SaveData.GetLevelStars("Level1x07") > 0)
			Show(2);
	}

	public void ShowBeach()
	{
		Show(1);
	}

	public void ShowVillage()
	{
		//if(SaveData.GetLevelStars("Level2x10") > 0)
			Show(3);
	}

	public void ShowHills()
	{
		//if(SaveData.GetLevelStars("Level3x10") > 0)
			Show(4);
	}

	public void ShowVolcano()
	{
		//if(SaveData.GetLevelStars("Level4x10") > 0)
			Show(5);
	}

	public void ShowPlain()
	{
		if(SaveData.plainTutorial == 1)
			Show(6);
		else
			ShowPlainTutorial();
	}
	public void ShowPlainTutorial()
	{
		Show(7);
		ShowPlainTutorialWindowOne();
	}

	public void ShowIsland()
	{
		Show(0);
	}

	public void ShowIslandFromPlain()
	{
		ShowIsland();

		if(SaveData.plainTutorial != 1)
			ShowPlainTutorialWindowFour();

		NotificationCenter.defaultCenter.postNotification(new TotemNotification(NotificationType.TotemClick, null));
	
	}

    public void ShowShoopWood()
    {
        GUIShop.Instance.ShowWood();
    }

    public void HideShoopWood()
    {
        GUIShop.Instance.CloseWood();
    }

	public void ShowVolcanoLocked()
	{
		volcanoLocketWindow.SetActive(true);
	}
	
	public void CloseVolcanoLocked()
	{
		volcanoLocketWindow.SetActive(false);
	}

	public void ShowNewWoodWindow()
	{
		WoodWindow.SetActive(true);
	}

	public void HideNewWoodWindow()
	{
		WoodWindow.SetActive(false);
	}

	public void ShowNoMoreWoodWindow()
	{
		windowNoMoreWood.SetActive(true);
	}

	public void CloseNoMoreWoodWindow()
	{
		windowNoMoreWood.SetActive(false);
	}

	public void ShowBlockSettingWindow()
	{
		blockedSettingWindow.SetActive(true);
	}

	public void CloseBlockSettingWindow()
	{
		blockedSettingWindow.SetActive(false);
	}

	public void ShowShopWindow()
	{
		shopWindow.SetActive(true);
	}

	public void CloseShopWindow()
	{
		shopWindow.SetActive(false);	
	}

	public void ShowPlainTutorialWindowOne()
	{
		plainTutorialOne.SetActive(true);
	}

	public void ShowPlainTutorialWindowFour()
	{
	
		plainTutorialfour.SetActive(true);
	}

	public void ClosePlainTutorialWindowFour()
	{
		SaveData.plainTutorial = 1;
		SaveData.Save();
		plainTutorialfour.SetActive(false);
	}


	private void Show(int index)
	{
		//Debug.Log("chuj 2");
		// Ustawiamy ilosc posiadanych gwiazdek na 0 w celu ponownego wyliczenia na wybranym settingu
		//starCountLabel.text = "0";

		for(int i = 0; i < settings.Length; i++)
		{
			if(i != index)
			{
				NGUITools.SetActive(settings[i], false);
				NGUITools.SetActive(backgrounds[i], false);
			}
			else
			{
				NGUITools.SetActive(settings[i], true);
				NGUITools.SetActive(backgrounds[i], true);
				
				if(i==0)
				{
					NGUITools.SetActive(settings[i], true);
					NGUITools.SetActive(settingsOptions, false); 
					NGUITools.SetActive(islandSettingsButton, true);
				}
				else
				{
					if(index != 6)
						NGUITools.SetActive(settingsOptions, true);
					NGUITools.SetActive(islandSettingsButton, false);

                    if (index == 7)
                    {
                        NGUITools.SetActive(islandSettingsButton, false);
                        NGUITools.SetActive(buttons, false);
                    }
				}
			}
		}
	}

}
