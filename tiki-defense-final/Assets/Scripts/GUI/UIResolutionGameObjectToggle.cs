﻿using UnityEngine;
using System.Collections;

public class UIResolutionGameObjectToggle : MonoBehaviour {

	public int requiredLevel;

	public GameObject turnOffObjects;
	public GameObject turnOnObjects;

	void Awake ()
	{
	
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnEnable()
	{
		if(Application.loadedLevel == requiredLevel)
		{
			turnOffObjects.SetActive(false);
			turnOnObjects.SetActive(true);
		}
		else
		{
			turnOffObjects.SetActive(true);
			turnOnObjects.SetActive(false);
		}
	}
}
