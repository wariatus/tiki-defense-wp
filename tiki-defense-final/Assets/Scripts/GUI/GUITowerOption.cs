﻿using UnityEngine;
using System.Collections;

public class GUITowerOption : MonoBehaviour {
	
	public UITweener tweener;
	public UILabel price;
	
	[HideInInspector]
	public Towers.TowerBase activeTower;
	
	public GUITowerOptionsManager manager;
	
	public bool isSellButton;

	public Transform transform;

	public bool isTutorialButton;

	public bool isShow;
	public GameObject panelPrice;
	
	private int sellPrice;
	private int buyPrice;
	private int upgradePrice;

	private bool attackBlocker;
	private UnitBase blockerToAttack;
	private bool tutorialFirstUpgrade; 
	private UIButton button;
	
	// Use this for initialization
	void Start () {
		
		NotificationCenter.defaultCenter.addListener(OnBlockerClicked, NotificationType.BlockerClick);
		NotificationCenter.defaultCenter.addListener(OnEnemyClicked, NotificationType.EnemyClick);

		NotificationCenter.defaultCenter.addListener(OnTutorialBuildTowerOn, NotificationType.TutorialBuildTowerOn);
		NotificationCenter.defaultCenter.addListener(OnTutorialBuildTowerOff, NotificationType.TutorialBuildTowerOff);
		button = GetComponent<UIButton>();

	
	}
	
	// Update is called once per frame
	void Update () {

		if(GameMaster.gameState == GameState.RUNNING)
		{
			if(!isSellButton)
			{
				CheckButtonStatus();
			}
		}
	
	}

	protected virtual void OnEnemyClicked(Notification n)
	{
		attackBlocker = false;
		blockerToAttack = null;
	}

	protected virtual void OnBlockerClicked(Notification n)
	{
		
		BlockerClickNotification noti = n as BlockerClickNotification;
		
		if(blockerToAttack == noti.unit)
		{
			attackBlocker = false;
			blockerToAttack = null;
		}
		else
		{
			
			blockerToAttack = noti.unit;
			attackBlocker = true;
		}
	}
	
	private bool CanBuy()
	{
		if(buyPrice > SaveData.money)
			return false;
		else
			return true;
	}
	
	private bool CanUpgrade()
	{
		if(upgradePrice > SaveData.money)
			return false;
		else
			return true;
			
	}
	
	void Sell()
	{
		if(activeTower.activeCell != null)
			activeTower.activeCell.activeTower = null;
		
		//PoolManager.AddItem(activeTower.gameObject, activeTower.poolObjectType);
		if(PathologicalGames.PoolManager.Pools["Towers"].IsSpawned(activeTower.MyTransform))
			PathologicalGames.PoolManager.Pools["Towers"].Despawn(activeTower.MyTransform);
		else
			activeTower.MyGameObject.SetActive(false);
		NotificationCenter.defaultCenter.postNotification(new SuperNotification(NotificationType.MoneyCountChanged, 0, sellPrice, activeTower.MyTransform.position, true));
		manager.DisableTowersGUI();

	

		
		
	}
	
	void Upgrade()
	{
		if(GameMaster.onTutorial && tutorialFirstUpgrade)
			return;
		
	
		if(activeTower.level < 3)
		{

			//Debug.Log(upgradePrice + " " + GetPlayerPrefs.money);
			if(CanUpgrade())
			{
//				PoolObjectType nextLevelTowerType = activeTower.nextLevelPoolObjectType;
//				//GameObject go = PoolManager.GetItem(nextLevelTowerType);
//				Towers.TowerBase newTower = PathologicalGames.PoolManager.Pools["Towers"].Spawn(nextLevelTowerType.ToString()).GetComponent<Towers.TowerBase>();
//				
//				if(activeTower.activeCell != null)
//				{
//					newTower.activeCell = activeTower.activeCell;
//					newTower.activeCell.activeTower = newTower;
//				}
//				
//			
//				newTower.state = TowerState.ReadyToFire;
//                newTower.MyTransform.position = activeTower.MyTransform.position;
//
//                if (activeTower.attackBlocker)
//                {
//                    newTower.attackBlocker = true;
//                    newTower.blockerToAttack = activeTower.blockerToAttack;
//                }
//
//                if (activeTower.currentTarget != null)
//                {
//                    newTower.currentTarget = activeTower.currentTarget;
//                    newTower.state = TowerState.Rotate;
//                }
//
//				//PoolManager.AddItem(activeTower.gameObject, activeTower.poolObjectType);
//				PathologicalGames.PoolManager.Pools["Towers"].Despawn(activeTower.MyTransform);
				activeTower.Upgrade();

				if(attackBlocker && activeTower.canAttackBlocker)
				{
					activeTower.attackBlocker = true;
					activeTower.blockerToAttack = blockerToAttack;
				}

				NotificationCenter.defaultCenter.postNotification(new SuperNotification(NotificationType.MoneyCountChanged, 0, -upgradePrice, activeTower.MyTransform.position, true));

				if(isTutorialButton && GameMaster.onTutorial)
				{
					NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.TutorialBuildTowerProgress));
					tutorialFirstUpgrade = true;
				 	manager.firstTutorialClickEnabled = true;
				}

				PathologicalGames.PoolManager.Pools["ParticleEffects"].Spawn(ParticleEffectType.Effect_UpgradeTower.ToString(), activeTower.transform.position, Quaternion.identity);

				manager.DisableTowersGUI();
			}
			
		}

	}
	
	void OnClick()
	{
		if(isSellButton)
			Sell();
		else
			Upgrade();
	}

	private void OnTutorialBuildTowerOn(Notification n)
	{
		if(!isTutorialButton)
		{
			gameObject.SetActive(false);

		}


		
	}
	
	private void OnTutorialBuildTowerOff(Notification n )
	{
		gameObject.SetActive(true);
	}

	public void Show(Towers.TowerBase tower)
	{
		if(GameMaster.onTutorial && Application.loadedLevel == 6) return;

		if(GameMaster.onTutorial && !isTutorialButton)
			gameObject.SetActive(false);
		else
			gameObject.SetActive(true);
			

		activeTower = tower;
		
		tweener.Toggle();



		sellPrice = LevelConfigurations.GetTowerPrice(tower.settings[tower.level -1].type).sellPrice;
		buyPrice = LevelConfigurations.GetTowerPrice(tower.settings[tower.level -1].type).buyPrice;
		upgradePrice = LevelConfigurations.GetTowerPrice(tower.settings[tower.level -1].type).upgradePrice;

		if(!isSellButton)
		{
			isShow = true;

		}

		CheckButtonStatus();

		if(isSellButton)
			price.text = "+"+ LevelConfigurations.GetTowerPrice(tower.settings[tower.level -1].type).sellPrice.ToString();
		else
			price.text = LevelConfigurations.GetTowerPrice(tower.settings[tower.level -1].type).upgradePrice.ToString();


	}

	private void CheckButtonStatus()
	{
		if(isSellButton) return;

		if(activeTower == null) return;

		if(activeTower.level < 3)
		{
			panelPrice.SetActive(true);
			if(CanUpgrade())
				button.isEnabled = true;
			else
				button.isEnabled = false;
		}
		else
		{
			button.isEnabled = false;
			panelPrice.SetActive(false);
		}

	}
}
