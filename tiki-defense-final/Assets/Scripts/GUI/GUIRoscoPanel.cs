﻿using UnityEngine;
using System.Collections;

public class GUIRoscoPanel : ExtendedMonoBehavior {

	#region Variables

	public bool isPublishedWindow;

	public TweenAlpha mainMenuAlphaTweener;
	public GameObject producentGameObject;
	
	public float time;
	
	#endregion

	#region Initialization

	protected override void Awake () 
	{
		base.Awake();
	}

	protected override void Start () 
	{
		base.Start();

		if(!this.isPublishedWindow)
			Invoke("TurnOff", 3f);
		else
			Invoke ("EnableProducentSplash", 3f);
	}

	#endregion
	
	// Update is called once per frame
	protected override void Update () {
	
		base.Update();
	}

	#region Public methods

	#endregion

	#region Protected methods

	#endregion
	
	#region Private methods

	private void EnableProducentSplash()
	{
		this.gameObject.SetActive(false);
		this.producentGameObject.SetActive(true);
	}

	private void TurnOff()
	{
		gameObject.SetActive(false);
		mainMenuAlphaTweener.PlayForward();
	}

	#endregion
	
	#region Events & Notifications

	#endregion



}
