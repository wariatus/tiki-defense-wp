﻿using UnityEngine;
using System.Collections;

public class UIPausaWindow : MonoBehaviour {

	public UITweener tweener;
	private bool ready;
	// Use this for initialization
	void Start () {

		NotificationCenter.defaultCenter.addListener(OnStateChange, NotificationType.OnGameStateChanged);
		NotificationCenter.defaultCenter.addListener(OnCountStart, NotificationType.OnCountStart);
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	void OnCountStart(Notification n)
	{
		ready = true;
	}

	void OnStateChange(Notification n)
	{
		if(!ready) return;
		if(Application.loadedLevel == 3 || Application.loadedLevel == 5) return;



		if(GameMaster.gameState == GameState.PAUSA)
		{
			//tweener.ResetToBeginning();
			tweener.PlayForward();
		}
		else if(GameMaster.gameState == GameState.RUNNING)
		{
			tweener.PlayReverse();
		}
	}
}
