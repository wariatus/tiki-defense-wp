﻿using UnityEngine;
using System.Collections;

public class GUIShamanHealth : MonoBehaviour {

	public UILabel label;
	private Tiki shaman;
	
	// Use this for initialization
	void Start () {

		shaman = GameObject.FindGameObjectWithTag("Shaman").GetComponent<Tiki>();
		Vector3 fixedPosition = Camera.main.WorldToViewportPoint(shaman.MyTransform.position);
		fixedPosition.z = 0;
		transform.position = GameObject.FindGameObjectWithTag("GUICamera").GetComponent<Camera>().ViewportToWorldPoint(fixedPosition);

		if(shaman.healthSite == Tiki.HealthSite.TOP)
			transform.localPosition = new Vector3(transform.localPosition.x-7.5f, transform.localPosition.y + 52f, transform.localPosition.z);
		else if(shaman.healthSite == Tiki.HealthSite.BOTTOM)
			transform.localPosition = new Vector3(transform.localPosition.x - 50f, transform.localPosition.y, transform.localPosition.z);
		else if(shaman.healthSite == Tiki.HealthSite.LEFT)
			transform.localPosition = new Vector3(transform.localPosition.x - 37f, transform.localPosition.y, transform.localPosition.z);
		else if(shaman.healthSite == Tiki.HealthSite.RIGHT)
			transform.localPosition = new Vector3(transform.localPosition.x + 34.5f, transform.localPosition.y, transform.localPosition.z);

		NotificationCenter.defaultCenter.addListener(OnShamanTakeDamage, NotificationType.ShamanTakeDamage);
		NotificationCenter.defaultCenter.addListener(OnGameWinOrDefeat, NotificationType.GameWon);
		NotificationCenter.defaultCenter.addListener(OnGameWinOrDefeat, NotificationType.GameDefeated);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	private void OnShamanTakeDamage(Notification n)
	{
		label.text = shaman.health.currentHealth.ToString("0");
	}

	private void OnGameWinOrDefeat(Notification n)
	{
		gameObject.SetActive(false);

	}
}
