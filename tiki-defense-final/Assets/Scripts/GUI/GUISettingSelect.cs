﻿using UnityEngine;
using System.Collections;

public class GUISettingSelect : MonoBehaviour {

	public string levelToCheck;
	public bool available;
	public int settingsStarID = 1;
	public bool isPlain;
	public GameObject lockGameObject;
	public GameObject starsGameObject;
	public GameObject unlockGameObject;
	public bool requireTotems;
	public UILabel starsLabel;

	// Use this for initialization
	void Start () {
	
		if(requireTotems)
			InvokeRepeating("CheckTotems", 1f, 1f);
	
		if(requireTotems)
		{
            lockGameObject.SetActive(true);
            unlockGameObject.SetActive(false);

			starsGameObject.SetActive(true);
            if (SaveData.tikiTotemRight_ID6 == 1 && SaveData.tikiTotemLeft_ID6 == 1)
            {
                if (SaveData.GetLevelStars(levelToCheck) > 0)
                {
                    lockGameObject.SetActive(false);
                    unlockGameObject.SetActive(true);
                }
            }
            
		}
		else
		{
	
			if(available || SaveData.GetLevelStars(levelToCheck) > 0)
			{
				if(!string.IsNullOrEmpty(levelToCheck))
				{
					lockGameObject.SetActive(false);
					starsGameObject.SetActive(true);
					unlockGameObject.SetActive(true);
	
				}
				else
				{
					lockGameObject.SetActive(false);
					starsGameObject.SetActive(false);
					unlockGameObject.SetActive(true);
				}
	
				available = true;
			}
			else
			{
				lockGameObject.SetActive(true);
				starsGameObject.SetActive(true);
				unlockGameObject.SetActive(false);
			}
			
		
		
		#if UNLOCK_ALL
		  if(!isPlain)
		  {
			lockGameObject.SetActive(false);
			starsGameObject.SetActive(true);
			unlockGameObject.SetActive(true);
		  }
		#endif
		
		
		}
		
		
		if(settingsStarID == 1)
			starsLabel.text = SaveData.settingStarsBeach.ToString() + "/27";
		else if(settingsStarID == 2)
			starsLabel.text = SaveData.settingStarsJungle.ToString() + "/27";
		else if(settingsStarID == 3)
			starsLabel.text = SaveData.settingStarsVillage.ToString() + "/27";
		else if(settingsStarID == 4)
			starsLabel.text = SaveData.settingStarsHills.ToString() + "/27";
		else if(settingsStarID == 4)
			starsLabel.text = SaveData.settingStarsVulcano.ToString() + "/27";
		else if (settingsStarID == 5)
			starsLabel.text = SaveData.settingStarsVulcano.ToString() + "/27";
	}
	
	
	// Update is called once per frame
	void Update () {
	
	}

	private void CheckTotems()
	{
		if(SaveData.tikiTotemRight_ID6 == 1 && SaveData.tikiTotemLeft_ID6 == 1)
		{
			if(SaveData.GetLevelStars(levelToCheck) > 0)
			{
			    lockGameObject.SetActive(false);
			    unlockGameObject.SetActive(true);
			}
		}
	}




}
