﻿using UnityEngine;
using System.Collections;

public class GUIPauseButton : MonoBehaviour {

	public UISprite sprite;
	public bool imPause;
	private bool isPause;
	public GameObject waveCounter;
	public GameObject waveCoutnerPausa;

    void Awake()
    {
    }

	// Use this for initialization
	void Start () {
	
		 NotificationCenter.defaultCenter.addListener(OnPauseClicked, NotificationType.PauseClicked);
		NotificationCenter.defaultCenter.addListener(OnGameStateChanged, NotificationType.OnGameStateChanged);
	}
	
	// Update is called once per frame
	void Update () {


	
	}

    public void ManualActivePasue()
    {
        if (!isPause)
        {
           // sprite.spriteName = "button_play";
            //isPause = true;
           // waveCounter.SetActive(false);
            //waveCoutnerPausa.SetActive(true);
        }
        else
        {
            //isPause = false;
            //sprite.spriteName = "button_pause";
            //waveCounter.SetActive(true);
            //waveCoutnerPausa.SetActive(false);
        }
    }

	public void OnPauseClicked(Notification n)
	{


		OnClick();
	}

	public void ShowGrid()
	{
		NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.ShowGridSelect));
	}

	void OnClick()
	{
		//BUGFIX:0000051
		if(GameMaster.onTutorial) return;

		if(GameMaster.gameState == GameState.GUIWINDOW) return;


		if(!isPause)
		{
			//sprite.spriteName = "button_play";
			//isPause = true;
			//waveCounter.SetActive(false);
			//waveCoutnerPausa.SetActive(true);
		}
		else
		{
			//isPause= false;
			//sprite.spriteName = "button_pause";
			//waveCounter.SetActive(true);
			//waveCoutnerPausa.SetActive(false);
		}
		
	}

	void OnGameStateChanged(Notification n)
	{
		if(Application.loadedLevel == 5) return;

		if(GameMaster.gameState == GameState.RUNNING)
		{
			//sprite.spriteName = "button_pause";
			if(!imPause) gameObject.SetActive(false);
			else gameObject.SetActive(true);
			collider.enabled = true;
		}
		else if(GameMaster.gameState == GameState.PAUSA)
		{
			//sprite.spriteName = "button_play";
			
		

		}
	}
}
