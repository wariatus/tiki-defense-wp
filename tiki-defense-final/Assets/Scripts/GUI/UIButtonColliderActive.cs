﻿using UnityEngine;
using System.Collections;

public class UIButtonColliderActive : MonoBehaviour {

	public Collider target;

	public bool state;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnClick()
	{
		target.enabled = state;
	}
}
