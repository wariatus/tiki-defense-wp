﻿using UnityEngine;
using System.Collections;

public class GUIGridTower : MonoBehaviour {
	
	public UITweener tweener;
	public UILabel price;
	public TowersType type;
	
	public GUIGridManager manager;
	public Transform transform;
	public bool isShow;

	public bool isTutorialGrid;
	
	public AudioClip soundOnBuy;
	
	private int buyPrice;
	private GridCell activeCell;

	private bool attackBlocker;
	private UnitBase blockerToAttack;
	private UIButton button;
	private bool firstTutorialUse;

	// Use this for initialization

	void Awake()
	{
		button =GetComponent<UIButton>();

	}
	void Start () {
	
		buyPrice = LevelConfigurations.GetTowerPrice(type).buyPrice;
		
		/// Pobranie aktualnej kwoty kupna
		price.text = buyPrice.ToString();

		NotificationCenter.defaultCenter.addListener(OnBlockerClicked, NotificationType.BlockerClick);
		NotificationCenter.defaultCenter.addListener(OnEnemyClicked, NotificationType.EnemyClick);

		NotificationCenter.defaultCenter.addListener(OnTutorialBuildTowerOn, NotificationType.TutorialBuildTowerOn);
		NotificationCenter.defaultCenter.addListener(OnTutorialBuildTowerOff, NotificationType.TutorialBuildTowerOff);



	}
	
	// Update is called once per frame
	void Update () {
	

		if(GameMaster.gameState == GameState.RUNNING)
		{
			if(isShow)
			{
				if(button != null)
				{
					if(CanBuy())
						button.isEnabled = true;
					else
						button.isEnabled = false;
				}

			}
		}
	}
	
	void OnClick()
	{
		
		if(CanBuy())
		{
			
			//GameObject towerObject = PoolManager.GetItem(GetPoolObjectType());
			Towers.TowerBase tower = PathologicalGames.PoolManager.Pools["Towers"].Spawn(GetPoolObjectType().ToString()).GetComponent<Towers.TowerBase>();

			tower.activeCell = activeCell;
			tower.state = TowerState.ReadyToFire;
			
			Vector3 pos = activeCell.MyTransform.position;
			pos.z = 0;
            tower.MyTransform.position = pos;

			NotificationCenter.defaultCenter.postNotification(new SuperNotification(NotificationType.MoneyCountChanged, 0, -buyPrice, pos, true));

			if(attackBlocker)
			{
				tower.attackBlocker = true;
				tower.blockerToAttack = blockerToAttack;
			}
			
			activeCell.activeTower = tower;
			activeCell.Unclicked();

			if(isTutorialGrid && GameMaster.onTutorial)
				NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.TutorialBuildTowerProgress));

			PathologicalGames.PoolManager.Pools["ParticleEffects"].Spawn(ParticleEffectType.Effect_BuyTower.ToString(), tower.transform.position, Quaternion.identity).GetComponent<tk2dSpriteAnimator>().Play();

			if(this.soundOnBuy != null)
				NGUITools.PlaySound(this.soundOnBuy);

			manager.DisableTowersGUI();
			
			

		}
	}

	protected virtual void OnBlockerClicked(Notification n)
	{
		
		BlockerClickNotification noti = n as BlockerClickNotification;
		
		if(blockerToAttack == noti.unit)
		{
			attackBlocker = false;
			blockerToAttack = null;
		}
		else
		{
			
			blockerToAttack = noti.unit;
			attackBlocker = true;
		}
	}
	
	protected virtual void OnEnemyClicked(Notification n)
	{
		attackBlocker = false;
		blockerToAttack = null;
	}
	
	private bool CanBuy()
	{
		if(buyPrice > SaveData.money)
			return false;
		else
			return true;
	}
	
	public void Show(GridCell cell)
	{
		//tweener.ResetToBeginning();
		tweener.PlayForward();
		activeCell = cell;

		isShow = true;

		if(button != null)
		{
			if(CanBuy())
				button.isEnabled = true;
			else
				button.isEnabled = false;
		}

		if(isTutorialGrid && GameMaster.onTutorial)
			if(!firstTutorialUse)
			{
				firstTutorialUse = true;
				NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.TutorialBuildTowerProgress));
			}
	}



	private void OnTutorialBuildTowerOn(Notification n)
	{
		if(!isTutorialGrid)
			gameObject.SetActive(false);
		
	}
	
	private void OnTutorialBuildTowerOff(Notification n )
	{
		gameObject.SetActive(true);
	}
			
	private PoolObjectType GetPoolObjectType()
	{
		switch(type)
		{
			case TowersType.Cloud_level_1:
				return PoolObjectType.Tower_Cloud_level_1;
				
			case TowersType.Tornado_level_1:
					return PoolObjectType.Tower_Tornado_level_1;
				
			case TowersType.Totem_level_1:
				return PoolObjectType.Tower_Totem_level_1;
	
			case TowersType.VooDoo_level_1:
				return PoolObjectType.Tower_VooDoo_level_1;
			
			case TowersType.Cave_level_1:
				return PoolObjectType.Tower_Cave_level_1;
			
			case TowersType.FireTotem_level_1:
				return PoolObjectType.Totem_FireTotem_level_1;

            case TowersType.Campfire_level_1:
                return PoolObjectType.Totem_Campfire_level_1;

			case TowersType.IceTotem_level_1:	
				return PoolObjectType.Totem_IceTotem_level_1;

			case TowersType.RockHammer_level_1:
				return PoolObjectType.Totem_RockHammer_level_1;

			case TowersType.MagicShot_level_1:
				return PoolObjectType.Totem_MagicShot_level_1;

			case TowersType.Melter_level_1:
				return PoolObjectType.Totem_Melter_level_1;
			case TowersType.Drum_level_1:
				return PoolObjectType.Totem_Drum_level_1;
			case TowersType.Necklace_level_1:
				return PoolObjectType.Totem_Necklace_level_1;
			case TowersType.Anvil_level_1:
				return PoolObjectType.Totem_Anvil_level_1;
			case TowersType.Cauldron_level_1:
				return PoolObjectType.Totem_Cauldron_level_1;
			case TowersType.RunicStone_level_1:
				return PoolObjectType.Tower_Runic_level_1;

			default:
				return PoolObjectType.Null;
		}
	}
}
