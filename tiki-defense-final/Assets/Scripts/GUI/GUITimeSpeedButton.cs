﻿using UnityEngine;
using System.Collections;

public class GUITimeSpeedButton : MonoBehaviour {
	
	public UISprite sprite;
	public UIButton button;
	private bool isSpeedUp;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnClick()
	{
		//BUGFIX:0000051
		if(GameMaster.onTutorial) return;

		//BUGFIX:0000023
		if(GameMaster.gameState == GameState.RUNNING || GameMaster.gameState == GameState.PAUSA)
		{
		//ENDBBUGFIX
			if(!isSpeedUp)
			{
				button.normalSprite = "button_speed2";  
				button.hoverSprite = "button_speed2";  
				button.pressedSprite = "button_speed2_click";
                Time.timeScale = 2f;
                Time.fixedDeltaTime = 0.01F * Time.timeScale;
               // Unit.speedScale = 2;
				isSpeedUp = true;

			}
			else
			{
				Time.timeScale = 1;
                Time.fixedDeltaTime = 0.01F * Time.timeScale;
				isSpeedUp= false;
               // Unit.speedScale = 1;
				button.normalSprite = "button_speed1";  
				button.hoverSprite = "button_speed1";  
				button.pressedSprite = "button_speed1_click"; 
			}
		}
		
	}
}
