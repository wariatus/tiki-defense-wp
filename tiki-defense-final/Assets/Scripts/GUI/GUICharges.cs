﻿using UnityEngine;
using System.Collections;

public class GUICharges : MonoBehaviour {


	public UISprite[] chargerSprite;
	public ParticleSystem[] particles;
	
	public UISprite sprite;
	
	public Vector3 worldPosition  
	{
		get 
		{
			Vector3 pos = GameObject.FindWithTag("GUICamera").GetComponent<Camera>().WorldToViewportPoint(transform.position);
			pos.z = 0;
			return pos;
		}	

	}

	// Use this for initialization
	void Start () {

		NotificationCenter.defaultCenter.addListener(OnChargerChanged, NotificationType.TikiMaskChargeChanged);
		NotificationCenter.defaultCenter.addListener(OnTIKIUnlock, NotificationType.OnShopBuyTIKIUnlock);

#if !BLOCK_MICROTRANSACTION
			sprite.spriteName = "board_mask_button+_right_frame3";
#else
        sprite.spriteName = "board_mask_button+_right_frame3B";
#endif

		if(SaveData.tikiTotemRight_ID3 == 1)
		{
#if !BLOCK_MICROTRANSACTION
			sprite.spriteName = "board_mask_button+_right_frame4";
#else
            sprite.spriteName = "board_mask_button+_right_frame4B";
#endif
			chargerSprite[chargerSprite.Length-1].gameObject.SetActive(true);
		}
		//Jezeli mamy kupiony totem tiki to ustawiamy aktywny na 1
		if(SaveData.tikiTotemRight_ID5 == 1)
		{
			//Debug.Log("chuj");/
			SaveData.tikiMaskCharge++;
			chargerSprite[0].spriteName = "board_mask_cristal_on";
		}

		GameMaster.guiChargers = this;
		
		this.OnChargerChanged(null);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	private void OnTIKIUnlock(Notification n)
	{
		if(SaveData.tikiTotemRight_ID3 == 1)
		{
			sprite.spriteName = "board_mask_button+_right_frame4";
			chargerSprite[chargerSprite.Length-1].gameObject.SetActive(true);
		}
		//Jezeli mamy kupiony totem tiki to ustawiamy aktywny na 1
		if(SaveData.tikiTotemRight_ID6 == 1)
		{
			//Debug.Log("chuj");
			SaveData.tikiMaskCharge++;
			chargerSprite[0].spriteName = "board_mask_cristal_on";
		}
	}



	private void OnChargerChanged(Notification n)
	{
		int count = SaveData.tikiMaskCharge;

		for(int i = 0; i < chargerSprite.Length; i++)
		{
			chargerSprite[i].spriteName = "board_mask_cristal_off";
		}

		if(count == 0) return;

		for(int i = 0; i < count; i++)
		{
			chargerSprite[i].spriteName = "board_mask_cristal_on";

			if(!GameMaster.onTutorial)
				particles[i].Play(true);
		}
	}
}
