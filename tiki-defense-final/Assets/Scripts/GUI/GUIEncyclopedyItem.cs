﻿using UnityEngine;
using System.Collections;
using GoogleFu;
using System.Collections.Generic;

public enum MonsterType
{
    Null,
    Heavy,
    Jump,
    Armore,
    Fast,
    Ghost,
    Normal,
    Special

}

public class GUIEncyclopedyItem : ExtendedMonoBehavior {

	#region Variables
    public MonsterType type;
    public Translate.rowIds titleId;
    public Translate.rowIds descriptionId;
    public UISprite sprite;
    public UISprite[] sprites;
    
    private TweenScale _tweener;
	#endregion

	#region Initialization

	protected override void Awake () 
	{
		base.Awake();

      
       
	}

	protected override void Start () 
	{
		base.Start();
	}

	#endregion
	
	// Update is called once per frame
	protected override void Update () {
	
		base.Update();
	}

	#region Public methods
    public void HandInit()
    {
        this.sprite = GetComponent<UISprite>();
        this._tweener = GetComponent<TweenScale>();


        this.sprites = GetComponentsInChildren<UISprite>(true);
    }
    public void Select()
    {
        GUIEncyclopedyWindow.Instance.ItemSelect(this);

        this.PlayTweener();
      
    }

    public void PlayTweener()
    {
        this._tweener.enabled = true;
        this._tweener.ResetToBeginning();
        this._tweener.PlayForward();
    }

    public void Unselect()
    {
        this._tweener.enabled = false;
        this.transform.localScale = new Vector3(1, 1, 1);
      
    }

	#endregion

	#region Protected methods

	#endregion
	
	#region Private methods
    private void OnClick()
    {
        this.Select();
    }
	#endregion
	
	#region Events & Notifications

	#endregion



}
