﻿using UnityEngine;
using System.Collections;

public class GUIStar : ExtendedMonoBehavior {

    public UISprite star;
    public UITweener[] tweeners;

 
    protected override void Awake()
    {
        base.Awake();
    }

	// Use this for initialization
	protected override void Start () {

        base.Start();
	
	}
	
	// Update is called once per frame
	void Update () {

	
	}

    public void Show()
    {
        MyGameObject.SetActive(true);
        for(int i = 0; i< tweeners.Length; i++)
        {
            tweeners[i].ResetToBeginning();
            tweeners[i].PlayForward();
        }
    }
}
