﻿using UnityEngine;
using System.Collections;

public class GUILoadingScreen : MonoBehaviour {

	// Use this for initialization
	void Start () {

		Invoke("RunLevel", 2f);
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	void RunLevel()
	{
		Application.LoadLevel(SaveData.levelToLoad);
	}
}
