﻿using UnityEngine;
using System.Collections;

public class GUIBuyChargeButton : MonoBehaviour {

    public UITweener tweener;
    public UIToggleColliderActivate toggleCollider;
    
    private bool isVisible;
    private bool isUseOnTutorial;

    void Awake()
    {
        GameMaster.guiOperation.buyChargeButton = this;
    }

	// Use this for initialization
	void Start () {
	
        
        
	}
	
	// Update is called once per frame
	void Update () {
	
	}


    public void Toggle()
    {

#if BLOCK_MICROTRANSACTION
        return;
#endif

        if (GameMaster.onTutorial)
            if (isUseOnTutorial)
                return;
        

        if (isVisible)
		{
            isVisible = false;

		}
        else
		{
            isVisible = true;


			GameMaster.guiOperation.OnClick_Pausa();
		}

        GameMaster.guiOperation.isBuyChargeButtonVisible = isVisible;



        Invoke("TurnOffColliders", 0.5f);

        tweener.Toggle();

        if (!isUseOnTutorial)
            isUseOnTutorial = true;
        
    }


    private void TurnOffColliders()
    {
        toggleCollider.Toggle();
    }
}
