﻿using UnityEngine;
using System.Collections;

public class GUIMoney : MonoBehaviour {
	
	public UILabel label;
	
	private int moneyCount;
	
	
	
	// Use this for initialization
	void Start () {
	
		NotificationCenter.defaultCenter.addListener(OnEnemyKilled, NotificationType.EnemyKilled);
        NotificationCenter.defaultCenter.addListener(OnBlockerKilled, NotificationType.BlockerDestroy);
		NotificationCenter.defaultCenter.addListener(OnMoneyCountChanged, NotificationType.MoneyCountChanged);

		int money = 0;

		// TIKI BONUS
		if(SaveData.tikiTotemLeft_ID1 == 1)
			money += GlobalConfiguration.TIKI_TOTEM_LEFT_ID_1_BONUS;

		// TIKI BONUS
		if(SaveData.tikiTotemLeft_ID4 == 1)
			money += GlobalConfiguration.TIKI_TOTEM_LEFT_ID_4_BONUS;
	
		money += LevelConfigurations.GetConfiguration(Application.loadedLevelName).startMoney;

		// Ustawiamy kase nastart
		SetMoney(money);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnMoneyCountChanged(Notification n)
	{
		SuperNotification notification = n as SuperNotification;
		if(notification != null)
			SetMoney(notification.varInt);
	
	}
	
	void OnBlockerKilled(Notification n)
    {
        EnemyKilledNotification notification = n as EnemyKilledNotification;
        SetMoney(notification.moneyForKill);
    }

	void OnEnemyKilled(Notification n)
	{
		EnemyKilledNotification notification = n as EnemyKilledNotification;
		
		SetMoney(notification.moneyForKill);
	}
	
	void SetMoney(int money)
	{
		moneyCount = int.Parse(label.text);
		moneyCount += money;
		
		SaveData.money = moneyCount;
		
		label.text = moneyCount.ToString();	
	}
	
	
}
