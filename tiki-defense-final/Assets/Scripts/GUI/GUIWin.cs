using UnityEngine;
using System.Collections;
using GoogleFu;
using System.Collections.Generic;

public class GUIWin : MonoBehaviour {

    public GameObject playNextButton;
    public GameObject replayButton;

    public GUIStar[] stars;
    public UISprite badge;

	public UILabel starsFinishScore;
	public UILabel badgeFinishScore;
	public UILabel healthFinishScore;
	public UILabel healthCount;
	public UILabel finishScore;
	public UILabel title;
	public List<GameObject> deactivates;
	public GUIGridManager towerManager;
	public GUITowerOptionsManager optionTowerManager;
	
	public Renderer background;
	public GameObject completeWindow;
    public GameObject winWindow;


    void Awake()
    {
     
    }

   	// Use this for initialization
	void Start () {

        playNextButton.SetActive(false);
        replayButton.SetActive(false);
        badge.gameObject.SetActive(true);

		this.background = GameObject.FindGameObjectWithTag("EncyclopedyBackground").GetComponent<Renderer>();
        for (int i = 0; i < stars.Length; i++)
        {
            stars[i].MyGameObject.SetActive(false);
        }

		GetReferences.winWindow = this;

       // Debug.Log("test");

		deactivates.Add(GameObject.FindGameObjectWithTag("Shaman"));
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void ShowComplete()
	{
        
        this.background.enabled = true;
		this.completeWindow.SetActive(true);
        DeactivateObjects();
	}
	
    public void ShowWin()
    {
    	this.Show(true);
    }
    public void Show(bool isWin)
    {
        DeactivateObjects();
        winWindow.SetActive(true);
        this.completeWindow.SetActive(false);

    	this.background.enabled = true;
		

		transform.localPosition = new Vector3(0f, 0f, 0f);
               
        if (isWin)
        {
			title.text = Translate.Instance.GetRow(Translate.rowIds.ID_VICTORY).GetStringData(GameMaster.language);

            StartCoroutine(RunStars(GameMaster.StarsCount(), true));
           
        }
		else
		{
			title.text = Translate.Instance.GetRow(Translate.rowIds.ID_DEFEAT).GetStringData(GameMaster.language);

        	StartCoroutine(RunStars(GameMaster.StarsCount(), false));
		}
       
    }
        
    public void DeactivateObjects()
    {
        towerManager.DisableTowersGUI();
        optionTowerManager.DisableTowersGUI();

        for (int i = 0; i < deactivates.Count; i++)
        {
            if (deactivates[i].transform.CompareTag("Shaman"))
                deactivates[i].transform.position = new Vector3(10000, 10000, 10000);
            else
                deactivates[i].SetActive(false);
        }
    }

    public IEnumerator RunStars(int howManyStars, bool isWin)
    {
        if (isWin)
        {

            stars[0].Show();
			starsFinishScore.text = (1 * GlobalConfiguration.starsMultiplierScore).ToString();
            yield return new WaitForSeconds(0.5f);

            if (howManyStars > 1)
			{
                stars[1].Show();
				starsFinishScore.text = (2 * GlobalConfiguration.starsMultiplierScore).ToString();
			}

            yield return new WaitForSeconds(0.5f);

            if (howManyStars == 3)
			{
                stars[2].Show();
				starsFinishScore.text = (3 * GlobalConfiguration.starsMultiplierScore).ToString();
			}

			yield return new WaitForSeconds(0.5f);
			
			if(SaveData.levelBadge == 1)
			{
				
				badgeFinishScore.text = (GlobalConfiguration.badgeMultiplierScore).ToString();
				yield return new WaitForSeconds(0.5f);
			}

			while(int.Parse(healthCount.text) < GetReferences.tiki.health.currentHealth)
			{
				healthCount.text = ((int.Parse(healthCount.text) +1)).ToString();
				healthFinishScore.text = (int.Parse(healthCount.text) * GlobalConfiguration.healthMultiplierScore).ToString();
				yield return new WaitForSeconds(0.1f);
			}


			finishScore.text = (int.Parse(starsFinishScore.text) + int.Parse(badgeFinishScore.text) + int.Parse(healthFinishScore.text)).ToString();

			//SaveData.premium += int.Parse(finishScore.text);
			Unibiller.CreditBalance("gems", int.Parse(finishScore.text));


            playNextButton.SetActive(true);
            replayButton.SetActive(true);
        }
        else
        {
            playNextButton.SetActive(true);
            replayButton.SetActive(true);
        }
    }

 

    
}
