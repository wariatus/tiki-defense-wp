﻿using UnityEngine;
using System.Collections;

public class GUIMissionSelect : MonoBehaviour {

	public string name;					// Nazwa sceny, w ktorej jest misja
	public string previousMissionName;  // Nazwa sceny, ktora musi zostac ukonczona aby obecna mogla byc odblokowana
	public bool requireStars;			// Czy dana misja wymaga gwiazdek?
	public int howManyStars;			// Ile gwiazdek wymaga misja
	public GameObject[] starsObject;	// Referencja doobiektow gwiazdek
	public GameObject lockObject;		// Referencja do klotki 
	public UILabel labelObject;			// Referencja do UILabel
	public bool available;				// True - dana misja jest odblkowana przy starcie
	public Collider collider;			// Referencja do kolidera
	public GameObject icon;				// Ikona	
	public GameObject starBorder;		// Border gwiazdek
	public GameObject specialBorder;	// Specjalny wood border
	public UILabel lockStar;
    public int settingID;
    private UITweener tweener;
    public GameObject bonusLevel;
    public ParticleSystem particleClick;
	private int stars;					// Ile gwiazdek zostalo osiagniete przy ukonczeniu gry
	private bool requiredStarLock;   // Czy obiekt posiada klotke

	void Awake ()
	{
		stars = SaveData.GetLevelStars(name);
		tweener = icon.GetComponent<UITweener>();

//		EncryptedPlayerPrefs.SetInt("Level 1x04_Stars", 2, 1);
//		EncryptedPlayerPrefs.SetInt("Level 1x01_Stars", 2, 1);
//		EncryptedPlayerPrefs.SetInt("Level 1x02_Stars", 2, 1);
//		EncryptedPlayerPrefs.SetInt("Level 1x03_Stars", 2, 1);
//
//		SaveData.settingStarsBeach = 10;
//		SaveData.Save();	

//		if(specialBorder.activeSelf)
//		{
//			if(SaveData.GetLevelStars(name) > 0)
//				specialBorder.SetActive(false);
//		}


    }

	// Use this for initialization
	void Start () {

		UISprite iconSprite = icon.GetComponent<UISprite>();
		if(GlobalConfiguration.saveGameSlot == 1)
			iconSprite.spriteName = "Profile1";
        ////else if(GlobalConfiguration.saveGameSlot == 2)
        ////    iconSprite.spriteName = "Profile2";
        ////else 
        ////    iconSprite.spriteName = "Profile3";




//#if SETTING_1
//
//        if (settingID == 1)
//            previousMissionName = string.Empty;
//
//#endif
//#if SETTING_2
//           if(settingID == 2)
//                previousMissionName = "Level1x07";
//        
//#endif
//#if SETTING_3
//        if(settingID == 3)
//            previousMissionName = "Level2x06";
//#endif
//
//#if SETTING_4
//        if(settingID == 4)
//            previousMissionName = "Level3x06";
//#endif
//#if SETTING_5
//        if(settingID == 5)
//            previousMissionName = "Level4x07";
//#endif
//
		#if UNLOCK_ALL
			available = true;
		#endif
	

        if (available)
		{
			labelObject.text = name.Remove(0, 5);
			if(labelObject.text.Contains("-bonus"))
				labelObject.text = labelObject.text.Remove(4, 6);
			lockObject.SetActive(false);
			collider.enabled = true;
			starBorder.SetActive(true);
			lockStar.gameObject.SetActive(false);
				
			if(requireStars)
			{
				//bonusLevel.SetActive(true);
				starBorder.SetActive(true);
			}	
				
			if(stars > 0)
			{
				NotificationCenter.defaultCenter.postNotification(new SuperNotification(NotificationType.StarCountChanged, 0, stars));
				ShowStars();
			}
		}
		else
		{
			if(!requireStars)
			{
				if(SaveData.GetLevelStars(previousMissionName) > 0)
				{
					lockObject.SetActive(false);
					collider.enabled =true;
					labelObject.text = name.Remove(0, 5);
					ShowStars();
					starBorder.SetActive(true);
					lockStar.gameObject.SetActive(false);
				}
				else
				{
					lockObject.SetActive(true);
					labelObject.text = "";
					collider.enabled = false;
					//icon.SetActive(false);
					starBorder.SetActive(true);
					lockStar.gameObject.SetActive(false);

				}
			}
			else 
			{
                iconSprite.spriteName = "Profile3";

				//BUGFIX: 0000038
				if(int.Parse(GameObject.FindWithTag("StarCount").GetComponent<UILabel>().text) >= howManyStars && SaveData.GetLevelStars(previousMissionName) > 0)
				//if(int.Parse(GameObject.FindWithTag("StarCount").GetComponent<UILabel>().text) >= howManyStars)
				{
					requiredStarLock = false;
					lockObject.SetActive(false);
					collider.enabled =true;
					labelObject.text = name.Remove(0, 5);
					ShowStars();
					starBorder.SetActive(true);
					lockStar.gameObject.SetActive(false);
				}
				else
				{

                    //icon.SetActive(false);
					requiredStarLock = true;
					collider.enabled = false;
					starBorder.SetActive(false);
					//specialBorder.SetActive(true);
					lockStar.gameObject.SetActive(true);
					lockStar.text = howManyStars.ToString("00");
				}
				
				//bonusLevel.SetActive(true);
				starBorder.SetActive(true);
			}



		}

	

	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnEnable()
	{

		if( SaveData.GetLevelStars(name) > 0)
		{
			//NotificationCenter.defaultCenter.postNotification(new SuperNotification(NotificationType.StarCountChanged, 0, stars));
			ShowStars();
		}

		if(requireStars || name == "Level1x04")
		{
			if(SaveData.GetLevelStars(name) > 0)
				specialBorder.SetActive(false);
		}
	}

	private void ShowStars()
	{
		for(int i = 0; i < stars; i++)
		{
			starsObject[i].SetActive(true);
		}
	}

	public void OnClick()
	{
//		if(LiveManager.Instance.CurrentLiveCount <= 0)
//		{
//			UILiveManager.Instance.Open();
//			return;
//
//		}

		NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.MainThemeMusicOff));

		if(name == "Level1x03")
		{
			///Usunąć aveData.tikiTutorialCompleted == 0 przy wersji prod
			if(SaveData.tikiTutorialCompleted == 0)
			{
				SaveData.levelToLoad = "Level1x03-tutorial-tiki";
				LoadingScreen.Instance.Show();

				//Application.LoadLevel("Level 1x03 - tutorial-tiki");
			}
			else
			{
				SaveData.levelToLoad = name;
				LoadingScreen.Instance.Show();
			}
		}
		else
		{
			SaveData.levelToLoad = name;
			LoadingScreen.Instance.Show();
		}

        tweener.PlayForward();

		SaveData.lastLevelSettingShow = int.Parse(name[5].ToString());
		EncryptedPlayerPrefs.SetInt("FIRST_TUTORIAL_ON_ISLAND", 1, 1);
		PreviewLabs.PlayerPrefs.Flush();
        particleClick.Play(true);

	}
}
