﻿using UnityEngine;
using System.Collections;
using GoogleFu;
using System.Collections.Generic;

public class GUIEncyclopedyWindow : Singleton<GUIEncyclopedyWindow> {

	#region Variables

    public GameObject background;

    public UILabel title;
    public UILabel description;
    public GameObject descriptionContainer;
    public UILabel type;
    public List<UISprite> sprites;
    public UIGrid spriteGird;

    public GameObject towersContener;
    public GameObject enemiesContener;
    public UISprite border;
    public GUIEncyclopedyItem defaultItem;

    private GUIEncyclopedyItem[] _items;

	#endregion

	#region Initialization

	protected override void Awake () 
	{
        base.Awake();

        _items = GetComponentsInChildren<GUIEncyclopedyItem>(true);

        for (int i = 0; i < _items.Length; i++)
        {
            this._items[i].HandInit();
        }
       
	}

    protected override void Start() 
	{
        base.Start();
       
	}

	#endregion
	
	// Update is called once per frame
	protected override void Update () {
	
	}

	#region Public methods


    public void ShowTowers()
    {
        this.border.SetDimensions(162, 162);
        this.description.transform.localPosition = new Vector3(0, -119f, 0);

    	descriptionContainer.SetActive(false);
        this.towersContener.SetActive(true);
        this.enemiesContener.SetActive(false);
        this.type.gameObject.SetActive(false);
    }

    public void ShowEnemies()
    {
        this.border.SetDimensions(162, 192);
        this.description.transform.localPosition = new Vector3(0, -150f, 0);

		descriptionContainer.SetActive(false);
        this.towersContener.SetActive(false);
        this.enemiesContener.SetActive(true);
        this.type.gameObject.SetActive(true);
    }

    public void Show()
    {
        transform.localPosition = new Vector3(1000, 0, 0);
       
        //GameObject.FindGameObjectWithTag("EncyclopedyBackground").GetComponent<MeshRenderer>().enabled = true;
        //GameObject.FindGameObjectWithTag("GameGUI").transform.localPosition = new Vector3(1000, 0, 0);

        this.ItemSelect(defaultItem);
        this.defaultItem.PlayTweener();

        this.background.SetActive(true);

        GameMaster.guiOperation.TurnOffForEncyclopedy();

        
    }

    public void Hide()
    {

        transform.localPosition = new Vector3(-10000, 0, 0);
        //GameObject.FindGameObjectWithTag("EncyclopedyBackground").GetComponent<MeshRenderer>().enabled = false;
        //GameObject.FindGameObjectWithTag("GameGUI").transform.localPosition = new Vector3(-10000, 0, 0);

        GameMaster.gameState = GameState.RUNNING;

        this.background.SetActive(false);

        GameMaster.guiOperation.TurnOnForEncyclopedy();
    }

    public void ItemSelect(GUIEncyclopedyItem _item)
    {
    
    	descriptionContainer.SetActive(true);
    
        for (int i = 0; i < this._items.Length; i++)
        {
            this._items[i].Unselect();
        }

        this.title.text = Translate.Instance.GetRow(_item.titleId).GetStringData(GameMaster.language);

        if (_item.type != MonsterType.Null)
        {
            switch (_item.type)
            {
                case MonsterType.Armore:
                    this.description.text = Translate.Instance.GetRow(Translate.rowIds.ID_MONSTER_ARMOUR_INFO).GetStringData(GameMaster.language);
                    this.type.text = Translate.Instance.GetRow(Translate.rowIds.ID_MONSTER_ARMOUR).GetStringData(GameMaster.language);
                    break;
                case MonsterType.Fast:
                    this.description.text = Translate.Instance.GetRow(Translate.rowIds.ID_MONSTER_FAST_INFO).GetStringData(GameMaster.language);
                    this.type.text = Translate.Instance.GetRow(Translate.rowIds.ID_MONSTER_FAST).GetStringData(GameMaster.language);
                    break;
                case MonsterType.Ghost:
                    this.description.text = Translate.Instance.GetRow(Translate.rowIds.ID_MONSTER_GHOST_INFO).GetStringData(GameMaster.language);
                    this.type.text = Translate.Instance.GetRow(Translate.rowIds.ID_MONSTER_GHOST).GetStringData(GameMaster.language);
                    break;
                case MonsterType.Heavy:
                    this.description.text = Translate.Instance.GetRow(Translate.rowIds.ID_MONSTER_HEAVY_INFO).GetStringData(GameMaster.language);
                    this.type.text = Translate.Instance.GetRow(Translate.rowIds.ID_MONSTER_HEAVY).GetStringData(GameMaster.language);
                    break;
                case MonsterType.Jump:
                    this.description.text = Translate.Instance.GetRow(Translate.rowIds.ID_MONSTER_JUMPER_INFO).GetStringData(GameMaster.language);
                    this.type.text = Translate.Instance.GetRow(Translate.rowIds.ID_MONSTER_JUMPER).GetStringData(GameMaster.language);
                    break;
                case MonsterType.Normal:
                    this.description.text = Translate.Instance.GetRow(Translate.rowIds.ID_MONSTER_NORMAL_INFO).GetStringData(GameMaster.language);
                    this.type.text = Translate.Instance.GetRow(Translate.rowIds.ID_MONSTER_NORMAL).GetStringData(GameMaster.language);
                    break;
                case MonsterType.Special:
                    this.description.text = Translate.Instance.GetRow(Translate.rowIds.ID_MONSTER_SPECIAL_INFO).GetStringData(GameMaster.language);
                    this.type.text = Translate.Instance.GetRow(Translate.rowIds.ID_MONSTER_SPECIAL).GetStringData(GameMaster.language);
                    break;
            }
        }
        else
        {
            
            this.description.text = Translate.Instance.GetRow(_item.descriptionId).GetStringData(GameMaster.language);
        }
        
        //this.sprite.spriteName = _item.sprite.spriteName;

        for (int i = 0; i < this.sprites.Count; i++)
        {
            this.sprites[i].gameObject.SetActive(false);
        }
     
        for (int i = 1; i < _item.sprites.Length; i++)
        {
            this.sprites[i -1].gameObject.SetActive(true);
            this.sprites[i -1].spriteName = _item.sprites[i].spriteName;
        }

        this.spriteGird.Reposition();
    }
   

	#endregion

	#region Protected methods

	#endregion
	
	#region Private methods

	#endregion
	
	#region Events & Notifications

	#endregion



}
