﻿	using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using GoogleFu;


public class GUIShop : Singleton<GUIShop> {


	public Transform shopItemGemsPrefab;
	public Transform shopItemWoodPrefab;
	public Transform shopItemUnlockContentPrefab;
	public Transform shopItemTikiPrefab;

	public GameObject gemsBar;

	public GameObject background;

	public string[] categoryGems;
	public Transform categoryGemsTransform;
	public UIGrid categoryGemsGrid;
   

	public string[] categoryUnlock;
	public UIGrid categoryUnlockGrid;
	public Transform categoryUnlockTransform;
  

	public string[] categoryWood;
	public Transform categoryWoodTransform;
	public UIGrid categoryWoodGrid;
   

	public string[] categoryGlobalShop;
	public Transform categoryGlobalShopTransform;
	public UIGrid categoryGlobalShopGrid;
	public GameObject informationLabel;
	
	public GameObject fade;
	
	public UIScrollView globalScrollView;
	
	public GameObject restoreButton;
	public GameObject watchAdButton;
	public GameObject backButton;

	private List<GUIShopItem> allGemsItems;
	private List<GUIShopItem> allUnlockItems;
	private List<GUIShopItem> allWoodItems;
	private List<GUIShopItem> allGlobalShopItems;
	//private GUIShopItem[] allGlobalShopItems;
	// Use this for initialization
	void Start () {

		allGemsItems = new List<GUIShopItem>();
		allUnlockItems = new List<GUIShopItem>();
		allWoodItems = new List<GUIShopItem>();
		allGlobalShopItems = new List<GUIShopItem>();

#if UNITY_EDITOR
		BuildGemsShopItems();
#endif
	}
	

  

	public void onInitialised()
	{
		BuildGemsShopItems();
	}

	// Update is called once per frame
	void Update () {
	
	}
	
	public void ShowBackground()
	{
		this.background.SetActive(true); 
	}

	public void HideBackground()
	{
		this.background.SetActive(false);
	}
	
	public void ShowUnlocks()
	{
		fade.SetActive(true);
		fade.collider.enabled = true;
		categoryUnlockTransform.gameObject.SetActive(true);

		StartCoroutine("ShowWithTweener", allUnlockItems);
				
	}

	public void ShowWood()
	{
		fade.SetActive(true);

		categoryWoodTransform.gameObject.SetActive(true);
		fade.collider.enabled = true;
		StartCoroutine("ShowWithTweener", allWoodItems);

	}

	public void ShowGems()
	{
		fade.SetActive(true);
		fade.collider.enabled = true;
		categoryGemsTransform.gameObject.SetActive(true);

		StartCoroutine("ShowWithTweener", allGemsItems);
		
		GameMaster.gameState = GameState.GUIWINDOW;

	}
	
	public void ShowGlobalShop()
	{
		fade.SetActive(true);
		gemsBar.SetActive(true);
		categoryGlobalShopTransform.gameObject.SetActive(true);
		
		StartCoroutine("ShowWithTweener", allGlobalShopItems);
		//this.informationLabel.SetActive(true);
		this.ShowBackground();
		GameMaster.gameState = GameState.GUIWINDOW;
		
		this.restoreButton.SetActive(true);
		//this.watchAdButton.SetActive(true);
		this.backButton.SetActive(true);
		
	}
	
	

	public void CloseGems()
	{
		fade.SetActive(false);
		fade.collider.enabled = false;
		categoryGemsTransform.gameObject.SetActive(false);
		GameMaster.gameState = GameState.RUNNING;

		for (int i = 0; i < allGemsItems.Count; i++)
		{
			allGemsItems[i].MyTransform.localScale = Vector3.zero;
			allGemsItems[i].tweener.ResetToBeginning();
		}
		GameMaster.guiOperation.OnClick_Pausa();
		NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.PauseClicked));
		//GameMaster.guiOperation.OnClick_Pausa();
	}


	public void CloseUnlocks()
	{
		fade.SetActive(false);
		fade.collider.enabled = false;
		categoryUnlockTransform.gameObject.SetActive(false);

		for (int i = 0; i < allUnlockItems.Count; i++)
		{
			allUnlockItems[i].MyTransform.localScale = Vector3.zero;
			allUnlockItems[i].tweener.ResetToBeginning();
		}
		NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.PauseClicked));
		//GameMaster.guiOperation.OnClick_Pausa();

	}

	public void CloseWood()
	{
		fade.SetActive(false);
		fade.collider.enabled = false;
		categoryWoodTransform.gameObject.SetActive(false);

		for (int i = 0; i < allWoodItems.Count; i++)
		{
			allWoodItems[i].MyTransform.localScale = Vector3.zero;
			allWoodItems[i].tweener.ResetToBeginning();
		}
		NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.PauseClicked));
		//GameMaster.guiOperation.OnClick_Pausa();

	}
	
	public void CloseGlobalShop()
	{
		fade.SetActive(false);
		
		categoryGlobalShopTransform.gameObject.SetActive(false);
		GameMaster.gameState = GameState.RUNNING;
		gemsBar.SetActive(false);
		for (int i = 0; i < allGlobalShopItems.Count; i++)
		{
			allGlobalShopItems[i].MyTransform.localScale = Vector3.zero;
			allGlobalShopItems[i].tweener.ResetToBeginning();
		}
		
		this.HideBackground();
		//this.informationLabel.SetActive(false);
		
		if(Application.loadedLevelName == "Mission Select")
		{
			GameMaster.guiOperation.mainPanel.gameObject.SetActive(true);
		}
		
		if(Application.loadedLevelName.Contains("Level"))
		{
			GameMaster.guiOperation.OnClick_CLOSE_GLOBAL_SHOP();
		}
		
		this.restoreButton.SetActive(false);
		//this.watchAdButton.SetActive(false);
		this.backButton.SetActive(false);
		
		//NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.PauseClicked));
	}

	public IEnumerator ShowWithTweenerGlobal(GUIShopItem[] items)
	{
		//Debug.Log(items.Length);
		for (int i = 0; i < items.Length ; i++)
		{
			globalScrollView.ResetPosition();
			items[i].transform.localScale = new Vector3(0.75f, 0.75f, 0.75f);
			//items[i].tweener.ResetToBeginning();
			//items[i].tweener.PlayForward();

			categoryGlobalShopGrid.Reposition();

			
		}

		yield return null;
	}
	
	public IEnumerator ShowWithTweener(List<GUIShopItem> items)
	{
		
		for (int i = 0; i < items.Count; i++)
		{
			globalScrollView.ResetPosition();
			items[i].transform.localScale = new Vector3(0.75f, 0.75f, 0.75f);
			//items[i].tweener.PlayForward();
			categoryGlobalShopGrid.Reposition();
			
		}
		
		yield return null;
	}

	private void BuildGemsShopItems()
	{

		#region Gems Category
		for (int itemIndex = 0; itemIndex < categoryGems.Length; itemIndex++)
		{
			GUIShopItem item = (Instantiate(shopItemGemsPrefab, Vector3.zero, Quaternion.identity) as Transform).GetComponent<GUIShopItem>();

			item.transform.parent = categoryGemsTransform;
			item.transform.localPosition = Vector3.zero;
			item.transform.localScale = Vector3.zero;

			if(!categoryGems[itemIndex].Equals("VIDEO_AD"))
			{
				item.item = Unibiller.GetPurchasableItemById(categoryGems[itemIndex]);
			
				//item.titleLabel.text = Translate.Instance.GetRow(Translate.rowIds.ID_SHOP_GEMS_1).GetStringData(GameMaster.language);
				//item.priceLabel.text = "1.99$";
				//item.descriptionLabel.text = Translate.Instance.GetRow(Translate.rowIds.ID_SHOP_GEMS_1_INFO).GetStringData(GameMaster.language);

				item.titleLabel.text = item.item.localizedTitle;
				item.priceLabel.text = item.item.localizedPriceString;
				item.descriptionLabel.text = item.item.localizedDescription;
			}
			else
			{
				item.isAdItem = true;
				item.titleLabel.text =  Translate.Instance.GetRow(Translate.rowIds.ID_SHOP_WATCH_AD).GetStringData(GameMaster.language);
				item.priceLabel.text = Translate.Instance.GetRow(Translate.rowIds.ID_REKLAMA).GetStringData(GameMaster.language);
				item.descriptionLabel.text = Translate.Instance.GetRow(Translate.rowIds.ID_SHOP_WATCH_AD_INFO).GetStringData(GameMaster.language);
			}

			allGemsItems.Add(item);


		}
		categoryGemsGrid.Reposition();
		categoryGemsTransform.gameObject.SetActive(false);
		#endregion

		#region Unlocks Tiki
		for (int itemIndex = 0; itemIndex < categoryUnlock.Length; itemIndex++)
		{
			GUIShopItem item = (Instantiate(shopItemTikiPrefab, Vector3.zero, Quaternion.identity) as Transform).GetComponent<GUIShopItem>();

			item.item = Unibiller.GetPurchasableItemById(categoryUnlock[itemIndex]);

			item.transform.parent = categoryUnlockTransform;
			item.transform.localPosition = Vector3.zero;
			item.transform.localScale = Vector3.zero;
		   
			//item.titleLabel.text = Translate.Instance.GetRow(Translate.rowIds.ID_SHOP_MASKS).GetStringData(GameMaster.language);
			item.titleLabel.text = item.item.localizedTitle;
			//item.priceLabel.text = "2,69€";
			item.priceLabel.text = item.item.localizedPriceString;
			//item.descriptionLabel.text = Translate.Instance.GetRow(Translate.rowIds.ID_SHOP_MASKS_INFO).GetStringData(GameMaster.language);;
			item.descriptionLabel.text = item.item.localizedDescription;
			allUnlockItems.Add(item);
		}
		//categoryUnlockGrid.Reposition();
		categoryUnlockTransform.gameObject.SetActive(false);
		#endregion

		#region Woods
		for (int itemIndex = 0; itemIndex < categoryWood.Length; itemIndex++)
		{
			GUIShopItem item = (Instantiate(shopItemWoodPrefab, Vector3.zero, Quaternion.identity) as Transform).GetComponent<GUIShopItem>();

			item.item = Unibiller.GetPurchasableItemById(categoryWood[itemIndex]);
			//Debug.Log(item.item.Id);
			item.transform.parent = categoryWoodTransform;
			item.transform.localPosition = Vector3.zero;
			item.transform.localScale = Vector3.zero;
		  
			item.titleLabel.text = Translate.Instance.GetRow(Translate.rowIds.ID_SHOP_WOOD_TITLE).GetStringData(GameMaster.language);
			item.name = "com.roscogames.AddWoodConsumable";
			//item.priceLabel.text = "1500";
			item.descriptionLabel.text = Translate.Instance.GetRow(Translate.rowIds.ID_SHOP_WOOD_DESCRIPTION).GetStringData(GameMaster.language);

			allWoodItems.Add(item);
		}
		//categoryUnlockGrid.Reposition();
		categoryWoodTransform.gameObject.SetActive(false);
		#endregion
		
		#region Global
		for (int itemIndex = 0; itemIndex < categoryGlobalShop.Length; itemIndex++)
		{
			Transform prefab;
			
			if(categoryGlobalShop[itemIndex].Contains("gems")|| categoryGlobalShop[itemIndex].Contains("VIDEO_AD") )
				prefab = shopItemGemsPrefab;
			else if(categoryGlobalShop[itemIndex].Contains("tiki"))
				prefab = shopItemTikiPrefab;
			else
				prefab = shopItemUnlockContentPrefab;
		
			GUIShopItem item = (Instantiate(prefab, Vector3.zero, Quaternion.identity) as Transform).GetComponent<GUIShopItem>();

			item.transform.parent = categoryGlobalShopTransform;
			item.transform.localPosition = Vector3.zero;
			item.transform.localScale = Vector3.zero;

			if(!categoryGlobalShop[itemIndex].Equals("VIDEO_AD"))
			{
				item.item = Unibiller.GetPurchasableItemById(categoryGlobalShop[itemIndex]);
				
				if(categoryGlobalShop[itemIndex].Contains("tiki"))
					item.name = "2. " + item.name;
				else if(categoryGlobalShop[itemIndex].Contains("Unlock"))
					item.name = "1. " + item.name;
				 else
				 {
					string prefix = Regex.Match(item.item.Id, @"\d+").Value;
					item.name = prefix + ". " + item.name;
				 }
		
				if(categoryGlobalShop[itemIndex].Contains("tikimasks"))
				{
					//item.titleLabel.text = Translate.Instance.GetRow(Translate.rowIds.ID_SHOP_MASKS).GetStringData(GameMaster.language);
					//item.priceLabel.text = "2,69€";
					//item.descriptionLabel.text = Translate.Instance.GetRow(Translate.rowIds.ID_SHOP_MASKS_INFO).GetStringData(GameMaster.language); ;

					item.titleLabel.text = item.item.localizedTitle;
					item.priceLabel.text = item.item.localizedPriceString;
					item.descriptionLabel.text = item.item.localizedDescription;
				
				}
				else
				{

					//if(item.item.Id.Contains("1000"))
					//{
					//    item.priceLabel.text = item.item.localizedPriceString;
					//    item.titleLabel.text = Translate.Instance.GetRow(Translate.rowIds.ID_SHOP_GEMS_1).GetStringData(GameMaster.language);
					//    item.descriptionLabel.text = Translate.Instance.GetRow(Translate.rowIds.ID_SHOP_GEMS_1_INFO).GetStringData(GameMaster.language);
					//}
					//if (item.item.Id.Contains("2000"))
					//{
					//    item.priceLabel.text = item.item.localizedPriceString;
					//    item.titleLabel.text = Translate.Instance.GetRow(Translate.rowIds.ID_SHOP_GEMS_2).GetStringData(GameMaster.language);
					//    item.descriptionLabel.text = Translate.Instance.GetRow(Translate.rowIds.ID_SHOP_GEMS_2_INFO).GetStringData(GameMaster.language);
					//}
					//if (item.item.Id.Contains("5000"))
					//{
					//    item.priceLabel.text = item.item.localizedPriceString;
					//    item.titleLabel.text = Translate.Instance.GetRow(Translate.rowIds.ID_SHOP_GEMS_3).GetStringData(GameMaster.language);
					//    item.descriptionLabel.text = Translate.Instance.GetRow(Translate.rowIds.ID_SHOP_GEMS_3_INFO).GetStringData(GameMaster.language);
					//}
					//if (item.item.Id.Contains("10000"))
					//{
					//    item.priceLabel.text = item.item.localizedPriceString;
					//    item.titleLabel.text = Translate.Instance.GetRow(Translate.rowIds.ID_SHOP_GEMS_4).GetStringData(GameMaster.language);
					//    item.descriptionLabel.text = Translate.Instance.GetRow(Translate.rowIds.ID_SHOP_GEMS_4_INFO).GetStringData(GameMaster.language);
					//}
					//if (item.item.Id.Contains("15000"))
					//{
					//    item.priceLabel.text = item.item.localizedPriceString;
					//    item.titleLabel.text = Translate.Instance.GetRow(Translate.rowIds.ID_SHOP_GEMS_5).GetStringData(GameMaster.language);
					//    item.descriptionLabel.text = Translate.Instance.GetRow(Translate.rowIds.ID_SHOP_GEMS_5_INFO).GetStringData(GameMaster.language);
					//}

					item.titleLabel.text = item.item.localizedTitle;
					item.priceLabel.text = item.item.localizedPriceString;
					item.descriptionLabel.text = item.item.localizedDescription;

				 
				}


			}
			else
			{
				item.isAdItem = true;
				item.titleLabel.text =  Translate.Instance.GetRow(Translate.rowIds.ID_SHOP_WATCH_AD).GetStringData(GameMaster.language);
				item.priceLabel.text = Translate.Instance.GetRow(Translate.rowIds.ID_REKLAMA).GetStringData(GameMaster.language);
				item.descriptionLabel.text = Translate.Instance.GetRow(Translate.rowIds.ID_SHOP_WATCH_AD_INFO).GetStringData(GameMaster.language);
			}
			allGlobalShopItems.Add(item);
		
			//allGlobalShopItems.Sort(delegate(GUIShopItem p1, GUIShopItem p2) { return p1.name.CompareTo(p2.name); });
		}
		
		//allGlobalShopItems = categoryGlobalShopTransform.GetComponentsInChildren<GUIShopItem>();
		//categoryUnlockGrid.Reposition();
		categoryGlobalShopTransform.gameObject.SetActive(false);
		#endregion
	}

	public void RestorePurchuase()
	{
		Unibiller.restoreTransactions();
	}
	
	#region IComparable implementation
	
	
	#endregion
}
