﻿using UnityEngine;
using System.Collections;

public class GUIShopGems : GUIShopItem {

	#region Variables
	
	public GameObject bagTier1;
	public GameObject bagTier2;
	public GameObject bagTier3;
	public GameObject bagTier4;
	
	#endregion

	#region Initialization

	protected override void Awake () 
	{
		base.Awake();
		
		
		
	}

	protected override void Start () 
	{
		base.Start();
		if(item != null)
		{
	 		if(item.Id.Contains("1000"))
				this.bagTier1.SetActive(true);
			else if(item.Id.Contains("2000"))
				this.bagTier2.SetActive(true);
			else if(item.Id.Contains("5000"))
				this.bagTier3.SetActive(true);
			else if(item.Id.Contains("15000"))
				this.bagTier4.SetActive(true);
			else if(item.Id.Contains("40000"))
				this.bagTier4.SetActive(true);
		

		}
		else if(gameObject.name.Contains("Item_ Gems"))
			this.bagTier4.SetActive(true);
        
	}

	#endregion
	
	// Update is called once per frame
	protected override void Update () {
	
		base.Update();
	}

	#region Public methods

	#endregion

	#region Protected methods

	#endregion
	
	#region Private methods

	#endregion
	
	#region Events & Notifications

	#endregion



}
