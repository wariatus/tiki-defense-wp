﻿using UnityEngine;
using System.Collections;

public class GUIShopItem : ExtendedMonoBehavior {

	public PurchasableItem item
	{
		get { return _item; }
		set { _item = value; }
	}

	public UILabel titleLabel
	{
		get 
		{
			if(_titleLabel == null)
                _titleLabel = transform.FindChild("Label:Title").GetComponent<UILabel>();

			return _titleLabel;
		}

	}

    public UILabel descriptionLabel
    {
        get
        {
            if (_descriptionLabel == null)
                _descriptionLabel = transform.FindChild("Label:Description").GetComponent<UILabel>();

            return _descriptionLabel;
        }

    }

	public UILabel priceLabel
	{
		get 
		{
			if(_priceLabel == null)
				_priceLabel = transform.FindChild("Button/Label").GetComponent<UILabel>();
			
			return _priceLabel;
		}
		
	}

    public UITweener tweener { get { return _tweener; } }
	public bool isAdItem;

	private PurchasableItem _item;
	private UILabel _titleLabel;
    private UILabel _descriptionLabel;
	private UILabel _priceLabel;
    private UITweener _tweener;

    override protected void Awake()
    {
        this._tweener = GetComponent<UITweener>();
    }
	// Use this for initialization
    override protected void Start()
    {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void BuyItem()
	{
		if(!isAdItem)
		{
			if(name == "com.roscogames.AddWoodConsumable")
			{
				if(Unibiller.DebitBalance("gems", GlobalConfiguration.woodChargeCost))
				{
					GUIShop.Instance.CloseWood();
					SaveData.premiumWood++;
				}
				else
				{
					//GUIShop.Instance.CloseWood();
					//GUIShop.Instance.ShowGlobalShop();
				}
			}
			else
			{

				Unibiller.initiatePurchase(item.Id);
			}


		}
		else
		{
			ADManager.Instance.PlayAdVideo(true);
			GA.API.Design.NewEvent("Free gems for watching a video");

		#if UNITY_EDITOR
			UIShopWindowConfirmation.Instance.Show();
		#endif
		}
	}
}
