﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class GUIGridManager : Singleton<GUIGridManager> {

	/// <summary>
	/// Offset o ile przetawiaja sie dostepne przyciski wiez
	/// </summary>
	public float swiftOffset;
	
	public List<GUIGridTower> towers;
	public Camera guiCamera;
	public GUITowerOptionsManager towerManager;
	public AudioClip soundOnClick;
	
	public static bool isShow;
	private Configuration configuration;
	private bool enableButtons;
	protected override void Awake ()
	{
		base.Awake ();
	}
	
	// Use this for initialization
	protected override void Start () {
	
		base.Start();
		NotificationCenter.defaultCenter.addListener(OnGridClicked, NotificationType.GridClicked);
		NotificationCenter.defaultCenter.addListener(OnShopEnter, NotificationType.OnEnterToShop);
		NotificationCenter.defaultCenter.addListener(OnShopEnter, NotificationType.OnLevelMainMenuOpen);
		NotificationCenter.defaultCenter.addListener(OnBlockerCLicked, NotificationType.BlockerClick);
		NotificationCenter.defaultCenter.addListener(OnTikiClicked, NotificationType.TikiMaskClicked);
		NotificationCenter.defaultCenter.addListener(OnCountStart, NotificationType.OnGameStateChanged);
		configuration = LevelConfigurations.GetConfiguration(Application.loadedLevelName);
		
		
	
		for(int i = towers.Count - 1;  i >= 0; i--)
		{
			//Debug.Log(towers[i].type);
			switch(towers[i].type)
			{
				
				case TowersType.Totem_level_1:	
					if(!configuration.towerTotem)
						towers.Remove(towers[i]);
				break;
				
				case TowersType.VooDoo_level_1:	
					if(!configuration.towerVooDoo)
						towers.Remove(towers[i]);
				
				break;
				
				case TowersType.Cave_level_1:	
					if(!configuration.towerCave)
						towers.Remove(towers[i]);
				
				break;
				
				case TowersType.Tornado_level_1:	
					if(!configuration.towerTornado)
						towers.Remove(towers[i]);
				
				break;
				
				case TowersType.Cloud_level_1:
					if(!configuration.towerThundercloud)
						towers.Remove(towers[i]);
				
				break;
				
				case TowersType.FireTotem_level_1:
					if(!configuration.towerFireTotem)
						towers.Remove(towers[i]);
				
				break;
			
				case TowersType.Campfire_level_1:
					if(!configuration.towerCampfire)
						towers.Remove(towers[i]);
				
				break;
				
				case TowersType.RollingBoulder_level_1:
					if(!configuration.towerRollingBoulder)
						towers.Remove(towers[i]);
				
				break;
				
				case TowersType.MagicShot_level_1:
					if(!configuration.towerMagicShot)
						towers.Remove(towers[i]);
				
				break;
				
				case TowersType.IceShot_level_1:
					if(!configuration.towerIceShot)
						towers.Remove(towers[i]);
				
				break;
				
				case TowersType.Melter_level_1:
					if(!configuration.towerMelter)
						towers.Remove(towers[i]);
				
				break;
				
				case TowersType.IceTotem_level_1:
					if(!configuration.towerIceTotem)
						towers.Remove(towers[i]);
				
				break;
				
				case TowersType.AirTotem_level_1:
					if(!configuration.towerAirTotem)
						towers.Remove(towers[i]);
				
				break;
				
				case TowersType.RockHammer_level_1:
					if(!configuration.towerRockHammer)
						towers.Remove(towers[i]);
				
				break;

				case TowersType.Drum_level_1:
					if(!configuration.towerDrum)
						towers.Remove(towers[i]);
				
				break;
				case TowersType.Necklace_level_1:
					if(!configuration.towerNecklace)
						towers.Remove(towers[i]);
				break;
				case TowersType.Anvil_level_1:
					if(!configuration.towerAnvil)
						towers.Remove(towers[i]);
				break;
				case TowersType.Cauldron_level_1:
					if(!configuration.towerCauldron)
						towers.Remove(towers[i]);
				break;
				case TowersType.RunicStone_level_1:
					if(!configuration.towerRunicStone)
						towers.Remove(towers[i]);
				break;
						
			}
		}
		
	
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	private void EnableButtons()
	{
		this.enableButtons = true;
		
	}
	
	public void DisableTowersGUI()
	{
	
		StopCoroutine("ShowTowers");
		for(int i = 0; i < towers.Count; i++)
		{
			//towers[i].gameObject.SetActive(false);
			towers[i].isShow = false;														
			towers[i].tweener.ResetToBeginning();
			towers[i].transform.localScale = Vector3.zero;		
		}

		isShow = false;
	}

	void GUIOff()
	{
		GameMaster.guiButtonShowUp = false;
	}

	void OnBlockerCLicked(Notification n)
	{
		DisableTowersGUI();
		towerManager.DisableTowersGUI();
	}

	void OnTikiClicked(Notification n)
	{
		this.DisableTowersGUI();
	}
	
	void OnShopEnter(Notification n)
	{
		if(isShow || towerManager.isShow)
		{
			DisableTowersGUI();
			towerManager.DisableTowersGUI();
		
		}
	}
	
	void OnGridClicked(Notification n)
	{
		GridClickNotification notification = n as GridClickNotification;
      
		if(notification == null) return;


		if(isShow || towerManager.isShow)
		{
			if(!GameMaster.onTutorial)
			{
				DisableTowersGUI();
				towerManager.DisableTowersGUI();
				notification.cell.Unclicked();
			}

			return;
		}

        Debug.Log(enableButtons);
		if(notification.cell != null && this.enableButtons)
		{
			if(notification.cell.isClickable)
			{
				Vector3 pos = mainCamera.WorldToScreenPoint(notification.position);
				pos.z = 0;
				pos.y += 15;
				MyTransform.position = guiCamera.ScreenToWorldPoint(pos);
					
				NGUITools.PlaySound(soundOnClick);

				StartCoroutine("ShowTowers", notification.cell);
			}
		}
	}
	
	private void OnCountStart(Notification n)
	{
        if (LevelConfigurations.GetConfiguration(Application.loadedLevelName).newTypeOfWeapon != PoolObjectType.Null)
            Invoke("EnableButtons", 0.1f);
        else
           Invoke("EnableButtons", 0.1f);
//		if(Application.loadedLevelName == "Level1x01")
//			enableButtons = true;
//		else
//			Invoke("EnableButtons", 0.1f);
	}

		
	private IEnumerator ShowTowers(GridCell cell)
	{
		if(!isShow)
		{
			isShow = true;
			int towersCount = towers.Count;
			int counter = 0;
			Vector3 viewPortPosition;

			// Aspect
			// 480x320 = 1.5 / 0.1 x 0.9
			// 960x640 = 1.5 / 0.1 x 0.9
			// 1024x768 = 1.333 / 0.1 x 0.9
			// 2048x768 = 1.333 / 0.1 x 0.9
			// 1136x640 = 1.775 / 0.2 x 0.8
			
			float minViewPortX;
			float maxViewPortX;

			if(mainCamera.aspect >= 1.5f && mainCamera.aspect <= 1.6f || mainCamera.aspect >= 1.3f && mainCamera.aspect <= 1.4)
			{
				minViewPortX = 0.1f;
				maxViewPortX = 0.9f;
			}
			else
			{
				minViewPortX = 0.2f;
				maxViewPortX = 0.8f;
			
			}

			float devision  = 1;

			if(towers.Count == 2)
				devision = 4;
			else if(towers.Count == 3)
				devision = 3;
			else if(towers.Count == 4)
				devision = 2.5f;
			else if(towers.Count == 5)
				devision = 2.5f;
			else if(towers.Count == 6)
				devision = 2.5f;

			viewPortPosition = mainCamera.WorldToViewportPoint(cell.transform.position);

			float penality = 0;

			#region 2-Towers order
			if(towers.Count == 2)
			{
				if(mainCamera.aspect < 1.7)
				{
					// Prawo
					if(viewPortPosition.x >= 0.85f && viewPortPosition.x <= 0.95f)
						penality = 0.5f;
					// Lewo
					else if(viewPortPosition.x >= 0.05f && viewPortPosition.x <= 0.15f)
						penality = -0.5f;
				}
				else
				{
					// Prawo
					if(viewPortPosition.x >= 0.75f && viewPortPosition.x <= 0.95f)
						penality = 0.5f;
					// Lewo
					else if(viewPortPosition.x >= 0.15f && viewPortPosition.x <= 0.2f)
						penality = -0.5f;
				}
			}
			#endregion
			#region 3-Towers order
			else if(towers.Count == 3)
			{
				if(mainCamera.aspect < 1.7)
				{
					// Prawo
					if(viewPortPosition.x >= 0.85f && viewPortPosition.x <= 0.95f)
						penality = 1f;
					// Lewo
					else if(viewPortPosition.x >= 0.05f && viewPortPosition.x <= 0.15f)
						penality = -1f;
				}
				else
				{
					// Prawo
					if(viewPortPosition.x >= 0.75f && viewPortPosition.x <= 0.95f)
						penality = 1f;
					// Lewo
					else if(viewPortPosition.x >= 0.15f && viewPortPosition.x <= 0.2f)
						penality = -1f;
				}
			}
			#endregion
			#region 4-Towers order
			else if(towers.Count == 4)
			{ 
				//Debug.Log(mainCamera.aspect + " " + viewPortPosition.x);
				if(mainCamera.aspect < 1.7)
				{
					// Prawo
					if(viewPortPosition.x >= 0.8f && viewPortPosition.x <= 0.95f)
						penality = 1.5f;
					// Lewo
					else if(viewPortPosition.x >= 0.05f && viewPortPosition.x <= 0.17f)
						penality = -1.5f;
				}
				else
				{
					// Prawo
					if(viewPortPosition.x >= 0.75f && viewPortPosition.x <= 0.95f)
						penality = 1.5f;
					// Lewo
					else if(viewPortPosition.x >= 0.15f && viewPortPosition.x <= 0.27f)
						penality = -1.5f;
				}
			}
			#endregion
			#region 5-Towers order
			else if(towers.Count == 5)
			{
			
				Debug.Log(viewPortPosition.x + " " + mainCamera.aspect);
				if(mainCamera.aspect < 1.7)
				{
					// Prawo
					if(viewPortPosition.x >= 0.82f && viewPortPosition.x <= 0.98f)
						penality = 2f;
					// Lewo
					else if(viewPortPosition.x >= 0.05f && viewPortPosition.x <= 0.17f)
						penality = -2f;
				}
				else
				{
					// Prawo
					if(viewPortPosition.x >= 0.79f && viewPortPosition.x <= 0.97f)
						penality = 2f;
					// Lewo
					else if(viewPortPosition.x >= 0.15f && viewPortPosition.x <= 0.27f)
						penality = -2f;
				}
			}
			#endregion
			#region 6-Towers order
			else if(towers.Count == 6)
			{
				if(mainCamera.aspect < 1.7)
				{
					// Prawo
					if(viewPortPosition.x >= 0.85f && viewPortPosition.x <= 0.95f)
						penality = 1f;
					// Lewo
					else if(viewPortPosition.x >= 0.05f && viewPortPosition.x <= 0.15f)
						penality = -1f;
				}
				else
				{
					// Prawo
					if(viewPortPosition.x >= 0.75f && viewPortPosition.x <= 0.95f)
						penality = 1f;
					// Lewo
					else if(viewPortPosition.x >= 0.15f && viewPortPosition.x <= 0.25f)
						penality = -1f;
				}
			}
			#endregion
		
			MyTransform.localPosition = new Vector3((MyTransform.localPosition.x - ((towers.Count * swiftOffset) / devision)) - (swiftOffset * penality), 
													MyTransform.localPosition.y,
			                                        MyTransform.localPosition.z);

			//Debug.Log(viewPortPosition + " Aspect: " + mainCamera.aspect + " MinViewPortX: " + minViewPortX);
			//Debug.Log(penality);
			while(towersCount-- > 0)
			{

				//towers[towersCount].gameObject.SetActive(true);
				towers[towersCount].Show(cell);	

				//if(towers.Count <=2)
				//{

				if(towers.Count >= 4)
				{
					towers[towersCount].transform.localPosition = new Vector3(swiftOffset * counter, 
					                                                          viewPortPosition.y > 0.2f ? -50f : 50, 
					                                                          0);

				}
				else
				{
					towers[towersCount].transform.localPosition = new Vector3(swiftOffset * counter, 
				                                                          viewPortPosition.y > 0.2f ? -50f : 50,
				                                                          0);
				}
			
//				}
//				else if (towers.Count <= 4)
//				{
//					if(mainCamera.WorldToViewportPoint(cell.transform.position).x > 0.7f)
//						towers[towersCount].transform.localPosition = new Vector3(-(swiftOffset * 3 - (counter * swiftOffset)), -50, 0);
//					else
//						towers[towersCount].transform.localPosition = new Vector3(swiftOffset / 2 + (counter * swiftOffset), -50, 0);
//				}

				counter++;

				//Debug.Log("Counter: " + towersCount );
				yield return new WaitForSeconds(0.1f);
				
			
			}

		
			
		}
		
	}
}
