﻿using UnityEngine;
using System.Collections;

public class DisableCollierOnTutorial : MonoBehaviour {

	// Use this for initialization
	private UIButton button;
	void Start () {

		button = GetComponent<UIButton>();

		if(Application.loadedLevel == 3 || Application.loadedLevel == 5)
		{
			button.isEnabled = false;
			NotificationCenter.defaultCenter.addListener(OnTutorialoff, NotificationType.TutorialBuildTowerOff);
		}
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	void OnTutorialoff(Notification n)
	{
		button.isEnabled = true;
	}
}
