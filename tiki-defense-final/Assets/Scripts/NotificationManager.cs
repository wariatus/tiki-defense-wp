﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

public class NotificationManager : MonoBehaviour {

	public NotifiationTemplate[] notifications;			// dostępne notyfikacje

	public GameObject iOSGameObjectNotification;		// GameObject z komponentem zarządzania notyfikacji na iOS	
	public GameObject androidGameObjectNotification;	// GameObject z komponentem zarządzania notyfikacji na Android

	// NotificationServices dostępny w Unity3D 4.5.5 nie jest z optymalizowany do iOS8
	// Aktualnie, iOS 8 wymaga, aby gracz zezwolił na wysyłanie notyfikacji zanim jaka kolwiek
	// zostanie wysłana. Poniższa metoda wywołuje natywny kod w Obj-C, który wymusi pojawienie 
	// się okienka zapytaniem o wysyłanie notyfikacji. Plugin dostępny w Plugins->iOS->IOS8NotificationFix.mm
	[DllImport ("__Internal")]
	private static extern float _EnableLocalNotificationIOS8();

	void Awake()
	{

		// W przypadku gdy mamy doczynienia z platformą iOS oraz gdy 
		// nie jesteśmy w edytorze, uruchamiamy metode, która wywoływana jest plugnie
		// IOS8NotificationFix.mm
#if UNITY_IPHONE && !UNITY_EDITOR
		_EnableLocalNotificationIOS8();
#endif
	}

	// Use this for initialization
	void Start () {


		// Jeżeli mamy doczynienia z platformą iOS, aktywujemy GO
		// który posiada komponent do zarządzania notyfikacjami na 
		// tej platformie
#if UNITY_IPHONE

		this.iOSGameObjectNotification.SetActive(true);
#else
		this.androidGameObjectNotification.SetActive(true);
#endif
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
