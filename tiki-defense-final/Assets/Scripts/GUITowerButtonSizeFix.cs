﻿using UnityEngine;
using System.Collections;

public class GUITowerButtonSizeFix : ExtendedMonoBehavior {

	#region Variables

    private UISprite _sprite;
	
	#endregion

	#region Initialization

	protected override void Awake () 
	{
		base.Awake();
	}

	protected override void Start () 
	{
		base.Start();
        this._sprite = GetComponent<UISprite>();
        this._sprite.SetDimensions(42, 42);

	}

	#endregion
	
	// Update is called once per frame
	protected override void Update () {
	
		base.Update();
	}

	#region Public methods

	#endregion

	#region Protected methods

	#endregion
	
	#region Private methods

	#endregion
	
	#region Events & Notifications

	#endregion



}
