using UnityEngine;
using System.Collections;

public class InputManager : MonoBehaviour {
	
	/// <summary>
	/// Aktualny stan inputu
	/// </summary>
	public static InputState state;
	
	/// <summary>
	/// The current input position.
	/// </summary>
	public Vector2 currentInputPosition;
	
	public Camera guiCamera;

	public LayerMask mask;
	public LayerMask guiMask;

	private  bool isTikiMaskClick;

	private bool canClick;
	private bool canClickOnCell = true;
	private bool canClickOnTower = true;
	// Use this for initialization
	void Start () {

        if (guiCamera == null)
            guiCamera = GameObject.FindGameObjectWithTag("GUICamera").GetComponent<Camera>();

		Invoke("EnableInput", 2f);
	}


	void EnableInput()
	{
		canClick = true;
		NotificationCenter.defaultCenter.addListener(OnTikiMaskUsed, NotificationType.TikimaskUsed);
		NotificationCenter.defaultCenter.addListener(OnTikiMaskClicked, NotificationType.TikiMaskClicked);
	}
	
	// Update is called once per frame
	void Update () {

        if (GameMaster.gameState == GameState.RUNNING || GameMaster.gameState == GameState.PAUSA)
        {

            if (GlobalConfiguration.platformType == PlatformType.MOBILE)
            {
                if (Input.touchCount > 0)
                {
                    Touch touch = Input.touches[0];

                    if (touch.phase == TouchPhase.Began)
                    {
                        state = InputState.Click;

                        currentInputPosition = touch.position;

                        // Wylaczanie okienka
                        //if (GameMaster.guiOperation.isBuyChargeButtonVisible)
                        //    GameMaster.guiOperation.buyChargeButton.Toggle();

                        CheckRayCollision(currentInputPosition);

                    }

                }
            }
            else if (GlobalConfiguration.platformType == PlatformType.STANDALONE)
            {
                if (Input.GetMouseButtonDown(0) )
                {
                    state = InputState.Click;

                    // Wylaczanie okienka
                    //if (GameMaster.guiOperation.isBuyChargeButtonVisible)
                    //    GameMaster.guiOperation.buyChargeButton.Toggle();

                    currentInputPosition = Input.mousePosition;
                    CheckRayCollision(currentInputPosition);

                }
            }

            if (state != InputState.Idle)
                state = InputState.Idle;
        }

		if(GameMaster.gameState == GameState.GUIWINDOW && GameMaster.onTutorial)
		{
			if (GlobalConfiguration.platformType == PlatformType.MOBILE)
			{
				if (Input.touchCount > 0)
				{
					Touch touch = Input.touches[0];
					
					if (touch.phase == TouchPhase.Began)
					{
                        // Wylaczanie okienka
                        //if (GameMaster.guiOperation.isBuyChargeButtonVisible)
                        //    GameMaster.guiOperation.buyChargeButton.Toggle();

						state = InputState.Click;
						
						currentInputPosition = touch.position;

						RaycastHit2D hit;
						
						hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(currentInputPosition), Vector2.zero, Mathf.Infinity, mask);
						
						if(hit.collider != null)
						{
							
							switch(hit.transform.tag)
							{
								case "Drop":
									if(Debug.isDebugBuild)
										Debug.Log("[InpuManager Tutorial]: Klik na drop");
									
									Drops.Drop drop = hit.transform.GetComponent<Drops.DropReference>().drop;
									drop.Collect();
									
									
									break;
	
								case "Tower":
									if(Debug.isDebugBuild)
										Debug.Log("[InpuManager Tutorial]: Klik na wieze");
									
									Towers.TowerBase tower = hit.transform.GetComponent<Towers.TowerBase>();
									
									if(isTikiMaskClick)
									{
										tower.ActivateTikiMask(GameMaster.tikiActieType);
										isTikiMaskClick = false;
									}
									break;
							}
						}

					}
					
				}
			}
			else if (GlobalConfiguration.platformType == PlatformType.STANDALONE)
			{
				if (Input.GetMouseButtonDown(0))
				{
					state = InputState.Click;
                    //// Wylaczanie okienka
                    //if (GameMaster.guiOperation.isBuyChargeButtonVisible)
                    //    GameMaster.guiOperation.buyChargeButton.Toggle();

					currentInputPosition = Input.mousePosition;

					RaycastHit2D hit;

					hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(currentInputPosition), Vector2.zero, Mathf.Infinity, mask);
					
					if(hit.collider != null)
					{
						
						switch(hit.transform.tag)
						{
						case "Drop":
							if(Debug.isDebugBuild)
								Debug.Log("[InpuManager]: Klik na drop");
							
							Drops.Drop drop = hit.transform.GetComponent<Drops.DropReference>().drop;
							drop.Collect();
							
							
							break;
						case "Tower":
							if(Debug.isDebugBuild)
								Debug.Log("[InpuManager]: Klik na wieze");



							Towers.TowerBase tower = hit.transform.GetComponent<Towers.TowerBase>();
							if(GameMaster.tikiActieType != TikiMaskType.NONE)
							{
								tower.ActivateTikiMask(GameMaster.tikiActieType);
								isTikiMaskClick = false;
							}
							break;
						}
					}

				}
			}
			
			if (state != InputState.Idle)
				state = InputState.Idle;
		}

	}


	private void OnTikiMaskUsed(Notification n)
	{
		isTikiMaskClick  = false;
	}

	private void OnTikiMaskClicked(Notification n)
	{
		isTikiMaskClick = true;
	}
	
	private void CheckRayCollision(Vector2 inputPosition)
	{
		if(!canClick ) return;

		RaycastHit hit3D;
		Ray ray;

		//Debug.Log("cipa 2");
		/// Jezeli mamy wyswietlony gui kupna wiez, to blokujemy wykrywanie dotyku na elementach, ktore
		/// przyslaniaja kolidery tego GUI
		ray = guiCamera.ScreenPointToRay(inputPosition);
		if(Physics.Raycast(ray, out hit3D, Mathf.Infinity, guiMask))
		{
			//Debug.Log("cipa");
			return; 
		}

		RaycastHit2D hit;

		hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(inputPosition), Vector2.zero, Mathf.Infinity, mask);

		if(hit.collider != null)
		{
			
			switch(hit.transform.tag)
			{
				case "Drop":
					if(Debug.isDebugBuild)
						Debug.Log("[InpuManager]: Klik na drop");
					
					Drops.Drop drop = hit.transform.GetComponent<Drops.DropReference>().drop;
					drop.Collect();


				break;

				case "Tower":
					if(Debug.isDebugBuild)
						Debug.Log("[InpuManager]: Klik na wieze");
				
					Towers.TowerBase tower = hit.transform.GetComponent<Towers.TowerBase>();
                   
                  
					if(GameMaster.onTutorial && Application.loadedLevelName == "Level 1x01")
					{
						NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.TutorialBuildTowerProgress));
						
					}

					if(GameMaster.tikiActieType != TikiMaskType.NONE)
					{
						tower.ActivateTikiMask(GameMaster.tikiActieType);
						isTikiMaskClick = false;
					}
					else
					{
						GameMaster.guiButtonShowUp = true;
						if(canClickOnTower)
						{
							NotificationCenter.defaultCenter.postNotification(new TowerClickedNotification(NotificationType.TowerClicked, tower, tower.MyTransform.position));
							canClickOnTower = false;
							Invoke("EnableClickOnTower", 0.45f);
						}
					}
                   
				break;
				case "Enemy":

					if(GameMaster.onTutorial) return;

					if(Debug.isDebugBuild)
                    	Debug.Log("[InpuManager]: Klik na enemy");

					UnitBase unit = hit.transform.GetComponent<UnitBase>();
					NotificationCenter.defaultCenter.postNotification(new BlockerClickNotification(NotificationType.EnemyClick, unit));
				break;
				case "Blocker":

					if(GameMaster.onTutorial || Application.loadedLevel == 3) return;
					


					if(Debug.isDebugBuild)
                    	Debug.Log("[InpuManager]: Klik na blokera");

					
					UnitBase unitBlocker = hit.transform.GetComponent<UnitBase>();
					NotificationCenter.defaultCenter.postNotification(new BlockerClickNotification(NotificationType.BlockerClick, unitBlocker));
					//Debug.Log("chuj Input");
				break;
				case "Grid":
					
//					if(Application.loadedLevelName == "Level1x01")
//				    {
//						Debug.Log(Time.timeSinceLevelLoad);	
//						if(Time.timeSinceLevelLoad < 2.5f)
//						{
//							return;
//						}
//				
//					}


					if(Debug.isDebugBuild)
                    	Debug.Log("[InpuManager]: Klik na grida");
					
					GameMaster.guiButtonShowUp = true;
					
					GridCell cell = hit.transform.GetComponent<GridCell>();
               
					// Wyslijmy posta tylko w tedy kiedy grid nie ma polozonej wiezy
                    if (cell.activeTower == null && canClickOnCell)
                    {
                        NotificationCenter.defaultCenter.postNotification(new GridClickNotification(NotificationType.GridClicked, cell.MyTransform.position, cell));
                        canClickOnCell = false;
                        int towerCount = GUIGridManager.Instance.towers.Count;
                      
                         float time = 0;
                        if(towerCount == 2)
							time = 0.05f;
						else if(towerCount == 3)
							time = 0.15f;
						else if(towerCount == 4)
							time = 0.25f;
						else if(towerCount == 5)
							time = 0.35f;
						else
							time = 0.01f;
							
                        Invoke("EnableClick", time);
                	}
					
				break;

			}
				
		}


	}
	private void EnableClickOnTower()
	{
		this.canClickOnTower = true;
	}	
	private void EnableClick()
	{
		this.canClickOnCell = true;
	}	
}
