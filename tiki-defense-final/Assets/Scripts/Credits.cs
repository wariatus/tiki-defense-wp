﻿using UnityEngine;
using System.Collections;

public class Credits : ExtendedMonoBehavior {

	#region Variables
	
	public float velocity;
	public Vector3 finishPosition;
	public bool loop;
	public float delay;
	
	private bool _canStart;
	private Vector3 _startPosition;
	
	#endregion

	#region Initialization

	protected override void Awake () 
	{
		base.Awake();
		
		this._startPosition = MyTransform.localPosition;
	}

	protected override void Start () 
	{
		base.Start();
	}

	#endregion
	
	// Update is called once per frame
	protected override void Update () {
	
		base.Update();
		
		if(this._canStart)
		{
			MyTransform.Translate(Vector3.up * this.velocity * Time.deltaTime, Space.Self);
			
			if(MyTransform.localPosition.y >= this.finishPosition.y)
			{
				if(loop)
					MyTransform.localPosition = this._startPosition;
				//else
					// Finish
			}
		
		}
		
	}

	#region Public methods

	public void StartTween()
	{
		if(this.delay == 0)
			this._canStart = true;
		else
			Invoke ("Delay", this.delay);
	}
	
	public void Stop()
	{
		this._canStart = false;
		MyTransform.localPosition = this._startPosition;
	}

	#endregion

	#region Protected methods

	#endregion
	
	#region Private methods
	
	private void Delay()
	{
		this._canStart = true;
	}

	#endregion
	
	#region Events & Notifications

	#endregion



}
