﻿using UnityEngine;
using System.Collections;

public class MainThemeMusic : MonoBehaviour {

	public AudioSource audio;

	// Use this for initialization
	void Start () {

		NotificationCenter.defaultCenter.addListener(OnMainMusicOn, NotificationType.MainThemeMusicOn);
		NotificationCenter.defaultCenter.addListener(OnMainMusicOff, NotificationType.MainThemeMusicOff);
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	private void OnMainMusicOn(Notification n)
	{
		if(Application.loadedLevel <= 2 || Application.loadedLevel == 53)
			audio.Play();
	}
	private void OnMainMusicOff(Notification n)
	{
		if(Application.loadedLevel <= 2 || Application.loadedLevel == 53)
			audio.Stop();
	}

	void OnLevelWasLoaded(int level)
	{
		NotificationCenter.defaultCenter.addListener(OnMainMusicOn, NotificationType.MainThemeMusicOn);
		NotificationCenter.defaultCenter.addListener(OnMainMusicOff, NotificationType.MainThemeMusicOff);
		
	}
}
