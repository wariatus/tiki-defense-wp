﻿using UnityEngine;
using System.Collections;

public class PersistentObject : MonoBehaviour {

	public GameObject persistentObject;

	void Awake ()
	{
		if(GameObject.FindGameObjectWithTag("Persistent") != null)
		{
			Destroy(this.gameObject);
			return;
		}


		if(GameObject.FindGameObjectWithTag("Persistent") == null)
		{
			GameObject o =  Instantiate(persistentObject, Vector3.zero, Quaternion.identity) as GameObject;
			o.transform.parent = this.transform;


		}



	}

	// Use this for initialization
	void Start () {

	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
