﻿using UnityEngine;
using System.Collections;

public class BackgroundResolutionSwitcher : MonoBehaviour {

	public string background;				// nazwa pliku

	public bool useNGUI;

	public tk2dSpriteFromTexture sprite;	// Referencja
	public UITexture nguiTexture;

	void Awake()
	{
		if (Application.isEditor)
		{
			return;
		}
		
		int ScreenHeight = Screen.height;
		int ScreenWidth = Screen.width;
		
		if (ScreenWidth >= GlobalConfiguration.SHDResolutionCutOff)
		{

			if(useNGUI)
			{
				nguiTexture.mainTexture = Resources.Load("Backgrounds/4x/"+background) as Texture;
			}
			else
			{
				sprite.texture = Resources.Load("Backgrounds/4x/"+background) as Texture;
				sprite.spriteCollectionSize.height = 1536;
			}
			if(Debug.isDebugBuild)
				Debug.Log("Setting Backgrounds to SHD " + background);
		}
		else if (ScreenWidth >= GlobalConfiguration.HDResolutionCutOff)
		{
			if(useNGUI)
			{
				nguiTexture.mainTexture = Resources.Load("Backgrounds/4x/"+background) as Texture;
			}
			else
			{
				sprite.texture = Resources.Load("Backgrounds/2x/"+background) as Texture;
				sprite.spriteCollectionSize.height = 768;
			}

			if(Debug.isDebugBuild)
				Debug.Log("Setting Background to HD " + background);
		}


	}	


	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	

	}





}
