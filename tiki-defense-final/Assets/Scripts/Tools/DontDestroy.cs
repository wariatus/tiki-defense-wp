﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Skrypt ustawia obiekt jako niezniszczalny gdy zmienia sie scena samej gry
/// 
/// </summary>
public class DontDestroy : MonoBehaviour {

	private bool created;
    void Awake()
    {
		if(!created)
		{
        	DontDestroyOnLoad(this);
			created = true;
		}
		else
		{
			Destroy(this.gameObject);
		}
    }
	
}
