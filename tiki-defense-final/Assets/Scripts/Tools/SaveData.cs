﻿using UnityEngine;
using System.Collections;
using PlayerPrefs = PreviewLabs.PlayerPrefs;

public static class SaveData {
	
	public static int money
	{
		get 
		{
			return EncryptedPlayerPrefs.GetInt("Money", 0, 1);	
		}
		set
		{
			EncryptedPlayerPrefs.SetInt("Money", value, 1);	
			NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.MoneyCountChanged));
		}
	}


    public static int newGame
    {
        get
        {
            return EncryptedPlayerPrefs.GetInt("NEW_GAME", 0, 1);
        }
        set
        {
            EncryptedPlayerPrefs.SetInt("NEW_GAME", value, 1);
        }

    }

	public static int newGameGM
	{
		get
		{
			return EncryptedPlayerPrefs.GetInt("NEW_GAME_GM", 0, 1);
		}
		set
		{
			EncryptedPlayerPrefs.SetInt("NEW_GAME_GM", value, 1);
		}
		
	}

	public static int addPremium
	{
		set
		{
			Unibiller.CreditBalance("gems", value);
			NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.PremiumChanged));
		}
	}

	public static int removePremium
	{
		set
		{
			Unibiller.DebitBalance("gems", value);
            Debug.Log(value);
			NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.PremiumChanged));
		}
	}

	public static int premium
	{
		get
		{
			return int.Parse(Unibiller.GetCurrencyBalance("gems").ToString());
		}
        
	}

	public static string lastMissionLoaded
	{
		get
		{
			return EncryptedPlayerPrefs.GetString("LAST_MISSION_LOADED", string.Empty, 1);
		}
		set
		{
			EncryptedPlayerPrefs.SetString("LAST_MISSION_LOADED", value, 1);
		}
	}

	public static int premiumWood
	{
		get
		{
			return EncryptedPlayerPrefs.GetInt("PREMIUM_WOOD", 0, 1);
		}
		set
		{
			EncryptedPlayerPrefs.SetInt("PREMIUM_WOOD", value, 1);
			NotificationCenter.defaultCenter.postNotification(new SuperNotification(NotificationType.PremiumWoodChange, 0, value));
			SaveData.Save();
		}
	}

	
	public static int plainTutorial
	{
		get
		{
			return EncryptedPlayerPrefs.GetInt("Plain_Tutorial_Finish", 0, 1);
			
		}
		set
		{
			EncryptedPlayerPrefs.SetInt("Plain_Tutorial_Finish", value, 1);
		}
		
	}

    public static int levelBadge
    {
        get
        {
            return EncryptedPlayerPrefs.GetInt(Application.loadedLevelName + "_Badge", 0, 1);

        }
        set
        {
            EncryptedPlayerPrefs.SetInt(Application.loadedLevelName + "_Badge", value, 1);
        }

    }
	public static int lastLevelSettingShow
	{
		get
		{
			return EncryptedPlayerPrefs.GetInt("LastLevelSettingShow", 0, 1);
		}
		set
		{
			EncryptedPlayerPrefs.SetInt("LastLevelSettingShow", value, 1);
			Save();
		}
	}

	public static string levelToLoad
	{
		get
		{
			return EncryptedPlayerPrefs.GetString("LevelToLoad", string.Empty, 1);
		}
		set
		{
			EncryptedPlayerPrefs.SetString("LevelToLoad", value, 1);
		}

		   
	}

	public static int tikiTutorialCompleted
	{
		get
		{
			return EncryptedPlayerPrefs.GetInt("TikiTutorialCompleted", 0, 1);
		}
		set
		{
			EncryptedPlayerPrefs.SetInt("TikiTutorialCompleted", value, 1);
		}
	}

    public static int levelStars
    {
        get
        {
            return EncryptedPlayerPrefs.GetInt(Application.loadedLevelName + "_Stars", 0, 1);
        }
        set
        {
            EncryptedPlayerPrefs.SetInt(Application.loadedLevelName + "_Stars", value, 1);
        }
    }


	public static int playerStarsAll
	{
		get
		{
			return EncryptedPlayerPrefs.GetInt("Stars", 0, 1);
		}
		set
		{
			EncryptedPlayerPrefs.SetInt("Stars", value, 1);
		}
	}

	public static int settingStarsBeach
	{
		get
		{
			return EncryptedPlayerPrefs.GetInt("Beach_Stars_All", 0, 1);
		}
		set
		{
			EncryptedPlayerPrefs.SetInt("Beach_Stars_All", value, 1);
		}
	}

	public static int settingStarsJungle
	{
		get
		{
			return EncryptedPlayerPrefs.GetInt("Jungle_Stars_All", 0, 1);
		}
		set
		{
			EncryptedPlayerPrefs.SetInt("Jungle_Stars_All", value, 1);
		}
	}

	public static int settingStarsVillage
	{
		get
		{
			return EncryptedPlayerPrefs.GetInt("Village_Stars_All", 0, 1);
		}
		set
		{
			EncryptedPlayerPrefs.SetInt("Village_Stars_All", value, 1);
		}
	}

	public static int settingStarsHills
	{
		get
		{
			return EncryptedPlayerPrefs.GetInt("Hills_Stars_All", 0, 1);
		}
		set
		{
			EncryptedPlayerPrefs.SetInt("Hills_Stars_All", value, 1);
		}
	}

	public static int settingStarsVulcano
	{
		get
		{
			return EncryptedPlayerPrefs.GetInt("Vulcano_Stars_All", 0, 1);
		}
		set
		{
			EncryptedPlayerPrefs.SetInt("Vulcano_Stars_All", value, 1);
		}
	}
	

	public static int starsAllCount
	{
		get
		{
			return EncryptedPlayerPrefs.GetInt("STARS_ALL_COUNT", 0, 1);
		}
		set
		{
			EncryptedPlayerPrefs.SetInt("STARS_ALL_COUNT", value, 1);

		}
	}

	public static string defaultLanguage
	{
		get
		{
			return EncryptedPlayerPrefs.GetString("DEFAULT_LANGUAGE", "EN", 1);
		}
		set
		{
			EncryptedPlayerPrefs.SetString("DEFAULT_LANGUAGE", value, 1);
			NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.OnLanguageChanged));
			Save();
			//Debug.Log(defaultLanguage + " " + value);

		}
	}

	/// <summary>
	/// Gets the level stars.
	/// </summary>
	/// <returns>The level stars.</returns>
	/// <param name="levelName">Level name.</param>
	public static int GetLevelStars(string levelName)
	{

		return EncryptedPlayerPrefs.GetInt(levelName + "_Stars", 0, 1);

	}

	public static int LiveTimeStemp
	{
		get
		{
			return EncryptedPlayerPrefs.GetInt("LIVE_LAST_TIME", 0, 1);
		}
		set
		{
			EncryptedPlayerPrefs.SetInt("LIVE_LAST_TIME", value, 1);
			
		}
	
	}

	public static string LiveDataTimePast
	{
		get
		{
			return EncryptedPlayerPrefs.GetString("LIVE_PAST_DT", "empty", 1);
		}
		set
		{
			EncryptedPlayerPrefs.SetString("LIVE_PAST_DT", value, 1);
			
		}
		
	}


	public static int tikiMaskCharge
	{
		get
		{
			return EncryptedPlayerPrefs.GetInt("TIKI_MASK_CHARGE", 0, 1);
		}
		set 
		{
			bool canAdd = true;
			if(tikiTotemRight_ID3 == 1)
			{
				if(value > 4) 
				{
					value = 4;
					canAdd = false;
				}
			}
			else
			{ 
				if(value > 3) 
				{
					value = 3;
					canAdd = false;
				}
			}
				
			if(canAdd)
			{
				EncryptedPlayerPrefs.SetInt("TIKI_MASK_CHARGE", value, 1);
				NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.TikiMaskChargeChanged));
			}
		}
	}


	#region MicroTransaction Data
	public static int tikiMaskUnlockAll
	{
		get
		{
			return EncryptedPlayerPrefs.GetInt("TIKI_MASK_ALL_UNLOCK", 0, 1);
		}
		set 
		{
			EncryptedPlayerPrefs.SetInt("TIKI_MASK_ALL_UNLOCK", value, 1);
		}
	}


	#endregion


	#region Totem Build Bonus

	public static int tikiTotemLeft_ID1
	{
		get
		{
			return EncryptedPlayerPrefs.GetInt("TIKI_TOTEM_LEFT_ID_1", 0, 1);
		}
		set 
		{
			EncryptedPlayerPrefs.SetInt("TIKI_TOTEM_LEFT_ID_1", value, 1);
		}
	}

	public static int tikiTotemLeft_ID2
	{
		get
		{
			return EncryptedPlayerPrefs.GetInt("TIKI_TOTEM_LEFT_ID_2", 0, 1);
		}
		set 
		{
			EncryptedPlayerPrefs.SetInt("TIKI_TOTEM_LEFT_ID_2", value, 1);
		}
	}

	public static int tikiTotemLeft_ID3
	{
		get
		{
			return EncryptedPlayerPrefs.GetInt("TIKI_TOTEM_LEFT_ID_3", 0, 1);
		}
		set 
		{
			EncryptedPlayerPrefs.SetInt("TIKI_TOTEM_LEFT_ID_3", value, 1);
		}
	}

	public static int tikiTotemLeft_ID4
	{
		get
		{
			return EncryptedPlayerPrefs.GetInt("TIKI_TOTEM_LEFT_ID_4", 0, 1);
		}
		set 
		{
			EncryptedPlayerPrefs.SetInt("TIKI_TOTEM_LEFT_ID_4", value, 1);
		}
	}

	public static int tikiTotemLeft_ID5
	{
		get
		{
			return EncryptedPlayerPrefs.GetInt("TIKI_TOTEM_LEFT_ID_5", 0, 1);
		}
		set 
		{
			EncryptedPlayerPrefs.SetInt("TIKI_TOTEM_LEFT_ID_5", value, 1);
		}
	}

	public static int tikiTotemLeft_ID6
	{
		get
		{
			return EncryptedPlayerPrefs.GetInt("TIKI_TOTEM_LEFT_ID_6", 0, 1);
		}
		set 
		{
			EncryptedPlayerPrefs.SetInt("TIKI_TOTEM_LEFT_ID_6", value, 1);
		}
	}

	public static int tikiTotemRight_ID1
	{
		get
		{
			return EncryptedPlayerPrefs.GetInt("TIKI_TOTEM_RIGHT_ID_1", 0, 1);
		}
		set 
		{
			EncryptedPlayerPrefs.SetInt("TIKI_TOTEM_RIGHT_ID_1", value, 1);
		}
	}

	public static int tikiTotemRight_ID2
	{
		get
		{
			return EncryptedPlayerPrefs.GetInt("TIKI_TOTEM_RIGHT_ID_2", 0, 1);
		}
		set 
		{
			EncryptedPlayerPrefs.SetInt("TIKI_TOTEM_RIGHT_ID_2", value, 1);
		}
	}

	public static int tikiTotemRight_ID3
	{
		get
		{
			return EncryptedPlayerPrefs.GetInt("TIKI_TOTEM_RIGHT_ID_3", 0, 1);
		}
		set 
		{
			EncryptedPlayerPrefs.SetInt("TIKI_TOTEM_RIGHT_ID_3", value, 1);
		}
	}

	public static int tikiTotemRight_ID4
	{
		get
		{
			return EncryptedPlayerPrefs.GetInt("TIKI_TOTEM_RIGHT_ID_4", 0, 1);
		}
		set 
		{
			EncryptedPlayerPrefs.SetInt("TIKI_TOTEM_RIGHT_ID_4", value, 1);
		}
	}

	public static int tikiTotemRight_ID5
	{
		get
		{
			return EncryptedPlayerPrefs.GetInt("TIKI_TOTEM_RIGHT_ID_5", 0, 1);
		}
		set 
		{
			EncryptedPlayerPrefs.SetInt("TIKI_TOTEM_RIGHT_ID_5", value, 1);
		}
	}

	public static int tikiTotemRight_ID6
	{
		get
		{
			return EncryptedPlayerPrefs.GetInt("TIKI_TOTEM_RIGHT_ID_6", 0, 1);
		}
		set 
		{
			EncryptedPlayerPrefs.SetInt("TIKI_TOTEM_RIGHT_ID_6", value, 1);
		}
	}

	#endregion

	public static void Save()
	{
		if (Debug.isDebugBuild)
			Debug.Log("[SaveData] Zapisuje dane");
		
		// Zapisujemy zmiany
		PlayerPrefs.Flush();
	}

	public static int GetSlotStar(int slot)
	{
		if(slot == 1)
		{
			return EncryptedPlayerPrefs.GetInt("Stars", 0, 1);
		}
		else if(slot == 2)
		{
		    return EncryptedPlayerPrefs.GetInt("Stars", 0, 2);
		}
		else if(slot == 3)
		{
			return EncryptedPlayerPrefs.GetInt("Stars", 0, 3);
		}
		else return 0;
	}

	
	private static void OnLevelWasLoaded(int level)
	{
		
	}
	
}
