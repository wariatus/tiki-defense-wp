﻿using UnityEngine;
using System.Collections;

public class RotateSprite : MonoBehaviour {

	public float speed;

	private Transform myTransform;
	// Use this for initialization
	void Start () {

		myTransform = GetComponent<Transform>();

	}
	
	// Update is called once per frame
	void Update () {
	
		myTransform.Rotate(new Vector3(0, 0, speed));

	}
}
