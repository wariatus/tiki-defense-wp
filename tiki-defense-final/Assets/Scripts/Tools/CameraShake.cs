﻿using UnityEngine;
public class CameraShake : ExtendedMonoBehavior
{

    public float shake_decay;
    public float shake_intensity;
    
    private Vector3 originPosition;
    private Quaternion originRotation;

    private static bool runShake;
   

	protected override void Awake ()
	{
		base.Awake ();

		originPosition = MyTransform.position;
		originRotation = MyTransform.rotation;
	}
    // Use this for initialization
    protected override void Start()
    {
		base.Start();

    }

    // Update is called once per frame
	protected override   void Update()
    {
        if (runShake)
        {
            ConfigureShake();
            runShake = false;
        }
        if (shake_intensity > 0)
        {
            Vector2 shake = Random.insideUnitSphere * shake_intensity;
            MyTransform.position = originPosition + (Vector3)shake;
            shake_intensity -= shake_decay;
        }
		else
		{
			if(MyTransform.position != Vector3.zero)
				MyTransform.position = originPosition;
		}
    }

    public static void Shake()
    {
		if(!runShake)
        	runShake = true;
	
    }

	public  void ShakeCamera()
	{
		if(!runShake)
			runShake= true;
	}

    public void ConfigureShake()
    {

        shake_intensity = 0.5f;
        shake_decay = 0.1f;
    }
}