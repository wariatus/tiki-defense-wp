﻿using System.Collections;
using UnityEngine;
public class ShakeGUIElement : ExtendedMonoBehavior
{
	
	private float shake_decay;
	private float shake_intensity;
	
	private Vector3 originPosition;
	private Quaternion originRotation;
	
	private static bool runShake;
	
	
	protected override void Awake ()
	{
		base.Awake ();
		
		originPosition = MyTransform.localPosition;
		originRotation = MyTransform.localRotation;
	}
	// Use this for initialization
	protected override void Start()
	{
		base.Start();
		
	}
	
	// Update is called once per frame
	protected override   void Update()
	{
        if (GameMaster.gameState == GameState.WON)
        {
            if (runShake)
            {
                ConfigureShake();
                runShake = false;
            }
            if (shake_intensity > 0)
            {
                Vector2 shake = Random.insideUnitSphere * shake_intensity;
                MyTransform.localPosition += (Vector3)shake;
                shake_intensity -= shake_decay;
            }
            else
                MyTransform.localPosition = new Vector3(0, 0, 0);

        }
	}
	
	public  void Shake()
	{
		if(!runShake)
			runShake = true;
			
		shake_intensity = 10f;
		shake_decay = 1f;
	}

    public void ShakeInvoke()
    {

        shake_intensity = 10f;
        shake_decay = 1f;

        StartCoroutine("ShakeIt");
    }
	
	public void ConfigureShake()
	{
		
		shake_intensity = 0.1f;
		shake_decay = 0.01f;
	}

    private IEnumerator ShakeIt()
    {
        while (shake_intensity > 0)
        {
            Vector2 shake = Random.insideUnitSphere * shake_intensity;
            MyTransform.localPosition += (Vector3)shake;
            shake_intensity -= shake_decay;

            yield return new WaitForEndOfFrame();
        }

        MyTransform.localPosition = originPosition;

        
    }
}