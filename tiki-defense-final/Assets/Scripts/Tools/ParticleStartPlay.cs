﻿using UnityEngine;
using System.Collections;

public class ParticleStartPlay : MonoBehaviour {

    public bool withChildrens;

    public ParticleSystem particle;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


    public void Play()
    {
        particle.Play(withChildrens);
    }
}
