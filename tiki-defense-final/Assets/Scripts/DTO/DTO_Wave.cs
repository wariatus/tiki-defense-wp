﻿using System.Collections.Generic;

public class DTO_Wave
{
    public int id;
    public string waveName;
    public float multiplier;
 
    public List<DTO_Enemy> enemy;

    public DTO_Wave()
    {
        enemy = new List<DTO_Enemy>();
    }

    public string WaveName
    {
        get { return waveName; }
    }

    public int Id
    {
        get { return id; }
    }

}

