﻿
public class EnemyKilledNotification : Notification
{
    public UnitBase unitBase;
	public int moneyForKill;

    public EnemyKilledNotification(NotificationType type, UnitBase unit, int moneyForKill)
        : base(type)
    {
        this.unitBase = unit;
		this.moneyForKill = moneyForKill;
    }
}
