
using UnityEngine;

public class SuperNotification : Notification
{
	public float varFloat;
	public int varInt;
	public bool isTower;
	public Vector3 position;
	
	public SuperNotification( NotificationType type, float varFloat, int varInt, bool isTower = false ) : base( type )
	{
		this.varFloat = varFloat;
		this.varInt = varInt;
		this.isTower = isTower;
	}

	public SuperNotification( NotificationType type, float varFloat, int varInt, Vector3 position, bool isTower = false ) : base( type )
	{
		this.varFloat = varFloat;
		this.varInt = varInt;
		this.isTower = isTower;
		this.position = position;
	}
}
