﻿
public class GridClickNotification : Notification
{
    public GridCell cell;
	public UnityEngine.Vector3 position;

    public GridClickNotification(NotificationType type, GridCell cell = null)
        : base(type)
    {
        this.cell = cell;

    }

    public GridClickNotification(NotificationType type, UnityEngine.Vector3 position,  GridCell cell = null)
        : base(type)
    {
        this.cell = cell;
        this.position = position;
    }
}
