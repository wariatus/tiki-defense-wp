
// Standard notification class.  For specific needs subclass
public class Notification
{
    public NotificationType type;
    public object userInfo;
	public GameState gameState;


    public Notification( NotificationType type )
    {
        this.type = type;
    }


    public Notification( NotificationType type, object userInfo )
    {
        this.type = type;
        this.userInfo = userInfo;
    }

	public Notification (NotificationType type, GameState gameState)
	{
		this.type = type;
		this.gameState = gameState;

	}
}
