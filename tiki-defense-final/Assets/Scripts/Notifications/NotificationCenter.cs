using System.Collections;
using UnityEngine;


// Each notification type should gets its own enum
public enum NotificationType {

    GameRunning,
    GameWon,
    GameDefeated,
    EnemyKilled,
    PauseClicked,
    SpeedIncreased,
    SpeedNormal,
    TowerDestroy,
    TowerBought,
    TowerSell,
    ObstacleDestroy,
    MoneyAdded,
    WaveIncreased,
    FinalWave,
	BlockerClick,
	EnemyClick,
	GridClicked,
	MoneyCountChanged,
	TowerClicked,
    BlockerDestroy,
	TikiMaskClicked,
	TikimaskUsed,
	TikiMaskChargeChanged,
	StarCountChanged,		// Zmiana iloscigwiazdek
	BackToPool,				// Nakazanie powrotu do poola
	MainThemeMusicOn,		// Wlacz muzyke glowna
	MainThemeMusicOff,		// Wylacz muzyke glowna
	TutorialBuildTowerOn,	// Wlaczony zostal tutorial budowy wiez
	TutorialBuildTowerOff,	// Wylaczony zostal tutorial budowy wiezy
	TutorialBuildTowerProgress,	//Gdy gracz zrobi progress w tutorialu
	TutorialTikiUseProgress,	//Gdy gracz zrobi progress w tutorialu tiki
	GameCountToStart,		// Game Count To Start
	ShamanTakeDamage,		// Gdy szamanka otrzyma obrazenia
	OnLanguageChanged,
	OnGameStateChanged,
	PremiumWoodChange,
	ResetTikiEffect,
	PremiumChanged,
	OnCountStart,
	OnEnemySpawn,
    OnFinalWave,
    OnEnterToShop,
    OnLevelMainMenuOpen,
    OnShopBuyTIKIUnlock,
	OnLiveChanged,
	TotemClick,
	ShowGridSelect,
	TotalNotifications,

	
};

public delegate void OnNotificationDelegate( Notification note );

public class NotificationCenter 
{
	private static NotificationCenter instance;

	private ArrayList[] listeners = new ArrayList[(int)NotificationType.TotalNotifications];

	// Instead of constructor we can use void Awake() to setup the instance if we sublcass MonoBehavoiur
	public NotificationCenter()
	{
		if( instance != null )
		{
			Debug.Log( "NotificationCenter instance is not null" );
			return;
		}
		instance = this;

	}


	public static NotificationCenter defaultCenter
	{
		get
		{
			if( instance == null )
				new NotificationCenter();
			return instance;
		}
	}



	public void ClearListener()
	{
		
		listeners = new ArrayList[(int)NotificationType.TotalNotifications];
	}

	public void addListener( OnNotificationDelegate newListenerDelegate, NotificationType type )
	{
		int typeInt = (int)type;
	  
		// Create the listener ArrayList lazily
		if( listeners[typeInt] == null )
			listeners[typeInt] = new ArrayList();

		listeners[typeInt].Add( newListenerDelegate );
	}
	
	public int GetListenersCount()
	{
		return listeners.Length;	
	}

	public int GetListenersCount(NotificationType type)
	{
		int typeInt = (int)type;

		return listeners[typeInt].Count;
	}


	public void removeListener( OnNotificationDelegate listenerDelegate, NotificationType type )
	{
		int typeInt = ( int )type;

		if( listeners[typeInt] == null )
			return;

		if( listeners[typeInt].Contains( listenerDelegate ) )
			listeners[typeInt].Remove( listenerDelegate );

		// Clean up empty listener ArrayLists
		if( listeners[typeInt].Count == 0 )
			listeners[typeInt] = null;
	}


	public void postNotification( Notification note )
	{
		int typeInt = ( int )note.type;

		if( listeners[typeInt] == null )
			return;

		foreach( OnNotificationDelegate delegateCall in listeners[typeInt] )
		{
			delegateCall( note );
		}
	}
	

}




// Usage:
// NotificationCenter.defaultCenter.addListener( onNotification );
// NotificationCenter.defaultCenter.sendNotification( new Notification( NotificationTypes.OnStuff, this ) );
// NotificationCenter.defaultCenter.removeListener( onNotification, NotificationType.OnStuff );