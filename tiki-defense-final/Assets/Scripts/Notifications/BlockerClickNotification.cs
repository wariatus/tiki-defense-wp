﻿
public class BlockerClickNotification : Notification
{
    public UnitBase unit;

    public BlockerClickNotification(NotificationType type, UnitBase unti)
        : base(type)
    {
        this.unit = unti;
    }
}
