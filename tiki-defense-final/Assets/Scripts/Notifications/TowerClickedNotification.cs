﻿
public class TowerClickedNotification : Notification
{
    public Towers.TowerBase tower;
	public UnityEngine.Vector3 position;

    public TowerClickedNotification(NotificationType type, Towers.TowerBase tower, UnityEngine.Vector3 position)
        : base(type)
    {
        this.tower = tower;
		this.position = position;
    }
}
