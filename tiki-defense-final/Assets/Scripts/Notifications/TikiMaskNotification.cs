﻿
public class TikiMaskNotification : Notification
{
	public TikiMaskType maskType;

	
	public TikiMaskNotification( NotificationType type, TikiMaskType maskType ) : base( type )
	{
		this.maskType = maskType; 
	}
}
