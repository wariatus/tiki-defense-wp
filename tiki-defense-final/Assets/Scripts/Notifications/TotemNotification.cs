﻿
public class TotemNotification : Notification
{
    public GUITikiTotemItem totemItem;

	public TotemNotification(NotificationType type, GUITikiTotemItem _totemItem )
        : base(type)
    {
		this.totemItem = _totemItem;

    }

}
