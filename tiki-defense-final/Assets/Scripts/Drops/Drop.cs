﻿using UnityEngine;
using System.Collections;

namespace Drops
{

	public class Drop : ExtendedMonoBehavior {

		/// <summary>
		/// Typ obiektu wedlug Poolmanagera
		/// </summary>
		public PoolObjectType objectType;

		public Collider2D[] colliders;

		public tk2dSprite sprite;

		protected override void Awake ()
		{

		}

		// Use this for initialization
		protected override void Start () {
		

			NotificationCenter.defaultCenter.addListener(OnBackToPoolRequest, NotificationType.BackToPool);
		}
		
		// Update is called once per frame
		protected override void Update () {
		
		}


		protected virtual void OnDisable()
		{
			for(int i = 0; i < colliders.Length; i ++)
			{
				colliders[i].enabled = false;
			}
		}

		protected virtual void OnEnable()
		{
			for(int i = 0; i < colliders.Length; i ++)
			{
				colliders[i].enabled = true;
			}
		}


		public virtual void Collect()
		{

			ReturnToPool();
		}


		protected void ReturnToPool()
		{
			PathologicalGames.PoolManager.Pools["Drops"].Despawn(MyTransform);
			//PoolManager.AddItem(MyGameObject, objectType);
		}

		
		void OnBackToPoolRequest(Notification n)
		{
			if(PathologicalGames.PoolManager.Pools["Drops"].IsSpawned(MyTransform))
				ReturnToPool();
			
		}
		
		void OnLevelWasLoaded(int level)
		{

			NotificationCenter.defaultCenter.addListener(OnBackToPoolRequest, NotificationType.BackToPool);
		}

		protected virtual void OnSpawned()
		{

		}

		protected virtual void OnDespawned()
		{

		}
	}
}