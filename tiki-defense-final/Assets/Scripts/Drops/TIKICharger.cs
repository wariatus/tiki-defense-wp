﻿using UnityEngine;
using System.Collections;

namespace Drops
{
	public class TIKICharger : Drop {

		public float velocity;
		public float timeOnScene;
		public float timeToBlink;
		public TweenPosition tweener;
		public AudioClip soundOnSpawn;
		public AudioClip soundOnCollect;
		private TweenPosition tweenerJump;
		private bool blinking;
		private bool useTween;
		

		protected override void Awake ()
		{
			base.Awake ();
		}

		protected override void Start ()
		{
			base.Start ();

		
		}

		protected override void Update ()
		{
			base.Update ();

			if(useTween)
			{
				useTween  = false;
				CreateTweener();
			}
		    if (!blinking) return;
		    sprite.renderer.enabled = !(Time.fixedTime%.5<.2);
		}

		public override void Collect ()
		{

		    if (GameMaster.onTutorial)
		    {
		        NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.TutorialTikiUseProgress));
		        SaveData.tikiMaskCharge++;
		        NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.TikiMaskChargeChanged));
		        base.ReturnToPool();

		    }
		    else
		    {
		        tweenerJump.enabled = false;

                StopCoroutine("RunTTL");
                StopBlinking();

                tweener.from = MyTransform.localPosition;

                tweener.to = Camera.main.ViewportToWorldPoint(GameMaster.guiChargers.worldPosition);
                tweener.ResetToBeginning();
                tweener.PlayForward();

		    }


		    NGUITools.PlaySound(soundOnCollect, 1, 1);
		}

		public void Pool()
		{
			SaveData.tikiMaskCharge++;
		
			
			if(GameMaster.onTutorial)
				NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.TutorialTikiUseProgress));

			base.ReturnToPool();
		}

		private IEnumerator StartBlink()
		{
			yield return new WaitForSeconds(timeToBlink);

			blinking = true;
		}
		
		private void StopBlinking()
		{
			StopCoroutine("StartBlink");
			sprite.renderer.enabled = true;
			blinking = false;
		}

		private IEnumerator RunTTL()
		{
			float time = timeOnScene;

			if(GameMaster.gameState == GameState.RUNNING)
			{
				time -= (Time.timeScale > 1 ? Time.deltaTime /2 : Time.deltaTime);
			}
		
			while(time > 0)
			{
				yield return null;
			}

			ReturnToPool();

		}
		private void CreateTweener()
		{
				tweenerJump = MyGameObject.AddComponent<TweenPosition>() as TweenPosition;
				tweenerJump.from = MyTransform.localPosition;
				
				Vector3 fixPosition = MyTransform.localPosition;
				fixPosition.y += 1f;
				tweenerJump.to = fixPosition;
				tweenerJump.duration = 0.5f;
				tweenerJump.style = UITweener.Style.PingPong;
		}

	    public void Init()
	    {
            useTween = true;

            base.OnSpawned();

            if (!GameMaster.onTutorial)
            {
                StartCoroutine("RunTTL");
                StartCoroutine("StartBlink");
            }

            NGUITools.PlaySound(this.soundOnSpawn);
	    }
		protected override void OnSpawned ()
		{
		
		

		}

		protected override void OnDespawned()
		{
			base.OnDespawned();
			
			if(tweenerJump != null)
			{
				Destroy(tweenerJump);
				tweenerJump = null;
			}

			StopCoroutine("RunTTL");
            StopBlinking();
			sprite.renderer.enabled = true;

            //tweener.ResetToBeginning();
		    tweener.enabled = false;

		}
	}
}