﻿ using UnityEngine;


public class GlobalConfiguration : ExtendedMonoBehavior  {


#if USE_FTP
    public const bool USE_FTP_DTO_WAVES = true;

#else
    public const bool USE_FTP_DTO_WAVES = false;
#endif
    
    /// <summary>
	/// Pobiera aktualnie wybrany slot gracza w celu zapisywania i wczytywania gry
	/// </summary>
	public static int saveGameSlot;
       
	/// <summary>
	/// Aktualny typ ekranu na urzadzeniu SD/HD/SHD. Ustawiany poprzez ResolutionAtlasSwitcher.cs
	/// </summary>
	public static ScreenType screenType;

	public static bool disableInEditor = true;
	public static int HDResolutionCutOff = 960;
	public static int SHDResolutionCutOff = 2048;
	
	/// <summary>
	/// Rodzaj platformy, z ktora mamy doczynienia  
	/// </summary>
	public static PlatformType platformType;

	
	/// <summary>
	/// Dostep do nazwy scen
	/// </summary>
	public static string[] scenes;


	#region Zmienne do podsumowania poziomu

	public static int starsMultiplierScore = 10;			// Mnoznik gwiazdek
	public static int badgeMultiplierScore = 20;			// Mnoznik badga
	public static int healthMultiplierScore = 10;		// Mnoznik zycia


	#endregion

	#region Zmienne ekonomiczne
	public const float SELL_TOWER_PENALITY = 75f;
	#endregion

	#region Offsety path 
	public const float PATH_TOP_OFFSET = 0.4f;
	public const float PATH_MIDDLE_OFFSET = 0;
	

	#endregion

	#region TIKI TOTEM BONUS VALUE

	public const int TIKI_TOTEM_LEFT_ID_1_BONUS = 40;
	public const float TIKI_TOTEM_LEFT_ID_2_BONUS = 3f;
	public const float TIKI_TOTEM_LEFT_ID_3_BONUS = 3f;
	public const int TIKI_TOTEM_LEFT_ID_4_BONUS = 60;
	//public const int TIKI_TOTEM_LEFT_ID_5_BONUS = 20;
	//public const float TIKI_TOTEM_LEFT_ID_6_BONUS = 2f;

	public const float TIKI_TOTEM_RIGHT_ID_1_BONUS = 90f;
	public const float TIKI_TOTEM_RIGHT_ID_2_BONUS = 50f;
	//public const float TIKI_TOTEM_RIGHT_ID_3_BONUS = 20f;
	public const float TIKI_TOTEM_RIGHT_ID_4_BONUS = 50f;
	//public const float TIKI_TOTEM_RIGHT_ID_5_BONUS = 50f;
	//public const float TIKI_TOTEM_RIGHT_ID_6_BONUS = 50f;

	#endregion

	public const int MAX_STARS = 144;

	#region Microtransaction
	public const int chargeCost = 250;				// Koszt kupna charga
    public const int chargeOnNewGame = 1500;
	public const int VIDEO_AD_FREE_GEMS = 40;
	public const int woodChargeCost = 1000;
	#endregion

	#region Lives
	public const int LIVE_TIME_TO_REFRESH = 100;
	public const int LIVE_MAX = 5;
	public const int LIVE_RESTART_COST = 500;

	#endregion
	
	/// <summary>
	/// The armore normal damage reduct.
	/// </summary>
	public static float armoreNormalDamageReduct = 75;


	protected override void Awake()
	{
        
		base.Awake();
		SetTk2d();
		SetPlatformType();
		 
		#region Encrypted PlayerPrefs Configuration

		// this array should be filled before you can use EncryptedPlayerPrefs :
		EncryptedPlayerPrefs.keys = new string[5];
		EncryptedPlayerPrefs.keys[0] = "23Wruedfdre";
		EncryptedPlayerPrefs.keys[1] = "SP9DedfupHa";
		EncryptedPlayerPrefs.keys[2] = "frA5defrAS3";
		EncryptedPlayerPrefs.keys[3] = "tHaedft2epr";
		EncryptedPlayerPrefs.keys[4] = "jaw3edfeDAs";
		#endregion

		/// Default Slot GAme
		saveGameSlot = 1;

        if (Debug.isDebugBuild)
        {
            Debug.Log("[GlobalConfiguration]: Save path: " + Application.persistentDataPath);
            
        }

	

#if UNITY_IPHONE
		//SetAchivement(dodać Achivmenty)

		GameCenterManager.dispatcher.addEventListener(GameCenterManager.GAME_CENTER_PLAYER_AUTHENTICATED, OnGameCenterAuthenticated);

		GameCenterManager.registerAchievement("KILL_1");
		GameCenterManager.registerAchievement("KILL_100");
		GameCenterManager.init();

#endif

        // TEMP
//		EncryptedPlayerPrefs.SetInt("Level 1x01_Stars", 1, 1);
//		EncryptedPlayerPrefs.SetInt("Level 1x02_Stars", 0, 1);
//		EncryptedPlayerPrefs.SetInt("Level 1x03_Stars", 0, 1);
//		EncryptedPlayerPrefs.SetInt("Level 1x04_Stars", 0, 1);

		Screen.sleepTimeout = SleepTimeout.NeverSleep;
	
	}

	// Use this for initialization
	protected override void Start()
	{
		base.Start();
	  
		#region PlayerPrefs
		ResetPlayerPrefs();
		
		//EncryptedPlayerPrefs.SetInt("Stars", 10, 1);
		//EncryptedPlayerPrefs.SetInt("Spell_2_Level", 1, 1);
		#endregion

		if (SaveData.newGame == 0)
		{
			LiveManager.Instance.InitializeLivesOnNewGame();
			SaveData.newGame = 1;
			SaveData.Save();

		}
		//Uruchamiamy sklep
		ShopManager.Init();
	}



	
	private void ResetPlayerPrefs()
	{
		//GetPlayerPrefs.money = 0;	
	}
	
	// Update is called once per frame
	protected override void Update()
	{
		base.Update();
	
	}

	/// <summary>
	/// Ustawia odpowiedni atlas w zaleznosci od uruchomionej platformy
	/// </summary>
	private void SetTk2d()
	{
		if (disableInEditor && Application.isEditor)
		{
			return;
		}

		int ScreenWidth = Screen.width;

		if (ScreenWidth >= SHDResolutionCutOff)
		{
			tk2dSystem.CurrentPlatform = "4x";

			Debug.Log("Setting TK2D to SHD (x4) ");
		}
		else 
		{
			tk2dSystem.CurrentPlatform = "2x";
			Debug.Log("Setting TK2D to HD (2x) ");
		}
        //else
        //{
        //    tk2dSystem.CurrentPlatform = "1x";
        //    Debug.Log("Setting TK2D to SD (1x) ");
        //}
	}

	/// <summary>
	/// Ustawia rodzaj platform, na ktorej uruchomiona jest gra
	/// </summary>
	private void SetPlatformType()
	{
		if (Application.platform == RuntimePlatform.Android)
			platformType = PlatformType.MOBILE;
		else if (Application.platform == RuntimePlatform.BB10Player)
			platformType = PlatformType.MOBILE;
		else if (Application.platform == RuntimePlatform.IPhonePlayer)
			platformType = PlatformType.MOBILE;
		else if (Application.platform == RuntimePlatform.WP8Player)
			platformType = PlatformType.MOBILE;
		else if (Application.platform == RuntimePlatform.LinuxPlayer)
			platformType = PlatformType.STANDALONE;
		else if (Application.platform == RuntimePlatform.OSXEditor)
			platformType = PlatformType.STANDALONE;
		else if (Application.platform == RuntimePlatform.OSXPlayer)
			platformType = PlatformType.STANDALONE;
		else if (Application.platform == RuntimePlatform.WindowsEditor)
			platformType = PlatformType.STANDALONE;
		else if (Application.platform == RuntimePlatform.WindowsPlayer)
			platformType = PlatformType.STANDALONE;
		else if (Application.platform == RuntimePlatform.OSXWebPlayer)
			platformType = PlatformType.WEB;
		else if (Application.platform == RuntimePlatform.WindowsWebPlayer)
			platformType = PlatformType.WEB;
	
	}

	private void OnGameCenterAuthenticated()
	{
		if(Debug.isDebugBuild)
			Debug.Log("[GlobalConfiguration]: Game Center Authentcated");	
	}

	void OnLevelWasLoaded(int level)
	{

        Time.timeScale = 1;
        if (level > 2)
        {
            Resources.UnloadUnusedAssets();
            System.GC.Collect();
        }
		
		//Reset
		ResetPlayerPrefs();
		
	}

}


