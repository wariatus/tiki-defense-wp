﻿using UnityEngine;
using System.Collections;
using Projectiles;

namespace Towers
{

	public class Magic : TowerBase
	{
		protected override void Awake ()
		{
			base.Awake ();
		}
		
		protected override void Start ()
		{
			base.Start ();
			state = TowerState.ReadyToFire;
		}
		
		protected override void Update ()
		{
			base.Update();
		}

		protected override void AttackTarget ()
		{
			if(isOnRecharge) return;

			//GameObject go = PoolManager.GetItem(projectileObjectType);
			ProjectileBase p = PathologicalGames.PoolManager.Pools["Bullets"].Spawn(settings[level -1].projectileObjectType.ToString()).GetComponent<ProjectileBase>();
			p.MyTransform.position = MyTransform.position;

			Vector3 targetDirection = currentTarget.MyTransform.position - p.MyTransform.position;
			targetDirection.Normalize();
			//Debug.Log(targetDirection);

			Vector3 targetRot = Vector3.zero;
			targetRot.x = p.MyTransform.position.x;
			targetRot.y = p.MyTransform.position.y;
			targetRot.z= currentTarget.MyTransform.position.z;


//			if(targetDirection.x <= -0.55f && targetDirection.x >= -0.85f && targetDirection.y >= 0.55f && targetDirection.y <= 0.95f)
//			{
//				Debug.Log("chuj");
//				// Lewy gorny
//				//p.MyTransform.Rotate(Vector3.forward, 135, Space.Self);
//				//p.MyTransform.LookAt(targetRot);
//				//p.MyTransform.rotation = turret.transform.rotation;
//
//				
//			}
//			else if(targetDirection.x >= 0.4f && targetDirection.x <= 1f && targetDirection.y >= 0.5f && targetDirection.y <= 1f)
//			{
//				// Prawo gorny
//				//p.MyTransform.Rotate(Vector3.forward, 45, Space.Self);
//				//p.MyTransform.LookAt(targetRot);
//				//p.MyTransform.rotation = Quaternion.Euler(0f, 0f, (Mathf.Atan2(targetDirection.y, targetDirection.x) * Mathf.Rad2Deg));
//			}
//			else if(targetDirection.x >= 0.5f && targetDirection.x <= 1f && targetDirection.y <= -0.4f && targetDirection.y <= 1f)
//			{
//				// Prawo dolny
//				///p.MyTransform.Rotate(Vector3.forward, 315, Space.Self);
//				//p.MyTransform.LookAt(targetRot);
//				//p.MyTransform.rotation = Quaternion.Euler(0f, 0f, (Mathf.Atan2(targetDirection.y, targetDirection.x) * Mathf.Rad2Deg));
//
//			}
//			else if(targetDirection.x <= -0.5f && targetDirection.x >= -1f && targetDirection.y <= -0.4f && targetDirection.y >= -1f)
//			{
//				// Lewo dolny
//				//p.MyTransform.Rotate(Vector3.forward, 225, Space.Self);
//				//p.MyTransform.LookAt(targetRot);
//				//p.MyTransform.rotation = Quaternion.Euler(0f, 0f, (Mathf.Atan2(targetDirection.y, targetDirection.x) * Mathf.Rad2Deg));
//			}
		    if(targetDirection.x >= -0.4f && targetDirection.x <= 0.1f && targetDirection.y >= 0.8f && targetDirection.y <= 1.5f)
			{
				// Gora
				p.MyTransform.Rotate(Vector3.forward, 90f, Space.Self);
				
			}
			else if(targetDirection.x >= -0.15f && targetDirection.x <= 0.15f && targetDirection.y <= -0.85f && targetDirection.y >= -1.1f)
			{
				// Dol
				p.MyTransform.Rotate(Vector3.forward, 270, Space.Self);
				
				
			}
			else if(targetDirection.x >= -1.15f && targetDirection.x <= 0.14f && targetDirection.y >= -0.25f && targetDirection.y <= 0.45f)
			{
				
				//Lewo
				p.MyTransform.Rotate(Vector3.forward, 180, Space.Self);
				
				
			}
			else if(targetDirection.x <= 1.1f && targetDirection.x >= 0.15f && targetDirection.y >= -0.1f && targetDirection.y < 0.4f)
			{
				//Prawo
				p.MyTransform.Rotate(Vector3.forward, 0, Space.Self);
			}
			else
				p.MyTransform.rotation = Quaternion.Euler(0f, 0f, (Mathf.Atan2(targetDirection.y, targetDirection.x) * Mathf.Rad2Deg));


			//p.MyTransform.rotation =  turret.transform.rotation;
			settings[level - 1].projectileSetting.unit = currentTarget;
			p.Setup(settings[level - 1].projectileSetting, Vector3.zero);

			if(settings[level -1].animation != null)
				settings[level -1].animation.Play();
			
			state = TowerState.Recharge;
			NGUITools.PlaySound(soundOnFire, 1, 1);
			StartCoroutine("Recharge");
		}
		
		
	}
}
