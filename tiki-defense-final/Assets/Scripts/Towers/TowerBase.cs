﻿using UnityEngine;
using System.Collections;
using Projectiles;
using Effects;

namespace Towers
{
	public class TowerBase : ExtendedMonoBehavior {
		
		public bool runOnStart;
		public bool canAttackBlocker = true;

		//public PoolObjectType projectileObjectType;
		
		public TowerSetting[] settings;
		
		public TowerState state = TowerState.Recharge;
		
		public Collider2D[] colliders;
		
		public PoolObjectType poolObjectType;
		public int level;
		
		public tk2dSpriteAnimator animation;
		public tk2dSprite sprite;
		
		[HideInInspector]
		public GridCell activeCell;
		
		public tk2dSprite rangeSprite;
		public AudioClip soundOnTikiUse;
		
		private float attackSpeedCounter;
		protected LayerMask enemyLayerToHit;
		protected LayerMask blockersLayerToHit;
		protected LayerMask enemyAndBlockersLayer;
		
		//[HideInInspector]
		public UnitBase currentTarget;
		
		[HideInInspector]
		public bool attackBlocker;

		[HideInInspector]
		public UnitBase blockerToAttack;
		
		public bool isTurret;
		public float turretSpeed = 4;
		public SmoothLookAtConstraint turret;

		public Transform spriteHud;

		[HideInInspector]
		public Transform tikiEffect;

		public AudioClip soundOnFire;
      
		/// <summary>
		/// Czy maska tiki dziala na tym obiekcie?
		/// </summary>
		[HideInInspector]
		public bool isTikiMaskEffectEnable;
		
		
		/// <summary>
		/// Zmienna  wymagania przy wykryciu czy zostal oddany strzal
		/// </summary>
		protected bool isOneShootBonus;
		
		/// <summary>
		/// Konfiguracja maskek Tiki.
		/// </summary>
		private TIKIMaskConfiguration tikiMaskConfig;

		protected bool isOnRecharge;

		private bool attackWithoutArmoreCreep;

		private Transform spriteTag;

		private bool isSpawned;

		private ProjectileSetting originalProjectileSettings;
	

		protected override void Awake ()
		{
			base.Awake ();


			enemyLayerToHit = 1 << LayerMask.NameToLayer("Enemies");
			blockersLayerToHit = 1 << LayerMask.NameToLayer("Blockers");

            enemyAndBlockersLayer = 1 << LayerMask.NameToLayer("Enemies Target Collider") | 1 << LayerMask.NameToLayer("Blockers");

			spriteTag = (Instantiate(spriteHud, Vector3.zero, Quaternion.identity) as Transform).GetComponent<Transform>();
			spriteTag.transform.parent = MyTransform;
			spriteTag.transform.position = Vector3.zero;
			spriteTag.transform.localPosition = Vector3.zero;
			spriteTag.gameObject.SetActive(false);

			for(int i =0; i < settings.Length; i++)
			{
				settings[i].projectileSetting.tower = this;
			}
		}
		
		protected override void Start ()
		{
			base.Start ();
			// Nasluchujemy czy bloker zostal klikniety

			turret.speed = turretSpeed;	

			if(runOnStart)
				OnSpawned();

			settings[level -1].sprite.SortingOrder = 5;
			
			CheckUpgradeAvailable();

			MyTransform.localPosition = new Vector3(MyTransform.localPosition.x, MyTransform.localPosition.y, 10);

			StartCoroutine("BlinkCollider");
			
		}

		private IEnumerator BlinkCollider()
		{
			BoxCollider2D c = MyTransform.GetComponentInChildren<BoxCollider2D>();
			c.enabled = false;

			
			yield return new WaitForSeconds(0.2f);
			
			c.enabled = true;
			
			yield return new WaitForSeconds(1f);
			
			MyTransform.localPosition = new Vector3(MyTransform.localPosition.x, MyTransform.localPosition.y, 0);
			
		}

		protected override void Update ()
		{
			base.Update();
			
				if(GameMaster.gameState == GameState.RUNNING)
				{
					//Maly hack na przelaczanie sie pomiedzy creepami z armorem wiekszym od 0
					if(settings[level - 1].projectileSetting.armoreDamage > 0)
					{
						if(currentTarget != null && !attackWithoutArmoreCreep)	
						{
							if(!attackBlocker && currentTarget.health.currentArmoreHealth <= 0)
							{
								state = TowerState.ReadyToFire;
							}
						}
					}

                    if(state == TowerState.Recharge)
                    {
                        Recharge();

                        //if (isTurret)
                        //    RotateToTarget();
                    }
                   
                    if (state == TowerState.ReadyToFire)
                    {
                        SeekTarget();
                    
                    }
                   
                    if (state == TowerState.Rotate)
                    {
                        if (isTurret)
                            RotateToTarget();
                        else
                            AttackTarget();
                    }
                   
                    if (state == TowerState.Attacking)
                    {
                        AttackTarget();
                    }
					
				}
			
		}
		
		protected virtual void RotateToTarget ()
		{
			SeekTarget();


			if(turret.target == null || currentTarget == null) return;

			//turret.speed =  Time.timeScale == 1 ? turretSpeed : turretSpeed / 2;	
			//Vector3 fixedPosition = MyTransform.InverseTransformDirection(Vector3.forward);
			Debug.DrawRay(turret.transform.position, turret.transform.up * settings[level - 1].attackRange * 2, Color.yellow);
			//Debug.Log(Vector3.Distance(MyTransform.position, currentTarget.MyTransform.position));


			//if(state != TowerState.Recharge)
			//{
				RaycastHit2D[] hit = Physics2D.RaycastAll(turret.transform.position, turret.transform.up, settings[level -1].attackRange * 2, enemyAndBlockersLayer);

				if(hit.Length > 0)
				{
					for(int i = 0; i < hit.Length; i++)
					{
						if(hit[i].transform == currentTarget.transform)
						{
	//						Debug.Log("Strzelam");
							if(state == TowerState.Rotate)
								state = TowerState.Attacking;
						}
					}
				}
			//}
//			else
//			{
//				Vector3 lookPos = currentTarget.MyTransform.position - turret.position;
//				//lookPos.y = 0;
//				lookPos.Normalize();
//				Quaternion rotation  = Quaternion.LookRotation(lookPos);
//
//				//turret.rotation = rotation;
//				//turret.rotation = Quaternion.Slerp(turret.rotation, rotation, rotateSpeed * Time.deltaTime);
//				//turret.rotation = Quaternion.RotateTowards(turret.rotation, rotation, rotateSpeed * Time.deltaTime);
//				//turret.rotation = Vector3.RotateTowards(turret.rotation, currentTarget.rotation, 1f, 1f);
//
//				turret.transform.localEulerAngles = new Vector3(turret.localEulerAngles.x, 90, 0);
//			}
//			
		}
		
		public virtual void Upgrade()
		{
			if(level <= 3)
			{
				level++;
				sprite = settings[level - 1].sprite;
				animation = settings[level - 1].animation;
				settings[level -1].sprite.SortingOrder = 5;
				for(int i = 0; i < settings.Length; i++)
				{
                    if (i != level - 1)
                        settings[i].sprite.gameObject.SetActive(false);
                    else
                        settings[i].sprite.gameObject.SetActive(true);
				}
			}
		}
		
		protected virtual void AttackTarget()
		{
			if(isOnRecharge) return;
			
			//GameObject go = PoolManager.GetItem(projectileObjectType);
			ProjectileBase p = PathologicalGames.PoolManager.Pools["Bullets"].Spawn(settings[level -1].projectileObjectType.ToString()).GetComponent<ProjectileBase>();

            //UnityEditor.EditorApplication.isPaused = true;
           // Debug.Log(currentTarget.MyTransform.position);

			p.MyTransform.position = MyTransform.position;
			settings[level -1].projectileSetting.unit = currentTarget;
			p.Setup(settings[level - 1].projectileSetting, Vector3.zero);
			
			state = TowerState.Recharge;
           
           // StartCoroutine("Recharge");
			
            if(isTikiMaskEffectEnable)
				CameraShake.Shake();
				

			if(isOneShootBonus)
				isOneShootBonus = false;

			if(settings[level -1].animation != null)
				settings[level -1].animation.Play();
				
			if(this.soundOnFire != null)
			{
				float volume = 1;
				
				if(poolObjectType == PoolObjectType.Totem_Anvil_level_1)
					volume = 0.6f;
				else if(poolObjectType == PoolObjectType.Totem_MagicShot_level_1)
					volume = 0.6f;
				else if(poolObjectType == PoolObjectType.Tower_Runic_level_1)
					volume = 0.6f;
				
				
				NGUITools.PlaySound(this.soundOnFire, volume);
	
			}

			
		}
		
		protected virtual void SeekTarget()
		{
			#region Blockers Target
			if(attackBlocker)
			{
				Collider2D[] hit = Physics2D.OverlapCircleAll(MyTransform.position, settings[level - 1].attackRange, blockersLayerToHit);

				if(hit.Length > 0)
				{
					for(int i = 0; i < hit.Length; i++)
					{
						UnitBase u = hit[i].GetComponent<UnitBaseReference>().unitBase;
						
						if(u == blockerToAttack)
						{
							
							currentTarget = u;
							attackBlocker = true;

                            if (isTurret)
                            {
                                turret.target = currentTarget.MyTransform;
                               // turret.speed = Time.timeScale == 1 ? turretSpeed : turretSpeed * 2;
                                turret.speed = turretSpeed;
                            }

							state = TowerState.Rotate;
							return;
						}
						else
						{
							attackBlocker = false;

						}
					}
				}
				else
				{
					attackBlocker = false;
					
				}
			}
			#endregion
			#region Creeps Targets
			if(!attackBlocker)
			{
				Collider2D[] hit = Physics2D.OverlapCircleAll(MyTransform.position, settings[level - 1 ].attackRange, enemyLayerToHit);

				if(hit.Length > 0)
				{
					
					
					//int lowerCountCreep = 10000000;
                    float lowerDistanseToFinal = 10000000;
					Unit targetToAttack = null;
					if(settings[level -1].projectileSetting.armoreDamage > 0)
					{
						for(int i = 0; i < hit.Length; i++)
						{
							Unit u = hit[i].transform.GetComponent<UnitBaseReference>().unitBase as Unit;
                            if (u.health.currentArmoreHealth > 0 && u.distanseToFinish < lowerDistanseToFinal)
							{
								targetToAttack = u;
                                lowerDistanseToFinal = u.distanseToFinish;

								attackWithoutArmoreCreep = false;
							}
						}

						if(targetToAttack == null)
						{

                            lowerDistanseToFinal = 100000000;
							for(int i = 0; i < hit.Length; i++)
							{
								Unit u = hit[i].transform.GetComponent<UnitBaseReference>().unitBase as Unit;

                                if (u.distanseToFinish < lowerDistanseToFinal)
								{
                                    lowerDistanseToFinal = u.distanseToFinish;
									targetToAttack = u;
                                    
									attackWithoutArmoreCreep = true;
								}
								
							}
						}
					}
					else
					{
						for(int i = 0; i < hit.Length; i++)
						{
							Unit u = hit[i].transform.GetComponent<UnitBaseReference>().unitBase as Unit;

                            if (u is Jumper)
                            {
                                targetToAttack = u;
                                break;
                            }
                            else if (u.distanseToFinish < lowerDistanseToFinal)
							{
                                lowerDistanseToFinal = u.distanseToFinish;
								targetToAttack = u;
							}
						}
					}
					
					currentTarget = targetToAttack as UnitBase;
                    if (isTurret)
                    {
                        turret.target = currentTarget.MyTransform;
                        //turret.speed = Time.timeScale == 1 ? turretSpeed : turretSpeed * 2;
                        turret.speed = turretSpeed;
                    }
					state = TowerState.Rotate;
					
					
				}
				else if(currentTarget != null)
				{
					currentTarget = null;
					if(isTurret)
						turret.target  = null;
				}
				
				
			}
			#endregion
			
		}

        private void Recharge()
        {

           if (GameMaster.gameState == GameState.RUNNING)
                attackSpeedCounter += Time.deltaTime * Unit.speedScale;
            
            if(attackSpeedCounter > settings[level - 1].attackSpeed)
            {

                //isOnRecharge = false;
                attackSpeedCounter = 0;
                state = TowerState.ReadyToFire;
               
            }
		}

        //private IEnumerator Recharge()
        //{
        //    isOnRecharge = true;
         
        //    while(attackSpeedCounter < settings[level -1 ].attackSpeed)
        //    {
           
        //        if(GameMaster.gameState == GameState.RUNNING)
        //            attackSpeedCounter += Time.deltaTime;

             
        //        yield return new WaitForEndOfFrame();

        //    }
        //    isOnRecharge = false;
        //    state = TowerState.ReadyToFire;
        //    attackSpeedCounter -= settings[level - 1].attackSpeed;
        //}
		
		protected virtual void OnDrawGizmosSelected()
		{
			if(settings[level - 1] != null)
			{
				Gizmos.color = Color.red;
				Gizmos.DrawWireSphere(transform.position, settings[level - 1].attackRange);
				
				Gizmos.color = Color.blue;
				
				if(currentTarget != null)
					Gizmos.DrawLine(transform.position, currentTarget.MyTransform.position);
			}
		}	

		private void OnSpawned()
		{

			NotificationCenter.defaultCenter.addListener(OnBlockerClicked, NotificationType.BlockerClick);
			NotificationCenter.defaultCenter.addListener(OnEnemyClicked, NotificationType.EnemyClick);
			NotificationCenter.defaultCenter.addListener(OnBackToPoolRequest, NotificationType.BackToPool);
			NotificationCenter.defaultCenter.addListener(OnMoneyCountChanged, NotificationType.MoneyCountChanged);
			NotificationCenter.defaultCenter.addListener(OnResetTikiEffect, NotificationType.ResetTikiEffect);

		}

		private void OnResetTikiEffect(Notification n)
		{
			DamageBonus db = GetComponent<DamageBonus>();
			if(db != null)
				db.Destroyme();

			StopCoroutine("ResetProjectile");

			if(originalProjectileSettings != null)
				settings[level - 1].projectileSetting = originalProjectileSettings;
			isTikiMaskEffectEnable = false;

			if(PathologicalGames.PoolManager.Pools["HitEffects"].IsSpawned(tikiEffect))
				PathologicalGames.PoolManager.Pools["HitEffects"].Despawn(tikiEffect);
			originalProjectileSettings = null;
		}

		private void OnDespawned()
		{

			//BUGFIX:0000024
			if(originalProjectileSettings != null)
				settings[level -1].projectileSetting = originalProjectileSettings;

			//tikiEfectSprite.SetActive(false);
			//ENDBUGFIX

			isTikiMaskEffectEnable = false;

			level = 1;

			sprite = settings[level - 1].sprite;
			animation = settings[level - 1].animation;
			settings[level -1].sprite.SortingOrder = 5;
			for(int i = 0; i < settings.Length; i++)
			{
				if(i != level - 1)
					settings[i].sprite.gameObject.SetActive(false);
				else
					settings[i].sprite.gameObject.SetActive(true);
			}

			activeCell = null;
			currentTarget = null;
			state = TowerState.ReadyToFire;
			isOnRecharge = false;

			NotificationCenter.defaultCenter.removeListener(OnBlockerClicked, NotificationType.BlockerClick);
			NotificationCenter.defaultCenter.removeListener(OnEnemyClicked, NotificationType.EnemyClick);
			NotificationCenter.defaultCenter.removeListener(OnBackToPoolRequest, NotificationType.BackToPool);
			NotificationCenter.defaultCenter.removeListener(OnMoneyCountChanged, NotificationType.MoneyCountChanged);
			NotificationCenter.defaultCenter.removeListener(OnResetTikiEffect, NotificationType.ResetTikiEffect);


		}

		
		protected virtual void OnEnable()
		{
			//StartCoroutine("Recharge");
			
			for (int i = 0; i < colliders.Length; i++)
			{
				colliders[i].enabled = true;
			}

			this.CheckUpgradeAvailable();

			isSpawned = true;
		}
		
		protected virtual void OnDisable()
		{
			StopCoroutine("Recharge");
			
			for (int i = 0; i < colliders.Length; i++)
			{
				colliders[i].enabled = false;
			}
			
			activeCell = null;
			attackSpeedCounter = 0;

			isSpawned = false;
		}
		
		protected virtual void OnBlockerClicked(Notification n)
		{
			if(!canAttackBlocker) return;

			BlockerClickNotification noti = n as BlockerClickNotification;

			//Debug.Log("BLOKER CHUJ ATAKUJE KURWA");

			if(blockerToAttack == noti.unit)
			{
				attackBlocker = false;
				blockerToAttack = null;
			}
			else
			{
				
				blockerToAttack = noti.unit;
				attackBlocker = true;
			}
		}
		
		protected virtual void OnEnemyClicked(Notification n)
		{
			attackBlocker = false;
			blockerToAttack = null;
		}
		
		public virtual void ActivateTikiMask(TikiMaskType type)
		{
			if(isTikiMaskEffectEnable)
			{	
				return;
			}

			if(GameMaster.onTutorial)
				NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.TutorialTikiUseProgress));

			isTikiMaskEffectEnable = true;
			Transform particleTikiEffect;
			tikiMaskConfig = LevelConfigurations.GetTikiMaskConfiguration(type);
			
			NGUITools.PlaySound(this.soundOnTikiUse);
			
			switch(type)
			{
			case TikiMaskType.FIRE:
				
				DamageBonus damageBonus = MyGameObject.AddComponent(typeof(DamageBonus)) as DamageBonus;
				damageBonus.Execute(tikiMaskConfig.damageBonus, tikiMaskConfig.ttl, this);
				//Debug.Log(tikiMaskConfig.damageBonus);
				tikiEffect = PathologicalGames.PoolManager.Pools["HitEffects"].Spawn("Fire_Tiki_Effect");
				tikiEffect.position = MyTransform.position;
				
				particleTikiEffect = PathologicalGames.PoolManager.Pools["ParticleEffects"].Spawn("ParticleEffectFire");
				particleTikiEffect.position = MyTransform.position;
				
				
				break;
				
			case TikiMaskType.FROST:

				originalProjectileSettings = new ProjectileSetting(){additiveDamage = settings[level -1].projectileSetting.additiveDamage,
																	 armoreDamage = settings[level -1].projectileSetting.armoreDamage,
																	 damage = settings[level -1].projectileSetting.damage,
																     jumpHit = settings[level -1].projectileSetting.jumpHit,
																	 radiusDamage = settings[level -1].projectileSetting.radiusDamage,
																	 radiusRange = settings[level -1].projectileSetting.radiusRange,
																     slowAmount = settings[level -1].projectileSetting.slowAmount,
																	 timeEffect = settings[level -1].projectileSetting.timeEffect,
															         unit = settings[level -1].projectileSetting.unit,
															         affectedByTiki = false
																	};

				/// Ustawiamy odpowiednie wartosci dla ustawien pocisku
				settings[level - 1].projectileSetting.slowAmount = tikiMaskConfig.slowAmount;
				settings[level - 1].projectileSetting.timeEffect = tikiMaskConfig.timeEffect;
				settings[level - 1].projectileSetting.affectedByTiki = true;
				/// Pobieramy referencje do oryginalnego pocisku
				//PoolObjectType originalProjectileType = projectileObjectType;

				
				/// Zmieniamy referencje pocisku na pocisk spowalniajacy
				//projectileObjectType = PoolObjectType.Projectile_SlowAOE_Level_1;
				
				/// ustawiamy warunek 1 strzal
				//isOneShootBonus = true;
				tikiMaskConfig = LevelConfigurations.GetTikiMaskConfiguration(type);
				/// Uruchamiamy osobny watek aby nasluchiwal czy strzal sie zakonczyl
				StartCoroutine("ResetProjectile", tikiMaskConfig.ttl);

				tikiEffect = PathologicalGames.PoolManager.Pools["HitEffects"].Spawn("Ice_Tiki_Effect");
				tikiEffect.position = MyTransform.position;
				
				
				particleTikiEffect = PathologicalGames.PoolManager.Pools["ParticleEffects"].Spawn("ParticleEffectWater");
				particleTikiEffect.position = MyTransform.position;
				
				break;
				
			case TikiMaskType.EARTH:

				
				originalProjectileSettings = new ProjectileSetting(){additiveDamage = settings[level -1].projectileSetting.additiveDamage,
					armoreDamage = settings[level -1].projectileSetting.armoreDamage,
					damage = settings[level -1].projectileSetting.damage,
					jumpHit = settings[level -1].projectileSetting.jumpHit,
					radiusDamage = settings[level -1].projectileSetting.radiusDamage,
					radiusRange = settings[level -1].projectileSetting.radiusRange,
					slowAmount = settings[level -1].projectileSetting.slowAmount,
					timeEffect = settings[level -1].projectileSetting.timeEffect,
					unit = settings[level -1].projectileSetting.unit,
					affectedByTiki = false
				};
				
				/// Ustawiamy odpowiednie wartosci dla ustawien pocisku
				settings[level - 1].projectileSetting.slowAmount = tikiMaskConfig.slowAmount;
				settings[level - 1].projectileSetting.timeEffect = tikiMaskConfig.timeEffect;
				settings[level - 1].projectileSetting.affectedByTiki = true;
				/// Pobieramy referencje do oryginalnego pocisku
				//originalProjectileType = projectileObjectType;
				
				/// Zmieniamy referencje pocisku na pocisk spowalniajacy
				//projectileObjectType = PoolObjectType.Projectile_SlowAOE_Level_1;
				
				/// ustawiamy warunek 1 strzal
				//isOneShootBonus = true;
				
				tikiMaskConfig = LevelConfigurations.GetTikiMaskConfiguration(type);
				/// Uruchamiamy osobny watek aby nasluchiwal czy strzal sie zakonczyl
				StartCoroutine("ResetProjectile", tikiMaskConfig.ttl);

				tikiEffect = PathologicalGames.PoolManager.Pools["HitEffects"].Spawn("Earth_Tiki_Effect");
				tikiEffect.position = MyTransform.position;
				
				
				particleTikiEffect = PathologicalGames.PoolManager.Pools["ParticleEffects"].Spawn("ParticleEffectEarth");
				particleTikiEffect.position = MyTransform.position;
				
				break;
				
			case TikiMaskType.WIND:
				
				RechargeBonus rechargeBonus = MyGameObject.AddComponent(typeof(RechargeBonus)) as RechargeBonus;
				rechargeBonus.Execute(tikiMaskConfig.rechargeBonus, tikiMaskConfig.ttl, this);

				tikiEffect = PathologicalGames.PoolManager.Pools["HitEffects"].Spawn("Air_Tiki_Effect");
				tikiEffect.position = MyTransform.position;
				
				
				particleTikiEffect = PathologicalGames.PoolManager.Pools["ParticleEffects"].Spawn("ParticleEffectWind");
				particleTikiEffect.position = MyTransform.position;
				
				break;
				
				
			}

			//tikiEfectSprite.SetActive(true);
			
			// Inbformujemy, ze tiki zostala uzyta
			NotificationCenter.defaultCenter.postNotification(new TikiMaskNotification( NotificationType.TikimaskUsed, type));
		}
		
		private IEnumerator ResetProjectile(float ttl)
		{
			while(ttl > 0)
			{
				if(GameMaster.gameState == GameState.RUNNING)
					ttl -= Time.deltaTime;

				yield return null;
			}
			
			/// jezeli strzal sie zakonczyl, podmieniamy referencje na oryginalny pocisk
			//projectileObjectType = originalProjectile;

			settings[level - 1].projectileSetting = originalProjectileSettings;
			isTikiMaskEffectEnable = false;

			PathologicalGames.PoolManager.Pools["HitEffects"].Despawn(tikiEffect);

			originalProjectileSettings = null;
		}
		
		void OnBackToPoolRequest(Notification n)
		{
			if(PathologicalGames.PoolManager.Pools["Towers"].IsSpawned(MyTransform))
				PathologicalGames.PoolManager.Pools["Towers"].Despawn(MyTransform);
		}
		
		public void ShowRange()
		{
			if(GameMaster.onTutorial && Application.loadedLevel == 6) return;

			rangeSprite.gameObject.SetActive(true);
			rangeSprite.scale = new Vector3(settings[level - 1].attackRange / 8, settings[level - 1].attackRange / 8, 1);
			//Debug.Log(rangeSprite.scale);
		}
		
		public void HideRange()
		{
			rangeSprite.gameObject.SetActive(false);
		}
		
		void OnLevelWasLoaded(int level)
		{
			// Nasluchujemy czy bloker zostal klikniety




		}

		private void OnMoneyCountChanged(Notification n)
		{
			//Debug.Log("chuj");
			if(!isSpawned) return;

			CheckUpgradeAvailable();
		}

		private void OnTriggerEnter2D(Collider2D c)
		{
			if(c.CompareTag("Grid"))
			{
				//Debug.Log(Vector2.Distance(MyTransform.position, c.transform.position));
				if(Vector2.Distance(MyTransform.position, c.transform.position) < 0.8f)
				{
					GridCell grid = c.GetComponent<GridCell>();
					activeCell = grid;
					grid.activeTower = this;
				}
			}
		}

		private void CheckUpgradeAvailable()
		{
			if(level < 3)
			{
				int price = LevelConfigurations.GetTowerPrice(settings[level -1].type).upgradePrice;
				
				if(SaveData.money >= price)
				{
					spriteTag.transform.position  = Vector3.zero;
					spriteTag.transform.localPosition  = Vector3.zero;
					Vector3 pos = spriteTag.transform.position;
					pos.y += sprite.GetBounds().size.y - 0.5f;
					spriteTag.transform.position = pos;
					
					spriteTag.gameObject.SetActive(true);
				}
				else
				{
					spriteTag.gameObject.SetActive(false);
					
					
				}
				
			}
			else
			{
				spriteTag.gameObject.SetActive(false);
				
			}
		}
	}
}

