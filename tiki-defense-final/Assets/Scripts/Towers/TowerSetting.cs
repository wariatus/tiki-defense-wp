﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class TowerSetting  {

	public PoolObjectType projectileObjectType;
	public tk2dSprite sprite;
	public tk2dSpriteAnimator animation;

	public TowersType type;
	public float attackRange;
	public float attackSpeed;
	public ProjectileSetting projectileSetting;



}
