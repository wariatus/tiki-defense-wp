﻿using UnityEngine;
using System.Collections;
using Towers;
using Projectiles;

public class Necklace : TowerBase {


	protected override void Awake ()
	{
		base.Awake ();
	}

	protected override void Start ()
	{
		base.Start ();
	}

	protected override void Update ()
	{
		base.Update ();
	}

	protected override void AttackTarget ()
	{
		if(isOnRecharge) return;
		
		if(level == 1)
		{
			for(int projectileCount =0; projectileCount < 4; projectileCount++)
			{
				//GameObject go = PoolManager.GetItem(projectileObjectType);
				ProjectileBase p = PathologicalGames.PoolManager.Pools["Bullets"].Spawn(settings[level -1].projectileObjectType.ToString()).GetComponent<ProjectileBase>();
				p.MyTransform.position = MyTransform.position;
				settings[level -1].projectileSetting.unit = currentTarget;

				Vector3 moveDirection = Vector3.zero;

				if(projectileCount == 0)
					moveDirection = Vector3.up;
				else if(projectileCount == 1)
					moveDirection = Vector3.down;
				else if(projectileCount == 2)
					moveDirection = Vector3.left;
				else if(projectileCount == 3)
					moveDirection = Vector3.right;

				p.Setup(settings[level - 1].projectileSetting, moveDirection);

			}

		}
		else if(level == 2)
		{
			for(int projectileCount =0; projectileCount < 8; projectileCount++)
			{
				//GameObject go = PoolManager.GetItem(projectileObjectType);
				ProjectileBase p = PathologicalGames.PoolManager.Pools["Bullets"].Spawn(settings[level -1].projectileObjectType.ToString()).GetComponent<ProjectileBase>();
				p.MyTransform.position = MyTransform.position;
				settings[level -1].projectileSetting.unit = currentTarget;
				
				Vector3 moveDirection = Vector3.zero;
				
				if(projectileCount == 0)
					moveDirection = Vector3.up;
				else if(projectileCount == 1)
					moveDirection = Vector3.down;
				else if(projectileCount == 2)
					moveDirection = Vector3.left;
				else if(projectileCount == 3)
					moveDirection = Vector3.right;
				else if(projectileCount == 4)
					moveDirection = new Vector3(1, 1, 0);
				else if(projectileCount == 5)
					moveDirection = new Vector3(-1, -1, 0);
				else if(projectileCount == 6)
					moveDirection = new Vector3(1, -1, 0);
				else if(projectileCount == 7)
					moveDirection = new Vector3(-1, 1, 0);

				
				p.Setup(settings[level - 1].projectileSetting, moveDirection);
			}
		}
		else if(level == 3)
		{
			for(int projectileCount =0; projectileCount < 8; projectileCount++)
			{
				//GameObject go = PoolManager.GetItem(projectileObjectType);
				ProjectileBase p = PathologicalGames.PoolManager.Pools["Bullets"].Spawn(settings[level -1].projectileObjectType.ToString()).GetComponent<ProjectileBase>();
				p.MyTransform.position = MyTransform.position;
				settings[level -1].projectileSetting.unit = currentTarget;
				
				Vector3 moveDirection = Vector3.zero;
				
				if(projectileCount == 0)
					moveDirection = Vector3.up;
				else if(projectileCount == 1)
					moveDirection = Vector3.down;
				else if(projectileCount == 2)
					moveDirection = Vector3.left;
				else if(projectileCount == 3)
					moveDirection = Vector3.right;
				else if(projectileCount == 4)
					moveDirection = new Vector3(1, 1, 0);
				else if(projectileCount == 5)
					moveDirection = new Vector3(-1, -1, 0);
				else if(projectileCount == 6)
					moveDirection = new Vector3(1, -1, 0);
				else if(projectileCount == 7)
					moveDirection = new Vector3(-1, 1, 0);
				
				
				p.Setup(settings[level - 1].projectileSetting, moveDirection);
			}
		}

		state = TowerState.Recharge;
		
		if(isTikiMaskEffectEnable)
			CameraShake.Shake();
		
		if(isOneShootBonus)
			isOneShootBonus = false;
		
		if(settings[level -1].animation != null)
			settings[level -1].animation.Play();
		
		if(GameMaster.canPlayFXSounds)
			NGUITools.PlaySound(soundOnFire, 1, 1);
		
		StartCoroutine("Recharge");
	}


}
