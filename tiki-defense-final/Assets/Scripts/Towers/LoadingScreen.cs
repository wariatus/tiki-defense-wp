﻿using UnityEngine;
using System.Collections;
using System.Runtime.Remoting.Messaging;

public class LoadingScreen : Singleton<LoadingScreen> {

	#region PUBLIC VARIABLE

	public UITweener leftSide;
	public UITweener rightSide;

    public float ShakeSpeed = 5.0f;
    public float ShakeAmount = 0.2f;

    private bool startShake;
	
	#endregion

	#region PRIVATE VARIABLE

    private ShakeGUIElement shake;
    private Vector3 originalPosition;

    private bool isShowing;
    private bool isHideing;
	#endregion

 	#region INIT

	protected override void Awake ()
	{
		base.Awake ();

       // shake = GetComponent<ShakeGUIElement>();
        //originalPosition = MyTransform.position;
	}


	protected override void Start ()
	{
		base.Start ();
	}

	protected override void Update ()
	{
		base.Update ();

	    if (!startShake) return;
	    leftSide.transform.localPosition = Shake(leftSide.transform.localPosition);
	    rightSide.transform.localPosition = Shake(rightSide.transform.localPosition);
	}

	#endregion


	#region PUBLIC METHODS

	public void Show()
	{
        if (!isShowing)
        {
            // leftSide.ResetToBeginning();
            leftSide.PlayForward();
            //  rightSide.ResetToBeginning();
            rightSide.PlayForward();

            isShowing = true;
        //    Invoke("RunLevel", 1.1f);
        }
	}

    public void Hide()
	{
        if (!isHideing)
        {
            //MyTransform.position = originalPosition;
            //  leftSide.ResetToBeginning();
            leftSide.PlayReverse();
            //  rightSide.ResetToBeginning();
            rightSide.PlayReverse();

            isHideing = true;
        }
	}

    public void StartShake()
    {
        startShake = true;

        Invoke("Finished", 0.2f);
    }

    public void Finished()
    {

        startShake = false;
        if (isShowing)
        {
            isShowing = false;
            RunLevel();
        }

        if (isHideing)
            isHideing = false;
    }

   


	#endregion

	#region PROTECTED METHODS
	
	
	
	#endregion

	#region PRIVATE METHODS

    private Vector3 Shake(Vector3 position)
    {
        
        Vector3 shakeVector = position;
        shakeVector.x += Random.insideUnitCircle.x*ShakeSpeed;
        shakeVector.y += Random.insideUnitCircle.y * ShakeSpeed;
        return shakeVector;
    }

	private void RunLevel()
	{
		Application.LoadLevel(SaveData.levelToLoad);
	}

	#endregion

	#region EVENTS & NOTIFICATIONS

	private void OnLevelWasLoaded(int level)
	{
		Invoke ("Hide", 0.5f);
		//this.Hide();

	}



	#endregion
	
}
