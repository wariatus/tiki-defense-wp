﻿using UnityEngine;
using System.Collections;
using Towers;

public class Melter : TowerBase {

	public LineRenderer beam;
	public PoolObjectType hitEffect = PoolObjectType.Null;

	private DamageInfo damageInfo;
	private UnitBase previousTarget;
	private ProjectileHitEffect previousEffect;
	protected override void Awake ()
	{
		base.Awake ();
		
		
	}

	protected override void Start ()
	{
		base.Start ();
		beam.sortingLayerName = "Range";
		beam.sortingOrder = 2;
		
	}

	protected override void Update ()
	{
		base.Update ();
		

	}

	protected virtual void Splash(UnitBase u)
	{
		if(hitEffect != PoolObjectType.Null && u != null)
		{
			ProjectileHitEffect effect;
			if(previousTarget == null) 
			{
				previousTarget = u;

				effect = PathologicalGames.PoolManager.Pools["HitEffects"].Spawn(hitEffect.ToString()).GetComponent<ProjectileHitEffect>();
				
				effect.MyTransform.position = u.MyTransform.position;
				
				effect.SetTarget(u.MyTransform);
				previousEffect = effect;
			}
			else if(previousTarget != u)
			{
			    effect = PathologicalGames.PoolManager.Pools["HitEffects"].Spawn(hitEffect.ToString()).GetComponent<ProjectileHitEffect>();
				
				effect.MyTransform.position = u.MyTransform.position;
				
				effect.SetTarget(u.MyTransform);

				previousTarget = u;
			}
			else if(!PathologicalGames.PoolManager.Pools["HitEffects"].IsSpawned(previousEffect.MyTransform))
			{
				effect = PathologicalGames.PoolManager.Pools["HitEffects"].Spawn(hitEffect.ToString()).GetComponent<ProjectileHitEffect>();
				
				effect.MyTransform.position = u.MyTransform.position;
				
				effect.SetTarget(u.MyTransform);
				previousEffect = effect;
			}
			
			
		}
	}

	protected override void AttackTarget ()
	{
		if(currentTarget != null && currentTarget.isSpawned)
		{
			//Debug.Log(Vector3.Distance(MyTransform.position, currentTarget.MyTransform.position) + " " +settings[level -1].attackRange + 2.4f) ;
			if(Vector3.Distance(MyTransform.position, currentTarget.MyTransform.position) <= settings[level -1].attackRange + 2.4f)
			{
				
				Splash(currentTarget);
				
				beam.SetVertexCount(2); 
				beam.SetPosition(0, MyTransform.position);
				beam.SetPosition(1, currentTarget.MyTransform.position);

				if(currentTarget.health.armoreHealth > 0)
					damageInfo = new DamageInfo(settings[level -1].projectileSetting.armoreDamage * Time.deltaTime, settings[level-1].projectileSetting.armoreDamage * Time.deltaTime, Element.None, null, false, MyTransform.position, false);
				else
					damageInfo = new DamageInfo(settings[level -1].projectileSetting.damage * Time.deltaTime, settings[level -1].projectileSetting.armoreDamage * Time.deltaTime, Element.None, null, false, MyTransform.position, false);
			
				currentTarget.RecieveDamage(damageInfo);

				if(settings[level -1].projectileSetting.slowAmount > 0)
				{
					Unit unit = currentTarget as Unit;
					if(unit != null)
					{
						MoveSlowData slowData = new MoveSlowData(){ modifier = settings[level -1].projectileSetting.slowAmount, ttl = settings[level -1].projectileSetting.timeEffect };
						unit.SetSpeedModifier(slowData);
		
					}
				}

				if(isOneShootBonus)
					isOneShootBonus = false;

				if(settings[level -1].animation != null)
					settings[level -1].animation.Play();

			}
			else
			{
				//Debug.Log("chuj 2");
				state = TowerState.Recharge;
				beam.SetVertexCount(0);
				StartCoroutine("Recharge");
			}
		}
		else
		{
			beam.SetVertexCount(0);

			state = TowerState.Recharge;

			StartCoroutine("Recharge");
		}

	}

	protected override void OnBlockerClicked (Notification n)
	{
		base.OnBlockerClicked (n);

		currentTarget = null;
	}

	protected override void OnEnemyClicked (Notification n)
	{
		base.OnEnemyClicked (n);

		currentTarget = null;
	}

	protected override void OnDisable ()
	{
		base.OnDisable ();
		previousTarget = null;
		currentTarget = null;
	}

}
