﻿using UnityEngine;
using System.Collections;

namespace Towers
{
    public class Campfire : TowerBase
    {
        protected override void Awake()
        {
            base.Awake();
        }

        protected override void Start()
        {
            base.Start();
        }

        protected override void Update()
        {
            if (GameMaster.gameState == GameState.RUNNING)
            {
                if (state == TowerState.ReadyToFire)
                {
                    SeekTarget();
                }
                else if (state == TowerState.Attacking)
                {
                    AttackTarget();
                }

            }
        }
  
    }
}