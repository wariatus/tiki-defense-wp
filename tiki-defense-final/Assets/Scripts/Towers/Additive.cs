﻿using UnityEngine;
using System.Collections;
using Projectiles;

namespace Towers
{
	public class Additive : TowerBase {

	
		private UnitBase previousTarget;
		private int counter;
		private float originalDamage;

		protected override void Awake ()
		{
			base.Awake ();
		} 

		protected override void Start ()
		{
			base.Start ();

			originalDamage = settings[level -1].projectileSetting.damage;
		}

		protected override void Update ()
		{
			base.Update ();
		}

		protected override void AttackTarget ()
		{
			if(isOnRecharge) return;
			
			//GameObject go = PoolManager.GetItem(projectileObjectType);
			ProjectileBase p = PathologicalGames.PoolManager.Pools["Bullets"].Spawn(settings[level -1].projectileObjectType.ToString()).GetComponent<ProjectileBase>();
			NGUITools.PlaySound(soundOnFire, 1, 1);
			p.MyTransform.position = MyTransform.position;

			if(previousTarget == null)
			{
				counter++;
				previousTarget = currentTarget;
			}
			else if(previousTarget == currentTarget)
			{
				counter++;

				if(counter > 4)
					counter = 4;

			}
			else
			{
				previousTarget = currentTarget;
				counter = 1;
			}

			if(counter == 1)
			{
				settings[level -1].projectileSetting.damage = originalDamage;
				//Debug.Log("Strzelam 1");
			}
			else if(counter == 2)
			{
				settings[level -1].projectileSetting.damage = originalDamage + settings[level -1].projectileSetting.additiveDamage;
				//Debug.Log("Strzelam 2");
			}
			else if(counter == 3)
			{
				settings[level -1].projectileSetting.damage = originalDamage + (2 * settings[level -1].projectileSetting.additiveDamage);
				//Debug.Log("Strzelam 3");
			}
			else if(counter == 4)
			{
				settings[level -1].projectileSetting.damage = originalDamage + (3 * settings[level -1].projectileSetting.additiveDamage);
				//Debug.Log("Strzelam 3");
			}

			//Debug.Log(settings[level -1].projectileSetting.damage);

			settings[level -1].projectileSetting.unit = currentTarget;
			p.Setup(settings[level - 1].projectileSetting, Vector3.zero);
			
			state = TowerState.Recharge;
			
			if(isOneShootBonus)
				isOneShootBonus = false;
			
			if(settings[level -1].animation != null)
				settings[level -1].animation.Play();
			
			StartCoroutine("Recharge");
		}
	}

}