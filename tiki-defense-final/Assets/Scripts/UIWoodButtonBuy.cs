﻿using UnityEngine;
using System.Collections;

public class UIWoodButtonBuy : ExtendedMonoBehavior {

	#region Variables
    public Collider collider;
    public UIButton button;
	#endregion

	#region Initialization

	protected override void Awake () 
	{
		base.Awake();
        if (SaveData.plainTutorial != 1)
        {

            button.isEnabled = false;
            InvokeRepeating("Check", 1f, 0.5f);
        }

	}

	protected override void Start () 
	{
		base.Start();

        
        
	}

	#endregion
	
	// Update is called once per frame
	protected override void Update () {
	
		base.Update();
	}

	#region Public methods

	#endregion

	#region Protected methods

	#endregion
	
	#region Private methods

    private void Check()
    {
        if(!collider.enabled)
            if(SaveData.plainTutorial == 1)
            {
                button.isEnabled = true;
               
            }
    }
	#endregion
	
	#region Events & Notifications

	#endregion



}
