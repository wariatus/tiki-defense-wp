﻿using UnityEngine;
using System.Collections;
using GoogleFu;

public enum ADState
{
	WATCH_AD,
	WATCH_IDLE,
	WATCH_AD_FOR_GEMS
}

public class ADManager : Singleton<ADManager> {

	public string[] adColonyZoneId;
	public ADState adState;

	private bool appLovinInterEnable;
	void Awake()
	{

	}

	// Use this for initialization
	void Start () {

		this.Initialize();
		this.adState = ADState.WATCH_IDLE;
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	[ConsoleCommandAttribute("adpg","Play Global")]
	[ConsoleCommandAttribute("ad_play_global", "Play GLobal")]
	public static void CPlayAdVide()
	{
		ADManager.Instance.PlayAdVideo();
	}

	[ConsoleCommandAttribute("adpc","Play Colony")]
	[ConsoleCommandAttribute("ad_play_colony", "Play Colony")]
	public static void CPlayAdColony()
	{
		ADManager.instance.PlayAdColony();

	}

	[ConsoleCommandAttribute("adpl","Play AppLovin")]
	[ConsoleCommandAttribute("ad_play_lovin", "Play AppLovin")]
	public static void CPlayAdLovin()
	{
		ADManager.instance.PlayAdAppLovin();

	}

	[ConsoleCommandAttribute("adpv","Play Vungle")]
	[ConsoleCommandAttribute("ad_play_vungle", "Play Vungle")]
	public static void CPlayAdVungle()
	{
		ADManager.instance.PlayAdVungle();
	}

	public void PlayAdVideoForGames()
	{
		this.PlayAdVideo(true);
		GA.API.Design.NewEvent("Free gems for watching a video");
	}

	public void PlayAdVideo(bool isForGems = false)
	{
		if(this.adState == ADState.WATCH_IDLE)
			this.CheckAvailableVideoAndPlay(isForGems);
	}

	public void PlayAdAppLovin()
	{
		 if(AppLovin.IsIncentInterstitialReady())
			AppLovin.ShowInterstitial();	
	}

	public void PlayAdColony()
	{
		if(AdColony.IsVideoAvailable(this.adColonyZoneId[0]))
			AdColony.ShowVideoAd(this.adColonyZoneId[0]);
	}

	public void PlayAdVungle()
	{
#if UNITY_IOS || UNITY_ANDROID
		 if(Vungle.isAdvertAvailable())
			Vungle.displayAdvert(true);
#endif
	}

	private void Initialize()
	{
		this.InitializeADColony();
		this.InitializeAdLovin();
		this.InitializeAdVungle();
	}

	

	private void InitializeADColony()
	{ 
		AdColony.OnVideoFinished += this.OnAdColonyVideoFinished;
		AdColony.Configure("version:1.0", "app424d54aca20e453082", "vzf85c00fdf5ad41a7bd", "vz3792863d10784f6392");
		
	}

	private void InitializeAdLovin()
	{
		AppLovin.SetSdkKey("ooGQQyOJQZMHO3y0a3u6py6BZCWAH2h3rzUFvtZJRxbq_TrZbkfPfnaDz6OE84I-1nDxeoF5iswiOlG_SxO7hz");
		AppLovin.InitializeSdk();
		AppLovin.PreloadInterstitial();
		AppLovin.SetUnityAdListener("Ad Manager");
	}

	private void InitializeAdVungle()
    {
#if UNITY_IOS || UNITY_ANDROID
		Vungle.init("5447fca6c88beb0c11000044", "54384df34b9d3a10600000c2");
		//Vungle.setSoundEnabled(true);
		Vungle.onAdViewedEvent += HandleonAdViewedEvent;
#endif

    }

	private void HandleonAdViewedEvent (double arg1, double arg2)
	{
		Debug.Log("Vungle: On Video Finished");

		if(this.adState == ADState.WATCH_AD_FOR_GEMS)
		{
			Unibiller.CreditBalance("gems", GlobalConfiguration.VIDEO_AD_FREE_GEMS);
			NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.PremiumChanged));
            UIShopWindowConfirmation.Instance.Show(Translate.Instance.GetRow(Translate.rowIds.ID_AD_WATCHED).GetStringData(GameMaster.language), true, "rubins");
			NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.MainThemeMusicOn));
		}

		adState = ADState.WATCH_IDLE;
	}

	private void OnAdColonyVideoFinished(bool ad_was_shown)
	{
		if(ad_was_shown)
		{
			Debug.Log("AdColony: On Video Finished");
			// Resume your app here.

			if(this.adState == ADState.WATCH_AD_FOR_GEMS)
			{
				Unibiller.CreditBalance("gems", GlobalConfiguration.VIDEO_AD_FREE_GEMS);
				NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.PremiumChanged));
                UIShopWindowConfirmation.Instance.Show(Translate.Instance.GetRow(Translate.rowIds.ID_AD_WATCHED).GetStringData(GameMaster.language), true, "rubins");
				NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.MainThemeMusicOn));
			}

			adState = ADState.WATCH_IDLE;
		}
	}

	private void onAppLovinEventReceived(string ev)
	{
		
		Debug.Log(ev);
		// Process an event of the format REWARDAPPROVEDINFO|10|Credits
		if(ev.Equals("LOADEDINTER"))
		{
			this.appLovinInterEnable = true;
			Debug.Log(this.appLovinInterEnable);
			
		}
		else if(ev.Equals("HIDDENINTER"))
		{
			this.adState = ADState.WATCH_IDLE;
			AppLovin.PreloadInterstitial();
		}
		else if(ev.Equals("LOADFAILED"))
		{
			Invoke("AppLovinPreloadInterstitate", 5f);
		}
	}
	
	private void AppLovinPreloadInterstitate()
	{
		AppLovin.PreloadInterstitial();
	}



	private void CheckAvailableVideoAndPlay(bool isForGems = false)
	{
		if(AdColony.IsVideoAvailable(this.adColonyZoneId[0]))
		{

            if (isForGems)
                this.adState = ADState.WATCH_AD_FOR_GEMS;
            else
                this.adState = ADState.WATCH_AD;

			AdColony.ShowVideoAd(this.adColonyZoneId[0]);

			NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.MainThemeMusicOff));

		}
		else if(this.appLovinInterEnable)
		{
			AppLovin.ShowInterstitial();
			this.appLovinInterEnable = false;

			NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.MainThemeMusicOff));
			if(isForGems)
				this.adState = ADState.WATCH_AD_FOR_GEMS;
			else
				this.adState = ADState.WATCH_AD;
        }
#if UNITY_IOS || UNITY_ANDROID
		else if(Vungle.isAdvertAvailable())
		{
			Vungle.displayAdvert(true);
			NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.MainThemeMusicOff));
			if(isForGems)
				this.adState = ADState.WATCH_AD_FOR_GEMS;
			else
				this.adState = ADState.WATCH_AD;
		}
#endif

    }
}
