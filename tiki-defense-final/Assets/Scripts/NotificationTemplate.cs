﻿
/// <summary>
/// Enumerator typów, które mówią o sposobie wywołania/zaplanowania
/// notyfikacji
/// </summary>
public enum NotificationServiceType
{
	ON_QUIT,		// Aplikacja została zamknięta
	ON_FOCUS_OFF,   // Aplikacja aktualnie nie jest katywna, ale nie została zamknięta (działa w tle)

}

public enum NotificationServiceCondition
{
	OFF,
	ON_LIVE_DECRESED
}

/// <summary>
/// Template notyfikacji, którą używamy w NotificationManager.cs
/// </summary>
[System.Serializable]
public class NotifiationTemplate
{
	public int minutes;							// Po ilu minutach od wywołania, powinna pojawić się notyfikacja
	public string alertBody;					// Treść notyfikacji
	public NotificationServiceType enableOn;	// Kiedy notyfikacja ma zostać dodana do listy dostepnych notyfikacji
	public NotificationServiceCondition serviceCondition;

}