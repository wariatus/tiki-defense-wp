﻿Shader "Unlit/Transparent Fix"
{
        Properties
        {
                _MainTex ("Base (RGB), Alpha (A)", 2D) = "black" {}
                _RayData ("Ray direction vector (XY), Percentage ray thickness (Z)", Vector) = (1.0, 0.0, 0.25, 1.0)
                _RayOffset ("Percentage ray offset", Vector) = (0.5, 0.0, 0.0, 1.0)
        }
       
        SubShader
        {
                LOD 100
 
                Tags
                {
                        "Queue" = "Transparent"
                        "IgnoreProjector" = "True"
                        "RenderType" = "Transparent"
                }
               
                Cull Off
                Lighting Off
                ZWrite Off
                Fog { Mode Off }
                Offset -1, -1
                Blend SrcAlpha OneMinusSrcAlpha
 
                Pass
                {
                        CGPROGRAM
                        #pragma vertex vert
                        #pragma fragment frag
                               
                        #include "UnityCG.cginc"
       
                        struct appdata_t
                        {
                                float4 vertex : POSITION;
                                float2 texcoord : TEXCOORD0;
                                float4 color : COLOR;
                        };
       
                        struct v2f
                        {
                                float4 vertex : SV_POSITION;
                                half2 texcoord : TEXCOORD0;
                                float4 color : COLOR;
                        };
       
                        sampler2D _MainTex;
                        float4 _MainTex_ST;
                        uniform float3 _RayData;
                        uniform float2 _RayOffset;
 
                        v2f vert (appdata_t v)
                        {
                                v2f o;
                                o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
                                o.texcoord = v.texcoord;
                                o.color = v.color;
                                return o;
                        }
                               
                        fixed4 frag (v2f i) : COLOR
                        {
                                fixed4 col = tex2D(_MainTex, i.texcoord) * i.color;
                                float d = dot(normalize(_RayData.xy), normalize(i.texcoord - _RayOffset));
                                float a = ceil(_RayData.z - abs(d));
                                return lerp(col, float4(1.0, 0.0, 0.0, 1.0), float4(a * col.a));
                        }
                        ENDCG
                }
        }
 
        SubShader
        {
                LOD 100
 
                Tags
                {
                        "Queue" = "Transparent"
                        "IgnoreProjector" = "True"
                        "RenderType" = "Transparent"
                }
               
                Pass
                {
                        Cull Off
                        Lighting Off
                        ZWrite Off
                        Fog { Mode Off }
                        Offset -1, -1
                        ColorMask RGB
                        Blend SrcAlpha OneMinusSrcAlpha
                        ColorMaterial AmbientAndDiffuse
                       
                        SetTexture [_MainTex]
                        {
                                Combine Texture * Primary
                        }
                }
        }
}