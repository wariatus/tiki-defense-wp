﻿using System.Collections;
using UnityEngine;

public class ExtendedMonoBehavior : MonoBehaviour {

    /// <summary>
    /// Cache na komponent Transform
    /// </summary>

    private Transform myTransform;
    public Transform MyTransform
    {
        get
        {
            if (myTransform == null)
                myTransform = GetComponent<Transform>();
            return myTransform;
        }
    }

    /// <summary>
    /// Cache na komponent GameObject
    /// </summary>
    [HideInInspector]
    private GameObject myGameObject;
    public GameObject MyGameObject
    {
        get
        {
            if (myGameObject == null)
                myGameObject = gameObject;
            return myGameObject;
        }
    }

    [HideInInspector]
    private Rigidbody2D myRigidbody;
    public Rigidbody2D MyRigidbody
    {
        get
        {
            if (myRigidbody == null)
                myRigidbody = GetComponent<Rigidbody2D>();
            return myRigidbody;
        }
    }
    
    public void Enable()
    {
    	MyGameObject.SetActive(true);
    }
    
    public void Disable()
    {
		MyGameObject.SetActive(false);
    }

    [HideInInspector]
    public Camera mainCamera;

    protected virtual void Awake()
    {
      
		
    }

	// Use this for initialization
    protected virtual void Start()
    {
      if (Camera.main != null)
            mainCamera = Camera.main.camera;

	}
	
	// Update is called once per frame
    protected virtual void Update()
    {
	
	}


  
}
