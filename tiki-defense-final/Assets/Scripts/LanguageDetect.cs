﻿using UnityEngine;
using System.Collections;

public class LanguageDetect : MonoBehaviour {

	// Use this for initialization
	void Awake () {
	
		SetCurrentLanguage();

	}

	void Start()
	{

	}
	
	// Update is called once per frame
	void Update () {
	
	}


	void SetCurrentLanguage()
	{
		SystemLanguage language = Application.systemLanguage;

		switch(language)
		{
			case SystemLanguage.German:
				SaveData.defaultLanguage = "DE";
			break;
			case SystemLanguage.French:
				SaveData.defaultLanguage = "FR";
			break;
			case SystemLanguage.Spanish:
				SaveData.defaultLanguage = "SP";
			break;
			case SystemLanguage.Polish:
				SaveData.defaultLanguage = "PL";
			break;
			case SystemLanguage.Portuguese:
				SaveData.defaultLanguage = "BR";
			break;
			case SystemLanguage.Unknown:
				SaveData.defaultLanguage = "EN";
			break;
			default:
				SaveData.defaultLanguage = "EN";
			break;
		}
	}
}
