﻿using UnityEngine;
using System.Collections;

public class iOSNotificationLocal : MonoBehaviour {


	public NotificationManager manager;  // Referencja do managera notyfikacji

	void Awake()
	{

	}

	// Use this for initialization
	void Start () {


	}
	
	// Update is called once per frame
	void Update () {
	
	}


	/// <summary>
	/// Metoda odpowiedzialna za dodanie notyfikacji do listy oczekujących na wykonanie
	/// </summary>
	/// <param name="_type">Rodzaj zdarzenia, w jakim rozpatujemy czy notyfikacja powinna się odpalić.</param>
	private void InitializeNotifications(NotificationServiceType _type)
	{
#if !UNITY_STANDALONE
		foreach(NotifiationTemplate template in this.manager.notifications)
		{
			// Jeżeli sprawdzany typ wywołania notyfikacji jest taki sam jak typ sprawdzanej notyfikacji
			if(_type == template.enableOn)
			{
				// Sprawdzamy czy notyfikacja nie jest już dodana do listy oczekującyh na wykonanie
				if(!this.CheckIfNotificationIsScheduled(template))
				{
					// Tworzymy nowy obiekt notyfikacji lokalnej
					LocalNotification noti = new LocalNotification();

					template.minutes = template.serviceCondition == NotificationServiceCondition.OFF ? template.minutes : this.MinutesFromCondition(template.serviceCondition);

					if(template.minutes == 0) continue;

					// Przypisujemy date (DateTime), o której ma się notyfikacja odpalić
					noti.fireDate = System.DateTime.Now.AddMinutes(template.minutes);


					// Dodajemy treść notyfikacji
					noti.alertBody = template.alertBody;
					//Debug.Log(noti.fireDate.ToString("yyyy-MM-dd HH:mm:ss tt"));
					// Dodajemy utworzony obiekt notyfikacji lokalnej do listy oczekujących na wykonanie
					NotificationServices.ScheduleLocalNotification(noti);

					if(Debug.isDebugBuild)
						Debug.Log("[NotificationService]: Rejestracja notyfikacji " + noti.alertBody);


				}
			}
		}
#endif
	}

	private int MinutesFromCondition(NotificationServiceCondition _condition)
	{
		switch(_condition)
		{
			case NotificationServiceCondition.ON_LIVE_DECRESED:
					return  Mathf.RoundToInt(LiveManager.Instance.GetTimeToFullLive() / 60f);
				break;
		}

		return 0;
	}

	/// <summary>
	/// Metoda spradza czy notyfikacja znajduje się już w liście oczekujących na wywołanie
	/// </summary>
	/// <returns><c>true</c>, Gdy jest już dodana <c>false</c> Gdy nie jest dodana.</returns>
	/// <param name="_template">Sprawdzana notyfikacja</param>
	private bool CheckIfNotificationIsScheduled(NotifiationTemplate _template)
	{
		//Debug.Log("Sprawdzam: " + _template.alertBody);
#if !UNITY_STANDALONE
		if(NotificationServices.scheduledLocalNotifications.Length > 0)
		{
			foreach(LocalNotification noti in NotificationServices.scheduledLocalNotifications)
			{
				if(noti.alertBody.Equals(_template.alertBody))
					return true;
			}
		}
#endif

		return false;
	}

	// Eventy natywne w Unity3D. 
	// OnApplicationPause - czy aplikacja jest aktualnie aktywna
	// OnApplicationQuit  - czy aplikacja zakończyła swoje działanie

	// Za każdym razem gdy event się odpali, próbujemy dodać notyfikację, która
	// pasuje aktualnie odpalonemu eventowi. 


	void OnApplicationPause(bool pasueStatus) 
	{
        #if !UNITY_STANDALONE
		if(pasueStatus)
		{
			this.InitializeNotifications(NotificationServiceType.ON_FOCUS_OFF);

		}
		else
		{
			// W przydku gdygracz jest znów wgrze, kasujemy wszelkie notyfikacje.
			// Nie chcemy aby gracz dostał notyfikację z gry, w którą obecnie gra
			NotificationServices.ClearLocalNotifications();
			NotificationServices.CancelAllLocalNotifications();

			if(Debug.isDebugBuild)
				Debug.Log("[NotificationServices]: Clear local notofication after pausa off");
		}
#endif

	}

	void OnApplicationQuit() 
	{
        #if !UNITY_STANDALONE
		this.InitializeNotifications(NotificationServiceType.ON_QUIT);
#endif

	}
}
