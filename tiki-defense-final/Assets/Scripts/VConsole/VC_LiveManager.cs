﻿using UnityEngine;
using System.Collections;

public class VC_LiveManager : MonoBehaviour {

	[ConsoleCommandAttribute("lm_increase_live", "Increase Live in LiveManager")]
	public static void CIncreaseLive()
	{
		LiveManager.Instance.IncreaseLive();
	}

	[ConsoleCommandAttribute("lm_decrease_live", "Derease Live in LiveManager")]
	public static void CDecraseLive()
	{
		LiveManager.Instance.DecreaseLive();
	}

	[ConsoleCommandAttribute("lm_time_to_reach", "Gete time to reach")]
	public static void CGetTimeToReach()
	{
		Debug.Log ("[LiveManager]: Time to Reach = " + LiveManager.Instance.TimeToReach);
	}

	[ConsoleCommandAttribute("lm_current_time", "Get current time from LiveManager")]
	public static void CGetCurrentTime()
	{
		Debug.Log ("[LiveManager]: Current Time = " + LiveManager.Instance.CurrentTime);
	}

	[ConsoleCommandAttribute("lm_get_live", "Get current time from LiveManager")]
	public static void CGetCurrentLive()
	{
		Debug.Log ("[LiveManager]: Current Live = " + LiveManager.Instance.CurrentLiveCount);
	}

	[ConsoleCommandAttribute("lm_time_to_full_live", "Time to full live")]
	public static void CGGetTimeToFullLive()
	{
		Debug.Log ("[LiveManager]: Time to full live = " + Mathf.RoundToInt(LiveManager.Instance.GetTimeToFullLive())+"S " 
		           + Mathf.RoundToInt(LiveManager.Instance.GetTimeToFullLive() / 60f)+"M");
	}

	[ConsoleCommandAttribute("lm_time_to_next_live", "Time to next added live")]
	public static void CGetTimeToNextLive()
	{
		Debug.Log ("[LiveManager]: Time to next live = " + Mathf.RoundToInt(LiveManager.Instance.GetTimeToNextLive())+"S " 
		           + Mathf.RoundToInt(LiveManager.Instance.GetTimeToNextLive() / 60f)+"M");
	}

	[ConsoleCommandAttribute("lm_coroutine_status", "Coroutine status")]
	public static void CGGetCoroutineStatus()
	{
		Debug.Log ("[LiveManager]: coroutine status = " + LiveManager.Instance.coroutineIsrunning);
	}

}
