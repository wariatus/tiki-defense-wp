﻿using UnityEngine;
using System.Collections;

public class JumpCell : MonoBehaviour {

	public Transform finishJumpPosition;		// Pozycja w ktorej konczy sie skok
	public int waypointID;				// Nastepny waypoint, z ktorego skorzysta creep po zakonczeniu skoku

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public  void OnTriggerEnter2D(Collider2D c)
	{

		if(c.tag == "Enemy")
		{
			Jumper u = c.GetComponent<UnitBaseReference>().unitBase as Jumper;

			if(u != null)
			{
				if(u.canJump)
				{
					if(u.state != UnitState.Jumping)
					{
						u.StartJump(finishJumpPosition.position);

					}
					else
					{

						u.StopJump();
						u.SwitchWaypoint(waypointID);
					}
				}
			}
		}

	}
}
