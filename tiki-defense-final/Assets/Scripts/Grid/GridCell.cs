﻿using UnityEngine;
using System.Collections;

public class GridCell : ExtendedMonoBehavior {
	
	public bool isClickable;
	public bool isTutorialGrid;
	public bool haveBlockerOnStart;

	public Transform SpriteSelected;

	public Towers.TowerBase activeTower
	{
		get { return _activeTower; }
		set 
		{
			if(value != null)
				gridSpriteTag.gameObject.SetActive(false);

			_activeTower = value;
		}
	}
	private Towers.TowerBase _activeTower;
	public ParticleSystem particleSpawn;

	private float shake_decay;
	private float shake_intensity;
	
	public bool isClicked;
	private Vector3 scaleUp = Vector3.zero;
	
	private Vector3 resetScale;
	
	private Vector3 originPosition;
	private Quaternion originRotation;

	[HideInInspector]
	public tk2dSprite sprite;
	
	private tk2dSprite gridSpriteTag;
	private AudioClip clip;
	public static Vector3 spriteBound = Vector3.zero;
	public GameObject particleTutorial;

	public bool StateCheckEnable { get;  set; }

	protected override void Awake()
	{
		base.Awake();	

		gridSpriteTag = (Instantiate(SpriteSelected, Vector3.zero, Quaternion.identity) as Transform).GetComponent<tk2dSprite>();
		gridSpriteTag.transform.parent = MyTransform;
		gridSpriteTag.transform.position = Vector3.zero;
		gridSpriteTag.transform.localPosition = Vector3.zero;
		gridSpriteTag.gameObject.SetActive(false);
		clip = gridSpriteTag.GetComponent<AudioSource>().clip;

		NotificationCenter.defaultCenter.addListener(OnGameCoutnToStart, NotificationType.OnCountStart);
		NotificationCenter.defaultCenter.addListener(OnShowGridSelect, NotificationType.ShowGridSelect);
		NotificationCenter.defaultCenter.addListener(OnStateChanged, NotificationType.OnGameStateChanged);
		//if(particleSpawn != null)
			//NotificationCenter.defaultCenter.addListener(OnEnemySpawn, NotificationType.OnEnemySpawn);


		if(sprite == null)
			sprite = GetComponent<tk2dSprite>();

		if(spriteBound == Vector3.zero)
		{
			//Debug.Log("chuj 1");
			if(sprite.CurrentSprite.name.Contains("path"))
			{
				//Debug.Log("chuj 2");
				spriteBound = sprite.CurrentSprite.GetBounds().size;
			}
			
		}

		haveBlockerOnStart = false;
	}




	
	// Use this for initialization
	protected override void Start () {
		base.Start();
		


		if(spriteBound == Vector3.zero)
			if(sprite.CurrentSprite.name.Contains("path"))
				spriteBound = sprite.CurrentSprite.GetBounds().size;
		
		if(sprite.CurrentSprite.name == "grid_place_holder")
			renderer.enabled = false;
		
		NotificationCenter.defaultCenter.addListener(OnGridClicked, NotificationType.GridClicked);
		NotificationCenter.defaultCenter.addListener(OnTowerClicked, NotificationType.TowerClicked);
		NotificationCenter.defaultCenter.addListener(OnTowerClicked, NotificationType.BlockerClick);
		NotificationCenter.defaultCenter.addListener(OnTutorialBuildTowerOn, NotificationType.TutorialBuildTowerOn);
		NotificationCenter.defaultCenter.addListener(OnTutorialBuildTowerOff, NotificationType.TutorialBuildTowerOff);
		NotificationCenter.defaultCenter.addListener(OnTikiClicked, NotificationType.TikiMaskClicked);
		resetScale = MyTransform.localScale;
	}
	
	// Update is called once per frame
	protected override void Update () 
	{
		base.Update();
	
		if(GameMaster.gameState == GameState.RUNNING)
		{
//			if(isClicked)
//			{
//				//scaleUp.x = Mathf.Sin(Time.time * 4) * 0.5f + 1.5f;
//				//scaleUp.y = Mathf.Sin(Time.time * 4) * 0.5f + 1.5f;
//                //MyTransform.localScale = scaleUp;
//			}
//			
//			if (shake_intensity > 0)
//	        {
//	            Vector2 shake = Random.insideUnitSphere * shake_intensity;
//                MyTransform.position = originPosition + (Vector3)shake;
//	            shake_intensity -= shake_decay;
//	        }
		}
	}
	
	public void Clicked()
	{
		if(!isClicked)
		{
			isClicked = true;
			//gridSpriteTag.SetSprite("fx_select_");
			gridSpriteTag.gameObject.SetActive(true);

			if(particleTutorial != null)
				particleTutorial.SetActive(false);
		}

	}
	
	public void Unclicked()
	{
		if(isClicked)
		{
			isClicked  = false;
			MyTransform.localScale = resetScale;

			if(GameMaster.gameState == GameState.RUNNING)
				gridSpriteTag.gameObject.SetActive(false);
		}
	}

	private void OnTikiClicked(Notification n)
	{
		this.Unclicked();
	}

	private void OnGameCoutnToStart(Notification n)
	{
		if(activeTower == null)
			if(isClickable && !haveBlockerOnStart)
				StartCoroutine("StartBlinkingSprite");

	}

	private void OnShowGridSelect(Notification n)
	{
		if(activeTower == null)
			if(!haveBlockerOnStart && isClickable)
				gridSpriteTag.gameObject.SetActive(true);
	}

	private void OnStateChanged(Notification n)
	{
		if(StateCheckEnable)
			if(GameMaster.gameState == GameState.RUNNING)
				gridSpriteTag.gameObject.SetActive(false);
	}

	private IEnumerator StartBlinkingSprite()
	{
		yield return new WaitForSeconds(2f);

		//gridSpriteTag.SetSprite("fx_selectstart_");
		gridSpriteTag.gameObject.SetActive(true);

		float maxAlpha = gridSpriteTag.color.a;
		Color color = new Color
		{
			r = gridSpriteTag.color.r,
			g = gridSpriteTag.color.g,
			b = gridSpriteTag.color.b,
			a = 0f
		};


		while (true)
		{

			color.a += Time.deltaTime;
			gridSpriteTag.color = color;

			if (color.a >= maxAlpha) break;

			yield return null;
		}

		yield return new WaitForSeconds(1f);

		while (true)
		{
			color.a -= Time.deltaTime;
			
			gridSpriteTag.color = color;
		 
			if (color.a <= 0) break;

			yield return null;
		}
	
		

		//gridSpriteTag.gameObject.SetActive(true);

		//yield return new WaitForSeconds(0.35f);

		//gridSpriteTag.gameObject.SetActive(false);

		//yield return new WaitForSeconds(0.35f);

		//gridSpriteTag.gameObject.SetActive(true);

		color.a = 1;
		gridSpriteTag.color = color;
		gridSpriteTag.gameObject.SetActive(false);

		StateCheckEnable = true;

		yield return null;
	}


	public void ResetTagColor()
	{
	    Color resetColor = gridSpriteTag.color;
	    resetColor.a = 1;
	    gridSpriteTag.color = resetColor;
	}

	private void OnTowerClicked(Notification n)
	{
		if (isClickable)
			Unclicked();
	}
	
	private void OnGridClicked(Notification n)
	{
		GridClickNotification grid = n as GridClickNotification;
		
		if(grid.cell == null)
		{
			Unclicked();
			return;
		}

		if(isClickable)
		{
			if(grid.cell != this || activeTower != null)
			{
				Unclicked();	
			}
			else
			{
				Clicked();	
			}
		}
		else if(grid.cell == this)
		{
			//	Shake();
			gridSpriteTag.SetSprite("fx_unable_");
			gridSpriteTag.gameObject.SetActive(true);
			StartCoroutine("TurnOffTag");
			NGUITools.PlaySound(clip);
		}
		
			
	}

	private IEnumerator TurnOffTag()
	{
		Color originalColor = gridSpriteTag.color;
		Color color = originalColor;

		while(gridSpriteTag.color.a > 0)
		{
			color.a -= Time.deltaTime;
			gridSpriteTag.color = color;
			yield return new WaitForEndOfFrame();
		}

		gridSpriteTag.gameObject.SetActive(false);
		gridSpriteTag.color= originalColor;
	}

	private void Shake()
	{
		originPosition = transform.position;
		originRotation = transform.rotation;
		shake_intensity = 0.5f;
		shake_decay = 0.1f;	
	}

	void OnDrawGizmos() {
		Gizmos.color = Color.green;
		Gizmos.DrawWireCube(MyTransform.position, new Vector3(2.05f, 2.05f, 0));
	}

	private void OnTutorialBuildTowerOn(Notification n)
	{
		if(!isTutorialGrid)
			collider2D.enabled = false;

	}

	private void OnEnemySpawn(Notification n)
	{
		if(!particleSpawn.isPlaying)
			particleSpawn.Play(true);
	}

	private void OnTutorialBuildTowerOff(Notification n )
	{
		collider2D.enabled = true;
	}
}
