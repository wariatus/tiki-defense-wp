﻿using UnityEngine;
using System;
using System.Collections;

public enum DateComparisonResult
{
	Earlier = -1,
	Later = 1,
	TheSame = 0
};


public class LiveManager : Singleton<LiveManager> {

	public int CurrentLiveCount
	{
		get { return this.currentLiveCount; }
	}

	private int currentLiveCount;
	
	float mRealTime = 0f;
	float mRealDelta = 0f;

    public float time
	{
		get
		{
			#if UNITY_EDITOR
			if (!Application.isPlaying) return Time.realtimeSinceStartup;
			#endif
			return mRealTime;
		}
	}

	public float avergeTime
	{
		get { return (GlobalConfiguration.LIVE_TIME_TO_REFRESH / GlobalConfiguration.LIVE_MAX) * 60f; }
	}
	
	/// <summary>
	/// Real delta time.
	/// </summary>
	
	public float deltaTime
	{
		get
		{
			#if UNITY_EDITOR
			if (!Application.isPlaying) return 0f;
			#endif
				return mRealDelta;
		}
	}

	public float TimeToReach
	{
		get { return this.timeToReach; }
	}

	private float timeToReach;

	public float CurrentTime
	{
		get { return this.currentTime; }
	}

	private float currentTime;

	public float timeToNextLife
	{
		get; private set;
	}

	public bool coroutineIsrunning{ get; private set; }

	// Use this for initialization
	void Start () {

		StartCor();
		GetCurrentTimeToReachFromSaveAndCalculate();
	}
	
	// Update is called once per frame
	void Update () {

		float rt = Time.realtimeSinceStartup;
		mRealDelta = Mathf.Clamp01(rt - mRealTime);
		mRealTime = rt;

		if(SaveData.newGameGM == 1 )
		{
			if(currentLiveCount == 0 && timeToReach <= 0)
			{
				timeToReach = GlobalConfiguration.LIVE_TIME_TO_REFRESH * 60;
			}
		}
	
	}

	public void InitializeLivesOnNewGame()
	{
		this.currentLiveCount = GlobalConfiguration.LIVE_MAX;
		NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.OnLiveChanged));
	}

	public void RestartLive()
	{
		timeToReach = 0;
		this.currentLiveCount = GlobalConfiguration.LIVE_MAX;
	}

	public bool CanBePlayed()
	{
		//if(this.currentLiveCount > 0)
		//{
		//	this.DecreaseLive();
	//		return true;
	//	}
	//	else 
	//		return false;

		return true;
	}

	public void DecreaseLive()
	{
		//if(this.currentLiveCount > 0)
			//this.currentLiveCount--;

		timeToReach += this.avergeTime;

		NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.OnLiveChanged));
	}

	public void IncreaseLive()
	{
		if(this.currentLiveCount < 5)
		{
			this.currentLiveCount++;
			NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.OnLiveChanged));
		}
		else
			StopCoroutine("StartTimer");
	}

	public float GetTimeToFullLive()
	{
		return this.timeToReach - this.currentTime;
	}

	public float GetTimeToNextLive()
	{
		return this.avergeTime - this.timeToNextLife;
	}

	private void StartCor()
	{
		if(!coroutineIsrunning)
		{
			coroutineIsrunning = true;
			StartCoroutine("StartTimer");
		}
	}

	private void StopCor()
	{
		if(coroutineIsrunning)
		{
			coroutineIsrunning = false;
			StopCoroutine("StartTimer");
		}
	}

	private IEnumerator StartTimer()
	{
		while(true)
		{

			if(timeToReach > 0)
			{

				//Debug.Log(currentTime + " " + timeToReach);

				currentTime += deltaTime;			
				timeToNextLife += deltaTime;

				if(timeToNextLife >= avergeTime)
				{
					this.IncreaseLive();
					timeToNextLife = 0;
				}
				
				if(currentTime >= this.timeToReach)
				{

					this.timeToReach = 0;
					this.currentTime = 0;
					this.IncreaseLive();
				}
			}

			yield return null;
		}
	}

	private void GetCurrentTimeToReachFromSaveAndCalculate()
	{
		if(SaveData.LiveDataTimePast.Equals("empty") || CurrentLiveCount == GlobalConfiguration.LIVE_MAX) return;

		timeToReach = 0;
		currentTime = 0;

		DateTime pastTime = new DateTime();
		//pastTime = DateTime.ParseExact(SaveData.LiveDataTimePast, "yyyy-MM-dd HH:mm tt", null);
		pastTime = Convert.ToDateTime(SaveData.LiveDataTimePast);
		DateTime presentTime = DateTime.Now;

		Debug.Log("Past: " + pastTime.ToString("yyyy-MM-dd HH:mm:ss tt") + " Present: " + presentTime.ToString("yyyy-MM-dd HH:mm:ss tt"));
		
  	    DateComparisonResult comparison;
		comparison = (DateComparisonResult) presentTime.CompareTo(pastTime);

		if(comparison == DateComparisonResult.Later || comparison == DateComparisonResult.TheSame)
		{
			currentLiveCount = GlobalConfiguration.LIVE_MAX;
			Debug.Log("wcześniej");
		}
		else
		{

			TimeSpan calculateTime = pastTime.Subtract(presentTime);
			Debug.Log("później");
			float secounds = (float)calculateTime.TotalSeconds;
			Debug.Log("S: " + calculateTime.TotalSeconds + " M " + calculateTime.TotalMinutes + " SS: " + calculateTime.Seconds);
			timeToReach = (float)calculateTime.TotalSeconds;
			Debug.Log(timeToReach);
			Debug.Log((timeToReach / 60f) / (GlobalConfiguration.LIVE_TIME_TO_REFRESH / GlobalConfiguration.LIVE_MAX));
			int live = GlobalConfiguration.LIVE_MAX -  Mathf.RoundToInt((timeToReach / 60f) / (GlobalConfiguration.LIVE_TIME_TO_REFRESH / GlobalConfiguration.LIVE_MAX));
			Debug.Log(live);
			currentLiveCount = 0;
			for(int i = 0; i < live; i++)
			{
				this.IncreaseLive();
			}

		}

	}

	private void SaveCurrentTimeToReach()
	{
		DateTime presentTime = DateTime.Now;
		presentTime = presentTime.AddSeconds(this.GetTimeToFullLive());
		Debug.Log(this.GetTimeToFullLive());
		SaveData.LiveDataTimePast = presentTime.ToString("yyyy-MM-dd HH:mm:ss tt");
		Debug.Log("SaveTime: " + presentTime.ToString("yyyy-MM-dd HH:mm:ss tt"));
		SaveData.Save();
	}

	void OnApplicationPause(bool pauseStatus)
	{
		if(pauseStatus)
		{
			StopCor();
			this.SaveCurrentTimeToReach();
		}
		else
		{
			StartCor();
			GetCurrentTimeToReachFromSaveAndCalculate();
		}
	}

	void OnApplicationQuit()
	{
		if(Application.loadedLevel >= 3)
			DecreaseLive();

		StopCor();
		this.SaveCurrentTimeToReach();
		
	
	}

	void OnLevelWasLoaded(int level)
	{

	}
		
}
	