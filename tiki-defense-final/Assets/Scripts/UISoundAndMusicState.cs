﻿using UnityEngine;
using System.Collections;

public class UISoundAndMusicState : ExtendedMonoBehavior {

	#region Variables
	
	public GameObject soundOn;
	public GameObject soundOff;
	public GameObject musicOn;
	public GameObject musicOff;
	
	#endregion

	#region Initialization

	protected override void Awake () 
	{
		base.Awake();
	}

	protected override void Start () 
	{
		base.Start();
	}

	#endregion
	
	// Update is called once per frame
	protected override void Update () {
	
		base.Update();
	}

	#region Public methods

	#endregion

	#region Protected methods

	#endregion
	
	#region Private methods

	#endregion
	
	#region Events & Notifications
	
	private void OnEnable()
	{
		if(GameMaster.canPlayFXSounds)
		{
			this.soundOff.SetActive(false);
			this.soundOn.SetActive(true);
		}
		else
		{
			this.soundOff.SetActive(true);
			this.soundOn.SetActive(false);
		}
		
		if(GameMaster.canPlayMusic)
		{
			this.musicOff.SetActive(false);
			this.musicOn.SetActive(true);
		}
		else
		{
			this.musicOff.SetActive(true);
			this.musicOn.SetActive(false);
		}
	}
	
	

	#endregion



}
