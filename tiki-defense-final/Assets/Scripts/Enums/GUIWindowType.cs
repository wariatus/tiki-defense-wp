﻿public enum GUIWindowType
{
	TIKIMASKUNLOCK,				// Okno odblokowujace tiki maski
	TIKIMASKCHARGEADD,			// Dodawanie chargy
	TUTORIALBUILDTOWER,			// Budowanie turreta
	MENUGAME,					// Menu glowne w samej grze
	TUTORIALTIKIMASK,			// Uzywamy tiki
	NEWEAPONG,
	ENCYCLOPEDY,				// Encyklopedia
	OBSTACLES,
	ARMORED

}