﻿public enum TowerState
{
	Recharge,
	Attacking,
	ReadyToFire,
	SeekTarget,
	Rotate,
}