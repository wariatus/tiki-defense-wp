﻿public enum ParticleEffectType
{
	Effect_UpgradeTower,
	Effect_Portal_Rainbow,
	Effect_Enemy_Die,
    Effect_BuyTower,
}