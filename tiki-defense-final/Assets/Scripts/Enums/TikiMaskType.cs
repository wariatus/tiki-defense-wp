﻿public enum TikiMaskType
{
	FIRE,
	EARTH,
	FROST,
	WIND,
	NONE
}