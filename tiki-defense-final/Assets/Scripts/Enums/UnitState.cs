﻿public enum UnitState
{
    Stopped,
    Attacking,
    Moving,
    Slowed,
    Stunned,
    Dying,
	Jumping, 	// Obiekt przeskakuje sciezki
	InPool,
}