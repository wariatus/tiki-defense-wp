﻿using UnityEngine;
using System.Collections;


public static class LevelConfigurations  {
	

	public static TIKIMaskConfiguration GetTikiMaskConfiguration(TikiMaskType type)
	{
		switch(type)
		{
			case TikiMaskType.FIRE:

				return new TIKIMaskConfiguration
				{
					damageBonus = 150 + (SaveData.tikiTotemLeft_ID4 == 1 ? (int)GlobalConfiguration.TIKI_TOTEM_RIGHT_ID_4_BONUS : 0) , // O ile procent zwiekszamy obrazenia
					ttl = 7f, // Ile czasu ma trwac efekt
					price = 1, // Ile ladunkow kosztuje uzycie maski

				};

			case TikiMaskType.FROST:

				return new TIKIMaskConfiguration
				{
					slowAmount = 65, // O ile zmniejszamy ruch
					timeEffect = 5f  + (SaveData.tikiTotemLeft_ID3 == 1 ? GlobalConfiguration.TIKI_TOTEM_LEFT_ID_3_BONUS : 0), // Ile czasu trwa efekt (TO NIE TTL)
					price = 1,// Ile ladunkow kosztuje uzycie maski
					ttl = 4f,
				};

			case TikiMaskType.EARTH:
				
				return new TIKIMaskConfiguration
				{
					slowAmount = 100, // O ile zmniejszamy ruch
					timeEffect = 2f + (SaveData.tikiTotemLeft_ID2 == 1 ? GlobalConfiguration.TIKI_TOTEM_LEFT_ID_2_BONUS : 0), // Ile czasu trwa efekt (TO NIE TTL)
					price = 1, // Ile ladunkow kosztuje uzycie maski
					ttl = 4f,
				};

			case TikiMaskType.WIND:
				
				return new TIKIMaskConfiguration
				{
					rechargeBonus = 50 + (SaveData.tikiTotemRight_ID2 == 1 ? Mathf.RoundToInt(((GlobalConfiguration.TIKI_TOTEM_RIGHT_ID_2_BONUS / 100) * 50)) : 0), // O ile przyspieszamy recharge
					ttl = 7f, // Ile czasu ma trwac efekt
					price = 1,	// Ile ladunkow kosztuje uzycie maski
				};
			
			default:
				return new TIKIMaskConfiguration();


		}
	}

	public static Configuration GetConfiguration(string levelName)
	{
	
		switch(levelName)
		{
			case "Level0x02":
				
				return new Configuration
				{
					startMoney = 600,
					maxWaves = 25,
					towerMagicShot = true,
					towerFireTotem = true,
					towerIceTotem = true,
					towerDrum = true,
					newTypeOfWeapon= PoolObjectType.Null,
				};
			#region Odd levels
			case "Level1x00":
				
				return new Configuration 
				{
					startMoney = 3000,
					maxWaves = 10,
					//towerMagicShot = true,
					towerTotem = true,
					towerVooDoo = true,
					//towerIceTotem = true,
					//towerRockHammer= true,
					//towerCampfire = true,
					
					towerFireTotem = true,

					newTypeOfWeapon= PoolObjectType.Null,
				};

			case "Levelx01-Lukasz":
			
				return new Configuration 
				{
					startMoney = 3000,
					maxWaves = 10,
					//towerMagicShot = true,
					towerAnvil = true,
					//towerNecklace = true,
					//towerTotem = true,
					//towerCauldron = true,
					towerVooDoo = true,
					//towerCave = true,
					//towerIceTotem = true,
					//towerRockHammer= true,
					//towerCampfire = true,
					towerFireTotem = true,
				    //towerMelter = true,
					//fireTiki = true,
				  	towerTornado = true,
					towerMelter = true,
					fireTiki = true,
					frostTiki = true,
					earthTiki = true,
					windTiki = true,
					newTypeOfWeapon= PoolObjectType.Null,
				};

			case "Level0x01":
			
			return new Configuration
			{
				startMoney = 375,
				maxWaves = 20,
				towerVooDoo = true,
				towerTotem = true,
				towerFireTotem = true,
				towerTornado = true,
				
				windTiki = true,
				HPMultiplier = 1,
				
				newTypeOfWeapon= PoolObjectType.EarthTikiMask,
			};
			
			
			#endregion
			#region Setting 1
			case "Level1x01":
			
				return new Configuration 
				{
					startMoney = 450,
					maxWaves = 7,
					towerTotem = true,
					towerVooDoo = true,

				HPMultiplier = 1,

				newTypeOfWeapon= PoolObjectType.Tower_VooDoo_level_1,

				};
			
			case "Level1x02":
			
				return new Configuration
				{
					startMoney = 375,
					maxWaves = 10,
					towerVooDoo = true,
					towerTornado = true,
				HPMultiplier = 1,
				newTypeOfWeapon= PoolObjectType.Totem_AirTotem_level_1,
				};

			case "Level1x03-tutorial-tiki":

				return new Configuration 
				{
					startMoney = 0,
					maxWaves = 4,
					
					fireTiki = true,
				HPMultiplier = 1,
					newTypeOfWeapon= PoolObjectType.Null,
				};

			case "Level1x03":
			
				return new Configuration
				{
					startMoney = 375,
					maxWaves = 10,
					towerVooDoo = true,
					towerFireTotem = true,

					fireTiki = true,
				HPMultiplier = 1,
				newTypeOfWeapon= PoolObjectType.Totem_FireTotem_level_1,
                //newTypeOfWeapon = PoolObjectType.Null
				};
			case "Level1x04":
			
				return new Configuration
				{
					startMoney = 375,
					maxWaves = 10,
					towerVooDoo = true,
					towerTornado = true,
					towerTotem = true,
					//towerDrum = true,
					//towerMagicShot = true,
				HPMultiplier = 1,
				fireTiki = true,
			
					newTypeOfWeapon= PoolObjectType.Null,
				};
			case "Level1x07":
				
				return new Configuration
				{
					startMoney = 375,
					maxWaves = 15,
					towerVooDoo = true,
					towerTotem = true,
					towerFireTotem = true,
					
					earthTiki = true,
				HPMultiplier = 1,
					newTypeOfWeapon= PoolObjectType.EarthTikiMask,
				};

			case "Level1x05":
			
				return new Configuration
				{
				startMoney = 375,
					maxWaves = 15,
					towerVooDoo = true,
					towerTornado = true,
					towerTotem = true,

					windTiki = true,
				HPMultiplier = 1,
					newTypeOfWeapon= PoolObjectType.AirTikiMask,
				};
			case "Level1x06":
				
				return new Configuration
				{
					startMoney = 375,
					maxWaves = 15,
					towerTornado = true,
					towerTotem = true,
					towerFireTotem = true,
					
					frostTiki = true,
				HPMultiplier = 1,
					newTypeOfWeapon= PoolObjectType.IceTikiMask,
					
				};

			
			
		case "Level1x08-bonus":
			
				return new Configuration
				{
					startMoney = 375,
					maxWaves = 20,
					towerVooDoo = true,
					towerTotem = true,
					towerTornado = true,

					fireTiki = true,
				HPMultiplier = 1,
					newTypeOfWeapon= PoolObjectType.Null,
					
				};
		case "Level1x09-bonus":
			
				return new Configuration
				{
				startMoney = 425	,
					maxWaves = 25,
					towerVooDoo = true,
					towerTornado = true,
					towerTotem = true,
					towerFireTotem = true,

					earthTiki = true,
				HPMultiplier = 1,
					newTypeOfWeapon= PoolObjectType.Null,
				};
			#endregion
			#region Setting 2
		case "Level2x02":
			
				return new Configuration
				{
					startMoney = 500,
					maxWaves = 15,
					towerTotem = true,
					towerVooDoo = true,
					towerMagicShot = true,

					fireTiki = true,
				HPMultiplier = 1,
					newTypeOfWeapon= PoolObjectType.Totem_MagicShot_level_1,
				};
			case "Level2x01":
			
				return new Configuration
				{
					startMoney = 450,
					maxWaves = 15,
					//towerVooDoo = true,
					towerTornado = true,
					towerIceTotem = true,
					towerFireTotem = true,

					windTiki = true,
				HPMultiplier = 1,
					newTypeOfWeapon= PoolObjectType.Totem_IceTotem_level_1,	
				};
			case "Level2x03":
			
				return new Configuration
				{
					startMoney = 800	,
					maxWaves = 15,
					towerIceTotem = true,
					towerMagicShot = true,
					towerFireTotem = true,
				HPMultiplier = 1,
					earthTiki = true,
					newTypeOfWeapon= PoolObjectType.Null,
			};
			case "Level2x06":
			
				return new Configuration
				{

					startMoney = 300,
					maxWaves = 20,
					towerIceTotem = true,
					towerDrum = true,
					towerTotem = true,
					towerTornado = true,
				HPMultiplier = 1,
					windTiki = true,
					newTypeOfWeapon= PoolObjectType.Null,

				};
		case "Level2x07-bonus":
				
				return new Configuration
				{
					startMoney = 300,
					maxWaves = 15,
					towerFireTotem = true,
					towerDrum = true,
					towerIceTotem = true,
				
				HPMultiplier = 1,
					fireTiki = true,
					newTypeOfWeapon= PoolObjectType.Null,
				};
			case "Level2x04":
				
				return new Configuration
				{
					startMoney = 400,
					maxWaves = 15,

					towerVooDoo = true,
					towerMagicShot = true,
					towerDrum = true, 

					earthTiki = true,
				HPMultiplier = 1,
					newTypeOfWeapon= PoolObjectType.Totem_Drum_level_1,	
				};
		case "Level2x08-bonus":
				
				return new Configuration
				{
					startMoney = 400,
					maxWaves = 15,

					towerFireTotem = true,
					towerDrum = true,
					towerMagicShot = true,
					towerVooDoo = true,
				HPMultiplier = 1,
					windTiki = true,
					newTypeOfWeapon= PoolObjectType.Null,

				};
			case "Level2x05":
				
				return new Configuration
				{
					startMoney = 600,
					maxWaves = 15,
					towerIceTotem = true,
					towerMagicShot = true,
					towerTotem = true,
					towerDrum = true,

					fireTiki = true,
				HPMultiplier = 1,
					newTypeOfWeapon= PoolObjectType.Null,
				};
		case "Level2x09-bonus":
				
				return new Configuration
				{
					startMoney = 300,
					maxWaves = 25,
					towerIceTotem = true,
					towerDrum = true,
					towerFireTotem = true,
					towerTornado = true,	

					frostTiki = true,
				HPMultiplier = 1,
					newTypeOfWeapon= PoolObjectType.Null,
				};

			#endregion
			#region Setting 3
		case "Level3x01":
			
			return new Configuration
			{
				startMoney = 600,
				maxWaves = 15,
				towerTotem = true,
				towerVooDoo = true,
				towerMelter = true,
				towerDrum = true,
				//towerRockPiercer = true,
				fireTiki = true,
				HPMultiplier = 1,
				newTypeOfWeapon= PoolObjectType.Totem_Melter_level_1,
			};

		case "Level3x02":
			return new Configuration
			{
				startMoney = 600,
				maxWaves = 15,
				towerVooDoo = true,
				towerRockPiercer = true,
				towerIceTotem = true,
				towerTotem = true,
				towerCave = true,
				HPMultiplier = 1,
				windTiki = true,
				newTypeOfWeapon= PoolObjectType.Tower_Cave_level_1,
			};
		case "Level3x03":
			return new Configuration
			{
				startMoney = 500,
				maxWaves = 15,
				towerVooDoo = true,
				towerTornado = true,
				towerIceTotem = true,
				towerMelter = true,
				HPMultiplier = 1,
				earthTiki = true,
				newTypeOfWeapon= PoolObjectType.Null,
			};
		case "Level3x04":
				return new Configuration
				{
					startMoney = 500,
					maxWaves = 15,
					towerVooDoo = true,
					towerCave = true,
					towerDrum = true,
					towerRockHammer = true,
				HPMultiplier = 1,
					windTiki  = true,	
					newTypeOfWeapon= PoolObjectType.Totem_RockHammer_level_1,
				};

		case "Level3x05":
			return new Configuration
			{
				startMoney = 500,
				maxWaves = 15,
				towerMelter = true,
				towerIceTotem = true,
				towerRockHammer = true,
				towerFireTotem = true,
				towerCave = true,
				HPMultiplier = 1,
				fireTiki = true,	
				newTypeOfWeapon= PoolObjectType.Null,
			};
		case "Level3x06":
			return new Configuration
			{
				startMoney = 500,
				maxWaves = 15,
				towerVooDoo = true,
				towerMelter = true,
				towerFireTotem = true,
				towerCave = true,
				HPMultiplier = 1,
				frostTiki = true,
				newTypeOfWeapon= PoolObjectType.Null,
			};
		case "Level3x07-bonus":
			return new Configuration
			{
				startMoney = 500,
				maxWaves = 15,

				towerMelter = true,
				towerRockHammer = true,
				towerMagicShot = true,
				//towerTotem = true,
				towerCave = true,
				HPMultiplier = 1,
				earthTiki = true,	
				newTypeOfWeapon= PoolObjectType.Null,
			};
		case "Level3x08-bonus":
			return new Configuration
			{
				startMoney = 500,
				maxWaves = 15,
				towerCave = true,
				towerMelter = true,
				towerDrum = true,
				towerRockHammer = true,
				HPMultiplier = 1,
				
				fireTiki = true,	
				newTypeOfWeapon= PoolObjectType.Null,
			};
		case "Level3x09-bonus":
			return new Configuration
			{
				startMoney = 500,
				maxWaves = 20,
				towerCave = true,
				towerMelter = true,
				towerIceTotem = true,
				towerRockHammer = true,
				HPMultiplier = 1,
				windTiki = true,	
				newTypeOfWeapon= PoolObjectType.Null,
			};
			#endregion
			#region Setting 4
		case "Level4x01":
			return new Configuration
			{
				startMoney = 600,
				maxWaves = 15,
				towerCave = true,
				towerDrum = true,
				towerNecklace = true,
				HPMultiplier = 1,
				frostTiki = true,	
				newTypeOfWeapon= PoolObjectType.Totem_Necklace_level_1,
			};
		case "Level4x02":
			return new Configuration
			{
				startMoney = 600,
				maxWaves = 15,
				towerTotem = true,
				towerMagicShot = true,
				towerIceTotem = true,
				towerAnvil = true,
				HPMultiplier = 1,
				frostTiki = true,	
				newTypeOfWeapon= PoolObjectType.Totem_Anvil_level_1,
			};
		case "Level4x03":
			return new Configuration
			{
				startMoney = 600,
				maxWaves = 15,
				towerIceTotem = true,
				towerMelter = true,
				towerTotem = true,
				towerCauldron = true,
				HPMultiplier = 1,
				earthTiki = true,	
				newTypeOfWeapon= PoolObjectType.Totem_Cauldron_level_1,
			};
		case "Level4x04":
			return new Configuration
			{
				startMoney = 600,
				maxWaves = 15,
				towerVooDoo = true,
				towerCauldron = true,
				towerDrum = true,
				//towerRockHammer = true,
				towerMagicShot = true,
				HPMultiplier = 1,
				windTiki = true,	
				newTypeOfWeapon= PoolObjectType.Null,
			};
		case "Level4x05":
			return new Configuration
			{
				startMoney = 600,
				maxWaves = 15,
				towerCauldron = true,
				towerCave = true,
				towerRockHammer = true,
				towerNecklace = true,
				HPMultiplier = 1,
				frostTiki = true,	
				newTypeOfWeapon= PoolObjectType.Null,
			};
		case "Level4x06":
			return new Configuration
			{
				startMoney = 500,
				maxWaves = 15,
				towerVooDoo = true,
				towerMelter = true,
				towerDrum = true,
				towerAnvil = true,
				HPMultiplier = 1,
				earthTiki = true,	
				newTypeOfWeapon= PoolObjectType.Null,
			};
		case "Level4x07-bonus":
			return new Configuration
			{
				startMoney = 500,
				maxWaves = 15,
				towerFireTotem = true,
				towerNecklace = true,
				towerCauldron = true,
				towerRockHammer = true,
				HPMultiplier = 1,
				windTiki = true,	
				newTypeOfWeapon= PoolObjectType.Null,
			};
		case "Level4x08-bonus":
			return new Configuration
			{
				startMoney = 600,
				maxWaves = 20,
				towerCave = true,
				//towerFireTotem = true,
				towerAnvil = true,
				towerIceTotem = true,
				towerTornado = true,
				HPMultiplier = 1,
				fireTiki = true,	
				newTypeOfWeapon= PoolObjectType.Null,
			};
		case "Level4x09-bonus":
			return new Configuration
			{
				startMoney = 500,
				maxWaves = 15,
				towerVooDoo = true,
				towerMelter = true,
				towerDrum = true,
				towerRockHammer = true,
				towerCauldron = true,
				HPMultiplier = 1,
				frostTiki = true,	
				newTypeOfWeapon= PoolObjectType.Null,
			};
			#endregion
			#region Setting 5
		case "Level5x01":
			return new Configuration
			{
				startMoney = 500,
				maxWaves = 15,
				towerRunicStone = true,
				towerCauldron = true,
				towerDrum = true,
				towerMagicShot = true,
				towerCave = true,
				HPMultiplier = 1,
				windTiki = true,	
				newTypeOfWeapon= PoolObjectType.Tower_Runic_level_1,
			};
		case "Level5x02":
			return new Configuration
			{
				startMoney = 500,
				maxWaves = 15,
				towerVooDoo = true,
				towerMelter = true,
				towerTotem = true,
				towerAnvil = true,
				HPMultiplier = 1,
				frostTiki = true,	
				newTypeOfWeapon= PoolObjectType.Null,
			};
		case "Level5x03":
			return new Configuration
			{
				startMoney = 500,
				maxWaves = 15,
				towerNecklace = true,
				towerTotem = true,
				towerIceTotem = true,
				towerRockHammer = true,
				towerMelter = true,
				HPMultiplier = 1,
				earthTiki = true,	
				newTypeOfWeapon= PoolObjectType.Null,
			};
		case "Level5x04":
			return new Configuration
			{
				startMoney = 600,
				maxWaves = 15,
				towerVooDoo = true,
				towerMelter = true,
				towerDrum = true,
				towerRockHammer = true,
				HPMultiplier = 1,
				frostTiki = true,	
				newTypeOfWeapon= PoolObjectType.Null,
			};
		case "Level5x05":
			return new Configuration
			{
				startMoney = 500,
				maxWaves = 15,
				towerRunicStone = true,
				towerMelter = true,
				towerDrum = true,
				towerTornado = true,
				HPMultiplier = 1,
				windTiki = true,	
				newTypeOfWeapon= PoolObjectType.Null,
			};
		case "Level5x06":
			return new Configuration
			{
				startMoney = 500,
				maxWaves = 15,
				towerRunicStone = true,
				towerAnvil = true,
				towerNecklace = true,
				towerCave = true,
				HPMultiplier = 1,
				frostTiki = true,	
				newTypeOfWeapon= PoolObjectType.Null,
			};
		case "Level5x07":
			return new Configuration
			{
				startMoney = 600,
				maxWaves = 15,
				towerCauldron = true,
				towerMelter = true,
				towerRunicStone = true,
				towerVooDoo = true,
				HPMultiplier = 1,
				earthTiki = true,	
				newTypeOfWeapon= PoolObjectType.Null,
			};
		case "Level5x08":
			return new Configuration
			{
				startMoney = 500,
				maxWaves = 20,
				towerRunicStone = true,
				towerNecklace = true,
				towerDrum = true,
				towerCave = true,
				HPMultiplier = 1,
				fireTiki = true,	
				newTypeOfWeapon= PoolObjectType.Null,
			};
		case "Level5x09":
			return new Configuration
			{
				startMoney = 500,
				maxWaves = 25,
				towerVooDoo = true,
				towerMelter = true,
				towerDrum = true,
				towerRockHammer = true,
				HPMultiplier = 1,
				frostTiki = true,	
				newTypeOfWeapon= PoolObjectType.Null,
			};
			#endregion
			
		default:
			return new Configuration();
				
		}
			
	}

	public static TowersPrice GetTowerPrice(TowersType type)
	{
		switch(type)	
		{
			#region Tower: Totem
		case TowersType.Totem_level_1:
			return new TowersPrice { buyPrice = 100, sellPrice = Mathf.RoundToInt((SaveData.tikiTotemRight_ID1 ==  1 ? GlobalConfiguration.TIKI_TOTEM_RIGHT_ID_1_BONUS / 100 : 75f / 100) * 100), upgradePrice = 120 };
			
		case TowersType.Totem_level_2:
			return new TowersPrice { buyPrice = 0, sellPrice = Mathf.RoundToInt((SaveData.tikiTotemRight_ID1 ==  1 ? GlobalConfiguration.TIKI_TOTEM_RIGHT_ID_1_BONUS  / 100 : 75f / 100) * 220), upgradePrice = 160 };
			
		case TowersType.Totem_level_3:
			return new TowersPrice { buyPrice = 0, sellPrice = Mathf.RoundToInt((SaveData.tikiTotemRight_ID1 ==  1 ? GlobalConfiguration.TIKI_TOTEM_RIGHT_ID_1_BONUS / 100 : 75f / 100) * 380), upgradePrice = 0 };
			
			#endregion
			#region Tower: VooDoo
		case TowersType.VooDoo_level_1:
			return new TowersPrice { buyPrice = 90, sellPrice = Mathf.RoundToInt((SaveData.tikiTotemRight_ID1 ==  1 ? GlobalConfiguration.TIKI_TOTEM_RIGHT_ID_1_BONUS / 100 : 75f / 100) * 90), upgradePrice = 100 };
			
		case TowersType.VooDoo_level_2:
			return new TowersPrice { buyPrice = 0, sellPrice = Mathf.RoundToInt((SaveData.tikiTotemRight_ID1 ==  1 ? GlobalConfiguration.TIKI_TOTEM_RIGHT_ID_1_BONUS / 100 : 75f / 100) * 190), upgradePrice = 120 };
			
		case TowersType.VooDoo_level_3:
			return new TowersPrice { buyPrice = 0, sellPrice = Mathf.RoundToInt((SaveData.tikiTotemRight_ID1 ==  1 ? GlobalConfiguration.TIKI_TOTEM_RIGHT_ID_1_BONUS / 100 : 75f / 100) * 310), upgradePrice = 0 };
			
			#endregion
			#region Tower: Tornado
		case TowersType.Tornado_level_1:
			return new TowersPrice { buyPrice = 120, sellPrice = Mathf.RoundToInt((SaveData.tikiTotemRight_ID1 ==  1 ? GlobalConfiguration.TIKI_TOTEM_RIGHT_ID_1_BONUS / 100 : 75f / 100) * 120), upgradePrice = 160 };
			
		case TowersType.Tornado_level_2:
			return new TowersPrice { buyPrice = 0, sellPrice = Mathf.RoundToInt((SaveData.tikiTotemRight_ID1 ==  1 ? GlobalConfiguration.TIKI_TOTEM_RIGHT_ID_1_BONUS / 100 : 75f / 100) * 280), upgradePrice = 200  };
			
		case TowersType.Tornado_level_3:
			return new TowersPrice { buyPrice = 0, sellPrice = Mathf.RoundToInt((SaveData.tikiTotemRight_ID1 ==  1 ? GlobalConfiguration.TIKI_TOTEM_RIGHT_ID_1_BONUS / 100 : 75f / 100) * 480), upgradePrice = 0 };
			
			#endregion
			#region Tower: FireTotem
		case TowersType.FireTotem_level_1:
			return new TowersPrice { buyPrice = 130, sellPrice = Mathf.RoundToInt((SaveData.tikiTotemRight_ID1 ==  1 ? GlobalConfiguration.TIKI_TOTEM_RIGHT_ID_1_BONUS / 100: 75f / 100) * 130), upgradePrice = 170 };
			
		case TowersType.FireTotem_level_2:
			return new TowersPrice { buyPrice = 0, sellPrice = Mathf.RoundToInt((SaveData.tikiTotemRight_ID1 ==  1 ? GlobalConfiguration.TIKI_TOTEM_RIGHT_ID_1_BONUS / 100 : 75f / 100) * 300), upgradePrice = 210 };
			
		case TowersType.FireTotem_level_3:
			return new TowersPrice { buyPrice = 0, sellPrice = Mathf.RoundToInt((SaveData.tikiTotemRight_ID1 ==  1 ? GlobalConfiguration.TIKI_TOTEM_RIGHT_ID_1_BONUS / 100 : 75f / 100) * 410), upgradePrice = 0 };
			
			#endregion
			#region Tower: MagicShot
		case TowersType.MagicShot_level_1:
			return new TowersPrice { buyPrice = 150, sellPrice = Mathf.RoundToInt((SaveData.tikiTotemRight_ID1 ==  1 ? GlobalConfiguration.TIKI_TOTEM_RIGHT_ID_1_BONUS / 100 : 75f / 100) * 150), upgradePrice = 170 };
			
		case TowersType.MagicShot_level_2:
			return new TowersPrice { buyPrice = 0, sellPrice = Mathf.RoundToInt((SaveData.tikiTotemRight_ID1 ==  1 ? GlobalConfiguration.TIKI_TOTEM_RIGHT_ID_1_BONUS / 100: 75f / 100) * 320), upgradePrice = 210 };
			
		case TowersType.MagicShot_level_3:
			return new TowersPrice { buyPrice = 0, sellPrice = Mathf.RoundToInt((SaveData.tikiTotemRight_ID1 ==  1 ? GlobalConfiguration.TIKI_TOTEM_RIGHT_ID_1_BONUS / 100: 75f / 100) * 430), upgradePrice = 0 };
			
			#endregion
			#region Tower: IceTotem
		case TowersType.IceTotem_level_1:
			return new TowersPrice { buyPrice = 130, sellPrice = Mathf.RoundToInt((SaveData.tikiTotemRight_ID1 ==  1 ? GlobalConfiguration.TIKI_TOTEM_RIGHT_ID_1_BONUS / 100: 75f / 100) * 130), upgradePrice = 170 };
			
		case TowersType.IceTotem_level_2:
			return new TowersPrice { buyPrice = 0, sellPrice = Mathf.RoundToInt((SaveData.tikiTotemRight_ID1 ==  1 ? GlobalConfiguration.TIKI_TOTEM_RIGHT_ID_1_BONUS / 100: 75f / 100) * 300), upgradePrice = 200 };
			
		case TowersType.IceTotem_level_3:
			return new TowersPrice { buyPrice = 0, sellPrice = Mathf.RoundToInt((SaveData.tikiTotemRight_ID1 ==  1 ? GlobalConfiguration.TIKI_TOTEM_RIGHT_ID_1_BONUS / 100: 75f / 100) * 500), upgradePrice = 0 };
			
			#endregion
			#region Tower: Drum
		case TowersType.Drum_level_1:
			return new TowersPrice { buyPrice = 140, sellPrice = Mathf.RoundToInt((SaveData.tikiTotemRight_ID1 ==  1 ? GlobalConfiguration.TIKI_TOTEM_RIGHT_ID_1_BONUS / 100: 75f / 100) * 140), upgradePrice = 180 };
			
		case TowersType.Drum_level_2:
			return new TowersPrice { buyPrice = 0, sellPrice = Mathf.RoundToInt((SaveData.tikiTotemRight_ID1 ==  1 ? GlobalConfiguration.TIKI_TOTEM_RIGHT_ID_1_BONUS / 100: 75f / 100) * 320), upgradePrice = 220 };

			
		case TowersType.Drum_level_3:

			return new TowersPrice { buyPrice = 0, sellPrice = Mathf.RoundToInt((SaveData.tikiTotemRight_ID1 ==  1 ? GlobalConfiguration.TIKI_TOTEM_RIGHT_ID_1_BONUS / 100: 75f / 100) * 540), upgradePrice = 0 };

			
			#endregion
			#region Tower: Melter
		case TowersType.Melter_level_1:
			return new TowersPrice { buyPrice = 120, sellPrice = Mathf.RoundToInt((SaveData.tikiTotemRight_ID1 ==  1 ? GlobalConfiguration.TIKI_TOTEM_RIGHT_ID_1_BONUS / 100: 75f / 100) * 100), upgradePrice = 140 };
			
		case TowersType.Melter_level_2:
			return new TowersPrice { buyPrice = 0, sellPrice = Mathf.RoundToInt((SaveData.tikiTotemRight_ID1 ==  1 ? GlobalConfiguration.TIKI_TOTEM_RIGHT_ID_1_BONUS / 100: 75f / 100) * 220), upgradePrice = 180 };
			
		case TowersType.Melter_level_3:
			return new TowersPrice { buyPrice = 0, sellPrice = Mathf.RoundToInt((SaveData.tikiTotemRight_ID1 ==  1 ? GlobalConfiguration.TIKI_TOTEM_RIGHT_ID_1_BONUS / 100: 75f / 100) * 360), upgradePrice = 0 };
			
			#endregion

			#region Tower: RockHammer
		case TowersType.RockHammer_level_1:
			return new TowersPrice { buyPrice = 160, sellPrice = Mathf.RoundToInt((SaveData.tikiTotemRight_ID1 ==  1 ? GlobalConfiguration.TIKI_TOTEM_RIGHT_ID_1_BONUS / 100: 75f / 100) * 160), upgradePrice = 180 };
			
		case TowersType.RockHammer_level_2:
			return new TowersPrice { buyPrice = 0, sellPrice = Mathf.RoundToInt((SaveData.tikiTotemRight_ID1 ==  1 ? GlobalConfiguration.TIKI_TOTEM_RIGHT_ID_1_BONUS / 100: 75f / 100) * 340), upgradePrice = 220 };
			
		case TowersType.RockHammer_level_3:
			return new TowersPrice { buyPrice = 0, sellPrice = Mathf.RoundToInt((SaveData.tikiTotemRight_ID1 ==  1 ? GlobalConfiguration.TIKI_TOTEM_RIGHT_ID_1_BONUS / 100: 75f / 100) * 560), upgradePrice = 0 };
			
			#endregion
			#region Tower: Cave
		case TowersType.Cave_level_1:
			return new TowersPrice { buyPrice = 180, sellPrice = Mathf.RoundToInt((SaveData.tikiTotemRight_ID1 ==  1 ? GlobalConfiguration.TIKI_TOTEM_RIGHT_ID_1_BONUS / 100: 75f / 100) * 180), upgradePrice = 220 };
			
		case TowersType.Cave_level_2:
			return new TowersPrice { buyPrice = 0, sellPrice = Mathf.RoundToInt((SaveData.tikiTotemRight_ID1 ==  1 ? GlobalConfiguration.TIKI_TOTEM_RIGHT_ID_1_BONUS / 100: 75f / 100) * 400), upgradePrice = 240 };
			
		case TowersType.Cave_level_3:
			return new TowersPrice { buyPrice = 0, sellPrice = Mathf.RoundToInt((SaveData.tikiTotemRight_ID1 ==  1 ? GlobalConfiguration.TIKI_TOTEM_RIGHT_ID_1_BONUS / 100: 75f / 100) * 640), upgradePrice = 0 };	
			#endregion
			#region Tower: Cauldron
		case TowersType.Cauldron_level_1:
			return new TowersPrice { buyPrice = 180, sellPrice = Mathf.RoundToInt((SaveData.tikiTotemRight_ID1 ==  1 ? GlobalConfiguration.TIKI_TOTEM_RIGHT_ID_1_BONUS / 100: 75f / 100) * 180), upgradePrice = 220 };
			
		case TowersType.Cauldron_level_2:
			return new TowersPrice { buyPrice = 0, sellPrice = Mathf.RoundToInt((SaveData.tikiTotemRight_ID1 ==  1 ? GlobalConfiguration.TIKI_TOTEM_RIGHT_ID_1_BONUS / 100: 75f / 100) * 400), upgradePrice = 240 };
			
		case TowersType.Cauldron_level_3:
			return new TowersPrice { buyPrice = 0, sellPrice = Mathf.RoundToInt((SaveData.tikiTotemRight_ID1 ==  1 ? GlobalConfiguration.TIKI_TOTEM_RIGHT_ID_1_BONUS / 100: 75f / 100) * 640), upgradePrice = 0 };	
			#endregion
			#region Tower: Necklace
		case TowersType.Necklace_level_1:
			return new TowersPrice { buyPrice = 180, sellPrice = Mathf.RoundToInt((SaveData.tikiTotemRight_ID1 ==  1 ? GlobalConfiguration.TIKI_TOTEM_RIGHT_ID_1_BONUS / 100: 75f / 100) * 180), upgradePrice = 220 };
			
		case TowersType.Necklace_level_2:
			return new TowersPrice { buyPrice = 0, sellPrice = Mathf.RoundToInt((SaveData.tikiTotemRight_ID1 ==  1 ? GlobalConfiguration.TIKI_TOTEM_RIGHT_ID_1_BONUS / 100: 75f / 100) * 400), upgradePrice = 300 };
			
		case TowersType.Necklace_level_3:
			return new TowersPrice { buyPrice = 0, sellPrice = Mathf.RoundToInt((SaveData.tikiTotemRight_ID1 ==  1 ? GlobalConfiguration.TIKI_TOTEM_RIGHT_ID_1_BONUS / 100: 75f / 100) * 640), upgradePrice = 0 };	
			#endregion
			#region Tower: Anvil
		case TowersType.Anvil_level_1:
			return new TowersPrice { buyPrice = 180, sellPrice = Mathf.RoundToInt((SaveData.tikiTotemRight_ID1 ==  1 ? GlobalConfiguration.TIKI_TOTEM_RIGHT_ID_1_BONUS / 100: 75f / 100) * 180), upgradePrice = 200 };
			
		case TowersType.Anvil_level_2:
			return new TowersPrice { buyPrice = 0, sellPrice = Mathf.RoundToInt((SaveData.tikiTotemRight_ID1 ==  1 ? GlobalConfiguration.TIKI_TOTEM_RIGHT_ID_1_BONUS / 100: 75f / 100) * 400), upgradePrice = 240 };
			
		case TowersType.Anvil_level_3:
			return new TowersPrice { buyPrice = 0, sellPrice = Mathf.RoundToInt((SaveData.tikiTotemRight_ID1 ==  1 ? GlobalConfiguration.TIKI_TOTEM_RIGHT_ID_1_BONUS / 100: 75f / 100) * 640), upgradePrice = 0 };	
			#endregion
			#region Tower: RuneStone
		case TowersType.RunicStone_level_1:
			return new TowersPrice { buyPrice = 180, sellPrice = Mathf.RoundToInt((SaveData.tikiTotemRight_ID1 ==  1 ? GlobalConfiguration.TIKI_TOTEM_RIGHT_ID_1_BONUS / 100: 75f / 100) * 180), upgradePrice = 240 };
			
		case TowersType.RunicStone_level_2:
			return new TowersPrice { buyPrice = 0, sellPrice = Mathf.RoundToInt((SaveData.tikiTotemRight_ID1 ==  1 ? GlobalConfiguration.TIKI_TOTEM_RIGHT_ID_1_BONUS / 100: 75f / 100) * 400), upgradePrice = 340 };
			
		case TowersType.RunicStone_level_3:
			return new TowersPrice { buyPrice = 0, sellPrice = Mathf.RoundToInt((SaveData.tikiTotemRight_ID1 ==  1 ? GlobalConfiguration.TIKI_TOTEM_RIGHT_ID_1_BONUS / 100: 75f / 100) * 640), upgradePrice = 0 };	
			#endregion

		}
		
		return new TowersPrice { buyPrice = 0, sellPrice = 0, upgradePrice = 0 };
	}

	
}


public struct TIKIMaskConfiguration
{
	public int damageBonus;
	public float timeEffect;
	public int slowAmount;
	public int shootsAmount;
	public int rechargeBonus;
	public float ttl;
	public int price;

}

public struct Configuration
{
	
	
	public int startMoney;
	public int maxWaves;
	
	public bool towerVooDoo;
	public bool towerTotem;
	public bool towerTornado;
	public bool towerThundercloud;
	public bool towerRockPiercer;
	public bool towerFireTotem;
	public bool towerCampfire;
	public bool towerRollingBoulder;
	public bool towerMagicShot;
	public bool towerIceShot;
	public bool towerMelter;
	public bool towerIceTotem;
	public bool towerAirTotem;
	public bool towerRockHammer;
	public bool towerDrum;
	public bool towerCave;
	public bool towerNecklace;
	public bool towerAnvil;
	public bool towerCauldron;
	public bool towerRunicStone;

	public bool fireTiki;
	public bool earthTiki;
	public bool frostTiki;
	public bool windTiki;

	public float HPMultiplier;

	//Nowy rodzaj broni
	public PoolObjectType newTypeOfWeapon;
	
	
}


public struct TowersPrice
{
	public int buyPrice;
	public int sellPrice;
	public int upgradePrice;
}


