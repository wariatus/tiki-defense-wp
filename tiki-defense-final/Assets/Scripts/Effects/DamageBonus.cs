﻿using UnityEngine;
using System.Collections;

namespace Effects
{

	public class DamageBonus : Buff {

		/// <summary>
		/// O ile podnosimy obrazenia
		/// </summary>
		[Range(0, 100)]
		public int bonus;


		private float originalDamage;
		private float originalArmoreDamage;
		private float originalDPS;

		protected override void Awake ()
		{
			base.Awake ();
		}

		protected override void Start ()
		{
			base.Start ();
		}

		protected override void Update ()
		{
			base.Update ();
		}


		public override void Execute (int bonus, float ttl, Towers.TowerBase target)
		{
			this.bonus = bonus;
			this.ttl = ttl;
			this.target = target;
		
			/// Pobieramy oryginalny damage w celu pozniejszego resetu
			originalDamage = target.settings[target.level - 1].projectileSetting.damage;

			originalArmoreDamage = target.settings[target.level -1].projectileSetting.armoreDamage;



			/// Zwiekszamy obrazenia
			target.settings[target.level - 1].projectileSetting.damage += IncreasedBonus(originalDamage, bonus);

			if(target.settings[target.level -1].projectileSetting.armoreDamage > 0)
			{
				target.settings[target.level - 1].projectileSetting.armoreDamage += IncreasedBonus(	originalArmoreDamage, bonus);
			}

		

			base.Execute(bonus, ttl, target);
		}




		protected override void Reset ()
		{
			target.settings[target.level - 1].projectileSetting.damage = originalDamage;

			if(target.settings[target.level -1].projectileSetting.armoreDamage > 0)
			{
				target.settings[target.level -1].projectileSetting.armoreDamage = originalArmoreDamage;
			}



			base.Reset();

		}
	}
}