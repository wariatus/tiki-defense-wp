﻿using UnityEngine;
using System.Collections;
using Towers;

namespace Effects
{
	public class Buff : MonoBehaviour {

		/// <summary>
		/// Czas dzialania efektu
		/// </summary>
		public float ttl;

		[HideInInspector]
		public TowerBase target;

		private bool isRunning;
		private bool ttlRunning;

		protected virtual void Awake () 
		{

		}

		// Use this for initialization
		protected virtual void Start () {

			NotificationCenter.defaultCenter.addListener(OnGameStateChanged, NotificationType.OnGameStateChanged);
			//NotificationCenter.defaultCenter.addListener(OnTikiEffectReset, NotificationType.ResetTikiEffect);
			
		}

		private void OnTikiEffectReset(Notification n)
		{
			
			NotificationCenter.defaultCenter.removeListener(OnGameStateChanged, NotificationType.OnGameStateChanged);
			//NotificationCenter.defaultCenter.removeListener(OnTikiEffectReset, NotificationType.ResetTikiEffect);

			Destroy(this);

		}
		
		// Update is called once per frame
		protected virtual void Update () {
			
		}
		
		
		public virtual void Execute (int bonus, float ttl, Towers.TowerBase target)
		{
			if(ttl > 0)
			{
				StartCoroutine("TTL");
				isRunning = true;
			}
		}
		
		/// <summary>
		/// Increaseds the damage.
		/// </summary>
		/// <returns>The damage.</returns>
		/// <param name="orignalDamage">Orignal damage.</param>
		protected float IncreasedBonus(float origianl, float bonus)
		{
			return (bonus / 100f) * origianl;
		}

		protected virtual void Reset()
		{
			target.isTikiMaskEffectEnable = false;
			//target.tikiEfectSprite.SetActive(false);

			if(PathologicalGames.PoolManager.Pools["HitEffects"].IsSpawned(target.tikiEffect))
				PathologicalGames.PoolManager.Pools["HitEffects"].Despawn(target.tikiEffect);
		
		}

		public void Destroyme()
		{
			Reset();
			StopCoroutine("TTL");
			NotificationCenter.defaultCenter.removeListener(OnGameStateChanged, NotificationType.OnGameStateChanged);
			Destroy(this);

		}

		private IEnumerator TTL()
		{
			if(ttlRunning) yield return null;

			ttlRunning = true;

			while(ttl > 0)
			{

				if(GameMaster.gameState == GameState.RUNNING)
					ttl -= Time.deltaTime;

				yield return new WaitForEndOfFrame();
			}

			Reset();
		NotificationCenter.defaultCenter.removeListener(OnGameStateChanged, NotificationType.OnGameStateChanged);
			//NotificationCenter.defaultCenter.removeListener(OnTikiEffectReset, NotificationType.ResetTikiEffect);
			isRunning = false;
			ttlRunning = false;
			Destroy(this);
		}

		private void OnGameStateChanged(Notification n)
		{
		
			if(!isRunning) return;

			if(n.gameState == GameState.PAUSA)
				StopCoroutine("TTL");
			else if(n.gameState == GameState.RUNNING)
				StartCoroutine("TTL");

		}


	}
}