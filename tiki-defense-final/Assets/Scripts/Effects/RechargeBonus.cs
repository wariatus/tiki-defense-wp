﻿using UnityEngine;
using System.Collections;

namespace Effects
{
	public class RechargeBonus : Buff {

		/// <summary>
		/// O ile zmniejszamy recharge
		/// </summary>
		[Range(0, 100)]
		public int bonus;
		
		
		private float originalRecharge;
		
		protected override void Awake ()
		{
			base.Awake ();
		}
		
		protected override void Start ()
		{
			base.Start ();
		}
		
		protected override void Update ()
		{
			base.Update ();
		}
		
		
		public override void Execute (int bonus, float ttl, Towers.TowerBase target)
		{
			this.bonus = bonus;
			this.ttl = ttl;
			this.target = target;
			
			/// Pobieramy oryginalny recharge w celu pozniejszego resetu
			originalRecharge = target.settings[target.level - 1].attackSpeed;
			
			/// Zwiekszamy recharge
			target.settings[target.level - 1].attackSpeed -= IncreasedBonus(originalRecharge, bonus);
			
			base.Execute(bonus, ttl, target);
		}

		
		protected override void Reset ()
		{
			target.settings[target.level - 1 ].attackSpeed = originalRecharge;
			base.Reset();
			
		}
	}
}