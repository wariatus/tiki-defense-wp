﻿using UnityEngine;
using System.Collections;

public class SpriteAnimationEffect : MonoBehaviour {

    public float ttl;
    public tk2dSpriteAnimator animation;
	// Use this for initialization
	void Start () {

	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    private void ReturnToPool()
    {
        if (PathologicalGames.PoolManager.Pools["ParticleEffects"].IsSpawned(transform))
            PathologicalGames.PoolManager.Pools["ParticleEffects"].Despawn(transform);
    }

    private void OnDespawn()
    { }

    private void OnSpawned()
    {
        Invoke("ReturnToPool", ttl);
        animation.Play();
    }

}
