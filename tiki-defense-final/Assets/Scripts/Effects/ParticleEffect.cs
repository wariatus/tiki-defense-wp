﻿using UnityEngine;
using System.Collections;

public class ParticleEffect : ExtendedMonoBehavior {

	#region PUBLIC VARIABLE

	public ParticleSystem particle;
	public string sortLayer = "Particle Effects";
	#endregion

	#region PRIVATE VARIABLE

	#endregion

	#region INIT

	protected override void Awake ()
	{
		base.Awake ();

        
		ParticleSystem[] particles = transform.GetComponentsInChildren<ParticleSystem>();
        
		for(int i = 0; i < particles.Length; i++)
		{
			particles[i].renderer.sortingLayerName = sortLayer;
		}
	}


	protected override void Start ()
	{
		base.Start ();
	}

	protected override void Update ()
	{
		base.Update ();

		if(GameMaster.gameState == GameState.RUNNING)
		{
			if(!particle.IsAlive(true))
				ReturnToPool();
		}
	
	}

	#endregion


	#region PUBLIC METHODS

	#endregion

	#region PROTECTED METHODS
	
	
	
	#endregion

	#region PRIVATE METHODS

	private void ReturnToPool()
	{
		if(PathologicalGames.PoolManager.Pools["ParticleEffects"].IsSpawned(transform))
			PathologicalGames.PoolManager.Pools["ParticleEffects"].Despawn(transform);
	}

	#endregion

	#region EVENTS & NOTIFICATIONS


	void OnSpawned()
	{
		particle.Play(true);
	}

	void OnDespawned()
	{
		particle.Stop(true);
	}

	#endregion
	
}
