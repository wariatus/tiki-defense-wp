﻿using UnityEngine;
using System.Collections;

public class GUIGameActive : ExtendedMonoBehavior {

	#region Variables
	
	#endregion

	#region Initialization

	protected override void Awake () 
	{

	}

	protected override void Start () 
	{
	
	}

	#endregion
	
	// Update is called once per frame
	protected override void Update () {
	
	}

	#region Public methods

	#endregion

	#region Protected methods

	#endregion
	
	#region Private methods

	#endregion
	
	#region Events & Notifications
    void OnLevelWasLoaded(int level)
    {
        if (level > 2)
            gameObject.SetActive(true);
        else
            gameObject.SetActive(false);
    }
	#endregion



}
