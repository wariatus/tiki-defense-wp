﻿using UnityEngine;
using System.Collections;

public static class ShopManager  {

	public static void Init()
	{
		Unibiller.onBillerReady += onInitialised;
		Unibiller.onTransactionsRestored += onTransactionsRestored;
		Unibiller.onPurchaseCancelled += onCancelled;
		Unibiller.onPurchaseFailed += onFailed;
		Unibiller.onPurchaseComplete += onPurchased;

		Unibiller.Initialise();

        if (Debug.isDebugBuild)
        {
            Unibiller.clearTransactions();
            
        }

	}
	
    static void onInitialised(UnibillState result) 
	{
		switch(result)
		{
		case UnibillState.SUCCESS:
				Debug.Log("[ShopManager]: UniBill init success");
			break;
		case UnibillState.SUCCESS_WITH_ERRORS:
				Debug.Log("[ShopManager]: UniBill init success with Errors!");
			break;
		case UnibillState.CRITICAL_ERROR:
				Debug.Log("[ShopManager]: UniBill init CRITICAL ERROR");
			break;
			
			
		}

        GUIShop.Instance.onInitialised();
	
	}

	/// <summary>
	/// This will be called after a call to Unibiller.restoreTransactions().
	/// </summary>
	private static void onTransactionsRestored (bool success) {
		if(Debug.isDebugBuild)
			Debug.Log("[ShopManager]: Transactions restored.");
	}
	
	/// <summary>
	/// This will be called when a purchase completes.
	/// </summary>
	private static void onPurchased(PurchasableItem item) {

		if(Debug.isDebugBuild)
		{
			Debug.Log("[ShopManager]: Purchase OK: " + item.Id);
			Debug.Log(string.Format ("[ShopManager]: {0} has now been purchased {1} times.",
			                         item.name,
			                         Unibiller.GetPurchaseCount(item)));
		}

        switch (item.Id)
        {
			case "com.roscostudios.tikidefense.tikimasks":

                if(Application.loadedLevel > 2)
                   GameMaster.guiOperation.AllTikiMaskUnlock();
              
                   SaveData.tikiMaskUnlockAll = 1;
                   SaveData.Save();
				GA.API.Design.NewEvent("Purchase TIKIMASKUNLOCK");
                break;
//            case "com.roscogames.AddWoodConsumable":
//                SaveData.premiumWood++;
//				GA.API.Design.NewEvent("Purchase Premium Wood");
//                break;
//			case "com.roscogames.UnlockAllContent":
//				GA.API.Design.NewEvent("Purchase UnlockAll Content");
//			
//			break;
        }

        NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.PremiumChanged));
        GUIShop.Instance.CloseGems();
        GUIShop.Instance.CloseWood();
        GUIShop.Instance.CloseUnlocks();
        GUIShop.Instance.CloseGlobalShop();
	}

	/// <summary>
	/// This will be called if a user opts to cancel a purchase
	/// after going to the billing system's purchase menu.
	/// </summary>
	private static void onCancelled(PurchasableItem item) {
		if(Debug.isDebugBuild)
			Debug.Log("[ShopManager]: Purchase cancelled: " + item.Id);
	}
	
	/// <summary>
	/// This will be called if a user opts to cancel a purchase
	/// after going to the billing system's purchase menu.
	/// </summary>
	private static  void onFailed(PurchasableItem item) {
		if(Debug.isDebugBuild)
			Debug.Log("[ShopManager]: Purchase failed: " + item.Id);
	}
}
