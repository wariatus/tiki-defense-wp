﻿using UnityEngine;
using System.Collections;

public class AchievmentManager : Singleton<AchievmentManager> {

	#region PUBLIC VARIABLE

	public Archievment[] achievments;
	
	#endregion

	#region PRIVATE VARIABLE

	private bool _isEnable;

	#endregion

	#region INIT

	protected override void Awake ()
	{
		base.Awake ();

	}


	protected override void Start ()
	{
		base.Start ();
	}

	protected override void Update ()
	{
		base.Update ();
	}

	#endregion


	#region PUBLIC METHODS

	public static void RaportEnemyKill()
	{
		for(int i = 0; i < AchievmentManager.Instance.achievments.Length; i++)
		{
			if(AchievmentManager.Instance.achievments[i].type == AchievmentType.EnemyKill)
				AchievmentManager.Instance.achievments[i].currentValue++;
		}
	}

	#endregion

	#region PROTECTED METHODS
	
	
	#endregion

	#region PRIVATE METHODS

	#endregion

	#region EVENTS & NOTIFICATIONS




	#endregion
	
}
