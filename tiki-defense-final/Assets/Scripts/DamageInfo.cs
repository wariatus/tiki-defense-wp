﻿using UnityEngine;

public struct DamageInfo {

    public float value;
	public float damageValue;
    public Element element;
    public Unit owner;
    public Vector3 position;
    public bool explosive;
    public bool isSpell;
	public bool instantDeath;

    public DamageInfo(float value, float damageValue, Element element, Unit owner, bool explosive, Vector3 position, bool isSpell = false, bool instantDeath = false)
    {
        this.value = value;
		this.damageValue = damageValue;
        this.element = element;
        this.owner = owner;
        this.explosive = explosive;
        this.isSpell = isSpell;
        this.position = position;
		this.instantDeath = instantDeath;
    }
}
