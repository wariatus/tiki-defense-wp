﻿using UnityEngine;

public class ResolutionAtlasSwitcher : MonoBehaviour
{
	[Multiline]
	public string description;

	public string SDAtlasName;
	public string HDAtlasName;
	public string SHDAtlasName;
	public string ReferenceAtlasName;

	private UIAtlas ReferenceAtlas;
	private UIAtlas Atlas;
	// Use this for initialization
	void Awake()
	{

		if (Application.isEditor)
		{
			return;
		}

		int ScreenHeight = Screen.height;
		int ScreenWidth = Screen.width;

		ReferenceAtlas = Resources.Load("NGUI/" + ReferenceAtlasName, typeof(UIAtlas)) as UIAtlas;

		if (ScreenWidth >= GlobalConfiguration.SHDResolutionCutOff)
		{
			Atlas = Resources.Load("NGUI/" + SHDAtlasName, typeof(UIAtlas)) as UIAtlas;

			GlobalConfiguration.screenType = ScreenType.SHD;
			Debug.Log("Setting GUI Atlas to SHD " + description);
		}
		else if (ScreenWidth >= GlobalConfiguration.HDResolutionCutOff)
		{

			Atlas = Resources.Load("NGUI/" + HDAtlasName, typeof(UIAtlas)) as UIAtlas;

			GlobalConfiguration.screenType = ScreenType.HD;
			Debug.Log("Setting GUI Atlas to HD " + description);
		}
//		else
//		{
//			Atlas = Resources.Load("NGUI/" + SDAtlasName, typeof(UIAtlas)) as UIAtlas;
//
//			GlobalConfiguration.screenType = ScreenType.SD;
//			Debug.Log("Setting GUI Atlas to SD " + description );
//		}

		ReferenceAtlas.replacement = Atlas;
	   
		Resources.UnloadUnusedAssets();
	}
	
	void OnLevelWasLoaded(int level)
	{
		
	}
}