﻿using UnityEngine;

public class ResolutionFontSwitcher : MonoBehaviour
{
    [Multiline]
    public string description;

    public bool disableInEditor = true;
    public int HDResolutionCutOff = 960;
    public int SHDResolutionCutOff = 2048;

   
    public string SDFontName;
    public string HDFontName;
    public string SHDFontName;
    public string ReferenceFontName;

    private UIFont ReferenceFont;
    private UIFont Fonts;
    // Use this for initialization
    void Awake()
    {

        if (disableInEditor && Application.isEditor)
        {
            return;
        }

        int ScreenHeight = Screen.height;
        int ScreenWidth = Screen.width;

        ReferenceFont = Resources.Load("Textures & Sprites/2D/Fonts/" + ReferenceFontName, typeof(UIFont)) as UIFont;
       
        if (ScreenWidth >= SHDResolutionCutOff)
        {
            Fonts = Resources.Load("Textures & Sprites/2D/Fonts/" + SDFontName, typeof(UIFont)) as UIFont;

            
            Debug.Log("Setting font to SHD " + description);
        }
        else if (ScreenWidth >= HDResolutionCutOff)
        {

            Fonts = Resources.Load("Textures & Sprites/2D/Fonts/" + HDFontName, typeof(UIFont)) as UIFont;
            
            Debug.Log("Setting fonts to HD " + description);
        }
        else
        {
            Fonts = Resources.Load("Textures & Sprites/2D/Fonts/" + SHDFontName, typeof(UIFont)) as UIFont;
  
            Debug.Log("Setting fonts to SD " + description );
        }

        ReferenceFont	.replacement = Fonts;
       
    }
}