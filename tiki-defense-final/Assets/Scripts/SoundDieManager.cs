﻿using UnityEngine;
using System.Collections;

public class SoundDieManager : Singleton<SoundDieManager> {

	#region Variables
	
	public AudioClip[] clips;
	
	#endregion

	#region Initialization

	protected override void Awake () 
	{
		base.Awake();
	}

	protected override void Start () 
	{
		base.Start();
	}

	#endregion
	
	// Update is called once per frame
	protected override void Update () {
	
		base.Update();
	}

	#region Public methods
	
	public static void Play()
	{
		int random = Random.Range(0, SoundDieManager.Instance.clips.Length -1);
		
		NGUITools.PlaySound(SoundDieManager.Instance.clips[random], 0.5f);
	
	}

	#endregion

	#region Protected methods

	#endregion
	
	#region Private methods

	#endregion
	
	#region Events & Notifications

	#endregion



}
