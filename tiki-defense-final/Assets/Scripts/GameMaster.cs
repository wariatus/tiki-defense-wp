using UnityEngine;
using System.Collections;
using PlayerPrefs = PreviewLabs.PlayerPrefs;

public class GameMaster : Singleton<GameMaster> {

	public static GameState gameState
	{
		get
		{
			return __gameState;
		}
		set
		{
			if(Debug.isDebugBuild)
				Debug.Log("[GameMaster]: Game State change " + __gameState.ToString() + " => " + value.ToString());
		
			__gameState = value;
			
			NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.OnGameStateChanged, value));
		}
	}

	public static GUICharges guiChargers;			// Pozycja
	public AudioSource introCountDownMusic;			// Referencja do sciezki audio, z odliczaniem
	public AudioSource victoryMusics;
	public AudioSource defeatMusic;
	public static AudioSource levelMusic;			// Referencja do muzyki poziomu. Taka muzyka powinna sama sie rejestrowac tutaj
	public tk2dSpriteAnimator countAnimation;
	public static bool canPlayFXSounds = true;
	public static bool canPlayMusic = true;
	public ParticleSystem countParticle;
	/// <summary>
	/// True wyswietlane sa przyciski GUI np. do obslugi wiezy czy grida
	/// </summary>
	public static bool guiButtonShowUp;

	/// <summary>
	/// Referencja do aktualnie wybranej maski TIKI
	/// </summary>
	public static TikiMaskType tikiActieType = TikiMaskType.NONE;

	/// <summary>
	/// Zwraca aktualnie wybrany ID jezyka
	/// </summary>
	/// <value>The language.</value>
	public static string language
	{
		get { return SaveData.defaultLanguage; }
	}

	/// <summary>
	/// Jezeli TRUE to wlaczony jest tutorial
	/// </summary>
	public static bool onTutorial;

	public static GUIOperations guiOperation;

	private static GameState __gameState = GameState.READYTOSTART;


	void Awake ()
	{
//		SaveData.tikiTotemLeft_ID1 = 1;
//		SaveData.tikiTotemLeft_ID2 = 1;
//		SaveData.tikiTotemLeft_ID3 = 1;
//		SaveData.tikiTotemLeft_ID4 = 1;
//		SaveData.tikiTotemLeft_ID5 = 1;
//		SaveData.tikiTotemLeft_ID6 = 0;
//		
//		SaveData.tikiTotemRight_ID1 = 1;
//		SaveData.tikiTotemRight_ID2 = 1;
//		SaveData.tikiTotemRight_ID3 = 1;
//		SaveData.tikiTotemRight_ID4 = 1;
//		SaveData.tikiTotemRight_ID5 = 1;
//		SaveData.tikiTotemRight_ID6 = 0;
//		//KONIEC
//		SaveData.Save();

		//SaveData.tikiTutorialCompleted = 0;

    

	}

	// Use this for initialization
	void Start () {

		gameState = GameState.PAUSA;

		// Podpinamy sie pod eventy
		NotificationCenter.defaultCenter.addListener(OnGameDefeat, NotificationType.GameDefeated);
		NotificationCenter.defaultCenter.addListener(OnGameWin, NotificationType.GameWon);

		NotificationCenter.defaultCenter.addListener(OnTutorialBuildTowerOn, NotificationType.TutorialBuildTowerOn);
		NotificationCenter.defaultCenter.addListener(OnTutorialBuildTowerOff, NotificationType.TutorialBuildTowerOff);
		NotificationCenter.defaultCenter.addListener(OnCountStart, NotificationType.OnCountStart);


	 	//Invoke ("StartLevel", introCountDownMusic.clip.length - 2.5f);
		StartLevel();
		ResetPlayerPrefs();

        Time.timeScale = 1;
        
		//TYMCZASOWO DO TESTOW

        if (SaveData.newGameGM == 0)
        {
            Unibiller.clearTransactions();
            Unibiller.DebitBalance("gems", Unibiller.GetCurrencyBalance("gems"));
            SaveData.addPremium = GlobalConfiguration.chargeOnNewGame ;
            SaveData.newGameGM = 1;
            SaveData.Save();

        }

		
	}

	private void ShowNewWeapon()
	{
		guiOperation.OnCLick_NewWeaponShow();
	}

	private void StartLevel()
	{
		//countAnimation.Stop();


		if(Application.loadedLevelName != "Level1x01")
		{
			//Jezeli do misji dolacza nowa bron, pokazujemy okno z ta informacja
			if(LevelConfigurations.GetConfiguration(Application.loadedLevelName).newTypeOfWeapon != PoolObjectType.Null)
			{
				//guiOperation.OnCLick_NewWeaponShow();
				Invoke ("ShowNewWeapon", introCountDownMusic.clip.length - 2.5f);
			}
			else
			{
				if(Application.loadedLevelName != "Level1x03-tutorial-tiki")
				{
					//gameState = GameState.RUNNING;
					Invoke("RunCountDelayed", 0.5f);
				}
				else
					TurnOffDrum();
			}
		}
		else
		{

			Invoke("RunTutorialBuildTower", 0.1f);
		}
	}

	private void RunCountDelayed()
	{
		NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.OnCountStart));
	}
	private void OnCountStart(Notification n)
	{
		//countAnimation.gameObject.SetActive(true);
		if(canPlayMusic)
			introCountDownMusic.Play();
			
		//NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.OnCountStart));

		Invoke("TurnOffDrum", 2.5f);
		Invoke ("TrunOnParticleCount", 2f);
		
		if(canPlayMusic)
			levelMusic.PlayDelayed(3f);
	}

	private void TurnOffDrum()
	{
		
		if(Application.loadedLevelName == "Level1x02")
		{
			guiOperation.OnClick_POPUPOBSTACLE();
		}
		else if(Application.loadedLevelName == "Level3x01")
		{
			guiOperation.OnClick_POPUPARMORED();
		}
		else
		{
			GameMaster.gameState = GameState.RUNNING;
			
		}
	}

	private void TrunOnParticleCount()
	{
		countParticle.Play(true);
	}

	// Update is called once per frame
	void Update () {
	
	}

	private void RunTutorialBuildTower()
	{
		guiOperation.BuildTotemWindowShow();
	}

	private void RunTutorialTikiMask()
	{
		guiOperation.TikiMaskTutorialShow();
	}

	public static void DespawnPools()
	{
		PathologicalGames.PoolManager.Pools["Bullets"].DespawnAll();
		PathologicalGames.PoolManager.Pools["Drops"].DespawnAll();
		PathologicalGames.PoolManager.Pools["Enemies"].DespawnAll();
		PathologicalGames.PoolManager.Pools["HitEffects"].DespawnAll();
		PathologicalGames.PoolManager.Pools["Towers"].DespawnAll();
	}
	
	void OnGameDefeat(Notification n)
	{
		DespawnPools();

		if(canPlayMusic)
		{
			levelMusic.Stop();
			defeatMusic.Play();
		}
		// Ustawiamy gamestate na defeat
		GameMaster.gameState = GameState.DEFEAT;


		GetReferences.defeatWindow.Show();
		if(Debug.isDebugBuild)
			Debug.Log("[GameWorkflow] Status: PRZEGRANA");

		GA.API.Design.NewEvent("Level: " + Application.loadedLevelName + " finish failed", PathManager.Instance.currentWave.Id);
		
	}
	
	void OnGameWin(Notification n)
	{

	    Time.timeScale = 1f;
		if(Application.loadedLevelName == "Level1x03-tutorial-tiki")
		{
			SaveData.tikiTutorialCompleted = 1;
			onTutorial = false;
			NotificationCenter.defaultCenter.ClearListener();
			SaveData.Save();
			SaveData.levelToLoad = "Level1x03";
			LoadingScreen.Instance.Show();
			//Application.LoadLevel("Level 1x03");

			return;
		}

		if(GetReferences.tiki.state == UnitState.Dying)
		{
			NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.GameDefeated));
			return;
		}

		DespawnPools();
		if(canPlayMusic)
		{
			levelMusic.Stop();
			victoryMusics.Play();
		}
		// Ustawiamy gamestate na wygrana
		GameMaster.gameState = GameState.WON;

		if(SaveData.levelBadge == 0)
        	SaveData.levelBadge = BadgeWorkflow.Blockers == 0 ? 1 : 0;

		int starsCount = StarsCount();

		if(SaveData.levelStars == 0)
		{
			SaveData.levelStars = StarsCount();

			// Zapisujemy ilosc posiadanych gwiazdek lacznie na settingx
			if(Application.loadedLevelName.Contains("1x"))
				SaveData.settingStarsBeach += StarsCount();
			if(Application.loadedLevelName.Contains("2x"))
				SaveData.settingStarsJungle += StarsCount();
			if(Application.loadedLevelName.Contains("3x"))
				SaveData.settingStarsVillage += StarsCount();
			if(Application.loadedLevelName.Contains("4x"))
				SaveData.settingStarsHills += StarsCount();
			if(Application.loadedLevelName.Contains("5x"))
				SaveData.settingStarsVulcano += StarsCount();

			// Zapisujemy ilosc posiadanych gwiazdek lacznie dla gracza
			SaveData.playerStarsAll += StarsCount();

			if(Application.loadedLevelName.Contains("bonus"))
			{
				SaveData.premiumWood++;
				SaveData.lastMissionLoaded = Application.loadedLevelName;
			}
			else if(Application.loadedLevelName == "Level1x04")
			{
				SaveData.premiumWood++;
				SaveData.lastMissionLoaded = Application.loadedLevelName;
			}


		}
		else 
		{
			starsCount -= SaveData.levelStars;
			if(starsCount > 0)
			{

				if(Application.loadedLevelName.Contains("1x"))
					SaveData.settingStarsBeach += starsCount;
				if(Application.loadedLevelName.Contains("2x"))
					SaveData.settingStarsJungle += starsCount;
				if(Application.loadedLevelName.Contains("3x"))
					SaveData.settingStarsVillage += starsCount;
				if(Application.loadedLevelName.Contains("4x"))
					SaveData.settingStarsHills += starsCount;
				if(Application.loadedLevelName.Contains("5x"))
					SaveData.settingStarsVulcano += starsCount;
				
				// Zapisujemy ilosc posiadanych gwiazdek lacznie dla gracza
				SaveData.playerStarsAll += starsCount;

				SaveData.levelStars += starsCount;
			}
		}
        // Uruchamiany okno wygranej
        
		if(Application.loadedLevelName.Equals("Level5x09"))
        	GetReferences.winWindow.ShowComplete();
        else
        	GetReferences.winWindow.Show(true);
		//Debug.Log(StarsCount());

		SaveData.Save();
              
		if(Debug.isDebugBuild)
        	Debug.Log("[GameWorkflow] Status: WYGRANA");

		GA.API.Design.NewEvent("Level: " + Application.loadedLevelName + " finish successed", GetReferences.tiki.health.currentHealth);
	}

	public static void NextLevel()
	{
		//NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.BackToPool));
		NotificationCenter.defaultCenter.ClearListener();
		LiveManager.Instance.DecreaseLive();
		DespawnPools();
		SaveData.levelToLoad = "Mission Select";
		LoadingScreen.Instance.Show();
		//Application.LoadLevel("Mission Select");	
	}

    public static void ResetLevel()
    {
		//NotificationCenter.defaultCenter.postNotification(new Notification(NotificationType.BackToPool));
		NotificationCenter.defaultCenter.ClearListener();

		DespawnPools();

		SaveData.levelToLoad = Application.loadedLevelName;
		LoadingScreen.Instance.Show();
		//Application.LoadLevel(Application.loadedLevelName);


		
		ResetPlayerPrefs();
    }

	public static void ResetPlayerPrefs()
	{
		SaveData.tikiMaskCharge = 0;
		//SaveData.tikiMaskUnlockAll = 1;


	}

    void OnLevelWasLoaded(int level)
    {
		ResetPlayerPrefs();

        //NotificationCenter.defaultCenter.ClearListener();

		DespawnPools();

		if(level > 2)
		{
			GA.API.Design.NewEvent("Level: " + Application.loadedLevelName + " started");
		}

    }

	public void ResetTIKITotemBonus()
	{
		SaveData.tikiTotemLeft_ID1 = 1;
		SaveData.tikiTotemLeft_ID2 = 1;
		SaveData.tikiTotemLeft_ID3 = 1;
		SaveData.tikiTotemLeft_ID4 = 1;
		SaveData.tikiTotemLeft_ID5 = 1;
		SaveData.tikiTotemLeft_ID6 = 1;

		SaveData.tikiTotemRight_ID1 = 1;
		SaveData.tikiTotemRight_ID2 = 1;
		SaveData.tikiTotemRight_ID3 = 1;
		SaveData.tikiTotemRight_ID4 = 1;
		SaveData.tikiTotemRight_ID5 = 1;
		SaveData.tikiTotemRight_ID6 = 1;
		SaveData.Save();

	}

	public void ResetProfile()
	{

		ResetTIKITotemBonus();
		ResetPlayerPrefs();
	}

	/// <summary>
	/// Raises the application pause event.
	/// </summary>
	/// <param name="pauseStatus">If set to <c>true</c> pause status.</param>
	void OnApplicationPause(bool pauseStatus) 
	{
//
//		if(onTutorial) return;
//
//		if(Debug.isDebugBuild)
//			Debug.Log("[GameManager] Game OnPause. Status: " + pauseStatus);
//		
//		if(pauseStatus)
//			gameState = GameState.PAUSA;
//		else
//			gameState = GameState.RUNNING;
//		
//		SaveData.Save();
	}
	
	public void OnApplicationQuit()
	{

		SaveData.Save();
	}


	
	private void OnTutorialBuildTowerOn(Notification n)
	{
		onTutorial = true;
		gameState = GameState.PAUSA;
	}
	
	private void OnTutorialBuildTowerOff(Notification n )
	{
		onTutorial = false;
		guiOperation.BuildTotemWindowHide();
		gameState = GameState.PAUSA;

		guiOperation.OnCLick_NewWeaponShow();
	}

    // Oblicza ilosc uzyskanech gwiazdek w danej misji
    public static int StarsCount()
    {
		float tikiHP = GetReferences.tiki.health.currentHealth;
		//Debug.Log(GetReferences.tiki.health.currentHealth + " " + GetReferences.tiki.health.CurrentHealth);

        int getStars = 0;

        if (tikiHP == 10)
        {
            getStars = 3;
        }
        else if (tikiHP >= 5 && tikiHP <= 9)
        {
            getStars = 2;
        }
        else
        {
            getStars = 1;
        }

        return getStars;

    }
}
