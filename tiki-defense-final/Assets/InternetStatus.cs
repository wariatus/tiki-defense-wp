﻿using UnityEngine;
using System.Collections;

public class InternetStatus : MonoBehaviour {

	public static bool internetAvailable;
	private const bool allowCarrierDataNetwork = false;
	private const string pingAddress = "8.8.8.8"; // Google Public DNS server
	private const float waitingTime = 5.0f;

	private Ping ping;
	private float pingStartTime;

	public void Start()
	{
		InvokeRepeating("CheckConnection", 1f, 10f);
		bool internetPossiblyAvailable;
		switch (Application.internetReachability)
		{
		
			case NetworkReachability.ReachableViaCarrierDataNetwork:
				internetPossiblyAvailable = allowCarrierDataNetwork;
				break;
			default:
				internetPossiblyAvailable = false;
				break;
		}

		if (!internetPossiblyAvailable)
		{
			InternetIsNotAvailable();
			return;
		}

	
	}

	public void Update()
	{
		if (ping != null)
		{
			bool stopCheck = true;
			if (ping.isDone)
				InternetAvailable();
			else if (Time.time - pingStartTime < waitingTime)
				stopCheck = false;
			else
				InternetIsNotAvailable();

			if (stopCheck)
				ping = null;
		}
	}

	private void CheckConnection()
	{
		if (ping == null)
		{
			ping = new Ping(pingAddress);
			pingStartTime = Time.time;
		}
	}

	private void InternetIsNotAvailable()
	{
		if(Debug.isDebugBuild)
		   Debug.Log("No Internet :(");

		internetAvailable = false;

	}

	private void InternetAvailable()
	{
		if(Debug.isDebugBuild)
			Debug.Log("Internet is available! ;)");

		internetAvailable = true;
	}
}

